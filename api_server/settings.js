var argv = process.argv.splice(2);

module.exports = argv.length > 0 && argv[0] === '--prod' ?
   require('./settings.prod.js') :
   argv[0] === '--qa' ? require('./settings.qa.js') : require('./settings.dev.js');
