// /*jshint sub:true*/

// var languages = [];
// var enUS_strings = [];
// var deDE_strings = [];
// languages['enUS'] = enUS_strings;
// languages['deDE'] = deDE_strings;

// enUS_strings['lang_id_products'] = 'Products';
// enUS_strings['lang_id_reservation'] = 'Reservation';
// enUS_strings['lang_id_custom_cake'] = 'Custom Cake';
// enUS_strings['lang_id_about_us'] = 'About Us';
// // enUS_strings['lang_id_testimonials'] = 'Testimonials';
// enUS_strings['lang_id_menu'] = 'Menu';
// enUS_strings['lang_id_contact_us'] = 'Contact Us';

// enUS_strings['lang_id_customer_and_pickup'] = 'Customer & Pickup';
// enUS_strings['lang_id_shape_and_size'] = 'Shape & Size';
// enUS_strings['lang_id_design_and_taste'] = 'Design & Taste';
// enUS_strings['lang_id_delivery_info'] = 'Delivery Info';
// enUS_strings['lang_id_review_order_details'] = 'Review Order Details';

// enUS_strings['lang_id_design_a_custom_cake'] = 'Order a custom cake';
// enUS_strings['lang_id_book_a_table'] = "Book a Table";
// enUS_strings['lang_id_book_an_event'] = 'Book an Event';

// deDE_strings['lang_id_products'] = 'Produkte';
// deDE_strings['lang_id_reservation'] = 'Reservierung';
// deDE_strings['lang_id_custom_cake'] = 'Bestellung';
// deDE_strings['lang_id_about_us'] = 'Unternehmen';
// deDE_strings['lang_id_testimonials'] = 'Testimonials';
// deDE_strings['lang_id_menu'] = 'Menukarte';
// deDE_strings['lang_id_contact_us'] = 'Kontakt';

// deDE_strings['lang_id_customer_and_pickup'] = 'My Customer & Pickup';
// deDE_strings['lang_id_shape_and_size'] = 'My Shape & Size';
// deDE_strings['lang_id_design_and_taste'] = 'My Design & Taste';
// deDE_strings['lang_id_delivery_info'] = 'My Delivery Info';
// deDE_strings['lang_id_review_order_details'] = 'My Review Order Details';

// deDE_strings['lang_id_design_a_custom_cake'] = 'Kuchen Bestellen';
// deDE_strings['lang_id_book_a_table'] = 'Tisch Reservieren';
// deDE_strings['lang_id_book_an_event'] = 'Ereignis Reservieren';

// function applyI18N(lang) {
//    var textNodes = document.getElementsByClassName('language-id');

//    for(var i = 0; i < textNodes.length; ++i) {
//       if(languages[lang][textNodes[i].id] != null) {
//          textNodes[i].innerText = languages[lang][textNodes[i].id];
//       }
//    }
// }
