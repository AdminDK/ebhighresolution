module.exports = function(katalogId, range, price, showGrid) {
    return {
        katalogId: katalogId,
        range: range,
        price: price,
        showGrid: showGrid
    };
};
