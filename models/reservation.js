
module.exports = function() {
    return {
    reservationId: new Date().getTime(),
    guestName: '',
    guestEmail: '',
    guestMobile: '',
    date: new Date(),
    createdDate: new Date()
    };
};