

module.exports = function() {
    return {
        menuOrderId: new Date().getTime(),
        name: '',
        email: '',
        mobile: '',
        text: '',
        price: 0.0,
        paymentType: "cod",
        mollieTransactionId:'',
        paymentDetails: null,
        // totalTax: 0.0,
        // totalPrice : 0.0,
        deliveryAddressPincod:'',
        deliveryCharge: 0,
        deliveryDistance:0.0,
        deliveryAddress:'',
        deliveryType: ' ',
        pickupTime:'',
        //productType: '',
        //productName:'',
        //productDescription:'',
        //productRate: 0.0,
        //productQty : 0,
        // productExtras : [''],
        cartContain : [''],
    };
};
