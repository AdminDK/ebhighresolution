

module.exports = function() {
    return {
        productId: new Date().getTime(),
        productType: '',
        productName: '',
        productDescription: '',
        extraInfo: '',
        productRate: 0.0,
        allergens: [''],
        extraProductType: '',
        productExtras : [''],
    };
};
