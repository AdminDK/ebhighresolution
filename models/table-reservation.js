var Reservation = require('./reservation');

module.exports = function() {
    var tableReservation = Reservation();
    tableReservation.guestCount = 0;
    tableReservation.time = '';
    tableReservation.newsletterConsent = '';
    return tableReservation;
};