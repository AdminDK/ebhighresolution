const Product = require('./cake-product');

module.exports = function (code, name, price, from, to) {
    var flavor = Product(code, name, 'flavor', false, price);

    flavor.availableFrom = from;
    flavor.availableTo = to;

    return flavor;
};
