var Reservation = require('./reservation');

module.exports = function() {
    var eventReservation = Reservation();

    eventReservation.type = '';
    eventReservation.size = '';
    eventReservation.startTime = '';
    eventReservation.duration = '';
    eventReservation.newsletterConsent = '';
    return eventReservation;
};