
echo "==================================="
echo "=  Building the Landing Page App  ="
echo "==================================="
npm run build

echo "==================================="
echo "=  Building the Custom Cake App   ="
echo "==================================="
cd custom-cake-form
npm run build
mkdir ../build/cake-form-build
cp -a ./build/. ../build/cake-form-build
