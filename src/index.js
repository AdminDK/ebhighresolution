import React from 'react';
import ReactDOM from 'react-dom';
import 'jquery';
import App from './App';
import OnlineOrderApp from './OnlineOrderApp';

import './index.css';
import {BrowserRouter} from 'react-router-dom';

require('materialize-css/dist/js/materialize.min.js');

// function noop() { }
console.log('process.env.NODE_ENV', process.env.NODE_ENV);
// localStorage.clear();
// console.log(" localStorage.getItem('myValueInLocalStorage') ", localStorage.getItem('myValueInLocalStorage'));

var url = window.location.href;
// console.log(" url ", url);

var urlSplit = url.split('/');

// Taking last array value
let checkOnline = urlSplit.slice(-1).pop();


if (process.env.NODE_ENV !== 'development') {
  // console.log = noop;
  // console.warn = noop;
  // console.error = noop;
}
//if (checkOnline === 'online') { //if (checkOnline === 'online') {
  //ReactDOM.render((
    //<OnlineOrderApp />),
    //document.getElementById('root'));
//}
//else {
  ReactDOM.render((
    <BrowserRouter>
      <App />
    </BrowserRouter>),
    document.getElementById('root'));
//}










