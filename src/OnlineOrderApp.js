import React, { Component } from 'react';
import { ReactScoped, ViewEncapsulation } from 'react-scoped';

// import logo from './logo.svg';
import styles from './App.css';
// import NavigationBar from './nav-bar/nav-bar';
// import Online from './component/online-order/onlineOrder'
import OnlineOrder from './component/online-order/onlineOrder';

//import FoodOrder from './component/food-order/foodOrder';

class App extends Component {
    state = {
        redirect: false
    }
    checkIfIsRedirect(isRedirect) {
        this.setState({
            redirect: isRedirect
        })
        localStorage.setItem('myValueInLocalStorage', isRedirect);
    }


    render() {

        return (
            <ReactScoped encapsulation={ViewEncapsulation.None} styles={[styles]}>
                <OnlineOrder />

            </ReactScoped>
        );

    }
}

export default App;
