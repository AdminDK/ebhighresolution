import React, { Component } from 'react';
import { ReactScoped, ViewEncapsulation } from 'react-scoped';
import $ from 'jquery';
import M from 'materialize-css/dist/js/materialize.min.js';

import footer_sultan from '../assets/footer-sultan.png';
// import qr_Code from '../assets/QRcode.jpeg';
import logo_facebook from '../assets/facebook.png';
import logo_instagram from '../assets/instagram.png';
import styles from './footer.css';

let today = new Date();
let getYear = today.getFullYear();

console.log('getYear:', getYear);

class Footer extends Component {

    // componentDidMount() {
    //$('.tap-target').tapTarget('open');
    //$('.tap-target').tapTarget('close');
    //Old materialize css modal
    //$('.modal').modal();

    //DK materialize 0.100.2 to 1.0.0
    //const M = window.M;
    // document.addEventListener('DOMContentLoaded', function () {
    //     var elemModal = document.querySelectorAll('.modal');
    //     M.Modal.init(elemModal);

    //     //Start AGB Modal
    //     // var singleModalElem = document.querySelector('#agb');
    //     // let instanceAGB = M.Modal.getInstance(singleModalElem);

    //     // const modalbtn = document.querySelector('#modalAGB')
    //     // modalbtn.addEventListener('click', () => {
    //     //     instanceAGB.open();
    //     // })
    //     //End AGB Modal

    //     var elemTabTarget = document.querySelectorAll('.tap-target');
    //     M.TapTarget.init(elemTabTarget, {});
    // });
    // }


    render() {
        // var more_reviews_link = "https://www.google.de/maps/place/Efendi+Bey/@52.3753126,9.7288407,17z/data=!4m7!3m6!1s0x47b074bb68dcd3e3:0xc8b114d1d2872982!8m2!3d52.3753126!4d9.7310294!9m1!1b1?hl=de";

        return (
            <ReactScoped encapsulation={ViewEncapsulation.Emulated} styles={[styles]}>
                <div>
                    <footer className="page-footer">
                        <div className="footer_gold_border"></div>
                        <div className="footer_pink_border"></div>
                        {/* <div className="container-fluid footer_content_container"> */}
                        {/* <div className="row">
                                <div className="FooterText">

                                    <div className="col l3 m4 s12 footer_details_wrapper">
                                        <a target="_blank" href={more_reviews_link} >
                                            <span>Efendi Bey - Patiserrie & Café</span><br />
                                            <span>Münzstraße 7 | 30159 Hannover</span><br />
                                        </a>
                                    </div>
                                    <div className="col l3 m4 s12 footer_details_wrapper footer_left_rule">

                                        <span>
                                            <i className="material-icons prefix icon-size">phone</i>
                                            <a href="tel:+4951117507"> 0511 17507</a></span><br />
                                        <span>
                                            <i className="material-icons prefix icon-size">email</i>
                                            <a href="mailto:info@efendibey.de?Subject=Information" target="_top"> info@efendibey.de</a></span>
                                    </div>
                                    <div className="col l3 m4 s12 footer_details_wrapper footer_left_rule">
                                        <a className="modal-trigger" data-target="agb"><span> AGB </span></a><br />
                                        <a className="modal-trigger" data-target="unsere"><span> Datenschutzerklärung </span></a><br />
                                        <a className="modal-trigger" data-target="impressum"><span> Impressum </span></a><br />
                                    </div> */}

                        {/* <div className="col l3 m4 s12 footer_details_wrapper footer_left_rule">
                                        <div className="qrCodeDiv">
                                            <img src={qr_Code} alt="footer_logo" className="qrCodeImg responsive-img" />
                                        </div>
                                    </div> */}

                        {/* <div className="col l3 m4 s12 footer_details_wrapper footer_left_rule">
                                        <div className="qrCodeDiv">
                                            <img src={qr_Code} alt="footer_logo" className="qrCodeImg responsive-img" />
                                        </div>
                                    </div> */}

                        {/* <div className="col l3 m4 s12 footer_details_wrapper footer_left_rule">
                                        <div className="verticalHeightCustom"></div>
                                    </div> */}

                        {/* </div>
                                <div className="col l3 m3 s3 footer_logo_wrapper responsive-img">
                                    <div className="footer_sultan_bg">
                                    </div>
                                    <img src={footer_sultan} alt="footer_logo" className="footer_sultan responsive-img" />
                                </div>
                            </div>

                        </div> */}

                        <div className="footer-copyright">
                            <div className="container">
                                <span className="copyright_company">
                                    © Designed & Developed by Digiklug.com
                                </span>
                                {/* <div className="right social_logos">
                                    <a target="_blank" rel="noopener noreferrer" href="https://www.instagram.com/efendibey/" className="no_link_highlight"><img src={logo_instagram} alt="instagram" /></a>
                                    <a target="_blank" rel="noopener noreferrer" href="https://www.facebook.com/efendibey/" className="no_link_highlight"><img src={logo_facebook} alt="facebook" /></a>
                                </div> */}
                            </div>
                        </div>
                    </footer>
                </div>
            </ReactScoped>
        )
    }
}

export default Footer
