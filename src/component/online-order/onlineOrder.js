import React, { Component } from 'react';
import { ReactScoped, ViewEncapsulation } from 'react-scoped';
import styles from './onlineOrder.css';
import $ from 'jquery';
import M from 'materialize-css/dist/js/materialize.min.js';
import Extras from '../Extras';
import PaypalButton from '../../custom-cake/paypal-checkout-button';
import ReactGA from 'react-ga';
import Filter from './filter';
let anyalyticKey = 'UA-120592631-1';
let dataForAnalytics = true;

const search = window.location.search;
let orderId = new URLSearchParams(search).get("menuOrderId");
let mollieMessageFunc = null;
var url = window.location.href;
var urlSplit = url.split('/');
if (urlSplit[2] === 'localhost:5443') {
    anyalyticKey = 'UA-162821392-1';
    dataForAnalytics = false;
}
if (urlSplit[2] === 'qa.efendibey.digiklug.com') {
    anyalyticKey = 'UA-106213435-1';
    dataForAnalytics = false;
}


ReactGA.plugin.require('ecommerce');
function initializeAnalytics() {
    ReactGA.initialize(anyalyticKey);
    ReactGA.pageview('/onlineOrder');
}

var lastScrollTop = 0;
let instanceExtraText, instanceCheckout,
    instancePayment, instanceProductInfo, instanceTextOnCake, instanceEditTextOnCake,
    instanceOrderConfirm, instanceAGB, modalOptions;
export default class OnlineOrder extends Component {

    constructor(props) {
        super(props);

        // Initial state
        this.state = {
            isDesktop: false, //This is where I am having problems
            open: false,
            mollieMessage: true,
            PickUp_Checkbox_Checked: false,
            onlineMenuResponse: [],
            totalMenuWithTitle: [],
            deliveryAddress: '',
            deliveryAddressPincod: '',
            deliveryCharge: 2.5,
            deliveryDistance: -1,
            deliveryTimeHour: '',
            deliveryTimeMin: '',
            pickupTime: '',
            deliveryType: 'Lieferung',
            fname: '',
            lname: '',
            mobile: '',
            email: '',
            extraText: '',
            paymentType: 'cod',
            isSubmitInProgress: false,
            productPrice: 0,
            orderIdReference: 0,
            hiddenOrderId: '',
            displayProductQty: 1,
            displayPriceOnButton: 1,
            singleArrayInCart: {},
            cartArray: [],
            cartContain: [],
            totalAmount: 0,
            grandTotalAount: 0,
            allProductIds: [],
            headerClicked: 0,
            colorForProductType: [],
            selectedDropdown: '',
            newArray: [],
            confirmationMsg: '',
            allergy: [],
            analyticsData: [],
            termsAndCondition: true,
            currenHour: 0,
            curentMinutes: 0,
            scrollProductId: '',
            submitButtonIsActive: false,
            fullAddress: {},
            scrollIdFlagTopToDown: false,
            checkGoogleAddress: [],
            shopOpenTime: 0,
            shopClosedTime: 0,
            currenHourMinutes: 0,
            numberOfTextbox: [0],
            addTextOnCakeCheckbox: false,
            textOnCake: '',
            indexNoOfEditableText: 0,
            deliveryTime: ''
        };
        this.updatePredicate = this.updatePredicate.bind(this);
        this.handleScroll = this.handleScroll.bind(this);
    }



    removeFromCart(indexOf, type) {
        let addInSession = [];
        $("#" + this.state.cartArray[indexOf].productId).removeClass("collapsein");
        $("#" + this.state.cartArray[indexOf].productId).addClass("collapse");
        $("." + this.state.cartArray[indexOf].productId).removeClass("divColor");
        this.state.cartArray.splice(indexOf, 1);
        if (this.state.cartArray.length > 0) {
        } else {
            localStorage.setItem("sessionCart", JSON.stringify(addInSession));
        }
        this.setState({
            cartContain: this.state.cartArray
        });
    }

    editTextOnCake(indexOf, text) {
        this.setState({
            textOnCake: text,
            indexNoOfEditableText: indexOf
        })
        //DK upgrade
        //$('#editModalTextOnCake').modal('open');
        instanceEditTextOnCake.open();
    }
    updateTextOnCake(indexOf) {
        if (this.state.textOnCake !== '') {
            let copy = this.state.cartArray;
            copy[indexOf].productExtras[0] = this.state.textOnCake;
            // let copyCartArray = [...this.state.cartArray, copy];

            this.setState({
                cartArray: copy,
                cartContain: copy
            });
        }
    }

    displayPriceOnButton(indexOfMenu, productId, indexOfData) {
        this.state.cartArray.forEach(element => {
            if (productId === element.productId) {
                $("#" + element.productId).removeClass("collapsein");
                $("#" + element.productId).addClass("collapse");
                $("." + element.productId).removeClass("divColor");
            }

        });
    }

    async componentDidMount() {

        modalOptions = {
            onOpenStart: () => {
                // console.log("Open Start");
            },
            onOpenEnd: () => {
                //console.log("Open End");
            },
            onCloseStart: () => {
                //console.log("Close Start");
            },
            onCloseEnd: () => {
                //console.log("Close End");
            },
            inDuration: 250,
            outDuration: 250,
            opacity: 0.5,
            dismissible: true,
            startingTop: "4%",
            endingTop: "10%"
        };

        var d = new Date();
        // initialize all modals  
        // old modal materialize css         
        //$('.modal').modal();

        //DK materialize 0.100.2 to 1.0.0
        // const M = window.M;
        document.addEventListener('DOMContentLoaded', function () {
            var elemModal = document.querySelectorAll('.modal');
            M.Modal.init(elemModal, modalOptions);


            //Start Modal checkout
            var checkoutModalElem = document.querySelector('#modal_Checkout');
            instanceCheckout = M.Modal.init(checkoutModalElem, modalOptions);
            //instanceCheckout = M.Modal.getInstance(checkoutModalElem);
            //Start Modal checkout

            //Start Modal extraText
            var extraTextModalElem = document.querySelector('#modal_ExtraText');
            instanceExtraText = M.Modal.init(extraTextModalElem, modalOptions);
            // instanceExtraText = M.Modal.getInstance(extraTextModalElem);
            //Start Modal extraText

            //Start Modal payment
            //var paymentModalElem = document.querySelector('#payment_review');
            //instancePayment = M.Modal.init(paymentModalElem, modalOptions);
            //instancePayment = M.Modal.getInstance(paymentModalElem);
            //Start Modal payment

            //Start Modal productInfo
            var productInfoModalElem = document.querySelector('#modalProductInfo');
            instanceProductInfo = M.Modal.init(productInfoModalElem, modalOptions);
            //instanceProductInfo = M.Modal.getInstance(productInfoModalElem);
            //Start Modal payment


            //Start Modal edit textOnCake
            var textOnCakeModalElem = document.querySelector('#modalTextOnCake');
            instanceTextOnCake = M.Modal.init(textOnCakeModalElem, modalOptions);
            //instanceTextOnCake = M.Modal.getInstance(textOnCakeModalElem);
            //Start Modal payment

            //Start Modal edit textOnCake
            var editTextOnCakeModalElem = document.querySelector('#editModalTextOnCake');
            instanceEditTextOnCake = M.Modal.init(editTextOnCakeModalElem, modalOptions);
            //instanceTextOnCake = M.Modal.getInstance(textOnCakeModalElem);
            //Start Modal payment



            //Start Modal orderConfirm
            //var orderConfirmModalElem = document.querySelector('#modal_OrderConfirm');
            //instanceOrderConfirm = M.Modal.init(orderConfirmModalElem, modalOptions);
            //instanceOrderConfirm = M.Modal.getInstance(orderConfirmModalElem);
            //Start Modal payment

            //Start Modal agb
            // var agbModalElem = document.querySelector('#agb');
            // instanceAGB = M.Modal.getInstance(agbModalElem);
            //Start Modal agb


        });

        this.state.colorForProductType['Salate'] = '#ffffff';
        this.state.colorForProductType['Suppen'] = '#ffffff';
        this.state.colorForProductType['Pizza'] = '#ffffff';
        this.state.colorForProductType['Nudelgerichte'] = '#ffffff';
        this.state.colorForProductType['Manti'] = '#ffffff';
        this.state.colorForProductType['Herzhafte Gerichte'] = '#ffffff';
        this.state.colorForProductType['Sandwiches und Wraps'] = '#ffffff';
        this.state.colorForProductType['Snacks'] = '#ffffff';
        this.state.colorForProductType['Torten'] = '#ffffff';
        this.updatePredicate();
        window.addEventListener("resize", this.updatePredicate);
        var self = this;
        var days = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
        var tDaya = new Date(new Date());
        var dayName = days[tDaya.getDay()];
        const shopTimeResponse = await fetch(`/api/shopOpenTime`);
        const shopTime = await shopTimeResponse.json();
        let startTime = '';
        let endTime = '';
        shopTime.timeInfo.forEach(element => {
            if (dayName === element.day) {
                startTime = element.timeFrom;
                endTime = element.timeTo;
            }

        });
        const deliveryTimeReq = await fetch(`/api/deliveryTime`);
        let deliveryData = await deliveryTimeReq.json();
        let deliveryTime = deliveryData[0].deliveryTime;
        if (deliveryData[0].deliveryTime === 'Keine')
            deliveryTime = ''
        const response = await fetch(`/api/getOnlineOrderMenu`);
        const json = await response.json();

        json.forEach(element => {
            element.data.forEach(element => {
                this.state.allProductIds.push(element.productId);
            });
        });

        this.setState({
            onlineMenuResponse: json,
            currenHour: d.getHours(),
            curentMinutes: d.getMinutes(),
            shopStartTime: startTime,
            shopClosedTime: endTime,
            currenHourMinutes: parseFloat(d.getHours() + "." + d.getMinutes()),
            deliveryTime: deliveryTime
        })
        var storedNames = JSON.parse(localStorage.getItem("sessionCart"));

        if (storedNames !== null && storedNames.length > 0) {
            await this.setState({
                cartArray: storedNames[0],
                cartContain: storedNames[0]
            })
        }

        $('select').on('change', this.handleExtraSelectChange);
        $('#dhour').on('change', this.handleHours);
        $('#dtime').on('change', this.handleMinutes);
        $('.extrasCheckbox').on('change', this.handleCheckBox);
        $(".productInfo").click(function () {
            let allAllergens = $(this).attr('data');
            if (allAllergens === 'undefined' || allAllergens == '') {
                self.setState({
                    allergy: []
                })
                //DK upgrade
                //$('#modalProductInfo').modal('open');
                instanceProductInfo.open();

            } else {
                let allValues = allAllergens.split(',');
                self.setState({
                    allergy: allValues
                })
                //DK upgrade
                //$('#modalProductInfo').modal('open');
                instanceProductInfo.open();
            }
            return false;


        });
        $(".headerContain").click(function () {
            let liId = $(this).attr('value');

            // lert(liId);
            self.state.allProductIds.forEach(element => {
                $("#" + element).removeClass("collapsein");
                $("#" + element).addClass("collapse");
                $("." + element).removeClass("headerPanelColor");
            });


            $("." + liId).addClass("headerPanelColor");

            let checkCollapse = $("#" + liId).hasClass("collapse");
            let checkCollapseIn = $("#" + liId).hasClass("collapsein");
            if (checkCollapse === true) {
                $("#" + liId).removeClass("collapse");
                $("#" + liId).addClass("collapsein");
            }
            if (checkCollapseIn === true) {
                $("#" + liId).addClass("collapse");
                $("#" + liId).removeClass("collapsein");
            }
            if (self.state.headerClicked === liId) {
                $("#" + liId).removeClass("collapsein");
                $("#" + liId).addClass("collapse");
                $("." + liId).removeClass("headerPanelColor");
                self.state.headerClicked = 0;
            } else {
                self.state.headerClicked = liId;
            }
        });

        $("#PickUp_Checkbox").on('click', function () {

            if ($(this).prop("checked") === true) {
                self.setState({
                    PickUp_Checkbox_Checked: true,
                    deliveryCharge: 0,
                    deliveryType: 'Selbstabholung'
                })

            } else {
                self.setState({
                    PickUp_Checkbox_Checked: false,
                    deliveryCharge: 2.5,
                    deliveryType: 'Lieferung'
                })
            }

        });

        $("#addTextOnCakeCheckbox").on('click', function () {
            if ($(this).prop("checked") === true) {
                self.setState({
                    addTextOnCakeCheckbox: true
                })
            } else {
                self.setState({
                    addTextOnCakeCheckbox: false
                })
            }
        });

        $("#cust_fname").on('click', function () {
            if (self.state.deliveryDistance === -1 && self.state.PickUp_Checkbox_Checked === false) {
                $("#deliveryDistanceError").show();
            } else {
                $("#deliveryDistanceError").hide();
            }
        });



        let searchInput = 'search_input';
        $(document).ready(function () {
            var autocomplete;
            var componentForm = {
                street_number: 'short_name',
                route: 'long_name',
                locality: 'long_name',
                country: 'long_name',
                postal_code: 'short_name'
            };
            // eslint-disable-next-line no-undef
            autocomplete = new google.maps.places.Autocomplete((document.getElementById(searchInput)), {
                types: ['geocode'],
                componentRestrictions: { country: 'de' }
            });

            // eslint-disable-next-line no-undef
            google.maps.event.addListener(autocomplete, 'place_changed', function () {
                var near_place = autocomplete.getPlace();
                let addressForPin = near_place.address_components;
                let lat = near_place.geometry.location.lat();
                let long = near_place.geometry.location.lng();
                let deliveryAddressPincod = 0;
                let fullAddress = [];
                let emptyAddress = ['Hausnummer', 'Straße', 'Stadt', 'Land', 'Postal Code'];
                for (var i = 0; i < near_place.address_components.length; i++) {
                    var addressType = near_place.address_components[i].types[0];
                    if (componentForm[addressType]) {
                        var val = near_place.address_components[i][componentForm[addressType]];
                        if (addressType === 'postal_code') {
                            deliveryAddressPincod = val;
                        }
                        fullAddress.push({
                            [addressType]: val
                        });
                    }

                    if (addressType === 'country')
                        emptyAddress = emptyAddress.filter(item => item !== 'Land');
                    if (addressType === 'street_number')
                        emptyAddress = emptyAddress.filter(item => item !== 'Hausnummer');
                    if (addressType === 'route')
                        emptyAddress = emptyAddress.filter(item => item !== 'Straße');
                    if (addressType === 'locality')
                        emptyAddress = emptyAddress.filter(item => item !== 'Stadt');
                    if (addressType === 'postal_code') {
                        emptyAddress = emptyAddress.filter(item => item !== 'Postal Code');
                    }

                }
                let submitButtonIsActive = true;
                if (emptyAddress.length > 0) {
                    submitButtonIsActive = false;
                }
                let distance = self.getDistanceFromLatLonInKm(lat, long, 52.3752655, 9.7313067);
                let calcDeliveryCharge = 2.5;
                if (parseFloat(distance) > 3) {
                    calcDeliveryCharge = 4.5;
                }
                self.setState({
                    deliveryAddressPincod: deliveryAddressPincod,
                    deliveryDistance: parseFloat(distance),
                    deliveryAddress: near_place.formatted_address,
                    checkGoogleAddress: emptyAddress,
                    fullAddress: fullAddress,
                    submitButtonIsActive: submitButtonIsActive,
                    deliveryCharge: calcDeliveryCharge
                })

            });

        });
        // $("#checkAGBCakeForm").click(function (e) {
        $("#checkAGBCakeForm").on('click', function (e) {

            // e.preventDefault();
            if ($("#checkAGBCakeForm").prop("checked") === true) {

                if (self.canGoNext()) {
                    self.setState({
                        termsAndCondition: false,
                        submitButtonIsActive: true

                    })
                } else {
                    self.setState({
                        termsAndCondition: false
                    })
                }

            } else {

                self.setState({
                    termsAndCondition: true,
                    submitButtonIsActive: false
                })
            }
        });

        //below function use for identify img id
        $(document).ready(function () {
            $(window).on('scroll', function () {
                var Wscroll = $(this).scrollTop();
                $('img[id^="menuTitile_"]').each(function () {
                    var ThisOffset = $(this).closest('ul').offset();
                    if (Wscroll > ThisOffset.top && Wscroll < ThisOffset.top + $(this).closest('ul').outerHeight(true)) {
                        let menutitleId = $(this).attr('id').split('menuTitile_');
                        if (parseInt(menutitleId[1]) === self.state.scrollProductId) {
                            self.setState({
                                scrollIdFlagTopToDown: false
                            })
                        }
                        else {
                            self.setState({
                                scrollProductId: parseInt(menutitleId[1]),
                                scrollIdFlagTopToDown: true
                            })
                        }

                    }
                });
            });
        });

        $('.validate').on('change', function () {
            if (self.canGoNext()) {
                self.setState({
                    submitButtonIsActive: true
                })
            }
            if (self.state.submitButtonIsActive && !self.canGoNext()) {
                self.setState({
                    submitButtonIsActive: false
                })
            }
        });

        window.addEventListener('scroll', self.handleScroll);

        if (window.performance) {
            if (performance.navigation.type == 1) {
                //alert("This page is reloaded");
            } else {
                //alert(' else ')
                this.showMollieMessage();
                // setInterval(this.showMollieMessage(), 5000)
                mollieMessageFunc = setInterval(() => {
                    this.showMollieMessage()
                }, 5000);
            }
        }



    }

    showMollieMessage = () => {
        console.log('this.state.mollieMessage:', this.state.mollieMessage)
        //alert(' showMollieMessage: ', this.state.mollieMessage)
        var self = this;
        if (orderId && this.state.mollieMessage) {
            // alert(' fes ')
            let postData = {
                "orderId": orderId,
                "onlineMenu": true
            }

            $.ajax({
                type: 'POST',
                url: '/api/getMolliePayment',
                data: JSON.stringify(postData),
                // async: false,
                datatype: 'json',
                contentType: 'application/json; charset=UTF-8',
            }).done((res) => {
                if (res.status === 'paid') {
                    self.state.mollieMessage = false;
                    clearInterval(mollieMessageFunc);

                    // Swal.fire(
                    //     `Vielen Dank! Ihre Bestellungs Nr:<b style="color:blue;">&nbsp;&nbsp;${orderId}.</b>.`,
                    //     `Bitte prüfen Sie ihre E-Mail und die Bestellung nochmal sorgfältig und folgen Sie den dortigen Anweisungen.`,
                    //     'success'
                    // )

                    var orderConfirmModalElem = document.querySelector('#modal_OrderConfirm');
                    instanceOrderConfirm = M.Modal.getInstance(orderConfirmModalElem);
                    instanceOrderConfirm.open();
                    $('#confirmMsgId').html(`Vielen Dank! Ihre Bestellungs Nr:<span style="color:#b1bf00;"> ${orderId}</span> ist eingegangen.`)
                    this.resetValues();

                } else if (res.status === 'canceled') {
                    self.state.mollieMessage = false;
                    clearInterval(mollieMessageFunc);
                    this.resetValues();
                }

            }).fail((jqXhr) => {
                console.log('Order was not saved. ', jqXhr);
                this.resetValues();
            });
        }
    }

    handleScroll(event) {
        var dist = window.scrollY;
        var st = $(window).scrollTop();
        if (dist > 80) {
            $(".rightSection").addClass("rightSectionAfterScroll");
        }
        else {
            $(".rightSection").removeClass("rightSectionAfterScroll");
        }


        if (st > lastScrollTop) {
            //down scrolling
        }
        else {
            // up scrolling
            $(".rightSection").removeClass("rightSectionAfterScroll");
        }
        lastScrollTop = st;
    }


    getDistanceFromLatLonInKm(lat1, lon1, lat2, lon2) {
        var R = 6371; // Radius of the earth in km
        var dLat = this.deg2rad(lat2 - lat1);  // deg2rad below
        var dLon = this.deg2rad(lon2 - lon1);
        var a =
            Math.sin(dLat / 2) * Math.sin(dLat / 2) +
            Math.cos(this.deg2rad(lat1)) * Math.cos(this.deg2rad(lat2)) *
            Math.sin(dLon / 2) * Math.sin(dLon / 2)
            ;
        var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        var d = R * c; // Distance in km
        return d;
    }
    deg2rad(deg) {
        return deg * (Math.PI / 180)
    }

    componentWillUnmount() {
        window.removeEventListener("resize", this.updatePredicate);
    }

    updatePredicate() {
        this.setState({ isDesktop: window.innerWidth > 1000 });
    }

    paymentDetailsModal = () => {
        //DK upgrade
        //$('#modal_Checkout').modal('close');
        //console.log('instanceCheckout:', instanceCheckout);
        //instanceCheckout.close();
        //$('#payment_review').modal('open');
        var paymentModalElem = document.querySelector('#payment_review');
        instancePayment = M.Modal.getInstance(paymentModalElem);
        instancePayment.open();
    }

    // agbModal = () => {
    //     //DK upgrade
    //     //$('#agb').modal('open');
    //     instanceAGB.open();
    // }

    // unsereModal = () => {
    //     //DK upgrade
    //     //$('#agb').modal('open');
    //     instanceAGB.open();
    // }


    checkOutModal = () => {
        //$('.modal').modal();
        //   $('#modal_Checkout').modal('open');

        //Dk upgrade
        //$('#modal_Checkout').modal('open');
        var checkoutModalElem = document.querySelector('#modal_Checkout');
        instanceCheckout = M.Modal.getInstance(checkoutModalElem);
        instanceCheckout.open();
    }
    extraTextModal = () => {
        //  $('.modal').modal();
        // $('#modal_Checkout').modal('open');

        //Dk upgrade
        //$('#modal_ExtraText').modal('open');
        instanceExtraText.open();
    }

    async toggle(indexOfMenu, productId, indexOfData, clickOn) {
        let extraProductType = this.state.onlineMenuResponse[indexOfMenu]['data'][indexOfData].extraProductType;
        let productType = this.state.onlineMenuResponse[indexOfMenu].productType;
        let productExtras = this.state.onlineMenuResponse[indexOfMenu]['data'][indexOfData].productExtras;
        let displayProductQty = 0;
        let displayPriceOnButton = 0;
        let selectedDropdown = '';
        if (clickOn === 'header') {
            let singleArrayInCart = {};
            singleArrayInCart.productExtras = [];
            singleArrayInCart.productExtrasCode = [];
            singleArrayInCart.productExtrasAmount = [0];
            singleArrayInCart.productType = productType;
            singleArrayInCart.textOnCake = [];
            if (productExtras.length > 0 && extraProductType !== 'checkbox') {
                selectedDropdown = `${productExtras[0].rate},${productExtras[0].name},${productExtras[0].code}==${productId}`;
                singleArrayInCart.productExtras[0] = productExtras[0].name;
                singleArrayInCart.productExtrasAmount[0] = productExtras[0].rate;
                singleArrayInCart.productExtrasCode[0] = productExtras[0].code;
                // singleArrayInCart.leastPrice = productExtras[0].rate;

            }
            singleArrayInCart.productId = this.state.onlineMenuResponse[indexOfMenu]['data'][indexOfData].productId;
            singleArrayInCart.productName = this.state.onlineMenuResponse[indexOfMenu]['data'][indexOfData].productName;
            singleArrayInCart.productRate = this.state.onlineMenuResponse[indexOfMenu]['data'][indexOfData].productRate;
            singleArrayInCart.amount = this.state.onlineMenuResponse[indexOfMenu]['data'][indexOfData].productRate;
            singleArrayInCart.productQty = 1;
            singleArrayInCart.itemFor = '';
            displayProductQty = singleArrayInCart.productQty;
            displayPriceOnButton = singleArrayInCart.amount;
            this.state.cartArray.forEach((element, index) => {
                if (element.productId === productId) {
                    $("#" + productId).removeClass("collapse");
                    $("#" + productId).addClass("collapsein");
                    $("." + productId).addClass("headerPanelColor");
                    return;
                }
            });
            await this.setState({
                singleArrayInCart: singleArrayInCart,
                displayProductQty: displayProductQty,
                displayPriceOnButton: displayPriceOnButton,
                selectedDropdown: selectedDropdown,
            });
        }
        if (extraProductType === 'noextra') {
            $(".orderBtn").addClass("waves-light");
            setTimeout(function () {
                $(".orderBtn").removeClass("waves-light");
            }, 2000);
            this.productWithNoExtras(indexOfMenu, productId, indexOfData);
        }
        if (clickOn === 'addToCart') {
            // if (clickOn === 'priceAddButton') {
            //     $("#" + productId).removeClass("collapsein");
            //     $("#" + productId).addClass("collapse");
            //     $("." + productId).removeClass("headerPanelColor");

            //     $(".orderBtn").addClass("waves-light");
            //     setTimeout(function () {
            //         $(".orderBtn").removeClass("waves-light");
            //     }, 2000);
            // }
            $(".orderBtn").addClass("waves-light");
            setTimeout(function () {
                $(".orderBtn").removeClass("waves-light");
            }, 2000);
            this.addToCart(indexOfMenu, productId, indexOfData);
        }
        if (clickOn === 'cartIncrement') {
            this.incrementInSigleElement(indexOfMenu, productId, indexOfData);
        }
        if (extraProductType === 'dropselect' && clickOn === 'header') {
            this.cakeMenu(indexOfMenu, productId, indexOfData);
        }

    }

    cakeMenu(indexOfMenu, productId, indexOfData) {
        let copyCake = this.state.onlineMenuResponse[indexOfMenu]['data'][indexOfData];
        let copy = this.state.singleArrayInCart;
        let selectedDropdown = '';
        copyCake.productExtras.forEach((element, index) => {
            if (element.rate === copyCake.productRate) {
                selectedDropdown = `${element.rate},${element.name},${element.code}==${copyCake.productId}`;
                copy.productExtras[0] = element.name;
                copy.productExtrasAmount[0] = 0;
                copy.productExtrasCode[0] = element.code;
                copy.leastPrice = element.rate;
            }
        });
        copy.itemFor = 'dropselect';
        this.setState({
            singleArrayInCart: copy,
            displayProductQty: copy.productQty,
            displayPriceOnButton: copy.productRate,
            selectedDropdown: selectedDropdown,
        });
    }

    incrementInSigleElement(indexOfMenu, productId, indexOfData) {
        let copy = this.state.singleArrayInCart;
        let productExtrasAmount = 0;
        if (copy.productExtras.length > 0) {
            productExtrasAmount = copy.productExtrasAmount.reduce(this.sumOfArrayValue);
        }

        copy.productQty += 1;
        let addExtras = copy.productQty * productExtrasAmount;
        copy.amount = (copy.productQty * copy.productRate) + productExtrasAmount;
        this.setState({
            singleArrayInCart: copy,
            displayProductQty: copy.productQty,
            displayPriceOnButton: (copy.productQty * copy.productRate) + addExtras
        });
    }

    async productWithNoExtras(indexOfMenu, productId, indexOfData) {
        let copyOfClickedProductData = this.state.onlineMenuResponse[indexOfMenu]['data'][indexOfData];
        let checkInCartArray = false;
        this.state.cartArray.forEach(element => {
            if (element.productId === productId) {
                checkInCartArray = true;
            }
        });

        if (checkInCartArray) {
            let copyCartArray = this.state.cartArray;
            this.state.cartArray.forEach((element, index) => {
                if (element.productId === productId) {
                    copyCartArray[index].productQty += 1;
                    copyCartArray[index].amount = copyCartArray[index].productQty * copyCartArray[index].productRate;
                    this.setState({
                        cartArray: copyCartArray,
                        cartContain: copyCartArray
                    });
                }
            });


        } else {
            let temp = {};

            temp.productId = copyOfClickedProductData.productId;
            temp.productName = copyOfClickedProductData.productName;
            temp.productRate = copyOfClickedProductData.productRate;
            temp.productExtras = [];
            temp.productExtrasAmount = [0];
            temp.amount = copyOfClickedProductData.productRate;
            temp.productQty = 1;

            let copyCartArray = [...this.state.cartArray, temp];
            await this.setState({
                cartArray: copyCartArray,
                cartContain: copyCartArray
            });
        }
    }

    async addToCart(indexOfMenu, productId, indexOfData) {
        let indexOf = 0;
        let duplicateInCartWithExtras = false;
        let copyOfCartArray = await this.state.cartArray;
        let temp = await this.state.singleArrayInCart;
        if (temp.productType === 'Torten') {
            let i = [];
            for (let index = 0; index < temp.productQty; index++) {
                i.push(1)

            }
            this.setState({
                numberOfTextbox: i
            })

            //DK upgrade
            //$('#modalTextOnCake').modal('open');
            instanceTextOnCake.open();
            return;
        }
        if (temp.itemFor === 'dropselect') {
            $("#productId_" + temp.productId).html(temp.leastPrice.toFixed(2).toString().replace(".", ",") + " €");
        }

        let indexNumberOfTrue = -1;
        await this.state.cartArray.forEach((element, index) => {
            if (element.productId === productId) {
                duplicateInCartWithExtras = this.arraysEqual(element.productExtrasCode, temp.productExtrasCode);
                if (duplicateInCartWithExtras === true) {
                    indexNumberOfTrue = index;
                    indexOf = index;
                    return false;
                }
            }
        });

        if (indexNumberOfTrue !== -1) {
            indexOf = indexNumberOfTrue;
            let productExtrasAmount = 0;
            if (temp.productExtras.length > 0) {
                for (let index = 0; index < 50; index++) {
                    $('#' + productId + index).prop('checked', false);
                }
                productExtrasAmount = this.state.singleArrayInCart.productExtrasAmount.reduce(this.sumOfArrayValue);
            }
            let test = copyOfCartArray[indexOf].product
            copyOfCartArray[indexOf].productQty += 1;
            productExtrasAmount = copyOfCartArray[indexOf].productQty * productExtrasAmount;
            copyOfCartArray[indexOf].amount = (temp.productRate * copyOfCartArray[indexOf].productQty) + productExtrasAmount;
            // let copyCartArray = [...this.state.cartArray, temp];
            await this.setState({
                cartArray: copyOfCartArray,
                cartContain: copyOfCartArray
            });
        } else {
            let temp = this.state.singleArrayInCart;
            let productExtrasAmount = 0;
            if (temp.productExtras.length > 0) {
                for (let index = 0; index < 50; index++) {
                    $('#' + productId + index).prop('checked', false);
                }

                productExtrasAmount = this.state.singleArrayInCart.productExtrasAmount.reduce(this.sumOfArrayValue);
            }
            let extraAmount = productExtrasAmount * temp.productQty;
            temp.amount = (temp.productRate * temp.productQty) + extraAmount;

            let copyCartArray = [...this.state.cartArray, temp];
            await this.setState({
                cartArray: copyCartArray,
                cartContain: copyCartArray
            });
        }

        $("#" + productId).addClass("collapse");
        $("#" + productId).removeClass("collapsein");
        $("." + productId).removeClass("headerPanelColor");


    }

    async addToCartForCake(noOfTextBox) {
        let temp = await this.state.singleArrayInCart;
        let productId = temp.productId;
        // let textOnCake = [];
        let textOnCakeAmount = 0;




        let indexOf = 0;
        let duplicateInCartWithExtras = false;
        let copyOfCartArray = await this.state.cartArray;
        if (temp.itemFor === 'dropselect') {
            // $("#productId_" + temp.productId).html(temp.leastPrice.toFixed(2).toString().replace(".", ",") + " €");
        }

        let indexNumberOfTrue = -1;
        await this.state.cartArray.forEach((element, index) => {
            if (element.productId === productId) {
                duplicateInCartWithExtras = this.arraysEqual(element.productExtrasCode, temp.productExtrasCode);
                if (duplicateInCartWithExtras === true) {
                    indexNumberOfTrue = index;
                    indexOf = index;
                    return false;
                }
            }
        });
        if (indexNumberOfTrue !== -1) {
            indexOf = indexNumberOfTrue;
            let productExtrasAmount = 0;
            if (temp.productExtras.length > 0) {
                for (let index = 0; index < 50; index++) {
                    $('#' + productId + index).prop('checked', false);
                }
                productExtrasAmount = this.state.singleArrayInCart.productExtrasAmount.reduce(this.sumOfArrayValue);
            }
            let test = copyOfCartArray[indexOf].product
            copyOfCartArray[indexOf].productQty += 1;
            productExtrasAmount = copyOfCartArray[indexOf].productQty * productExtrasAmount;
            copyOfCartArray[indexOf].amount = (temp.productRate * copyOfCartArray[indexOf].productQty) + productExtrasAmount;
            // let copyCartArray = [...this.state.cartArray, temp];
            await this.setState({
                cartArray: copyOfCartArray,
                cartContain: copyOfCartArray
            });
        } else {
            let temp = this.state.singleArrayInCart;
            let productExtrasAmount = 0;
            if (temp.productExtras.length > 0) {
                for (let index = 0; index < 50; index++) {
                    $('#' + productId + index).prop('checked', false);
                }

                productExtrasAmount = this.state.singleArrayInCart.productExtrasAmount.reduce(this.sumOfArrayValue);
            }
            let extraAmount = productExtrasAmount * temp.productQty;
            temp.amount = (temp.productRate * temp.productQty) + extraAmount;

            let copyCartArray = [...this.state.cartArray, temp];
            await this.setState({
                cartArray: copyCartArray,
                cartContain: copyCartArray
            });

        }

        let textOnCake = {};
        for (let i = 1; i <= noOfTextBox; i++) {
            $("#textOnCake" + i).val();
            // textOnCake.push($("#textOnCake"+i).val());
            if ($("#textOnCake" + i).val() !== '') {
                // textOnCakeAmount +=3;
                textOnCake.productId = temp.productId;
                textOnCake.productName = "Torten Text";
                textOnCake.productExtras = [$("#textOnCake" + i).val()];
                textOnCake.productExtrasAmount = [3];
                textOnCake.productExtrasCode = [''];
                textOnCake.productQty = 1;
                textOnCake.productRate = 3;
                textOnCake.productType = "TextOnCake";
                textOnCake.amount = 3;
                textOnCake.textOnCake = [];
                textOnCake.itemFor = temp.productName + " == " + temp.productExtras[0];
                textOnCake.leastPrice = 3;
                let copyCartArray = [...this.state.cartArray, textOnCake];
                await this.setState({
                    cartArray: copyCartArray,
                    cartContain: copyCartArray
                });
            }
            $("#textOnCake" + i).val("");
        }
        // temp.textOnCake = textOnCake;
        $("#" + productId).addClass("collapse");
        $("#" + productId).removeClass("collapsein");
        $("." + productId).removeClass("headerPanelColor");


    }

    arraysEqual(_arr1, _arr2) {
        if (!Array.isArray(_arr1) || !Array.isArray(_arr2) || _arr1.length !== _arr2.length)
            return false;
        var arr1 = _arr1.concat().sort();
        var arr2 = _arr2.concat().sort();
        for (var i = 0; i < arr1.length; i++) {
            if (arr1[i] !== arr2[i])
                return false;

        }
        return true;
    }

    async incrementDecrement(indexOfEl, type) {
        let productExtraAmount = 0;
        if (indexOfEl === 'NoIndex') {
            let copy = this.state.singleArrayInCart;
            if (copy.productExtras.length > 0) {
                productExtraAmount = copy.productExtrasAmount.reduce(this.sumOfArrayValue);
            }
            if (copy.productQty > 1) {
                copy.productQty -= 1;

                let mewAmount = copy.productQty * copy.productRate;
                let newProductAmount = copy.productQty * productExtraAmount;

                copy.amount = (mewAmount + newProductAmount);

                this.setState({
                    singleArrayInCart: copy,
                    displayProductQty: copy.productQty,
                    displayPriceOnButton: copy.amount
                });
            }


        } else {
            let copy = this.state.cartArray;

            if (copy[indexOfEl].productExtras.length > 0) {
                productExtraAmount = copy[indexOfEl].productExtrasAmount.reduce(this.sumOfArrayValue);
            }
            if (type === 'increment') {
                if (copy[indexOfEl].productType !== 'TextOnCake') {
                    copy[indexOfEl].productQty += 1;
                    copy[indexOfEl].amount += (copy[indexOfEl].productRate + productExtraAmount);
                }

            } else if (type === 'decrement') {
                if (copy[indexOfEl].productQty > 1) {
                    copy[indexOfEl].productQty -= 1;
                    copy[indexOfEl].amount -= (copy[indexOfEl].productRate + productExtraAmount);
                } else {
                    //console.log(" qty less than 1");
                }
            }
            this.setState({
                cartArray: copy,
                cartContain: copy
            });
        }




    }

    handleExtraSelectChange = (event) => {
        var n = event.target.value.includes("==");
        if (event.target.value !== 'select' && n) {
            let splitValue = event.target.value.split('==');
            let selectValue = splitValue[0].split('--');
            let toggleValue = splitValue[1].split('--');
            let productExtras = [];
            let productExtraAmount = [];
            let productExtraCode = [];
            let displayPriceOnButton = 0;
            productExtras[0] = selectValue[1];
            productExtraAmount[0] = parseFloat(selectValue[0]);
            productExtraCode[0] = selectValue[2];

            let copy = this.state.singleArrayInCart;
            copy.productExtrasAmount = productExtraAmount;
            if (copy.itemFor === 'dropselect') {
                copy.productRate = productExtraAmount[0];
                copy.productExtrasAmount[0] = 0;
                $("#productId_" + copy.productId).html(copy.productRate.toFixed(2).toString().replace(".", ",") + " €");
            }
            copy.productExtras = productExtras;

            copy.productExtrasCode = productExtraCode;
            copy.productExtrasType = 'dropdown';
            displayPriceOnButton = this.state.singleArrayInCart.amount + productExtraAmount[0];
            let newProductExtrasAmount = copy.productQty * productExtraAmount[0];
            copy.amount = (copy.productQty * copy.productRate) + newProductExtrasAmount
            displayPriceOnButton = (copy.productQty * copy.productRate) + newProductExtrasAmount;

            this.setState({
                singleArrayInCart: copy,
                displayPriceOnButton: displayPriceOnButton,
                selectedDropdown: `${selectValue}==${copy.productId}`,
            });

        }
        if (event.target.value === 'select') {

            let productExtras = [];
            let productExtraAmount = [0];
            let productExtraCode = [];
            let displayPriceOnButton = 0;

            let copy = this.state.singleArrayInCart;
            copy.productExtras = productExtras;
            copy.productExtrasAmount = productExtraAmount;
            copy.productExtrasCode = productExtraCode;
            copy.productExtrasType = 'dropdown';
            displayPriceOnButton = this.state.singleArrayInCart.amount;
            this.setState({
                singleArrayInCart: copy,
                displayPriceOnButton: displayPriceOnButton,
                selectedDropdown: '',

            });
        }


    }

    handleHours = (event) => {
        if (this.canGoNext()) {
            this.setState({
                deliveryTimeHour: event.target.value,
                //submitButtonIsActive: true
            })
        } else {
            if (!this.canGoNext()) {
                this.setState({
                    deliveryTimeHour: event.target.value,
                    // submitButtonIsActive: false
                })
            } else {
                this.setState({
                    deliveryTimeHour: event.target.value,
                })
            }
            if (this.state.submitButtonIsActive && !this.canGoNext()) {
                this.setState({
                    submitButtonIsActive: false
                })
            }

        }
        this.fixHandle();

    }
    handleMinutes = (event) => {
        if (this.canGoNext()) {
            this.setState({
                deliveryTimeMin: event.target.value,
                // submitButtonIsActive: true
            })
        } else {
            if (!this.canGoNext()) {
                this.setState({
                    deliveryTimeMin: event.target.value,
                    // submitButtonIsActive: false
                })
            } else {
                this.setState({
                    deliveryTimeMin: event.target.value
                })
            }
        }
        // $('.validate').trigger("change");
        this.fixHandle();
    }

    fixHandle() {
        if (this.canGoNext()) {
            this.setState({
                submitButtonIsActive: true
            })
        }
        if (this.state.submitButtonIsActive && !this.canGoNext()) {
            this.setState({
                submitButtonIsActive: false
            })
        }
    }

    handleCheckBox = (event) => {
        let splitValue = event.target.value.split('==');

        let selectValue = splitValue[0].split('--');
        let toggleValue = splitValue[1].split('--');
        let checkboxIsChecked = true;
        if ($("#" + selectValue[3]).prop("checked")) {
            checkboxIsChecked = true;
        } else {
            checkboxIsChecked = false;
        }


        let productExtras = [];
        let productExtraAmount = [];
        let productExtrasCode = [];
        let displayPriceOnButton = 0;




        let checkProductExtras = false;
        if (this.state.singleArrayInCart.productExtras.length > 0)
            checkProductExtras = true;

        let copySingleArrayInCart = this.state.singleArrayInCart;
        let checkboxLength = copySingleArrayInCart.productExtras.length;
        if (checkboxLength < 5 || checkboxIsChecked === false) {
            if (checkProductExtras === false) {
                $(`.${selectValue[3]}`).addClass('colorChange');
                productExtras[0] = selectValue[1];
                productExtraAmount[0] = parseFloat(selectValue[0]);
                productExtrasCode[0] = selectValue[2];
                copySingleArrayInCart.productExtras = productExtras;
                copySingleArrayInCart.productExtrasAmount = productExtraAmount;
                copySingleArrayInCart.productExtrasCode = productExtrasCode;
            } else {
                if (checkboxIsChecked) {
                    $(`.${selectValue[3]}`).addClass('colorChange');
                    copySingleArrayInCart.productExtras.push(selectValue[1]);
                    copySingleArrayInCart.productExtrasAmount.push(parseFloat(selectValue[0]));
                    // copySingleArrayInCart.productExtrasCode.push(selectValue[2]);
                    copySingleArrayInCart.productExtrasCode.push(selectValue[2]);
                } else {
                    $(`.${selectValue[3]}`).removeClass('colorChange');
                    this.state.singleArrayInCart.productExtras.forEach((element, index) => {
                        if (element === selectValue[1]) {
                            copySingleArrayInCart.productExtras.splice(index, 1);
                            copySingleArrayInCart.productExtrasAmount.splice(index, 1);
                            copySingleArrayInCart.productExtrasCode.splice(index, 1);
                        }
                    });
                }

            }
            let PExtraAmount = 0;
            if (copySingleArrayInCart.productExtrasAmount.length > 0) {
                PExtraAmount = copySingleArrayInCart.productExtrasAmount.reduce(this.sumOfArrayValue);
            }
            copySingleArrayInCart.amount = (copySingleArrayInCart.productQty * copySingleArrayInCart.productRate) + PExtraAmount
            copySingleArrayInCart.productExtrasType = 'checkbox';
            displayPriceOnButton = copySingleArrayInCart.amount;
            this.setState({
                singleArrayInCart: copySingleArrayInCart,
                displayPriceOnButton: displayPriceOnButton,
                // cartContain: this.state.cartArray
            });
        } else {
            $("#" + selectValue[3]).prop("checked", false);

            M.toast({ html: "Mehr als 5 Auswahl nicht möglich", inDuration: 4000, classes: 'red-Materialize' });
        }
    }

    sumOfArrayValue(total, value, index, array) {
        return total + value;
    }

    checkEmailvalidation = (text) => {
        const regexp = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return regexp.test(text);
    }

    checkIntergeridation = (params) => {
        const regexp = /^\d{5,14}$/;
        return regexp.test(params);
    }

    canGoNext = () => {
        if (this.state.checkGoogleAddress.length > 0 && this.state.deliveryDistance < 6 && this.state.PickUp_Checkbox_Checked === false) {
            return false;
        }
        if (this.state.deliveryDistance > 6 && this.state.PickUp_Checkbox_Checked === false) {
            return;
        }
        if (this.state.deliveryDistance === -1 && this.state.PickUp_Checkbox_Checked === false) {
            return;
        }
        if (this.state.email) {
            var temp = this.checkEmailvalidation(this.state.email);
            if (!temp) {
                return false;
            }
            // return temp === true;
        }

        if (this.state.mobile) {
            let temp = this.checkIntergeridation(this.state.mobile);
            if (!temp) {
                return false;
            }
        }

        if (this.state.fname === '' || this.state.fname === undefined || this.state.fname === null) {
            return this.state.fname !== '';
        }
        else if ((this.state.deliveryTimeHour === '' || this.state.deliveryTimeHour === undefined || this.state.deliveryTimeHour === null) && this.state.PickUp_Checkbox_Checked === true) {
            return this.state.deliveryTimeHour !== '';
        }
        else if ((this.state.deliveryTimeMin === '' || this.state.deliveryTimeMin === undefined || this.state.deliveryTimeMin === null) && this.state.PickUp_Checkbox_Checked === true) {
            return this.state.deliveryTimeMin !== '';
        }
        else if (this.state.lname === '' || this.state.lname === undefined || this.state.lname === null) {
            return this.state.lname !== '';
        }
        else if (this.state.email === '' || this.state.email === undefined || this.state.email === null) {
            return this.state.email !== '';
        }
        else if (this.state.mobile === '' || this.state.mobile === undefined || this.state.mobile === null) {
            return this.state.mobile !== '';
        }
        else if ((this.state.deliveryAddress === '' || this.state.deliveryAddress === undefined || this.state.deliveryAddress === null) && this.state.PickUp_Checkbox_Checked === false) {
            return this.state.deliveryAddress !== '';
        }
        else {
            return true;
        }
    }

    handleDataChange = async (e) => {
        this.setState({ [e.target.name]: e.target.value });
        if (e.target.name === 'paymentType') {
            var newmenuOrderId = 0;
            if (e.target.value === 'paypal') {
                $('.cash-notice').addClass('hidden');

                if ($('#hiddenOrderId').val() == '' && this.canGoNext() && !this.state.termsAndCondition) {
                    newmenuOrderId = await this.getNewOrderId();
                    // this.setState({
                    //     orderIdReference: 10000000 + newmenuOrderId,
                    //     hiddenOrderId: 10000000 + newmenuOrderId
                    // });
                    this.handleSubmit({
                        preventDefault: function () { },
                        storeType: 'paypal',
                        orderId: newmenuOrderId
                    });
                }
                else {
                    // M.toast({ html: "Bitte zunächst Details ausfüllen", inDuration: 2000, classes: 'red-Materialize' });
                    if ($('#hiddenOrderId').val() == '') {
                        this.setState({ [e.target.name]: 'cod' });
                        $('#paypalRadioButton').prop('checked', false);
                    }
                    $('.cash-notice').removeClass('hidden');
                }
            } else {
                $('.cash-notice').removeClass('hidden');
            }
        }
    }


    handleSubmit = async (e) => {
        if (!this.canGoNext()) {
            M.toast({ html: "Bitte zunächst Details ausfüllen", inDuration: 2000, classes: 'red-Materialize' });
            return;
        }
        else {
            if (e) e.preventDefault();
            this.setState({
                isSubmitInProgress: true
            });
            let cartContainObject = [];
            // let subTotal = this.getPrice();
            let subTotal = 0;
            this.state.cartContain.forEach((element, index) => {
                subTotal += element.amount;
            });
            let data = {};
            data.productPrice = subTotal;
            data.price = this.getPrice();
            data.taxAmount = this.state.taxAmount;
            data.totalAmount = this.getPrice();
            data.deliveryAddress = this.state.deliveryAddress;
            data.deliveryAddressPincod = this.state.deliveryAddressPincod;
            data.deliveryDistance = this.state.deliveryDistance;
            data.fname = this.state.fname;
            data.lname = this.state.lname;
            data.mobile = this.state.mobile;
            data.email = this.state.email;
            data.text = this.state.extraText;
            data.paymentType = this.state.paymentType;
            data.isSubmitInProgress = this.state.isSubmitInProgress;
            data.orderIdReference = this.state.orderIdReference;
            data.hiddenOrderId = this.state.hiddenOrderId;
            data.cartContain = this.state.cartContain;
            data.deliveryTimeHour = this.state.deliveryTimeHour;
            data.deliveryTimeMin = this.state.deliveryTimeMin;
            data.pickupTime = `${data.deliveryTimeHour}:${data.deliveryTimeMin}`;
            data.deliveryType = this.state.deliveryType;
            data.deliveryCharge = this.state.deliveryCharge;
            data.pickupchecked = this.state.PickUp_Checkbox_Checked;
            if (this.state.deliveryTimeHour !== '' || this.state.deliveryTimeMin !== '') {
                data.pickupTime = `${data.deliveryTimeHour}:${data.deliveryTimeMin}`;
            } else {
                data.pickupTime = '';
            }
            if (e && e.payment) {
                data.paymentDetails = e.payment;
            }

            console.log('data:', data);

            if (e.storeType == 'paypal') {
                this.setState({
                    isSubmitInProgress: false,
                    isLoading: true
                });
                data.menuOrderId = e.orderId;
                data.paymentType = 'paypal';
                data = JSON.stringify(data);
                $.ajax({
                    type: 'POST',
                    url: '/api/menuOrder',
                    data: data,
                    // async: false,
                    datatype: 'json',
                    contentType: 'application/json; charset=UTF-8',
                }).done((res) => {
                    if (res.error) {

                        M.toast({ html: "Technical error occurred. Please try again later", inDuration: 8000, classes: 'red-Materialize' });
                        return;
                    }
                    this.setState({
                        isLoading: false
                    });
                }).fail((jqXhr) => {
                    console.log('paaypal fail');
                    M.toast({ html: "Ein Server Fehler ist aufgetreten. Bitte Kontaktieren Sie uns Telefonisch +4951117507.", inDuration: 8000, classes: 'red-Materialize' });
                    this.setState({
                        isLoading: false
                    });
                });

            } else if (e.storeType === 'paypalTrnSuccess') {
                data.menuOrderIdUpdate = e.orderIdForUpdate;
                data.status = 'ORDR';
                data = JSON.stringify(data);
                $.ajax({
                    type: 'PUT',
                    url: '/api/menuOrderUpdate',
                    data: data,
                    datatype: 'json',
                    contentType: 'application/json; charset=UTF-8',
                }).done((res) => {
                    //DK upgrade
                    //$('#modal_OrderConfirm').modal('open');
                    var orderConfirmModalElem = document.querySelector('#modal_OrderConfirm');
                    instanceOrderConfirm = M.Modal.getInstance(orderConfirmModalElem);
                    instanceOrderConfirm.open();
                    $('#confirmMsgId').html(`Vielen Dank! Ihre Bestellungs Nr:<span style="color:#b1bf00;"> ${e.orderIdForUpdate}</span> ist eingegangen.`)


                    //DK upgrade
                    //$('#payment_review').modal('close');

                    this.setState({
                        termsAndCondition: true,
                        submitButtonIsActive: false
                    })
                    this.resetValues();
                    var paymentModalElem = document.querySelector('#payment_review');
                    instancePayment = M.Modal.getInstance(paymentModalElem);
                    instancePayment.close();
                    if (!this.state.isDesktop) {
                        var checkoutModalElem = document.querySelector('#modal_Checkout');
                        instanceCheckout = M.Modal.getInstance(checkoutModalElem);
                        instanceCheckout.close();
                    }
                    //$('#modal_Checkout').modal('close');

                }).fail(function (err) {

                    console.log('Error occured - in calling menuOrderUpdate ', err);


                    this.setState({
                        termsAndCondition: true,
                        submitButtonIsActive: false
                    })
                    this.resetValues();
                    //DK upgrade
                    //$('#payment_review').modal('close');
                    var paymentModalElem = document.querySelector('#payment_review');
                    instancePayment = M.Modal.getInstance(paymentModalElem);
                    instancePayment.close();
                    if (!this.state.isDesktop) {
                        var checkoutModalElem = document.querySelector('#modal_Checkout');
                        instanceCheckout = M.Modal.getInstance(checkoutModalElem);
                        instanceCheckout.close();
                    }
                    //$('#modal_Checkout').modal('close');

                });
            } else if (e.storeType === 'paypalTrnCancelled') {

                data.menuOrderIdUpdate = e.orderIdForUpdate;
                data.status = 'CLSE';
                data = JSON.stringify(data);
                $.ajax({
                    type: 'PUT',
                    url: '/api/menuOrderUpdateAsCLSE',
                    data: data,
                    datatype: 'json',
                    contentType: 'application/json; charset=UTF-8',
                }).done((res) => {
                    if (res.error) {
                        M.toast({ html: "Technical error has occurred. Please try again later", inDuration: 8000, classes: 'red-Materialize' });
                        return;
                    }
                    this.setState({
                        termsAndCondition: true,
                        submitButtonIsActive: false
                    })
                    this.resetValues();
                    //DK upgrade
                    //$('#payment_review').modal('close');
                    var paymentModalElem = document.querySelector('#payment_review');
                    instancePayment = M.Modal.getInstance(paymentModalElem);
                    instancePayment.close();
                    if (!this.state.isDesktop) {
                        var checkoutModalElem = document.querySelector('#modal_Checkout');
                        instanceCheckout = M.Modal.getInstance(checkoutModalElem);
                        instanceCheckout.close();
                    }
                    //$('#modal_Checkout').modal('close');

                });

            } else if (data.paymentType === 'mollie') {

                if (this.state.orderIdReference === 0) {
                    data.menuOrderId = await this.getNewOrderId();
                } else {
                    data.menuOrderId = this.state.orderIdReference;
                }

                // this.setState({
                //     isLoading: true
                // });


                console.log('mollie data:', data);
                data = JSON.stringify(data);

                $.ajax({
                    type: 'POST',
                    url: '/api/menuOrder',
                    data: data,
                    datatype: 'json',
                    contentType: 'application/json; charset=UTF-8',
                }).done((res) => {
                    console.log('mollie menu order res:', res);
                    window.open(res._links.checkout.href, '_self');
                });
            } else {

                if ($('#hiddenOrderId').val() == '') {
                    data.menuOrderId = await this.getNewOrderId();
                } else {
                    data.menuOrderId = this.state.orderIdReference;
                }

                this.setState({
                    isLoading: true
                });

                data = JSON.stringify(data);
                $.ajax({
                    type: 'POST',
                    url: '/api/menuOrder',
                    data: data,
                    datatype: 'json',
                    contentType: 'application/json; charset=UTF-8',
                }).done((res) => {

                    if (res.error) {

                        M.toast({ html: "Technical error has occurred. Please try again later", inDuration: 8000, classes: 'red-Materialize' });
                        return;
                    }
                    this.setState({
                        isLoading: false
                    });
                    var orderConfirmModalElem = document.querySelector('#modal_OrderConfirm');
                    instanceOrderConfirm = M.Modal.getInstance(orderConfirmModalElem);
                    instanceOrderConfirm.open();
                    $('#confirmMsgId').html(`Vielen Dank! Ihre Bestellungs Nr:<span style="color:#b1bf00;"> ${res.orderId}</span> ist eingegangen.`)
                    this.setState({
                        termsAndCondition: true,
                        submitButtonIsActive: false
                    })


                    this.resetValues();
                    //DK upgrade
                    //$('#payment_review').modal('close');
                    var paymentModalElem = document.querySelector('#payment_review');
                    instancePayment = M.Modal.getInstance(paymentModalElem);
                    instancePayment.close();
                    if (!this.state.isDesktop) {
                        var checkoutModalElem = document.querySelector('#modal_Checkout');
                        instanceCheckout = M.Modal.getInstance(checkoutModalElem);
                        instanceCheckout.close();
                    }
                    //$('#modal_Checkout').modal('close');

                }).fail((jqXhr) => {
                    console.log('cash order fail');
                    M.toast({ html: "Ein Server Fehler ist aufgetreten. Bitte Kontaktieren Sie uns Telefonisch +4951117507.", inDuration: 8000, classes: 'red-Materialize' });
                    this.setState({
                        termsAndCondition: true,
                        submitButtonIsActive: false,
                        isLoading: false
                    })
                    this.resetValues();
                    //DK upgrade
                    //$('#payment_review').modal('close');
                    var paymentModalElem = document.querySelector('#payment_review');
                    instancePayment = M.Modal.getInstance(paymentModalElem);
                    instancePayment.close();
                    if (!this.state.isDesktop) {
                        var checkoutModalElem = document.querySelector('#modal_Checkout');
                        instanceCheckout = M.Modal.getInstance(checkoutModalElem);
                        instanceCheckout.close();
                    }
                    //$('#modal_Checkout').modal('close');
                });
            }
        }
    }

    getPrice = () => {
        let subTotal = 0;
        this.state.cartContain.forEach((element, index) => {
            subTotal += element.amount;
        });
        return subTotal + this.state.deliveryCharge;
    }

    getOrderIdForReference = () => {
        return this.state.orderIdReference;
    }

    getNewOrderId = () => {
        let _this = this;
        return new Promise((resolve, reject) => {
            $.ajax({
                url: '/api/getNextSequenceValueForMenueOrder',
                async: false,
                type: 'GET',
                dataType: 'json', // added data type
                success: function (res) {
                    // newmenuOrderId = res.value.sequence_value;
                    _this.setState({
                        orderIdReference: 10000000 + res.value.sequence_value,
                        hiddenOrderId: 10000000 + res.value.sequence_value
                    });
                    resolve(10000000 + res.value.sequence_value);
                }
            });
        })
    }

    sendAnalyticsData(data) {
        ReactGA.plugin.execute('ecommerce', 'addTransaction', data);
        ReactGA.plugin.execute('ecommerce', 'send');
        ReactGA.plugin.execute('ecommerce', 'clear');
    }

    resetValues = (e) => {
        initializeAnalytics();
        console.log(" e ", e);
        let allItem = [];
        let revenue = 0;
        let date = new Date();
        let uniqueId = date.getTime();
        this.state.cartContain.forEach((elOfCart, i) => {
            date = new Date();
            let temp = {};
            revenue += elOfCart.amount;
            // element.productId
            if (elOfCart.productExtras.length > 0) {
                let amountFromExtra = 0;
                let i = 0;
                elOfCart.productExtras.forEach((element, index) => {
                    date = new Date();
                    uniqueId = "Tr" + date.getTime() + index;
                    temp.id = uniqueId;
                    temp.name = elOfCart.productExtras[index];
                    temp.category = 'product Extras';
                    temp.quantity = elOfCart.productQty;
                    temp.price = elOfCart.productExtrasAmount[index] * elOfCart.productQty;
                    amountFromExtra += elOfCart.productExtrasAmount[index];
                    // let copyAnalytics = [...this.state.analyticsData, temp];
                    allItem.push(temp);
                    // this.sendAnalyticsData(temp);
                    temp = {};
                    // this.setState({
                    //     analyticsData: copyAnalytics
                    // })
                    i++;
                });
                if (elOfCart.productExtras.length === i) {
                    date = new Date();
                    uniqueId = "Tr" + date.getTime() + i;
                    temp.id = uniqueId;
                    temp.name = elOfCart.productName;
                    // temp.sku = elOfCart.productId;
                    temp.category = 'Main Product';
                    temp.quantity = elOfCart.productQty;
                    temp.price = elOfCart.amount - amountFromExtra;
                    allItem.push(temp);
                    // this.sendAnalyticsData(temp);

                    // let copyAnalytics = [...this.state.analyticsData, temp];
                    temp = {};
                    // this.setState({
                    //     analyticsData: copyAnalytics
                    // })
                }


            } else {

                temp.id = "Tr" + uniqueId;
                temp.name = elOfCart.productName;
                // temp.sku = elOfCart.productId;
                temp.category = 'Main Product With No Extras';
                temp.quantity = elOfCart.productQty;
                temp.price = elOfCart.amount;
                allItem.push(temp);
                // this.sendAnalyticsData(temp);
                // let copyAnalytics = [...this.state.analyticsData, temp];
                temp = {};
                // this.setState({
                //     analyticsData: copyAnalytics
                // })
            }

        });

        // console.log(" allItem ", allItem);
        // console.log(" revenue ", revenue);
        if (dataForAnalytics) {
            window.gtagEcommerce('event', 'purchase', {
                "transaction_id": uniqueId,
                "value": revenue,
                "currency": "EUR",
                "tax": 0.00,
                "shipping": this.state.deliveryCharge,
                "items": allItem
            });
        }

        localStorage.clear();
        // $("#PickUp_Checkbox")if ($(this).prop("checked") === true)
        $("#PickUp_Checkbox").prop("checked", false);
        $("#checkAGBCakeForm").prop("checked", false);

        this.setState({
            // isDesktop: false, //This is where I am having problems
            open: false,
            PickUp_Checkbox_Checked: false,
            totalMenuWithTitle: [],
            deliveryAddress: '',
            deliveryAddressPincod: '',
            deliveryCharge: 2.5,
            deliveryDistance: 0,
            deliveryTimeHour: '',
            deliveryTimeMin: '',
            fname: '',
            lname: '',
            mobile: '',
            email: '',
            extraText: '',
            paymentType: 'cod',
            isSubmitInProgress: false,
            productPrice: 0,
            orderIdReference: 0,
            hiddenOrderId: '',
            displayProductQty: 1,
            displayPriceOnButton: 1,
            singleArrayInCart: {},
            cartArray: [],
            cartContain: [],
            totalAmount: 0,
            taxAmount: 2.5,
            grandTotalAount: 0,
            allProductIds: [],
            headerClicked: 0,
            //   colorForProductType: [],
            isLoading: false,
            selectedDropdown: 0,
            deliveryType: 'Lieferung',
            scrollIdFlagTopToDown: false
        });
        $(".warningMsg").removeClass("warningMsgColor");
        window.scrollTo(0, 0);


        var checkFname = $('#cust_fname').hasClass('valid');
        var checkLname = $('#cust_lname').hasClass('valid');
        var checkEmail = $('#cust_email').hasClass('valid');
        var checkMobile = $('#cust_mobile').hasClass('valid');
        var checkSearch_input = $('#search_input').hasClass('valid');

        var checkFnameLbl = $('#cust_fname_lbl').hasClass('active');
        var checkLnameLbl = $('#cust_lname_lbl').hasClass('active');
        var checkEmailLbl = $('#cust_email_lbl').hasClass('active');
        var checkMobileLbl = $('#cust_mobile_lbl').hasClass('active');

        if (checkFname) {
            $('#cust_fname').removeClass('valid');
        }

        if (checkLname) {
            $('#cust_lname').removeClass('valid');
        }
        if (checkEmail) {
            $('#cust_email').removeClass('valid');
        }
        if (checkMobile) {
            $('#cust_mobile').removeClass('valid');
        }
        if (checkSearch_input) {
            $('#search_input').removeClass('valid');
        }


        if (checkFnameLbl) {
            $('#cust_fname_lbl').removeClass('active');
        }

        if (checkLnameLbl) {
            $('#cust_lname_lbl').removeClass('active');
        }
        if (checkEmailLbl) {
            $('#cust_email_lbl').removeClass('active');
        }
        if (checkMobileLbl) {
            $('#cust_mobile_lbl').removeClass('active');
        }


    }

    render() {
        let buttonIsActive = true;
        if (!this.state.termsAndCondition && this.state.submitButtonIsActive) {
            buttonIsActive = false;
        }
        let addInSession = [];
        if (this.state.cartContain.length > 0) {
            addInSession[0] = this.state.cartContain;
            localStorage.setItem("sessionCart", JSON.stringify(addInSession));
        }
        let showMessage = 'Leider kannst Du noch nicht bestellen. Wir liefern erst ab einem Mindestbestellwert von 20,00 € (exkl. Lieferkosten).';
        let checkoutButtonStatus = true;
        let totalDisplayOnMobile = 0;
        this.state.cartContain.forEach(element => {
            totalDisplayOnMobile += element.amount;
            /* condition to check if order is not self-pickup then apply min. order value check & enbale/disable "Bezahlen" btn accordingly, else enable for self-pickup order */
            if (this.state.PickUp_Checkbox_Checked === false) {
                if (totalDisplayOnMobile > 20) {
                    checkoutButtonStatus = false;
                    $(".warningMsg").removeClass("warningMsgColor");
                    showMessage = '';
                }
            }
            else {
                checkoutButtonStatus = false;
                $(".warningMsg").removeClass("warningMsgColor");
                showMessage = '';
            }
            // if (totalDisplayOnMobile > 20) {
            //     checkoutButtonStatus = false;
            //     $(".warningMsg").removeClass("warningMsgColor");
            //     // showMessage = 'Du hast den Mindestbestellwert von 10,00 € erreicht und kannst jetzt fortfahren.';
            //     showMessage = '';
            // }
        });

        if (this.state.currenHourMinutes > this.state.shopClosedTime || this.state.currenHourMinutes < this.state.shopOpenTime) {
            checkoutButtonStatus = true;
            $(".warningMsg").addClass("warningMsgColor");
            showMessage = `Leider hat die Küche gerade zu. Probieren Sie gerne zwischen ${this.state.shopStartTime}-${this.state.shopClosedTime} Uhr täglich.`;
            setTimeout(function () {
                $(".warningMsg").removeClass("warningMsgColor");
            }, 4500);
        }
        else {
            $(".warningMsg").removeClass("warningMsgColor");
        }

        let renderTotalAmount = 0;
        return (
            <ReactScoped encapsulation={ViewEncapsulation.Emulated} styles={[styles]}>
                {this.state.isLoading && <div class="overlayForSpinner">
                    <div class="overlay__inner">
                        <div class="overlay__content"><span class="spinner"></span></div>
                    </div>
                </div>
                }
                <section id="onlineOrder">
                    <div className="filter-div">
                        <Filter scrollIdFlagDown={this.state.scrollIdFlagTopToDown} scrollProduct_Id={this.state.scrollProductId} menu={this.state.onlineMenuResponse} />
                    </div>
                    <div className="mainOnlineOrder" id="mainOnlineOrder">


                        <div className="main-title">
                            <h5 className="Title1">Speisekarte </h5>
                            {/* <i className="material-icons">local_shipping</i> */}
                        </div>
                        <hr />
                        <div id="content">
                            {this.state.onlineMenuResponse.map((el, indexOfEl) => (
                                <ul className="collapsible z-depth-0" data-collapsible="accordion">
                                    <img className="product-type-img" src={`/productTypeImage/${el.productType}.jpg`} id={`menuTitile_${el.id}`}
                                        width="100%" height={`${this.state.isDesktop ? '200' : '150'}`}></img>
                                    <h5 className="MenuTitle" style={{ height: el.productType === 'Pizza' ? '7%' : '' }}>
                                        {el.productType}
                                        <div className="pizzaClass" style={{ display: el.productType === 'Pizza' ? '' : 'none' }}>
                                            {el.productType === 'Pizza' ? 'Handausgerollte Italian Pizza. Auch bekannt als New York Pizza 30cm Durchmesser.' : ''}
                                        </div>

                                    </h5>

                                    {el.data.map((elOfData, indexOf) => (
                                        <li>
                                            <div
                                                style={{ backgroundColor: this.state.colorForProductType[el.productType] }}
                                                className={`${elOfData.productId} headerContain`} value={elOfData.productId}
                                                onClick={this.toggle.bind(this, indexOfEl, elOfData.productId, indexOf, 'header')}>
                                                <span className="subTitle">
                                                    {elOfData.productName}
                                                    <span className="productInfo redirect-cls" data={`${elOfData.allergens}`} >Produktinfo</span>
                                                </span>

                                                <div className="addItemDiv"><i className="material-icons addItemIcon">add</i></div>
                                                <span className="lightGrayText">{elOfData.productDescription}</span>
                                                {/* <span className="lightGrayText" style={{display: this.state.onlineMenuResponse[indexOfEl]['data'][0].extraProductType === 'dropselect' ? 'block' : 'none'}}>
                                                Wählbar aus 4 Größen, bitte im Bemerkungfeld alternative Sorte angeben falls Wunschtorte nicht lieferbar ist
                                            </span> */}
                                                <span style={{ fontStyle: 'italic', marginTop: '3px', marginBottom: '3px' }} className="extraInfoText">{elOfData.extraInfo}</span>
                                                <span className="priceTextColor" id={`productId_${elOfData.productId}`} > {elOfData.productRate.toFixed(2).toString().replace(".", ",")} €</span>
                                            </div>
                                            <div
                                                className={"collapse" + (this.state.open ? 'in' : '')}
                                                id={(elOfData.extraProductType === 'checkbox' ? elOfData.productId : '') || (elOfData.extraProductType === 'dropselect' ? elOfData.productId : '') || (elOfData.extraProductType === 'dropd' ? elOfData.productId : '') || (elOfData.extraProductType === 'redirect' ? 'redirect' : '') || (elOfData.extraProductType === 'noextra' ? 'noextra' : '')}>
                                                <Extras
                                                    productExtras={elOfData.productExtras}
                                                    extraProductType={elOfData.extraProductType}
                                                    indexOfEl={indexOfEl}
                                                    productId={elOfData.productId}
                                                    indexOf={indexOf}
                                                    selectedDropdown={this.state.selectedDropdown}
                                                />

                                                <div className="incrDecBtnDiv">
                                                    <button name="apply" class="incrDecBtn" onClick={this.incrementDecrement.bind(this, 'NoIndex', elOfData.productId)}>-</button>
                                                    <span className="itemCount"> {this.state.displayProductQty} </span>
                                                    <button name="apply" class="incrDecBtn" onClick={this.toggle.bind(this, indexOfEl, elOfData.productId, indexOf, 'cartIncrement')}>+</button>
                                                    <button class="totalPriceCount" onClick={this.toggle.bind(this, indexOfEl, elOfData.productId, indexOf, 'addToCart')}> {this.state.displayPriceOnButton.toFixed(2).toString().replace(".", ",")}
                                                        <i className="material-icons cartIconLaptopView">add_shopping_cart</i>
                                                    </button>
                                                </div>
                                            </div>
                                        </li>

                                    ))}
                                </ul>
                            ))}
                        </div>
                    </div>


                    <div>
                        {this.state.isDesktop ? (
                            <section>
                                <div className="rightSection right">
                                    <h5 className="cart">Warenkorb</h5>
                                    <hr className="horizontalLine" />
                                    <div style={{ display: `${this.state.cartContain.length > 0 ? `none` : `block`}` }}>
                                        <div className="emptyShoppingCartIconDiv">
                                            <i className="material-icons emptyShoppingCartIcon">shopping_basket</i>
                                        </div>
                                        <div>
                                            <span className="emptyShoppingCartMsg">Wähle leckere Gerichte aus der Karte und bestelle Dein Menü.</span>
                                        </div>
                                    </div>
                                    <div className="cartContainDiv" >
                                        {this.state.cartContain.map((el, indexOfEl) => (
                                            <div>
                                                <div className="cartContain">

                                                    <div className="cartItemDiv">
                                                        <span className="subTitle cartItem"> {el.productName} </span>
                                                        {/* <span style={{display: `${el.productType === 'TextOnCake' ? `block` : `none`}`, cursor:'pointer' }}> <i onClick={this.editTextOnCake.bind(this, indexOfEl, el.productName)} className="material-icons deleteItemIcon">create</i> </span>  */}
                                                        <div className="editItemDiv" style={{ display: `${el.productType === 'TextOnCake' ? `block` : `none`}` }} onClick={this.editTextOnCake.bind(this, indexOfEl, el.productExtras[0])}>
                                                            <button className="editItemBtn"><i className="material-icons editItemIcon">create</i></button>
                                                        </div>
                                                    </div>
                                                    <div className="qtyPlusMinusDiv">
                                                        <button name="apply" class="qtyPlusMinusBtn" onClick={this.incrementDecrement.bind(this, indexOfEl, 'decrement')}>-</button>
                                                        <span className="qtyCount"> {('0' + el.productQty).slice(-2)} </span>
                                                        <button name="apply" class="qtyPlusMinusBtn" onClick={this.incrementDecrement.bind(this, indexOfEl, 'increment')}>+</button>
                                                    </div>
                                                    <div renderTotalAmountCalc={renderTotalAmount += el.amount} className="itemPriceCountDiv"> {el.amount.toFixed(2).toString().replace(".", ",")} €</div>
                                                    <div className="deleteItemDiv" onClick={this.removeFromCart.bind(this, indexOfEl, 'remove')}><button className="deleteItemBtn"><i className="material-icons deleteItemIcon">delete_outline</i></button></div>

                                                </div>
                                                <div className="extrasItem" style={{ display: el.productExtras.length > 0 ? 'block' : 'none' }}>
                                                    {/* {el.productExtras.map((el, indexOfEl) => ( */}
                                                    <span className="subTitle cartItem">{el.productExtras.join()}</span>
                                                    {/* ))} */}
                                                </div>
                                            </div>
                                        ))}
                                    </div>
                                    <hr className="horizontalLine" />

                                    <div className="totalPriceDiv">
                                        <div className="lightGrayText subTotal"><span>Zwischensumme</span><span className="subPrice"> {renderTotalAmount.toFixed(2).toString().replace(".", ",")} €</span></div>
                                        <div className="lightGrayText subTotal"><span>Lieferkosten</span><span className="extra-km-distance">(ab 3KM Radius 4,50€)</span><span className="subPrice">{this.state.deliveryCharge.toFixed(2).toString().replace(".", ",")} €</span></div>
                                        <div className="totalLbl"><span>Gesamt</span><span className="totalPrice">{(renderTotalAmount + this.state.deliveryCharge).toFixed(2).toString().replace(".", ",")} €</span></div>
                                    </div>

                                    <hr className="horizontalLine" />

                                    <div className="toOrderDiv">
                                        <div>
                                            <label className="lblPickUp_Checkbox" htmlFor="PickUp_Checkbox">
                                                <input type="checkbox" className="PickUpCheckbox filled-in" id="PickUp_Checkbox" defaultChecked={this.state.PickUp_Checkbox_Checked} />
                                                <span className="badge" style={{ color: this.state.PickUp_Checkbox_Checked ? 'var(--dark-brown)' : '#757575' }}>Selbstabholung</span>
                                                {/* <a >
                                                    <span className="badge" style={{ color: this.state.PickUp_Checkbox_Checked ? 'var(--dark-brown)' : '#757575' }}>Selbstabholung</span>
                                                </a> */}
                                            </label>
                                        </div>
                                        <div className="extraText" onClick={this.extraTextModal}>
                                            <label htmlFor="additional_remark" className="lblExtraText"><i className="material-icons extraTextIcon">create</i>Bemerkungen (Nachricht an Efendibey)&nbsp;<span className="brownColor">{this.state.extraText}</span></label>
                                        </div>

                                        {this.state.deliveryTime && (
                                            <div className="lightGrayText subTotal">
                                                <div className="deliveryTime"><i className="material-icons motorCycleIcon">motorcycle</i>{this.state.deliveryTime}</div>
                                            </div>
                                        )}
                                        <div className="lightGrayText subTotal">
                                            <span className="warningMsg">{showMessage} </span>
                                        </div>
                                        <div className="orderBtnDiv">

                                            <button name="submit"
                                                disabled={checkoutButtonStatus}
                                                className="btn hoverable submit_btn waves-effect hoverable orderBtn orderBtnDesktop checkoutButton"
                                                style={{ backgroundColor: 'var(--dark-brown)' }} onClick={this.paymentDetailsModal}>Bezahlen
                                                <i className="material-icons EuroIcon">payment</i>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </section>
                        ) : (
                            <section>
                                <div className="bottomSection">
                                    <div className="checkOutBtnDiv">
                                        <button name="submit" className="btn hoverable orderBtn submit_btn waves-effect hoverable"
                                            onClick={this.checkOutModal}>
                                            <div className="iconQty">
                                                <i className="material-icons cartIcon">local_mall</i>
                                                <span class="qtyCountinMobile"> {this.state.cartContain.length} </span>
                                            </div>
                                                Warenkorb <span> ({totalDisplayOnMobile.toFixed(2).toString().replace(".", ",")}) €</span>
                                            {this.state.deliveryTime && (
                                                <div className="subTotal">
                                                    <div className="deliveryTimeInWarenkorb">{this.state.deliveryTime}</div>
                                                </div>
                                            )}
                                        </button>
                                    </div>
                                </div>
                                <div className="col s12 m12 modalTotalSummary">
                                    <div id="modal_Checkout" className="modal bottom-sheet">
                                        <div className="closeModalDiv">
                                            <a className="modal-action modal-close waves-effect waves-dark btn-flat">
                                                <i className="material-icons prefix">clear</i>
                                            </a>
                                        </div>
                                        <div className="modal-content">
                                            <h5 className="cart">Warenkorb</h5>
                                            <hr className="horizontalLine" />
                                            <div style={{ display: `${this.state.cartContain.length > 0 ? `none` : `block`}` }}>
                                                <div className="emptyShoppingCartIconDiv">
                                                    <i className="material-icons emptyShoppingCartIcon">shopping_basket</i>
                                                </div>
                                                <div>
                                                    <span className="emptyShoppingCartMsg">Wähle leckere Gerichte aus der Karte und bestelle Dein Menü.</span>
                                                </div>
                                            </div>
                                            <div className="cartContainDiv">
                                                {this.state.cartContain.map((el, indexOfEl) => (
                                                    <div>
                                                        <div className="cartContain">

                                                            <div className="cartItemDiv"><span className="subTitle cartItem"> {el.productName}  </span>
                                                                <div className="editItemDiv" style={{ display: `${el.productType === 'TextOnCake' ? `block` : `none`}` }} onClick={this.editTextOnCake.bind(this, indexOfEl, el.productExtras[0])}>
                                                                    <button className="editItemBtn"><i className="material-icons editItemIcon">create</i></button>
                                                                </div>
                                                            </div>
                                                            <div className="qtyPlusMinusDiv">
                                                                <button name="apply" class="qtyPlusMinusBtn" onClick={this.incrementDecrement.bind(this, indexOfEl, 'decrement')}>-</button>
                                                                <span className="qtyCount"> {('0' + el.productQty).slice(-2)} </span>
                                                                <button name="apply" class="qtyPlusMinusBtn" onClick={this.incrementDecrement.bind(this, indexOfEl, 'increment')}>+</button>
                                                            </div>
                                                            <div className="itemPriceCountDiv"> {el.amount.toFixed(2).toString().replace(".", ",")} €</div>
                                                            <div className="deleteItemDiv" onClick={this.removeFromCart.bind(this, indexOfEl, 'remove')}><button className="deleteItemBtn"><i className="material-icons deleteItemIcon">delete_outline</i></button></div>

                                                        </div>

                                                        {/* {el.productExtras.map((el, indexOfEl) => ( */}
                                                        <div className="extrasItem" style={{ display: el.productExtras.length > 0 ? 'block' : 'none' }}>
                                                            <span className="subTitle cartItem">{el.productExtras.join()}</span>
                                                        </div>
                                                        {/* ))} */}
                                                    </div>
                                                ))}
                                            </div>
                                            <hr className="horizontalLine" />

                                            <div className="totalPriceDiv">
                                                <div className="subTotal"><span>Zwischensumme</span><span className="subPrice"> {totalDisplayOnMobile.toFixed(2).toString().replace(".", ",")} €</span></div>
                                                <div className="subTotal"><span>Lieferkosten</span><span className="extra-km-distance">(ab 3KM Radius 4,50€)</span><span className="subPrice"> {this.state.deliveryCharge.toFixed(2).toString().replace(".", ",")} €</span></div>
                                                <div className="totalLbl"><span>Gesamt</span><span className="totalPrice"> {(totalDisplayOnMobile + this.state.deliveryCharge).toFixed(2).toString().replace(".", ",")}  €</span></div>
                                            </div>

                                            <hr className="horizontalLine" />

                                            <div className="toOrderDiv">
                                                <div>
                                                    <label className="lblPickUp_Checkbox" htmlFor="PickUp_Checkbox">
                                                        <input type="checkbox" className="PickUpCheckbox filled-in" id="PickUp_Checkbox" defaultChecked={this.state.PickUp_Checkbox_Checked} />
                                                        <span className="badge" style={{ color: this.state.PickUp_Checkbox_Checked ? 'var(--dark-brown)' : '#757575' }}>Selbstabholung</span>
                                                        {/* <a >
                                                            <span className="badge" style={{ color: this.state.PickUp_Checkbox_Checked ? 'var(--dark-brown)' : '#757575' }}>Selbstabholung</span>
                                                        </a> */}
                                                    </label>
                                                </div>
                                                <div className="extraText" onClick={this.extraTextModal}>
                                                    <label htmlFor="additional_remark" className="lblExtraText"><i className="material-icons extraTextIcon">create</i>Bemerkungen (Nachricht an Efendibey)&nbsp;<span className="brownColor">{this.state.extraText}</span></label>

                                                </div>
                                                <div className="subTotal">
                                                    <span className="warningMsg"> {showMessage} </span>
                                                </div>
                                                <div className="orderBtnDiv">

                                                    <button name="submit" disabled={checkoutButtonStatus} className="btn hoverable submit_btn waves-effect hoverable orderBtnMobile checkoutButton"
                                                        style={{ backgroundColor: 'var(--dark-brown)', margin: '15px', float: 'right' }} onClick={this.paymentDetailsModal}>Bezahlen
                                                             <i className="material-icons EuroIcon">payment</i></button>
                                                </div>
                                                <div className="orderBtnDiv">
                                                    <a className="modal-action modal-close backButton"><i className="material-icons backBtnIcon">keyboard_arrow_left</i>Zurück</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </section>
                        )}
                    </div>



                    <div className="col s12 m12 modalTotalSummary">
                        <div id="payment_review" className="modal bottom-sheet">
                            <div className="modal-content">

                                <div className="order-summary">

                                    <div className="delivery-tag">
                                        <h6>SUMME<br /></h6>
                                    </div>
                                    <div className="price-tag">
                                        <h4> {(totalDisplayOnMobile + this.state.deliveryCharge).toFixed(2).toString().replace(".", ",")} €</h4>
                                        <span className="tax-notice">
                                            Inkl. MwSt:<br />

                                        </span>
                                    </div>
                                </div>


                                <form id="msform" ref="regform" onSubmit={this.handleSubmit}>
                                    <h4 className="fs-title">Kundendaten und bezahlen</h4>
                                    <div style={{ display: `${this.state.PickUp_Checkbox_Checked ? `none` : ``}` }} className="input-field col m6 16 s12 marginTopSpace">
                                        <i className="material-icons prefix">room</i>
                                        <input id="search_input" type="text" className="validate" autocomplete="off" placeholder="Adresse, z.B. Munzstraße 7" required="true" aria-required="true" name="deliveryAddress" value={this.state.deliveryAddress} onChange={this.handleDataChange} />
                                        {/*<label htmlFor="search_input" data-error="Falsche Eingabe"></label>*/}
                                        <div className="col m12 l12 s12 alertMsg" style={{ display: `${(this.state.deliveryDistance > 4 && this.state.PickUp_Checkbox_Checked === false) ? `block` : `none`}` }}>
                                            <span className="more-then-five">Wir liefern nur bis 4Km zu uns. </span>
                                        </div>
                                        <div className="col m12 l12 s12 alertMsg" style={{ display: `${(this.state.checkGoogleAddress.length > 0 && this.state.deliveryDistance < 6) ? `` : `none`}` }}>
                                            <span className="more-then-five" > Bitte gib die {this.state.checkGoogleAddress.join()} </span>
                                            {/* {this.state.checkGoogleAddress.join()} */}
                                        </div>
                                    </div>

                                    <div style={{ display: `${this.state.PickUp_Checkbox_Checked ? `` : `none`}` }} className="col m6 16 s12 marginTopSpace selectDrpdnDiv">
                                        <i className="material-icons prefix input-field col">access_time</i>
                                        <select id="dhour" type="text" className="input-field displayShow" value={this.state.deliveryTimeHour} required="true" name="deliveryTimeHour">
                                            <option value="" disabled selected>Abholzeit</option>
                                            {/* <option value="08">08</option> */}
                                            <option value="09" disabled={this.state.currenHour >= 9 ? true : false}>09</option>
                                            <option value="10" disabled={this.state.currenHour >= 10 ? true : false}>10</option>
                                            <option value="11" disabled={this.state.currenHour >= 11 ? true : false}>11</option>
                                            <option value="12" disabled={this.state.currenHour >= 12 ? true : false}>12</option>
                                            <option value="13" disabled={this.state.currenHour >= 13 ? true : false}>13</option>
                                            <option value="14" disabled={this.state.currenHour >= 14 ? true : false}>14</option>
                                            <option value="15" disabled={this.state.currenHour >= 15 ? true : false}>15</option>
                                            <option value="16" disabled={this.state.currenHour >= 16 ? true : false}>16</option>
                                            <option value="17" disabled={this.state.currenHour >= 17 ? true : false}>17</option>
                                            <option value="18" disabled={this.state.currenHour >= 18 ? true : false}>18</option>
                                            <option value="19" disabled={this.state.currenHour >= 19 ? true : false}>19</option>
                                            <option value="20" disabled={this.state.currenHour >= 20 ? true : false}>20</option>
                                            <option value="21" disabled={this.state.currenHour >= 21 ? true : false}>21</option>
                                            <option value="22" disabled={this.state.currenHour >= 22 ? true : false}>22</option>
                                            {/* <option value="23">23</option> */}

                                        </select>
                                        <select id="dtime" type="text" className="input-field displayShow" value={this.state.deliveryTimeMin} required="true" name="deliveryTimeMin">
                                            <option value="" disabled selected>Min</option>
                                            <option value="00" >00</option>
                                            <option value="15" >15</option>
                                            <option value="30">30</option>
                                            <option value="45">45</option>
                                        </select>
                                        {/*<label htmlFor="deltimehour">Uhr</label>
                                       <label htmlFor="deltimemin">Min</label>*/}
                                    </div>

                                    {/* <div className="input-field col m6 16 s12 marginTopSpace">
                                        <i className="material-icons prefix">room</i>
                                        <input id="search_input" type="text" className="validate" required="true" aria-required="true" pattern="[a-zA-Za-zA-ZäöüßğüşöçİĞÜŞÖÇ ]+(?:(?:\. |[' -])[a-zA-Za-zA-ZäöüßğüşöçİĞÜŞÖÇ ]+)*" name="deliveryAddress" value={this.state.deliveryAddress} onChange={this.handleDataChange} />
                                        <label htmlFor="search_input" data-error="Falsche Eingabe">Adresse</label>
                                    </div> */}
                                    <div className="input-field col m6 16 s12 marginTopSpace">
                                        <i className="material-icons prefix form_icon">person</i>
                                        <input id="cust_fname" type="text" className="validate" required="true" aria-required="true" pattern="[a-zA-Za-zA-ZäöüßğüşöçİĞÜŞÖÇ ]+(?:(?:\. |[' -])[a-zA-Za-zA-ZäöüßğüşöçİĞÜŞÖÇ ]+)*" name="fname" value={this.state.fname} onChange={this.handleDataChange} />
                                        <label id="cust_fname_lbl" htmlFor="cust_fname" data-error="Falsche Eingabe">Vorname</label>
                                    </div>
                                    <div className="input-field col m6 16 s12 marginTopSpace">
                                        <i className="material-icons prefix form_icon">person_outline</i>
                                        <input id="cust_lname" type="text" className="validate" required="true" aria-required="true" pattern="[a-zA-Za-zA-ZäöüßğüşöçİĞÜŞÖÇ ]+(?:(?:\. |[' -])[a-zA-Za-zA-ZäöüßğüşöçİĞÜŞÖÇ ]+)*" name="lname" value={this.state.lname} onChange={this.handleDataChange} />
                                        <label id="cust_lname_lbl" htmlFor="cust_lname" data-error="Falsche Eingabe">Nachname</label>
                                    </div>

                                    <div className="input-field col m6 l6 s12 marginTopSpace">
                                        <i className="material-icons prefix">email</i>
                                        <input id="cust_email" type="email" className="validate" required="true" aria-required="true" name="email" pattern="[A-Za-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[A-Za-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[A-Za-z0-9](?:[A-Za-z0-9-]*[A-Za-z0-9])?\.)+[A-Za-z0-9](?:[A-Za-z0-9-]*[A-Za-z0-9])?" value={this.state.email} onChange={this.handleDataChange} />
                                        <input type="hidden" id="hiddenOrderId" value={this.state.hiddenOrderId} name="hiddenOrderId" />
                                        <label id="cust_email_lbl" htmlFor="cust_email" data-error="Falsche Eingabe">E-Mail</label>
                                    </div>
                                    <div className="input-field col m6 l6 s12 marginTopSpace">
                                        <i className="material-icons prefix">phone</i>
                                        <input id="cust_mobile" type="tel" className="validate" required="true" aria-required="true" name="mobile" pattern="^\d{5,14}$" value={this.state.mobile} onChange={this.handleDataChange} />
                                        <label id="cust_mobile_lbl" htmlFor="cust_mobile" data-error="Falsche Eingabe">Handy (für Rückmeldung) </label>
                                    </div>

                                    <div className="agbDiv">
                                        <label htmlFor="checkAGBCakeForm">
                                            <input style={{ pointerEvents: 'auto', marginTop: '15px', marginLeft: '15px' }} type="checkbox" className="filled-in validate" id="checkAGBCakeForm" name="checkAGB" ref="check_meCakeForm" />
                                            <span style={{ color: this.state.termsAndCondition ? '#757575' : 'var(--dark-brown)' }} className="badge agbSpan" >Ich habe <a className="modal-trigger" data-target="agb"><u style={{ color: '#757575' }}>AGB, Widerrufsrecht</u></a> & <a className="modal-trigger" data-target="agb"><u style={{ color: '#757575' }}
                                            >Datenschutzbestimmungen</u></a> gelesen und stimme zu.</span>
                                            {/* <a  >
                                                <span style={{ color: this.state.termsAndCondition ? '#757575' : 'var(--dark-brown)' }} className="badge agbSpan" >Ich habe <u onClick={this.agbModal} >AGB, Widerrufsrecht</u> & <u onClick={this.unsereModal}>Datenschutzbestimmungen</u> gelesen und stimme zu.</span>
                                            </a> */}
                                        </label>
                                    </div>
                                    <div className="payment_option" style={{ marginTop: '100px' }}>
                                        <div className="payment-choice">
                                            <label for="test1" style={{ color: this.state.paymentType === "cod" ? 'var(--dark-brown)' : 'gray' }}>
                                                <input className="with-gap" name="paymentType" type="radio" id="test1" value="cod" onChange={this.handleDataChange} checked={this.state.paymentType === "cod"} />
                                                <span>Bar</span>
                                            </label>
                                            {/* <input className="with-gap" name="paymentType" type="radio" id="test2" value="creditcard" onChange={this.handleDataChange} checked={this.state.paymentType === "creditcard"}  />
                                            <label for="test2">Überweisung</label> */}

                                            <label for="paypalRadioButton" style={{ color: this.state.paymentType === "paypal" ? 'var(--dark-brown)' : 'gray' }}>
                                                <input className="with-gap" name="paymentType" type="radio" id="paypalRadioButton" value="paypal" onChange={this.handleDataChange} checked={this.state.paymentType === "paypal"} />
                                                <span> PayPal</span>
                                            </label>
                                            <label for="test4">
                                                <input className="with-gap" name="paymentType" type="radio" id="test4" value="mollie" onChange={this.handleDataChange} checked={this.state.paymentType === "mollie"} />
                                                <span> Sofort/ Giropay/ Kreditkarte</span>
                                            </label>
                                            {/* checked={this.state.paymentType === "cod"} */}
                                        </div>
                                    </div>
                                    <div className="modal-footer modalFooterMargin">
                                        <a style={{ marginRight: '20px', marginLeft: '-40px', verticalAlign: 'top', float: 'left' }} className="modal-action modal-close waves-effect waves-dark btn-flat">Zurück</a>
                                        { } <a style={{ marginRight: '12px', verticalAlign: 'top' }} disabled={buttonIsActive} onClick={this.handleSubmit} className={("modal-action waves-effect waves-dark btn " + (this.state.paymentType === 'paypal' ? 'hide-btn' : ''))} >Bestätigen</a>
                                        <PaypalButton getPrice={this.getPrice} getOrderIdForReference={this.getOrderIdForReference} active={(this.state.paymentType === 'paypal' && this.state.submitButtonIsActive && !this.state.termsAndCondition) ? 'true' : 'false'} handleSubmit={this.handleSubmit} />
                                    </div>
                                </form>

                            </div>
                        </div>
                    </div>


                    <div className="col s12 m12 modalExtraText" >
                        <div id="modal_ExtraText" className="modal">
                            <div className="closeModal">
                                <a className="modal-action modal-close waves-effect waves-dark btn-flat">
                                    <i className="material-icons prefix">clear</i>
                                </a>
                            </div>
                            <div className="modal-content">
                                <div className="input-field col s12 m12 extraTextMargin">
                                    <i className="material-icons prefix commentIcon">comment</i>
                                    <textarea id="additional_remark" className="materialize-textarea" maxLength="200" data-length="200" draggable="false" name="extraText" value={this.state.extraText} onChange={this.handleDataChange}></textarea>
                                    <label htmlFor="additional_remark">Bemerkungen (Nachricht an Efendibey)</label>
                                </div>
                                <div className="modal-footer modalFooterMargin">
                                    <a className="modal-action modal-close waves-effect waves-dark btn-flat extraTextBtn">OK</a>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div className="modalOrderConfirm" >
                        <div id="modal_OrderConfirm" className="modal">
                            <div className="closeModal">
                                <a className="modal-action modal-close waves-effect waves-dark btn-flat">
                                    <i className="material-icons prefix">clear</i>
                                </a>
                            </div>
                            <div className="modal-content">
                                <div id="confirmMsgId" className="input-field col s12 m12 extraTextMargin">
                                    {this.state.confirmationMsg}
                                </div>
                                <div className="modal-footer modalFooterMargin">
                                    <a className="modal-action modal-close waves-effect waves-dark btn-flat extraTextBtn">OK</a>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div className="col s12 m12 modalExtraText" >
                        <div id="modalProductInfo" className="modal">
                            <div className="closeModal">
                                <a className="modal-action modal-close waves-effect waves-dark btn-flat">
                                    <i className="material-icons prefix">clear</i>
                                </a>
                            </div>
                            <div className="modal-content modal_content_productInfo">
                                <div className="input-field col s12 m12 extraTextMargin">
                                    <div>
                                        <h5 className="productInfoHeading"> {this.state.allergy.length > 0 ? 'Weitere Produktinformationen' : 'Keine Produktinformationen verfügbar'} </h5>
                                        <h3 className="productInfoSubHeading" style={{ display: this.state.allergy.length > 0 ? 'block' : 'none' }}>Allergien</h3>
                                        <p style={{ display: this.state.allergy.length > 0 ? 'block' : 'none' }}>
                                            {this.state.allergy.map((elOfData, indexOf) => (
                                                <div className="productInfo_Data">
                                                    <span><i className="material-icons prefix arrowIcon">arrow_right_alt</i></span>
                                                    <span className="productInfoAllergy">{elOfData}</span>
                                                </div>
                                            ))}
                                        </p>
                                        <p className="productInfoFooter">Bitte kontaktiere unseren <a className="contactUsLink" href="/#contact-us" >Kundenservice</a> bezüglich etwaiger Allergien oder Unverträglichkeiten.</p>
                                    </div>
                                </div>
                                <div className="modal-footer modalFooterMargin">
                                    <a className="modal-action modal-close waves-effect waves-dark btn-flat extraTextBtn">OK</a>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div className="col s12 m12 modalExtraText" >
                        <div id="modalTextOnCake" className="modal">
                            <div className="closeModal">
                                <a className="modal-action modal-close waves-effect waves-dark btn-flat">
                                    <i className="material-icons prefix">clear</i>
                                </a>
                            </div>
                            <div className="modal-content">

                                <div className="" style={{ paddingLeft: '12px', paddingTop: '30px' }}>
                                    <label className="lblPickUp_Checkbox" htmlFor="addTextOnCakeCheckbox">
                                        <input type="checkbox" className="filled-in" id="addTextOnCakeCheckbox" defaultChecked={this.state.addTextOnCakeCheckbox} />
                                        <span className="badge" style={{ color: this.state.addTextOnCakeCheckbox ? 'var(--dark-brown)' : '#757575' }}>Kurzer Wunschtext auf der Torte? (3€) </span>
                                        {/* <a >
                                            <span className="badge" style={{ color: this.state.addTextOnCakeCheckbox ? 'var(--dark-brown)' : '#757575' }}>Kurzer Wunschtext auf der Torte? (3€) </span>
                                        </a> */}
                                    </label>
                                </div>

                                <div style={{ display: `${this.state.addTextOnCakeCheckbox ? `block` : `none`}` }} >
                                    {this.state.numberOfTextbox.map((el, index) => (
                                        <div className="input-field col s12 m12 extraTextMargin">
                                            <i className="material-icons prefix commentIcon">comment</i>
                                            <textarea id={`textOnCake${index + 1}`} className="materialize-textarea" maxLength="75" data-length="75" draggable="false" name="textOnCake[]"></textarea>
                                            <label htmlFor={`textOnCake${index + 1}`}>Torten Text {index + 1} </label>
                                        </div>
                                    ))}

                                </div>
                                <div className="modal-footer modalFooterMargin" onClick={this.addToCartForCake.bind(this, this.state.numberOfTextbox.length)}>
                                    <a className="modal-action modal-close waves-effect waves-dark btn-flat extraTextBtn">OK</a>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div className="col s12 m12 modalExtraText" >
                        <div id="editModalTextOnCake" className="modal">
                            <div className="closeModal">
                                <a className="modal-action modal-close waves-effect waves-dark btn-flat">
                                    <i className="material-icons prefix">clear</i>
                                </a>
                            </div>
                            <div className="modal-content">
                                <div className="input-field col s12 m12 extraTextMargin">
                                    <i className="material-icons prefix commentIcon">comment</i>
                                    <textarea id={`editTextOnCake`} className="materialize-textarea" maxLength="75" data-length="75" draggable="false" name="textOnCake" value={this.state.textOnCake} onChange={this.handleDataChange}>  </textarea>
                                    <label htmlFor={`editTextOnCake`}> {this.state.textOnCake !== '' ? '' : 'Torten Text'} </label>
                                </div>
                                <div className="modal-footer modalFooterMargin" onClick={this.updateTextOnCake.bind(this, this.state.indexNoOfEditableText)}>
                                    <a className="modal-action modal-close waves-effect waves-dark btn-flat extraTextBtn">OK</a>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div className="bottomSpaceManage">

                    </div>

                    {/* <div className="col l3 m4 s12 footer_details_wrapper footer_left_rule">
                        <a onClick={()=>{$('#agb').modal('open');}}><span> </span> </a><br />
                        <a onClick={()=>{$('#unsere').modal('open');}}><span> </span></a><br />                         
                    </div> */}

                    <div id="unsere" className="modal modal-fixed-footer termsModal">
                        <div className="modal-content">

                            <h5><center>Datenschutzerklärung</center></h5>
                            <br />
                            <h6>1. Information über die Erhebung personenbezogener Daten und Kontaktdaten des Verantwortlichen</h6>
                            <p>1.1 Wir freuen uns, dass Sie unsere Website besuchen und bedanken uns für Ihr Interesse. Im Folgenden informieren wir Sie über den Umgang mit Ihren personenbezogenen Daten bei Nutzung unserer Website. Personenbezogene Daten sind hierbei alle Daten, mit denen Sie persönlich identifiziert werden können.
                        </p><p>1.2 Verantwortlicher für die Datenverarbeitung auf dieser Website im Sinne der Datenschutz-Grundverordnung (DSGVO) ist Ince GmbH, Münzstraße 7, 30159 Hannover, Deutschland, Tel.: 0511 2155 001, E-Mail: info@efendibey.de. Der für die Verarbeitung von personenbezogenen Daten Verantwortliche ist diejenige natürliche oder juristische Person, die allein oder gemeinsam mit anderen über die Zwecke und Mittel der Verarbeitung von personenbezogenen Daten entscheidet.
                        </p><p>1.3 Diese Website nutzt aus Sicherheitsgründen und zum Schutz der Übertragung personenbezogener Daten und anderer vertraulicher Inhalte (z.B. Bestellungen oder Anfragen an den Verantwortlichen) eine SSL-bzw. TLS-Verschlüsselung. Sie können eine verschlüsselte Verbindung an der Zeichenfolge „https://“ und dem Schloss-Symbol in Ihrer Browserzeile erkennen.
                        </p><br />
                            <h6>2. Datenerfassung beim Besuch unserer Website</h6>
                            <p>Bei der bloß informatorischen Nutzung unserer Website, also wenn Sie sich nicht registrieren oder uns anderweitig Informationen übermitteln, erheben wir nur solche Daten, die Ihr Browser an unseren Server übermittelt (sog. „Server-Logfiles“). Wenn Sie unsere Website aufrufen, erheben wir die folgenden Daten, die für uns technisch erforderlich sind, um Ihnen die Website anzuzeigen:
                        </p><ul>
                                <li><p>- Unsere besuchte Website
                        </p></li><li><p>- Datum und Uhrzeit zum Zeitpunkt des Zugriffes
                        </p></li><li><p>- Menge der gesendeten Daten in Byte
                        </p></li><li><p>- Quelle/Verweis, von welchem Sie auf die Seite gelangten
                        </p></li><li><p>- Verwendeter Browser
                        </p></li><li><p>- Verwendetes Betriebssystem
                        </p></li><li><p>- Verwendete IP-Adresse (ggf.: in anonymisierter Form)
                        </p></li><p>Die Verarbeitung erfolgt gemäß Art. 6 Abs. 1 lit. f DSGVO auf Basis unseres berechtigten Interesses an der Verbesserung der Stabilität und Funktionalität unserer Website. Eine Weitergabe oder anderweitige Verwendung der Daten findet nicht statt. Wir behalten uns allerdings vor, die Server-Logfiles nachträglich zu überprüfen, sollten konkrete Anhaltspunkte auf eine rechtswidrige Nutzung hinweisen.
                        </p></ul><br />

                            <h6>3. Cookies</h6>
                            <p>Um den Besuch unserer Website attraktiv zu gestalten und die Nutzung bestimmter Funktionen zu ermöglichen, verwenden wir auf verschiedenen Seiten sogenannte Cookies. Hierbei handelt es sich um kleine Textdateien, die auf Ihrem Endgerät abgelegt werden. Einige der von uns verwendeten Cookies werden nach dem Ende der Browser-Sitzung, also nach Schließen Ihres Browsers, wieder gelöscht (sog. Sitzungs-Cookies). Andere Cookies verbleiben auf Ihrem Endgerät und ermöglichen uns oder unseren Partnerunternehmen (Cookies von Drittanbietern), Ihren Browser beim nächsten Besuch wiederzuerkennen (persistente Cookies). Werden Cookies gesetzt, erheben und verarbeiten diese im individuellen Umfang bestimmte Nutzerinformationen wie Browser- und Standortdaten sowie IP-Adresswerte. Persistente Cookies werden automatisiert nach einer vorgegebenen Dauer gelöscht, die sich je nach Cookie unterscheiden kann.
                        </p><p>Teilweise dienen die Cookies dazu, durch Speicherung von Einstellungen den Bestellprozess zu vereinfachen (z.B. Merken des Inhalts eines virtuellen Warenkorbs für einen späteren Besuch auf der Website). Sofern durch einzelne von uns implementierte Cookies auch personenbezogene Daten verarbeitet werden, erfolgt die Verarbeitung gemäß Art. 6 Abs. 1 lit. b DSGVO entweder zur Durchführung des Vertrages oder gemäß Art. 6 Abs. 1 lit. f DSGVO zur Wahrung unserer berechtigten Interessen an der bestmöglichen Funktionalität der Website sowie einer kundenfreundlichen und effektiven Ausgestaltung des Seitenbesuchs.
                        </p><p>Wir arbeiten unter Umständen mit Werbepartnern zusammen, die uns helfen, unser Internetangebot für Sie interessanter zu gestalten. Zu diesem Zweck werden für diesen Fall bei Ihrem Besuch unserer Website auch Cookies von Partnerunternehmen auf Ihrer Festplatte gespeichert (Cookies von Drittanbietern). Wenn wir mit vorbenannten Werbepartnern zusammenarbeiten, werden Sie über den Einsatz derartiger Cookies und den Umfang der jeweils erhobenen Informationen innerhalb der nachstehenden Absätze individuell und gesondert informiert.
                        </p><p>Bitte beachten Sie, dass Sie Ihren Browser so einstellen können, dass Sie über das Setzen von Cookies informiert werden und einzeln über deren Annahme entscheiden oder die Annahme von Cookies für bestimmte Fälle oder generell ausschließen können. Jeder Browser unterscheidet sich in der Art, wie er die Cookie-Einstellungen verwaltet. Diese ist in dem Hilfemenü jedes Browsers beschrieben, welches Ihnen erläutert, wie Sie Ihre Cookie-Einstellungen ändern können. Diese finden Sie für die jeweiligen Browser unter den folgenden Links:
                        </p><ul>
                                <li><p>Internet Explorer: <a href="https://support.microsoft.com/de-de/help/17442/windows-internet-explorer-delete-manage-cookies" target="_blank" rel="noopener">https://support.microsoft.com/de-de/help/17442/windows-internet-explorer-delete-manage-cookies</a>
                                </p></li><li><p>Firefox: <a href="https://support.mozilla.org/de/kb/cookies-erlauben-und-ablehnen" target="_blank" rel="noopener">https://support.mozilla.org/de/kb/cookies-erlauben-und-ablehnen</a>
                                </p></li><li><p>Chrome: <a href="https://support.google.com/chrome/answer/95647?hl=de&hlrm=en" target="_blank" rel="noopener">https://support.google.com/chrome/answer/95647?hl=de&hlrm=en</a>
                                </p></li><li><p>Safari: <a href="https://support.apple.com/de-de/guide/safari/sfri11471/mac" target="_blank" rel="noopener">https://support.apple.com/de-de/guide/safari/sfri11471/mac</a>
                                </p></li><li><p>Opera: <a href="https://help.opera.com/en/latest/web-preferences/#cookies" target="_blank" rel="noopener">https://help.opera.com/en/latest/web-preferences/#cookies</a>
                                </p></li>
                            </ul>
                            <p>Bitte beachten Sie, dass bei Nichtannahme von Cookies die Funktionalität unserer Website eingeschränkt sein kann.
                        </p><br />

                            <h6>4. Kontaktaufnahme</h6>
                            <p>4.1 Im Rahmen der Kontaktaufnahme mit uns (z.B. per Kontaktformular oder E-Mail) werden personenbezogene Daten erhoben. Welche Daten im Falle eines Kontaktformulars erhoben werden, ist aus dem jeweiligen Kontaktformular ersichtlich. Diese Daten werden ausschließlich zum Zweck der Beantwortung Ihres Anliegens bzw. für die Kontaktaufnahme und die damit verbundene technische Administration gespeichert und verwendet. Rechtsgrundlage für die Verarbeitung der Daten ist unser berechtigtes Interesse an der Beantwortung Ihres Anliegens gemäß Art. 6 Abs. 1 lit. f DSGVO. Zielt Ihre Kontaktierung auf den Abschluss eines Vertrages ab, so ist zusätzliche Rechtsgrundlage für die Verarbeitung Art. 6 Abs. 1 lit. b DSGVO. Ihre Daten werden nach abschließender Bearbeitung Ihrer Anfrage gelöscht, dies ist der Fall, wenn sich aus den Umständen entnehmen lässt, dass der betroffene Sachverhalt abschließend geklärt ist und sofern keine gesetzlichen Aufbewahrungspflichten entgegenstehen.
                        </p><p>4.2 WhatsApp-Business
                        </p><p>Wir bieten Besuchern unserer Webseite die Möglichkeit, mit uns über den Nachrichtendienst WhatsApp der Facebook Inc., 1 Hacker Way, Menlo Park, CA 94025, USA in Kontakt zu treten. Hierfür verwenden wir die sog. „Business-Version“ von WhatsApp.
                        </p><p>Sofern Sie uns anlässlich eines konkreten Geschäfts (beispielsweise einer getätigten Bestellung) per Whatsapp kontaktieren, speichern und verwenden wir die von Ihnen bei WhatsApp genutzte Mobilfunknummer sowie – falls bereitgestellt – Ihren Vor- und Nachnamen gemäß Art. 6 Abs. 1 lit. b. DSGVO zur Bearbeitung und Beantwortung Ihres Anliegens. Auf Basis derselben Rechtsgrundlage werden wir Sie per WhatsApp gegebenenfalls um die Bereitstellung weiterer Daten (Bestellnummer, Kundennummer, Anschrift oder Mailadresse) bitten, um Ihre Anfrage einem bestimmten Vorgang zuordnen zu können.
                        </p><p>Nutzen Sie unseren WhatsApp-Kontakt für allgemeine Anfragen (etwa zum Leistungsspektrum, zu Verfügbarkeiten oder zu unserem Internetauftritt) speichern und verwenden wir die von Ihnen bei WhatsApp genutzte Mobilfunknummer sowie – falls bereitgestellt – Ihren Vor- und Nachnamen gemäß Art. 6 Abs. 1 lit. f. DSGVO auf Basis unseres berechtigten Interesses an der effizienten und zeitnahen Bereitstellung der gewünschten Informationen.
                        </p><p>Ihre Daten werden stets nur zur Beantwortung Ihres Anliegens per WhatsApp verwendet. Eine Weitergabe an Dritte findet nicht statt.
                        </p><p>Bitte beachten Sie, dass WhatsApp Business Zugriff auf das Adressbuch des von uns hierfür verwendeten mobilen Endgeräts erhält und im Adressbuch gespeicherte Telefonnummern automatisch an einen Server von Facebook in den USA überträgt.
                        </p><p>Für den Betrieb unseres WhatsApp-Business-Kontos verwenden wir ein mobiles Endgerät, in dessen Adressbuch ausschließlich die WhatsApp-Kontaktdaten solcher Nutzer gespeichert werden, die mit uns per WhatsApp auch in Kontakt getreten sind.
                        </p><p>Hierdurch wird sichergestellt, dass jeder in unserem Adressbuch gespeicherte WhatsApp-Kontakt bereits bei erstmaliger Nutzung der App auf seinem Gerät durch Akzeptanz der Whatsapp-Nutzungsbedingungen in die Übermittlung seiner WhatsApp-Telefonnummer aus den Adressbüchern seiner Chat-Kontakte gemäß Art. 6 Abs. 1 lit. a DSGVO eingewilligt hat.
                        </p><p>Eine Übermittlung von Daten solcher Nutzer, die Whatsapp nicht verwenden und/oder uns nicht über WhatsApp kontaktiert haben, wird insofern ausgeschlossen.
                        </p><p>Facebook Inc. mit Sitz in den USA ist für das us-europäische Datenschutzübereinkommen „Privacy Shield“ zertifiziert, welches die Einhaltung des in der EU geltenden Datenschutzniveaus gewährleistet.
                        </p><p>Zweck und Umfang der Datenerhebung und die weitere Verarbeitung und Nutzung der Daten durch Whatsapp sowie Ihre diesbezüglichen Rechte und Einstellungsmöglichkeiten zum Schutz Ihrer Privatsphäre entnehmen Sie bitte den Datenschutzhinweisen von WhatsApp:
                        </p><p><a href="https://www.whatsapp.com/legal/?eea=1#privacy-policy" target="_blank" rel="noopener">https://www.whatsapp.com/legal/?eea=1#privacy-policy</a>
                            </p><br />

                            <h6>5. Online-Terminvereinbarung</h6>
                            <p>Wir verarbeiten Ihre personenbezogenen Daten im Rahmen der zur Verfügung gestellten Online-Terminvereinbarung. Welche Daten wir zur Online-Terminvereinbarung erheben, können Sie aus dem jeweiligen Eingabeformular bzw. der Terminabfrage zur Terminvereinbarung ersehen. Sofern gewisse Daten notwendig sind, um eine Online-Terminvereinbarung durchführen zu können, machen wir diese im Eingabeformular bzw. bei der Terminabfrage entsprechend kenntlich. Sofern wir Ihnen ein Freitextfeld beim Eingabeformular zur Verfügung stellen, können Sie dort Ihr Anliegen näher beschreiben. Sie können dann auch selbst steuern, welche Daten Sie zusätzlich eintragen möchten.
                        </p><p>Ihre mitgeteilten Daten werden ausschließlich zum Zweck der Terminvereinbarung gespeichert und verwendet. Bei der Verarbeitung von personenbezogenen Daten, die zur Erfüllung eines Vertrages mit Ihnen erforderlich sind (dies gilt auch für Verarbeitungsvorgänge, die zur Durchführung vorvertraglicher Maßnahmen erforderlich sind), dient Art. 6 Abs. 1 lit. b DSGVO als Rechtsgrundlage. Haben Sie uns eine Einwilligung für die Verarbeitung Ihrer Daten erteilt, erfolgt die Verarbeitung auf Grundlage des Art. 6 Abs. 1 lit. a DSGVO. Eine erteilte Einwilligung kann jederzeit durch eine Nachricht an den zu Beginn dieser Erklärung genannten Verantwortlichen widerrufen werden.
                        </p><br />

                            <h6>6. Datenverarbeitung bei Eröffnung eines Kundenkontos und zur Vertragsabwicklung</h6>
                            <p>Gemäß Art. 6 Abs. 1 lit. b DSGVO werden personenbezogene Daten weiterhin erhoben und verarbeitet, wenn Sie uns diese zur Durchführung eines Vertrages oder bei der Eröffnung eines Kundenkontos mitteilen. Welche Daten erhoben werden, ist aus den jeweiligen Eingabeformularen ersichtlich. Eine Löschung Ihres Kundenkontos ist jederzeit möglich und kann durch eine Nachricht an die o.g. Adresse des Verantwortlichen erfolgen. Wir speichern und verwenden die von Ihnen mitgeteilten Daten zur Vertragsabwicklung. Nach vollständiger Abwicklung des Vertrages oder Löschung Ihres Kundenkontos werden Ihre Daten mit Rücksicht auf steuer- und handelsrechtliche Aufbewahrungsfristen gesperrt und nach Ablauf dieser Fristen gelöscht, sofern Sie nicht ausdrücklich in eine weitere Nutzung Ihrer Daten eingewilligt haben oder eine gesetzlich erlaubte weitere Datenverwendung von unserer Seite vorbehalten wurde, über die wir Sie nachstehend entsprechend informieren.</p>
                            <br />

                            <h6>7. Nutzung Ihrer Daten zur Direktwerbung</h6>
                            <p>7.1 Anmeldung zu unserem E-Mail-Newsletter
                        </p><p>Wenn Sie sich zu unserem E-Mail Newsletter anmelden, übersenden wir Ihnen regelmäßig Informationen zu unseren Angeboten. Pflichtangabe für die Übersendung des Newsletters ist allein Ihre E-Mail-Adresse. Die Angabe weiterer evtl. Daten ist freiwillig und wird verwendet, um Sie persönlich ansprechen zu können. Für den Versand des Newsletters verwenden wir das sog. Double Opt-in Verfahren. Dies bedeutet, dass wir Ihnen erst dann einen E-Mail Newsletter übermitteln werden, wenn Sie uns ausdrücklich bestätigt haben, dass Sie in den Versand von Newsletter einwilligen. Wir schicken Ihnen dann eine Bestätigungs-E-Mail, mit der Sie gebeten werden durch Anklicken eines entsprechenden Links zu bestätigen, dass Sie künftig Newsletter erhalten wollen.
                        </p><p>Mit der Aktivierung des Bestätigungslinks erteilen Sie uns Ihre Einwilligung für die Nutzung Ihrer personenbezogenen Daten gemäß Art. 6 Abs. 1 lit. a DSGVO. Bei der Anmeldung zum Newsletter speichern wir Ihre vom Internet Service-Provider (ISP) eingetragene IP-Adresse sowie das Datum und die Uhrzeit der Anmeldung, um einen möglichen Missbrauch Ihrer E-Mail-Adresse zu einem späteren Zeitpunkt nachvollziehen zu können. Die von uns bei der Anmeldung zum Newsletter erhobenen Daten werden ausschließlich für Zwecke der werblichen Ansprache im Wege des Newsletters benutzt. Sie können den Newsletter jederzeit über den dafür vorgesehenen Link im Newsletter oder durch entsprechende Nachricht an den eingangs genannten Verantwortlichen abbestellen. Nach erfolgter Abmeldung wird Ihre E-Mail-Adresse unverzüglich in unserem Newsletter-Verteiler gelöscht, soweit Sie nicht ausdrücklich in eine weitere Nutzung Ihrer Daten eingewilligt haben oder wir uns eine darüberhinausgehende Datenverwendung vorbehalten, die gesetzlich erlaubt ist und über die wir Sie in dieser Erklärung informieren.
                        </p><p>7.2 Versand des E-Mail-Newsletters an Bestandskunden
                        </p><p>Wenn Sie uns Ihre E-Mail-Adresse beim Kauf von Waren bzw. Dienstleistungen zur Verfügung gestellt haben, behalten wir uns vor, Ihnen regelmäßig Angebote zu ähnlichen Waren bzw. Dienstleistungen, wie den bereits gekauften, aus unserem Sortiment per E-Mail zuzusenden. Hierfür müssen wir keine gesonderte Einwilligung von Ihnen einholen. Die Datenverarbeitung erfolgt insoweit allein auf Basis unseres berechtigten Interesses an personalisierter Direktwerbung gemäß Art. 6 Abs. 1 lit. f DSGVO. Haben Sie der Nutzung Ihrer E-Mail-Adresse zu diesem Zweck anfänglich widersprochen, findet ein Mailversand unsererseits nicht statt. Sie sind berechtigt, der Nutzung Ihrer E-Mail-Adresse zu dem vorbezeichneten Werbezweck jederzeit mit Wirkung für die Zukunft durch eine Mitteilung an den zu Beginn genannten Verantwortlichen zu widersprechen. Hierfür fallen für Sie lediglich Übermittlungskosten nach den Basistarifen an. Nach Eingang Ihres Widerspruchs wird die Nutzung Ihrer E-Mail-Adresse zu Werbezwecken unverzüglich eingestellt.
                        </p><p>7.3 WhatsApp-Newsletter
                        </p><p>Wenn Sie sich zu unserem WhatsApp-Newsletter anmelden, übersenden wir Ihnen regelmäßig Informationen zu unseren Angeboten per WhatsApp. Pflichtangabe für die Übersendung des Newsletters ist allein Ihre Mobilfunknummer. Für den Versand des Newsletters nehmen Sie unsere mitgeteilte Mobilfunknummer in Ihre Adress-Konakte in Ihrem Mobilfunkgerät auf und senden uns die Nachricht „Start“ per WhatsApp. Mit der Übersendung dieser WhatsApp-Nachricht erteilen Sie uns Ihre Einwilligung für die Nutzung Ihrer personenbezogenen Daten gemäß Art. 6 Abs. 1 lit. a DSGVO zum Zwecke der Newsletteübersendung, wir nehmen Sie sodann in unseren Newsletter-Verteiler auf.
                        </p><p>Die von uns bei der Anmeldung zum Newsletter erhobenen Daten werden ausschließlich für Zwecke der werblichen Ansprache im Wege des Newsletters verarbeitet. Sie können sich jederzeit vom Newsletterabmelden, indem Sie uns die Nachricht „Stop“ per WhatsApp senden. Nach erfolgter Abmeldung wird Ihre E-Mail-Adresse unverzüglich in unserem Newsletter-Verteiler gelöscht, soweit Sie nicht ausdrücklich in eine weitere Nutzung Ihrer Daten eingewilligt haben oder wir uns eine darüberhinausgehende Datenverwendung vorbehalten, die gesetzlich erlaubt ist und über die wir Sie in dieser Erklärung informieren.
                        </p><p>7.4 Werbung per Briefpost
                        </p><p>Auf Grundlage unseres berechtigten Interesses an personalisierter Direktwerbung behalten wir uns vor, Ihren Vor- und Nachnamen, Ihre Postanschrift und - soweit wir diese zusätzlichen Angaben im Rahmen der Vertragsbeziehung von Ihnen erhalten haben - Ihren Titel, akademischen Grad, Ihr Geburtsjahr und Ihre Berufs-, Branchen- oder Geschäftsbezeichnung gemäß Art. 6 Abs. 1 lit. f DSGVO zu speichern und für die Zusendung von interessanten Angeboten und Informationen zu unseren Produkten per Briefpost zu nutzen.
                        </p><p>Sie können der Speicherung und Nutzung Ihrer Daten zu diesem Zweck jederzeit durch eine entsprechende Nachricht an den Verantwortlichen widersprechen.
                        </p>
                            <br />

                            <h6>8. Datenverarbeitung zur Bestellabwicklung</h6>
                            <p>8.1 Die von uns erhobenen personenbezogenen Daten werden im Rahmen der Vertragsabwicklung an das mit der Lieferung beauftragte Transportunternehmen weitergegeben, soweit dies zur Lieferung der Ware erforderlich ist. Ihre Zahlungsdaten geben wir im Rahmen der Zahlungsabwicklung an das beauftragte Kreditinstitut weiter, sofern dies für die Zahlungsabwicklung erforderlich ist. Sofern Zahlungsdienstleister eingesetzt werden, informieren wir hierüber nachstehend explizit. Die Rechtsgrundlage für die Weitergabe der Daten ist hierbei Art. 6 Abs. 1 lit. b DSGVO.
                        </p><p>8.2 Zur Erfüllung unserer vertraglichen Pflichten unseren Kunden gegenüber arbeiten wir mit externen Versandpartnern zusammen. Wir geben Ihren Namen sowie Ihre Lieferadresse ausschließlich zu Zwecken der Warenlieferung Art. 6 Abs. 1 lit. b DSGVO an einen von uns ausgewählten Versandpartner weiter.
                        </p><p>8.3 Verwendung von Paymentdienstleistern (Zahlungsdiensten)
                        </p><p>- Paypal
                        </p><p>Bei Zahlung via PayPal, Kreditkarte via PayPal, Lastschrift via PayPal oder – falls angeboten - "Kauf auf Rechnung" oder „Ratenzahlung“ via PayPal geben wir Ihre Zahlungsdaten im Rahmen der Zahlungsabwicklung an die PayPal (Europe) S.a.r.l. et Cie, S.C.A., 22-24 Boulevard Royal, L-2449 Luxembourg (nachfolgend "PayPal"), weiter. Die Weitergabe erfolgt gemäß Art. 6 Abs. 1 lit. b DSGVO und nur insoweit, als dies für die Zahlungsabwicklung erforderlich ist.
                        </p><p>PayPal behält sich für die Zahlungsmethoden Kreditkarte via PayPal, Lastschrift via PayPal oder – falls angeboten - "Kauf auf Rechnung" oder „Ratenzahlung“ via PayPal die Durchführung einer Bonitätsauskunft vor. Hierfür werden Ihre Zahlungsdaten gegebenenfalls gemäß Art. 6 Abs. 1 lit. f DSGVO auf Basis des berechtigten Interesses von PayPal an der Feststellung Ihrer Zahlungsfähigkeit an Auskunfteien weitergegeben. Das Ergebnis der Bonitätsprüfung in Bezug
                            auf die statistische Zahlungsausfallwahrscheinlichkeit verwendet PayPal zum Zwecke der Entscheidung über die Bereitstellung der jeweiligen Zahlungsmethode. Die Bonitätsauskunft kann Wahrscheinlichkeitswerte enthalten (sog. Score-Werte). Soweit Score-Werte in das Ergebnis der Bonitätsauskunft einfließen, haben diese ihre Grundlage in einem wissenschaftlich anerkannten mathematisch-statistischen Verfahren. In die Berechnung der Score-Werte fließen unter anderem,
                            aber nicht ausschließlich, Anschriftendaten ein. Weitere datenschutzrechtliche Informationen, unter anderem zu den verwendeten Auskunfteien, entnehmen Sie bitte der Datenschutzerklärung von PayPal: <a href="https://www.paypal.com/de/webapps/mpp/ua/privacy-full" target="_blank" rel="noopener">https://www.paypal.com/de/webapps/mpp/ua/privacy-full</a>
                            </p><p>Sie können dieser Verarbeitung Ihrer Daten jederzeit durch eine Nachricht an PayPal widersprechen. Jedoch bleibt PayPal ggf. weiterhin berechtigt, Ihre personenbezogenen Daten zu verarbeiten, sofern dies zur vertragsgemäßen Zahlungsabwicklung erforderlich ist.
                        </p><br />

                            <h6>9. Webanalysedienste</h6>
                            <p>Google (Universal) Analytics
                        </p><p>- Google Analytics
                        </p><p>Diese Website benutzt Google Analytics, einen Webanalysedienst der Google LLC, 1600 Amphitheatre Parkway, Mountain View, CA 94043, USA ("Google"). Google Analytics verwendet sog. "Cookies", Textdateien, die auf Ihrem Computer gespeichert werden und die eine Analyse der Benutzung der Website durch Sie ermöglichen. Die durch das Cookie erzeugten Informationen über Ihre Benutzung dieser Website (einschließlich der gekürzten IP-Adresse) werden in der Regel an einen Server von Google in den USA übertragen und dort gespeichert.
                        </p><p>Diese Website verwendet Google Analytics ausschließlich mit der Erweiterung "_anonymizeIp()", die eine Anonymisierung der IP-Adresse durch Kürzung sicherstellt und eine direkte Personenbeziehbarkeit ausschließt. Durch die Erweiterung wird Ihre IP-Adresse von Google innerhalb von Mitgliedstaaten der Europäischen Union oder in anderen Vertragsstaaten des Abkommens über den Europäischen Wirtschaftsraum zuvor gekürzt. Nur in Ausnahmefällen wird die volle IP-Adresse an einen Server von Google in den USA übertragen und dort gekürzt. In diesen Ausnahmefällen erfolgt diese Verarbeitung gemäß Art. 6 Abs. 1 lit. f DSGVO auf Grundlage unseres berechtigten Interesses an der statistischen Analyse des Nutzerverhaltens zu Optimierungs- und Marketingzwecken.
                        </p><p>In unserem Auftrag wird Google diese Informationen benutzen, um Ihre Nutzung der Website auszuwerten, um Reports über die Websiteaktivitäten zusammenzustellen und um weitere mit der Websitenutzung und der Internetnutzung verbundene Dienstleistungen uns gegenüber zu erbringen. Die im Rahmen von Google Analytics von Ihrem Browser übermittelte IP-Adresse wird nicht mit anderen Daten von Google zusammengeführt.
                        </p><p>Sie können die Speicherung der Cookies durch eine entsprechende Einstellung Ihrer Browser-Software verhindern; wir weisen Sie jedoch darauf hin, dass Sie in diesem Fall gegebenenfalls nicht sämtliche Funktionen dieser Website vollumfänglich werden nutzen können. Sie können darüber hinaus die Erfassung der durch das Cookie erzeugten und auf Ihre Nutzung der Website bezogenen Daten (inkl. Ihrer IP-Adresse) an Google sowie die Verarbeitung dieser Daten durch Google verhindern, indem Sie das unter dem folgenden Link verfügbare Browser-Plugin herunterladen und installieren:
                        </p><p><a href="https://tools.google.com/dlpage/gaoptout?hl=de" target="_blank" rel="noopener">https://tools.google.com/dlpage/gaoptout?hl=de</a>
                            </p><p>Alternativ zum Browser-Plugin oder innerhalb von Browsern auf mobilen Geräten klicken Sie bitte auf den folgenden Link, um ein Opt-Out-Cookie zu setzen, der die Erfassung durch Google Analytics innerhalb dieser Website zukünftig verhindert (dieses Opt-Out-Cookie funktioniert nur in diesem Browser und nur für diese Domain, löschen Sie Ihre Cookies in diesem Browser, müssen Sie diesen Link erneut klicken): <a onclick="alert('Google Analytics wurde deaktiviert');" href="javascript:gaOptout()">Google Analytics deaktivieren</a>
                            </p><p>Google LLC mit Sitz in den USA ist für das us-europäische Datenschutzübereinkommen „Privacy Shield“ zertifiziert, welches die Einhaltung des in der EU geltenden Datenschutzniveaus gewährleistet.
                        </p><p>Mehr Informationen zum Umgang mit Nutzerdaten bei Google Analytics finden Sie in der Datenschutzerklärung von Google: <a href="https://support.google.com/analytics/answer/6004245?hl=de" target="_blank" rel="noopener">https://support.google.com/analytics/answer/6004245?hl=de</a>
                            </p><p>- Google Universal Analytics
                        </p><p>Diese Website benutzt Google Analytics, einen Webanalysedienst der Google LLC, 1600 Amphitheatre Parkway, Mountain View, CA 94043, USA ("Google"). Google Analytics verwendet sog. "Cookies", Textdateien, die auf Ihrem Computer gespeichert werden und die eine Analyse der Benutzung der Website durch Sie ermöglichen. Die durch das Cookie erzeugten Informationen über Ihre Benutzung dieser Website (einschließlich der gekürzten IP-Adresse) werden in der Regel an einen Server von Google in den USA übertragen und dort gespeichert.
                        </p><p>Diese Website verwendet Google Analytics ausschließlich mit der Erweiterung "_anonymizeIp()", die eine Anonymisierung der IP-Adresse durch Kürzung sicherstellt und eine direkte Personenbeziehbarkeit ausschließt. Durch die Erweiterung wird Ihre IP-Adresse von Google innerhalb von Mitgliedstaaten der Europäischen Union oder in anderen Vertragsstaaten des Abkommens über den Europäischen Wirtschaftsraum zuvor gekürzt. Nur in Ausnahmefällen wird die volle IP-Adresse an einen Server von Google in den USA übertragen und dort gekürzt. In diesen Ausnahmefällen erfolgt diese Verarbeitung gemäß Art. 6 Abs. 1 lit. f DSGVO auf Grundlage unseres berechtigten Interesses an der statistischen Analyse des Nutzerverhaltens zu Optimierungs- und Marketingzwecken.
                        </p><p>In unserem Auftrag wird Google diese Informationen benutzen, um Ihre Nutzung der Website auszuwerten, um Reports über die Websiteaktivitäten zusammenzustellen und um weitere mit der Websitenutzung und der Internetnutzung verbundene Dienstleistungen uns gegenüber zu erbringen. Die im Rahmen von Google Analytics von Ihrem Browser übermittelte IP-Adresse wird nicht mit anderen Daten von Google zusammengeführt.
                        </p><p>Sie können die Speicherung der Cookies durch eine entsprechende Einstellung Ihrer Browser-Software verhindern; wir weisen Sie jedoch darauf hin, dass Sie in diesem Fall gegebenenfalls nicht sämtliche Funktionen dieser Website vollumfänglich werden nutzen können. Sie können darüber hinaus die Erfassung der durch das Cookie erzeugten und auf Ihre Nutzung der Website bezogenen Daten (inkl. Ihrer IP-Adresse) an Google sowie die Verarbeitung dieser Daten durch Google verhindern, indem Sie das unter dem folgenden Link verfügbare Browser-Plugin herunterladen und installieren:
                        </p><p><a href="https://tools.google.com/dlpage/gaoptout?hl=de" target="_blank" rel="noopener">https://tools.google.com/dlpage/gaoptout?hl=de</a>
                            </p><p>Alternativ zum Browser-Plugin oder innerhalb von Browsern auf mobilen Geräten klicken Sie bitte auf den folgenden Link, um ein Opt-Out-Cookie zu setzen, der die Erfassung durch Google Analytics innerhalb dieser Website zukünftig verhindert (dieses Opt-Out-Cookie funktioniert nur in diesem Browser und nur für diese Domain, löschen Sie Ihre Cookies in diesem Browser, müssen Sie diesen Link erneut klicken): <a onclick="alert('Google Analytics wurde deaktiviert');" href="javascript:gaOptout()">Google Analytics deaktivieren</a>
                            </p><p>Google LLC mit Sitz in den USA ist für das us-europäische Datenschutzübereinkommen „Privacy Shield“ zertifiziert, welches die Einhaltung des in der EU geltenden Datenschutzniveaus gewährleistet.
                        </p><p>Diese Website verwendet Google Analytics zudem für eine geräteübergreifende Analyse von Besucherströmen, die über eine User-ID durchgeführt wird. Beim erstmaligen Aufrufen einer Seite wird dem Nutzer eine eindeutige, dauerhafte und anonymisierte ID zugeteilt, die geräteübergreifend gesetzt wird. Dies ermöglicht es, Interaktionsdaten von verschiedenen Geräten und aus unterschiedlichen Sitzungen einem einzelnen Nutzer zuzuordnen. Die User-ID enthält keine personenbezogenen Daten und übermittelt solche auch nicht an Google.
                        </p><p>Der Datenerhebung und -speicherung über die User-ID kann jederzeit mit Wirkung für die Zukunft widersprochen werden. Hierfür müssen Sie Google Analytics auf allen Systemen deaktivieren, die Sie nutzen, beispielsweise in einem anderen Browser oder auf Ihrem mobilen Endgerät.
                        </p><p>Die Deaktivierung können Sie mithilfe eines Browser-Plugins von Google <a href="https://tools.google.com/dlpage/gaoptout?hl=de" target="_blank" rel="noopener">https://tools.google.com/dlpage/gaoptout?hl=de</a> vornehmen. Alternativ zum Browser-Plugin oder innerhalb von Browsern auf mobilen Geräten klicken Sie bitte auf den folgenden Link, um ein Opt-Out-Cookie zu setzen, der die Erfassung durch Google Analytics innerhalb dieser Website zukünftig verhindert (dieses Opt-Out-Cookie funktioniert nur in diesem Browser und nur für diese Domain, löschen Sie Ihre Cookies in diesem Browser, müssen Sie diesen Link erneut klicken): <a onclick="alert('Google Analytics wurde deaktiviert');" href="javascript:gaOptout()">Google Analytics deaktivieren</a>
                            </p><p>Weitere Hinweise zu Universal Analytics finden Sie hier: <a href="https://support.google.com/analytics/answer/2838718?hl=de&ref_topic=6010376" target="_blank" rel="noopener">https://support.google.com/analytics/answer/2838718?hl=de&ref_topic=6010376</a>
                            </p><br />

                            <h6>10. Tools und Sonstiges</h6>
                            <p>10.1 Google Kundenrezensionen (ehemals Google Zertifizierter-Händler-Programm)
                        </p><p>Wir arbeiten mit Google LLC im Rahmen des Programms „Google Kundenrezensionen“ zusammen, der Anbieter ist die Google LLC., 1600 Amphitheatre Parkway, Mountain View, CA 94043, USA (“Google”). Das Programm gibt uns die Möglichkeit Kundenrezensionen von Nutzern unserer Website einzuholen. Hierbei werden Sie nach einem Einkauf auf unserer Website gefragt, ob Sie an einer E-Mail-Umfrage von Google teilnehmen möchten. Wenn Sie Ihre Einwilligung gemäß Art. 6 Abs. 1 lit. a DSGVO erteilen, übermitteln wir Ihre E-Mail-Adresse an Google. Sie erhalten eine E-Mail von Google Kundenrezensionen, in der Sie gebeten werden, die Kauferfahrung auf unserer Website zu bewerten. Die von Ihnen abgegebene Bewertung wird anschließend mit unseren anderen Bewertungen zusammengefasst und in unserem Logo von Google Kundenrezensionen sowie in unserem Merchant Center-Dashboard angezeigt, außerdem wird diese für Google Verkäuferbewertungen genutzt.
                        </p><p>Sie können Ihre Einwilligung jederzeit durch eine Nachricht an den für die Datenverarbeitung Verantwortlichen oder gegenüber Google widerrufen.
                        </p><p>Google LLC mit Sitz in den USA ist für das us-europäische Datenschutzübereinkommen „Privacy Shield“ zertifiziert, welches die Einhaltung des in der EU geltenden Datenschutzniveaus gewährleistet.
                        </p><p>Weitere Informationen zum Datenschutz von Google im Zusammenhang mit dem Programm Google Kundenrezensionen können Sie unter nachstehendem Link abrufen: <a href="https://support.google.com/merchants/answer/7188525?hl=de" target="_blank" rel="noopener">https://support.google.com/merchants/answer/7188525?hl=de</a>
                            </p><p>Weitere Informationen zum Datenschutz von Google Verkäuferbewertungen können Sie unter diesem Link nachlesen: <a href="https://support.google.com/adwords/answer/2375474" target="_blank" rel="noopener">https://support.google.com/adwords/answer/2375474</a>
                            </p><p>10.2 Google Maps
                        </p><p>Auf unserer Website verwenden wir Google Maps (API) von Google LLC., 1600 Amphitheatre Parkway, Mountain View, CA 94043, USA (“Google”). Google Maps ist ein Webdienst zur Darstellung von interaktiven (Land-)Karten, um geographische Informationen visuell darzustellen. Über die Nutzung dieses Dienstes wird Ihnen unser Standort angezeigt und eine etwaige Anfahrt erleichtert.
                        </p><p>Bereits beim Aufrufen derjenigen Unterseiten, in die die Karte von Google Maps eingebunden ist, werden Informationen über Ihre Nutzung unserer Website (wie z.B. Ihre IP-Adresse) an Server von Google in den USA übertragen und dort gespeichert. Dies erfolgt unabhängig davon, ob Google ein Nutzerkonto bereitstellt, über das Sie eingeloggt sind, oder ob kein Nutzerkonto besteht. Wenn Sie bei Google eingeloggt sind, werden Ihre Daten direkt Ihrem Konto zugeordnet. Wenn Sie die Zuordnung mit Ihrem Profil bei Google nicht wünschen, müssen Sie sich vor Aktivierung des Buttons ausloggen. Google speichert Ihre Daten (selbst für nicht eingeloggte Nutzer) als Nutzungsprofile und wertet diese aus. Die Erhebung, Speicherung und die Auswertung erfolgen gemäß Art. 6 Abs. 1 lit.f DSGVO auf Basis der berechtigten Interessen von Google an der Einblendung personalisierter Werbung, Marktforschung und/oder der bedarfsgerechten Gestaltung von Google-Websites. Ihnen steht ein Widerspruchsrecht gegen die Bildung dieser Nutzerprofile zu, wobei Sie sich für dessen Ausübung an Google wenden müssen.
                        </p><p>Google LLC mit Sitz in den USA ist für das us-europäische Datenschutzübereinkommen „Privacy Shield“ zertifiziert, welches die Einhaltung des in der EU geltenden Datenschutzniveaus gewährleistet.
                        </p><p>Wenn Sie mit der künftigen Übermittlung Ihrer Daten an Google im Rahmen der Nutzung von Google Maps nicht einverstanden sind, besteht auch die Möglichkeit, den Webdienst von Google Maps vollständig zu deaktivieren, indem Sie die Anwendung JavaScript in Ihrem Browser ausschalten. Google Maps und damit auch die Kartenanzeige auf dieser Internetseite kann dann nicht genutzt werden.
                        </p><p>Die Nutzungsbedingungen von Google können Sie unter <a href="https://www.google.de/intl/de/policies/terms/regional.html" target="_blank" rel="noopener">https://www.google.de/intl/de/policies/terms/regional.html</a> einsehen, die zusätzlichen Nutzungsbedingungen für Google Maps finden Sie unter <a href="https://www.google.com/intl/de_US/help/terms_maps.html" target="_blank" rel="noopener">https://www.google.com/intl/de_US/help/terms_maps.html</a>
                            </p><p>Ausführliche Informationen zum Datenschutz im Zusammenhang mit der Verwendung von Google Maps finden Sie auf der Internetseite von Google („Google Privacy Policy“): <a href="https://www.google.de/intl/de/policies/privacy/" target="_blank" rel="noopener">https://www.google.de/intl/de/policies/privacy/</a>
                            </p><p>10.3 Google Web Fonts
                        </p><p>Diese Seite nutzt zur einheitlichen Darstellung von Schriftarten so genannte Web Fonts die von der Google LLC., 1600 Amphitheatre Parkway, Mountain View, CA 94043, USA („Google“) bereitgestellt werden. Beim Aufruf einer Seite lädt Ihr Browser die benötigten Web Fonts in ihren Browser-Cache, um Texte und Schriftarten korrekt anzuzeigen.
                        </p><p>Zu diesem Zweck muss der von Ihnen verwendete Browser Verbindung zu den Servern von Google aufnehmen. Hierdurch erlangt Google Kenntnis darüber, dass über Ihre IP-Adresse unsere Website aufgerufen wurde. Die Nutzung von Google Web Fonts erfolgt im Interesse einer einheitlichen und ansprechenden Darstellung unserer Online-Angebote. Dies stellt ein berechtigtes Interesse im Sinne von Art. 6 Abs. 1 lit. f DSGVO dar. Wenn Ihr Browser Web Fonts nicht unterstützt, wird eine Standardschrift von Ihrem Computer genutzt.
                        </p><p>Google LLC mit Sitz in den USA ist für das us-europäische Datenschutzübereinkommen „Privacy Shield“ zertifiziert, welches die Einhaltung des in der EU geltenden Datenschutzniveaus gewährleistet.
                        </p><p>Weitere Informationen zu Google Web Fonts finden Sie unter <a href="https://developers.google.com/fonts/faq" target="_blank" rel="noopener">https://developers.google.com/fonts/faq</a> und in der Datenschutzerklärung von Google: <a href="https://www.google.com/policies/privacy/" target="_blank" rel="noopener">https://www.google.com/policies/privacy/</a>
                            </p><br />



                            <h6>11. Rechte des Betroffenen</h6>
                            <p>11.1 Das geltende Datenschutzrecht gewährt Ihnen gegenüber dem Verantwortlichen hinsichtlich der Verarbeitung Ihrer personenbezogenen Daten umfassende Betroffenenrechte (Auskunfts- und Interventionsrechte), über die wir Sie nachstehend informieren:
                        </p><ul>
                                <li><p>- Auskunftsrecht gemäß Art. 15 DSGVO: Sie haben insbesondere ein Recht auf Auskunft über Ihre von uns verarbeiteten personenbezogenen Daten, die Verarbeitungszwecke, die Kategorien der verarbeiteten personenbezogenen Daten, die Empfänger oder Kategorien von Empfängern, gegenüber denen Ihre Daten offengelegt wurden oder werden, die geplante Speicherdauer bzw. die Kriterien für die Festlegung der Speicherdauer, das Bestehen eines Rechts auf Berichtigung, Löschung, Einschränkung der Verarbeitung, Widerspruch gegen die Verarbeitung, Beschwerde bei einer Aufsichtsbehörde, die Herkunft Ihrer Daten, wenn diese nicht durch uns bei Ihnen erhoben wurden, das Bestehen einer automatisierten Entscheidungsfindung einschließlich Profiling und ggf. aussagekräftige Informationen über die involvierte Logik und die Sie betreffende Tragweite und die angestrebten Auswirkungen einer solchen Verarbeitung, sowie Ihr Recht auf Unterrichtung, welche Garantien gemäß Art. 46 DSGVO bei Weiterleitung Ihrer Daten in Drittländer bestehen;
                        </p></li><li><p>- Recht auf Berichtigung gemäß Art. 16 DSGVO: Sie haben ein Recht auf unverzügliche Berichtigung Sie betreffender unrichtiger Daten und/oder Vervollständigung Ihrer bei uns gespeicherten unvollständigen Daten;
                        </p></li><li><p>- Recht auf Löschung gemäß Art. 17 DSGVO: Sie haben das Recht, die Löschung Ihrer personenbezogenen Daten bei Vorliegen der Voraussetzungen des Art. 17 Abs. 1 DSGVO zu verlangen. Dieses Recht besteht jedoch insbesondere dann nicht, wenn die Verarbeitung zur Ausübung des Rechts auf freie Meinungsäußerung und Information, zur Erfüllung einer rechtlichen Verpflichtung, aus Gründen des öffentlichen Interesses oder zur Geltendmachung, Ausübung oder Verteidigung von Rechtsansprüchen erforderlich ist;
                        </p></li><li><p>- Recht auf Einschränkung der Verarbeitung gemäß Art. 18 DSGVO: Sie haben das Recht, die Einschränkung der Verarbeitung Ihrer personenbezogenen Daten zu verlangen, solange die von Ihnen bestrittene Richtigkeit Ihrer Daten überprüft wird, wenn Sie eine Löschung Ihrer Daten wegen unzulässiger Datenverarbeitung ablehnen und stattdessen die Einschränkung der Verarbeitung Ihrer Daten verlangen, wenn Sie Ihre Daten zur Geltendmachung, Ausübung oder Verteidigung von Rechtsansprüchen benötigen, nachdem wir diese Daten nach Zweckerreichung nicht mehr benötigen oder wenn Sie Widerspruch aus Gründen Ihrer besonderen Situation eingelegt haben, solange noch nicht feststeht, ob unsere berechtigten Gründe überwiegen;
                        </p></li><li><p>- Recht auf Unterrichtung gemäß Art. 19 DSGVO: Haben Sie das Recht auf Berichtigung, Löschung oder Einschränkung der Verarbeitung gegenüber dem Verantwortlichen geltend gemacht, ist dieser verpflichtet, allen Empfängern, denen die Sie betreffenden personenbezogenen Daten offengelegt wurden, diese Berichtigung oder Löschung der Daten oder Einschränkung der Verarbeitung mitzuteilen, es sei denn, dies erweist sich als unmöglich oder ist mit einem unverhältnismäßigen Aufwand verbunden. Ihnen steht das Recht zu, über diese Empfänger unterrichtet zu werden.
                        </p></li><li><p>- Recht auf Datenübertragbarkeit gemäß Art. 20 DSGVO: Sie haben das Recht, Ihre personenbezogenen Daten, die Sie uns bereitgestellt haben, in einem strukturierten, gängigen und maschinenlesebaren Format zu erhalten oder die Übermittlung an einen anderen Verantwortlichen zu verlangen, soweit dies technisch machbar ist;
                        </p></li><li><p>- Recht auf Widerruf erteilter Einwilligungen gemäß Art. 7 Abs. 3 DSGVO: Sie haben das Recht, eine einmal erteilte Einwilligung in die Verarbeitung von Daten jederzeit mit Wirkung für die Zukunft zu widerrufen. Im Falle des Widerrufs werden wir die betroffenen Daten unverzüglich löschen, sofern eine weitere Verarbeitung nicht auf eine Rechtsgrundlage zur einwilligungslosen Verarbeitung gestützt werden kann. Durch den Widerruf der Einwilligung wird die Rechtmäßigkeit der aufgrund der Einwilligung bis zum Widerruf erfolgten Verarbeitung nicht berührt;
                        </p></li><li><p>- Recht auf Beschwerde gemäß Art. 77 DSGVO: Wenn Sie der Ansicht sind, dass die Verarbeitung der Sie betreffenden personenbezogenen Daten gegen die DSGVO verstößt, haben Sie - unbeschadet eines anderweitigen verwaltungsrechtlichen oder gerichtlichen Rechtsbehelfs - das Recht auf Beschwerde bei einer Aufsichtsbehörde, insbesondere in dem Mitgliedstaat Ihres Aufenthaltsortes, Ihres Arbeitsplatzes oder des Ortes des mutmaßlichen Verstoßes.
                        </p></li></ul>
                            <p>11.2 WIDERSPRUCHSRECHT
                        </p><p>WENN WIR IM RAHMEN EINER INTERESSENABWÄGUNG IHRE PERSONENBEZOGENEN DATEN AUFGRUND UNSERES ÜBERWIEGENDEN BERECHTIGTEN INTERESSES VERARBEITEN, HABEN SIE DAS JEDERZEITIGE RECHT, AUS GRÜNDEN, DIE SICH AUS IHRER BESONDEREN SITUATION ERGEBEN, GEGEN DIESE VERARBEITUNG WIDERSPRUCH MIT WIRKUNG FÜR DIE ZUKUNFT EINZULEGEN.
                        </p><p>MACHEN SIE VON IHREM WIDERSPRUCHSRECHT GEBRAUCH, BEENDEN WIR DIE VERARBEITUNG DER BETROFFENEN DATEN. EINE WEITERVERARBEITUNG BLEIBT ABER VORBEHALTEN, WENN WIR ZWINGENDE SCHUTZWÜRDIGE GRÜNDE FÜR DIE VERARBEITUNG NACHWEISEN KÖNNEN, DIE IHRE INTERESSEN, GRUNDRECHTE UND GRUNDFREIHEITEN ÜBERWIEGEN, ODER WENN DIE VERARBEITUNG DER GELTENDMACHUNG, AUSÜBUNG ODER VERTEIDIGUNG VON RECHTSANSPRÜCHEN DIENT.
                        </p><p>WERDEN IHRE PERSONENBEZOGENEN DATEN VON UNS VERARBEITET, UM DIREKTWERBUNG ZU BETREIBEN, HABEN SIE DAS RECHT, JEDERZEIT WIDERSPRUCH GEGEN DIE VERARBEITUNG SIE BETREFFENDER PERSONENBEZOGENER DATEN ZUM ZWECKE DERARTIGER WERBUNG EINZULEGEN. SIE KÖNNEN DEN WIDERSPRUCH WIE OBEN BESCHRIEBEN AUSÜBEN.
                        </p><p>MACHEN SIE VON IHREM WIDERSPRUCHSRECHT GEBRAUCH, BEENDEN WIR DIE VERARBEITUNG DER BETROFFENEN DATEN ZU DIREKTWERBEZWECKEN.
                        </p><br />

                            <h6>12. Dauer der Speicherung personenbezogener Daten</h6>
                            <p>Die Dauer der Speicherung von personenbezogenen Daten bemisst sich anhand der jeweiligen gesetzlichen Aufbewahrungsfrist (z.B. handels- und steuerrechtliche Aufbewahrungsfristen). Nach Ablauf der Frist werden die entsprechenden Daten routinemäßig gelöscht, sofern sie nicht mehr zur Vertragserfüllung oder Vertragsanbahnung erforderlich sind und/oder unsererseits kein berechtigtes Interesse an der Weiterspeicherung fortbesteht.
                        </p><br />

                        </div>
                        <div class="modal-footer">
                            <a class="modal-action modal-close waves-effect btn-flat close-button">OK</a>
                        </div>
                    </div>

                    <div id="agb" className="modal modal-fixed-footer termsModal">
                        <div className="modal-content">
                            <p><h4><center>Allgemeine Geschäftsbedingungen mit Kundeninformationen</center></h4></p>
                            <br />
                            <h5>Inhaltsverzeichnis</h5>
                            <p><h6>
                                1. Geltungsbereich <br />
                        2. Vertragsschluss <br />
                        3. Widerrufsrecht <br />
                        4. Preise und Zahlungsbedingungen <br />
                        5. Liefer- und Versandbedingungen <br />
                        6. Eigentumsvorbehalt <br />
                        7. Mängelhaftung (Gewährleistung) <br />
                        8. Besondere Bedingungen für die Verarbeitung von Waren nach bestimmten Vorgaben des Kunden <br />
                        9. Einlösung von Aktionsgutscheinen <br />
                        10. Einlösung von Geschenkgutscheinen <br />
                        11. Anwendbares Recht <br />
                        12. Gerichtsstand <br />
                        13. Alternative Streitbeilegung <br />
                        14. Widerrufsbelehrung & Widerrufsformular<br />
                            </h6></p><br />

                            <h5>§ 1. Geltungsbereich </h5>
                            <p>1.1 Diese Allgemeinen Geschäftsbedingungen (nachfolgend "AGB") der Ince GmbH (nachfolgend "Verkäufer"), gelten für alle Verträge über die Lieferung von Waren, die ein Verbraucher oder Unternehmer (nachfolgend „Kunde“) mit dem Verkäufer hinsichtlich der vom Verkäufer in seinem Online-Shop dargestellten Waren abschließt. Hiermit wird der Einbeziehung von eigenen Bedingungen des Kunden widersprochen, es sei denn, es ist etwas anderes vereinbart.
                        </p><p>1.2 Für Verträge über die Lieferung von Gutscheinen gelten diese AGB entsprechend, sofern insoweit nicht ausdrücklich etwas Abweichendes geregelt ist.
                        </p><p>1.3 Verbraucher im Sinne dieser AGB ist jede natürliche Person, die ein Rechtsgeschäft zu Zwecken abschließt, die überwiegend weder ihrer gewerblichen noch ihrer selbständigen beruflichen Tätigkeit zugerechnet werden können. Unternehmer im Sinne dieser AGB ist eine natürliche oder juristische Person oder eine rechtsfähige Personengesellschaft, die bei Abschluss eines Rechtsgeschäfts in Ausübung ihrer gewerblichen oder selbständigen beruflichen Tätigkeit handelt.
                        </p><br />

                            <h5>§ 2. Vertragsschluss </h5>
                            <p>2.1 Die im Online-Shop des Verkäufers enthaltenen Produktbeschreibungen stellen keine verbindlichen Angebote seitens des Verkäufers dar, sondern dienen zur Abgabe eines verbindlichen Angebots durch den Kunden.
                        </p><p>2.2 Der Kunde kann das Angebot über das in den Online-Shop des Verkäufers integrierte Online-Bestellformular abgeben. Dabei gibt der Kunde, nachdem er die ausgewählten Waren in den virtuellen Warenkorb gelegt und den elektronischen Bestellprozess durchlaufen hat, durch Klicken des den Bestellvorgang abschließenden Buttons ein rechtlich verbindliches Vertragsangebot in Bezug auf die im Warenkorb enthaltenen Waren ab.
                        </p><p>2.3 Der Verkäufer kann das Angebot des Kunden innerhalb von fünf Tagen annehmen,
                        </p><p>- indem er dem Kunden eine schriftliche Auftragsbestätigung oder eine Auftragsbestätigung in Textform (Fax oder E-Mail) übermittelt, wobei insoweit der Zugang der Auftragsbestätigung beim Kunden maßgeblich ist, oder
                        </p><p>- indem er dem Kunden die bestellte Ware liefert, wobei insoweit der Zugang der Ware beim Kunden maßgeblich ist, oder
                        </p><p>- indem er den Kunden nach Abgabe von dessen Bestellung zur Zahlung auffordert.
                            Liegen mehrere der vorgenannten Alternativen vor, kommt der Vertrag in dem Zeitpunkt zustande, in dem eine der vorgenannten Alternativen zuerst eintritt. Die Frist zur Annahme des Angebots beginnt am Tag nach der Absendung des Angebots durch den Kunden zu laufen und endet mit dem Ablauf des fünften Tages, welcher auf die Absendung des Angebots folgt. Nimmt der Verkäufer das Angebot des Kunden innerhalb vorgenannter Frist nicht an, so gilt dies als Ablehnung des Angebots mit der Folge, dass der Kunde nicht mehr an seine Willenserklärung gebunden ist.
                        </p><p>2.4 Bei der Abgabe eines Angebots über das Online-Bestellformular des Verkäufers wird der Vertragstext nach dem Vertragsschluss vom Verkäufer gespeichert und dem Kunden nach Absendung von dessen Bestellung in Textform (z. B. E-Mail, Fax oder Brief) übermittelt. Eine darüber hinausgehende Zugänglichmachung des Vertragstextes durch den Verkäufer erfolgt nicht.
                        </p><p>2.5 Vor verbindlicher Abgabe der Bestellung über das Online-Bestellformular des Verkäufers kann der Kunde mögliche Eingabefehler durch aufmerksames Lesen der auf dem Bildschirm dargestellten Informationen erkennen. Ein wirksames technisches Mittel zur besseren Erkennung von Eingabefehlern kann dabei die Vergrößerungsfunktion des Browsers sein, mit deren Hilfe die Darstellung auf dem Bildschirm vergrößert wird. Seine Eingaben kann der Kunde im Rahmen des elektronischen Bestellprozesses so lange über die üblichen Tastatur- und Mausfunktionen korrigieren, bis er den den Bestellvorgang abschließenden Button anklickt.
                        </p><p>2.6 Für den Vertragsschluss stehen die deutsche und die englische Sprache zur Verfügung.
                        </p><p>2.7 Die Bestellabwicklung und Kontaktaufnahme finden in der Regel per E-Mail und automatisierter Bestellabwicklung statt. Der Kunde hat sicherzustellen, dass die von ihm zur Bestellabwicklung angegebene E-Mail-Adresse zutreffend ist, so dass unter dieser Adresse die vom Verkäufer versandten E-Mails empfangen werden können. Insbesondere hat der Kunde bei dem Einsatz von SPAM-Filtern sicherzustellen, dass alle vom Verkäufer oder von diesem mit der Bestellabwicklung beauftragten Dritten versandten E-Mails zugestellt werden können.
                        </p><br />

                            <h5>§ 3. Widerrufsrecht </h5>
                            <p>3.1 Verbrauchern steht grundsätzlich ein Widerrufsrecht zu.
                        </p><p>3.2 Nähere Informationen zum Widerrufsrecht ergeben sich aus der Widerrufsbelehrung §14  des Verkäufers.</p><br />

                            <h5>§ 4. Preise und Zahlungsbedingungen</h5>
                            <p>4.1 Sofern sich aus der Produktbeschreibung des Verkäufers nichts anderes ergibt, handelt es sich bei den angegebenen Preisen um Gesamtpreise, die die gesetzliche Umsatzsteuer enthalten. Gegebenenfalls zusätzlich anfallende Liefer- und Versandkosten werden in der jeweiligen Produktbeschreibung gesondert angegeben.
                        </p><p>4.2 Bei Lieferungen in Länder außerhalb der Europäischen Union können im Einzelfall weitere Kosten anfallen, die der Verkäufer nicht zu vertreten hat und die vom Kunden zu tragen sind. Hierzu zählen beispielsweise Kosten für die Geldübermittlung durch Kreditinstitute (z.B. Überweisungsgebühren, Wechselkursgebühren) oder einfuhrrechtliche Abgaben bzw. Steuern (z.B. Zölle). Solche Kosten können in Bezug auf die Geldübermittlung auch dann anfallen, wenn die Lieferung nicht in ein Land außerhalb der Europäischen Union erfolgt, der Kunde die Zahlung aber von einem Land außerhalb der Europäischen Union aus vornimmt.
                        </p><p>4.3 Die Zahlungsmöglichkeit/en wird/werden dem Kunden im Online-Shop des Verkäufers mitgeteilt.
                        </p><p>4.4 Ist Vorauskasse per Banküberweisung vereinbart, ist die Zahlung sofort nach Vertragsabschluss fällig, sofern die Parteien keinen späteren Fälligkeitstermin vereinbart haben.
                        </p><p>4.5 Bei Zahlung mittels einer von PayPal angebotenen Zahlungsart erfolgt die Zahlungsabwicklung über den Zahlungsdienstleister PayPal (Europe) S.à r.l. et Cie, S.C.A., 22-24 Boulevard Royal, L-2449 Luxembourg (im Folgenden: "PayPal"), unter Geltung der PayPal-Nutzungsbedingungen, einsehbar unter <a href="https://www.paypal.com/de/webapps/mpp/ua/useragreement-full" target="_blank" rel="noopener">https://www.paypal.com/de/webapps/mpp/ua/useragreement-full</a> oder - falls der Kunde nicht über ein PayPal-Konto verfügt – unter Geltung der Bedingungen für Zahlungen ohne PayPal-Konto, einsehbar unter <a href="https://www.paypal.com/de/webapps/mpp/ua/privacywax-full" target="_blank" rel="noopener">https://www.paypal.com/de/webapps/mpp/ua/privacywax-full</a>.
                        </p><br />

                            <h5>§ 5. Liefer- und Versandbedingungen</h5>
                            <p>5.1 Die Lieferung von Waren erfolgt auf dem Versandweg an die vom Kunden angegebene Lieferanschrift, sofern nichts anderes vereinbart ist. Bei der Abwicklung der Transaktion ist die in der Bestellabwicklung des Verkäufers angegebene Lieferanschrift maßgeblich. Abweichend hiervon ist bei Auswahl der Zahlungsart PayPal die vom Kunden zum Zeitpunkt der Bezahlung bei PayPal hinterlegte Lieferanschrift maßgeblich.
                        </p><p>5.2 Sendet das Transportunternehmen die versandte Ware an den Verkäufer zurück, da eine Zustellung beim Kunden nicht möglich war, trägt der Kunde die Kosten für den erfolglosen Versand. Dies gilt nicht, wenn der Kunde den Umstand, der zur Unmöglichkeit der Zustellung geführt hat, nicht zu vertreten hat oder wenn er vorübergehend an der Annahme der angebotenen Leistung verhindert war, es sei denn, dass der Verkäufer ihm die Leistung eine angemessene Zeit vorher angekündigt hatte. Ferner gilt dies im Hinblick auf die Kosten für die Hinsendung nicht, wenn der Kunde sein Widerrufsrecht wirksam ausübt. Für die Rücksendekosten gilt bei wirksamer Ausübung des Widerrufsrechts durch den Kunden die in der Widerrufsbelehrung des Verkäufers hierzu getroffene Regelung.
                        </p><p>5.3 Bei Selbstabholung informiert der Verkäufer den Kunden zunächst per E-Mail darüber, dass die von ihm bestellte Ware zur Abholung bereit steht. Nach Erhalt dieser E-Mail kann der Kunde die Ware nach Absprache mit dem Verkäufer am Sitz des Verkäufers abholen. In diesem Fall werden keine Versandkosten berechnet.
                        </p><p>5.4 Gutscheine werden dem Kunden wie folgt überlassen:
                        </p><p>- per Download
                        </p><p>- per E-Mail
                        </p><p>- postalisch
                        </p><br />

                            <h5>§ 6. Eigentumsvorbehalt</h5>
                            <p>Tritt der Verkäufer in Vorleistung, behält er sich bis zur vollständigen Bezahlung des geschuldeten Kaufpreises das Eigentum an der gelieferten Ware vor.</p>
                            <br />

                            <h5>§ 7. Mängelhaftung (Gewährleistung)</h5>
                            <p>7.1 Ist die Kaufsache mangelhaft, gelten die Vorschriften der gesetzlichen Mängelhaftung.
                        </p><p>7.2 Der Kunde wird gebeten, angelieferte Waren mit offensichtlichen Transportschäden bei dem Zusteller zu reklamieren und den Verkäufer hiervon in Kenntnis zu setzen. Kommt der Kunde dem nicht nach, hat dies keinerlei Auswirkungen auf seine gesetzlichen oder vertraglichen Mängelansprüche.
                        </p><br />

                            <h5>§ 8. Besondere Bedingungen für die Verarbeitung von Waren nach bestimmten Vorgaben des Kunden</h5>
                            <p>8.1 Schuldet der Verkäufer nach dem Inhalt des Vertrages neben der Warenlieferung auch die Verarbeitung der Ware nach bestimmten Vorgaben des Kunden, hat der Kunde dem Betreiber alle für die Verarbeitung erforderlichen Inhalte wie Texte, Bilder oder Grafiken in den vom Betreiber vorgegebenen Dateiformaten, Formatierungen, Bild- und Dateigrößen zur Verfügung zu stellen und ihm die hierfür erforderlichen Nutzungsrechte einzuräumen. Für die Beschaffung und den Rechteerwerb an diesen Inhalten ist allein der Kunde verantwortlich. Der Kunde erklärt und übernimmt die Verantwortung dafür, dass er das Recht besitzt, die dem Verkäufer überlassenen Inhalte zu nutzen. Er trägt insbesondere dafür Sorge, dass hierdurch keine Rechte Dritter verletzt werden, insbesondere Urheber-, Marken- und Persönlichkeitsrechte.
                        </p><p>8.2 Der Kunde stellt den Verkäufer von Ansprüchen Dritter frei, die diese im Zusammenhang mit einer Verletzung ihrer Rechte durch die vertragsgemäße Nutzung der Inhalte des Kunden durch den Verkäufer diesem gegenüber geltend machen können. Der Kunde übernimmt hierbei auch die angemessenen Kosten der notwendigen Rechtsverteidigung einschließlich aller Gerichts- und Anwaltskosten in gesetzlicher Höhe. Dies gilt nicht, wenn die Rechtsverletzung vom Kunden nicht zu vertreten ist. Der Kunde ist verpflichtet, dem Verkäufer im Falle einer Inanspruchnahme durch Dritte unverzüglich, wahrheitsgemäß und vollständig alle Informationen zur Verfügung zu stellen, die für die Prüfung der Ansprüche und eine Verteidigung erforderlich sind.
                        </p><p>8.3 Der Verkäufer behält sich vor, Verarbeitungsaufträge abzulehnen, wenn die vom Kunden hierfür überlassenen Inhalte gegen gesetzliche oder behördliche Verbote oder gegen die guten Sitten verstoßen. Dies gilt insbesondere bei Überlassung verfassungsfeindlicher, rassistischer, fremdenfeindlicher, diskriminierender, beleidigender, Jugend gefährdender und/oder Gewalt verherrlichender Inhalte.
                        </p><br />

                            <h5>§ 9. Einlösung von Aktionsgutscheinen</h5>
                            <p>9.1 Gutscheine, die vom Verkäufer im Rahmen von Werbeaktionen mit einer bestimmten Gültigkeitsdauer unentgeltlich ausgegeben werden und die vom Kunden nicht käuflich erworben werden können (nachfolgend "Aktionsgutscheine"), können nur im Online-Shop des Verkäufers und nur im angegebenen Zeitraum eingelöst werden.
                        </p><p>9.2 Einzelne Produkte können von der Gutscheinaktion ausgeschlossen sein, sofern sich eine entsprechende Einschränkung aus dem Inhalt des Aktionsgutscheins ergibt.
                        </p><p>9.3 Aktionsgutscheine können nur vor Abschluss des Bestellvorgangs eingelöst werden. Eine nachträgliche Verrechnung ist nicht möglich.
                        </p><p>9.4 Bei einer Bestellung können auch mehrere Aktionsgutscheine eingelöst werden.
                        </p><p>9.5 Der Warenwert muss mindestens dem Betrag des Aktionsgutscheins entsprechen. Etwaiges Restguthaben wird vom Verkäufer nicht erstattet.
                        </p><p>9.6 Reicht der Wert des Aktionsgutscheins zur Deckung der Bestellung nicht aus, kann zur Begleichung des Differenzbetrages eine der übrigen vom Verkäufer angebotenen Zahlungsarten gewählt werden.
                        </p><p>9.7 Das Guthaben eines Aktionsgutscheins wird weder in Bargeld ausgezahlt noch verzinst.
                        </p><p>9.8 Der Aktionsgutschein wird nicht erstattet, wenn der Kunde die mit dem Aktionsgutschein ganz oder teilweise bezahlte Ware im Rahmen seines gesetzlichen Widerrufsrechts zurückgibt.
                        </p><p>9.9 Der Aktionsgutschein ist nur für die Verwendung durch die auf ihm benannte Person bestimmt. Eine Übertragung des Aktionsgutscheins auf Dritte ist ausgeschlossen. Der Verkäufer ist berechtigt, jedoch nicht verpflichtet, die materielle Anspruchsberechtigung des jeweiligen Gutscheininhabers zu prüfen.
                        </p><br />

                            <h5>§ 10. Einlösung von Geschenkgutscheinen</h5>
                            <p>10.1 Gutscheine, die über den Online-Shop des Verkäufers käuflich erworben werden können (nachfolgend "Geschenkgutscheine"), können nur im Online-Shop des Verkäufers eingelöst werden, sofern sich aus dem Gutschein nichts anderes ergibt.
                        </p><p>10.2 Geschenkgutscheine und Restguthaben von Geschenkgutscheinen sind bis zum Ende des dritten Jahres nach dem Jahr des Gutscheinkaufs einlösbar. Restguthaben werden dem Kunden bis zum Ablaufdatum gutgeschrieben.
                        </p><p>10.3 Geschenkgutscheine können nur vor Abschluss des Bestellvorgangs eingelöst werden. Eine nachträgliche Verrechnung ist nicht möglich.
                        </p><p>10.4 Pro Bestellung kann immer nur ein Geschenkgutschein eingelöst werden.
                        </p><p>10.5 Geschenkgutscheine können nur für den Kauf von Waren und nicht für den Kauf von weiteren Geschenkgutscheinen verwendet werden.
                        </p><p>10.6 Reicht der Wert des Geschenkgutscheins zur Deckung der Bestellung nicht aus, kann zur Begleichung des Differenzbetrages eine der übrigen vom Verkäufer angebotenen Zahlungsarten gewählt werden.
                        </p><p>10.7 Das Guthaben eines Geschenkgutscheins wird weder in Bargeld ausgezahlt noch verzinst.
                        </p><p>10.8 Der Geschenkgutschein ist übertragbar. Der Verkäufer kann mit befreiender Wirkung an den jeweiligen Inhaber, der den Geschenkgutschein im Online-Shop des Verkäufers einlöst, leisten. Dies gilt nicht, wenn der Verkäufer Kenntnis oder grob fahrlässige Unkenntnis von der Nichtberechtigung, der Geschäftsunfähigkeit oder der fehlenden Vertretungsberechtigung des jeweiligen Inhabers hat.
                        </p><br />

                            <h5>§ 11. Anwendbares Recht</h5>
                            <p>Für sämtliche Rechtsbeziehungen der Parteien gilt das Recht der Bundesrepublik Deutschland unter Ausschluss der Gesetze über den internationalen Kauf beweglicher Waren. Bei Verbrauchern gilt diese Rechtswahl nur insoweit, als nicht der gewährte Schutz durch zwingende Bestimmungen des Rechts des Staates, in dem der Verbraucher seinen gewöhnlichen Aufenthalt hat, entzogen wird.
                        </p><br />

                            <h5>§ 12. Gerichtsstand</h5>
                            <p>Handelt der Kunde als Kaufmann, juristische Person des öffentlichen Rechts oder öffentlich-rechtliches Sondervermögen mit Sitz im Hoheitsgebiet der Bundesrepublik Deutschland, ist ausschließlicher Gerichtsstand für alle Streitigkeiten aus diesem Vertrag der Geschäftssitz des Verkäufers. Hat der Kunde seinen Sitz außerhalb des Hoheitsgebiets der Bundesrepublik Deutschland, so ist der Geschäftssitz des Verkäufers ausschließlicher Gerichtsstand für alle Streitigkeiten aus diesem Vertrag, wenn der Vertrag oder Ansprüche aus dem Vertrag der beruflichen oder gewerblichen Tätigkeit des Kunden zugerechnet werden können. Der Verkäufer ist in den vorstehenden Fällen jedoch in jedem Fall berechtigt, das Gericht am Sitz des Kunden anzurufen.
                        </p><br />

                            <h5>§ 13. Alternative Streitbeilegung</h5>
                            <p>13.1 Die EU-Kommission stellt im Internet unter folgendem Link eine Plattform zur Online-Streitbeilegung bereit: https://ec.europa.eu/consumers/odr
                            Diese Plattform dient als Anlaufstelle zur außergerichtlichen Beilegung von Streitigkeiten aus Online-Kauf- oder Dienstleistungsverträgen, an denen ein Verbraucher beteiligt ist.
                        </p><p>13.2 Der Verkäufer ist zur Teilnahme an einem Streitbeilegungsverfahren vor einer Verbraucherschlichtungsstelle weder verpflichtet noch bereit.
                        </p><br />

                            <h5>§ 14. Widerrufsbelehrung & Widerrufsformular </h5>
                            <p>Verbrauchern steht ein Widerrufsrecht nach folgender Maßgabe zu, wobei Verbraucher jede natürliche Person ist, die ein Rechtsgeschäft zu Zwecken abschließt, die überwiegend weder ihrer gewerblichen noch ihrer selbständigen beruflichen Tätigkeit zugerechnet werden können:</p><br />
                            <h6>14.1 Widerrufsbelehrung</h6>
                            <p>Widerrufsrecht</p>
                            <p>Sie haben das Recht, binnen vierzehn Tagen ohne Angabe von Gründen diesen Vertrag zu widerrufen.</p>
                            <p>Die Widerrufsfrist beträgt vierzehn Tage ab dem Tag, an dem Sie oder ein von Ihnen benannter Dritter, der nicht der Beförderer ist, die letzte Ware in Besitz genommen haben bzw. hat. </p>
                            <p>Um Ihr Widerrufsrecht auszuüben, müssen Sie uns (Ince GmbH, Münzstraße 7, 30159 Hannover, Deutschland, Tel.: 0511 2155 001, E-Mail: info@efendibey.de) mittels einer eindeutigen Erklärung (z. B. ein mit der Post versandter Brief oder E-Mail) über Ihren Entschluss, diesen Vertrag zu widerrufen, informieren. Sie können dafür das beigefügte Muster-Widerrufsformular verwenden, das jedoch nicht vorgeschrieben ist.</p>
                            <p>Zur Wahrung der Widerrufsfrist reicht es aus, dass Sie die Mitteilung über die Ausübung des Widerrufsrechts vor Ablauf der Widerrufsfrist absenden.</p><br />

                            <p>Folgen des Widerrufs</p>
                            <p>Wenn Sie diesen Vertrag widerrufen, haben wir Ihnen alle Zahlungen, die wir von Ihnen erhalten haben, einschließlich der Lieferkosten (mit Ausnahme der zusätzlichen Kosten, die sich daraus ergeben, dass Sie eine andere Art der Lieferung als die von uns angebotene, günstigste Standardlieferung gewählt haben), unverzüglich und spätestens binnen vierzehn Tagen ab dem Tag zurückzuzahlen, an dem die Mitteilung über Ihren Widerruf dieses Vertrags bei uns eingegangen ist. Für diese Rückzahlung verwenden wir dasselbe Zahlungsmittel, das Sie bei der ursprünglichen Transaktion eingesetzt haben, es sei denn, mit Ihnen wurde ausdrücklich etwas anderes vereinbart; in keinem Fall werden Ihnen wegen dieser Rückzahlung Entgelte berechnet. Wir können die Rückzahlung verweigern, bis wir die Waren wieder zurückerhalten haben oder bis Sie den Nachweis erbracht haben, dass Sie die Waren zurückgesandt haben, je nachdem, welches der frühere Zeitpunkt ist.</p>
                            <p>Sie haben die Waren unverzüglich und in jedem Fall spätestens binnen vierzehn Tagen ab dem Tag, an dem Sie uns über den Widerruf dieses Vertrags unterrichten, an uns zurückzusenden oder zu übergeben. Die Frist ist gewahrt, wenn Sie die Waren vor Ablauf der Frist von vierzehn Tagen absenden.
                        </p><p>Sie tragen die unmittelbaren Kosten der Rücksendung der Waren.
                        </p><p>Sie müssen für einen etwaigen Wertverlust der Waren nur aufkommen, wenn dieser Wertverlust auf einen zur Prüfung der Beschaffenheit, Eigenschaften und Funktionsweise der Waren nicht notwendigen Umgang mit ihnen zurückzuführen ist.
                        <br />
                            </p><p>Ausschluss bzw. vorzeitiges Erlöschen des Widerrufsrechts
                        </p><p>Das Widerrufsrecht besteht nicht bei Verträgen zur Lieferung von Waren, die nicht vorgefertigt sind und für deren Herstellung eine individuelle Auswahl oder Bestimmung durch den Verbraucher maßgeblich ist oder die eindeutig auf die persönlichen Bedürfnisse des Verbrauchers zugeschnitten sind.
                        </p><p>Das Widerrufsrecht besteht nicht bei Verträgen zur Lieferung von Waren, die schnell verderben können oder deren Verfallsdatum schnell überschritten würde.
                        </p>
                            <h6>14.2 Widerrufsformular</h6>
                            <p>Wenn Sie den Vertrag widerrufen wollen, dann füllen Sie bitte dieses Formular aus und senden es zurück.
                        </p><p>An
                        </p><p>Ince GmbH
                        </p><p>Münzstraße 7
                        </p><p>30159 Hannover
                        </p><p>Deutschland
                        </p><p>E-Mail: info@efendibey.de
                        </p><p>Hiermit widerrufe(n) ich/wir (*) den von mir/uns (*) abgeschlossenen Vertrag über den Kauf der folgenden Waren (*)/die Erbringung der folgenden Dienstleistung (*)
                        </p><p>___________________________
                        </p><p>Bestellt am (*) _________ /
                        </p><p>erhalten am (*) _________
                        </p><p>___________________________
                        </p><p>Name des/der Verbraucher(s)
                        </p><p>____________________________
                        </p><p>Anschrift des/der Verbraucher(s)
                        </p><p>____________________________
                        </p><p>Unterschrift des/der Verbraucher(s) (nur bei Mitteilung auf Papier)
                        </p><p>_______________
                        </p><p>Datum
                        </p><p>(*) Unzutreffendes streichen</p>
                        </div>
                        <div class="modal-footer">
                            <a class="modal-action modal-close waves-effect btn-flat close-button">OK</a>
                        </div>
                    </div>



                </section >
            </ReactScoped >
        );
    }
}