import React, { Component } from 'react';
import { ReactScoped, ViewEncapsulation } from 'react-scoped';
import styles from './filter.css';
import $ from 'jquery';

var lastScrollTop = 0;
export default class Filter extends Component {

    constructor(props) {
        super(props);


        // Initial state
        this.state = {
            filterOpen: true
        }
        this.handleScroll = this.handleScroll.bind(this);
    }
    componentDidMount() {
        var self = this;
        $(".drop-icon").click(function () {
            if (self.state.filterOpen) {
                self.setState({
                    filterOpen: false
                });
            } else {
                self.setState({
                    filterOpen: true
                });
            }

            var dist = window.scrollY;
            if (dist > 80) {
                $(".main-div").addClass("sticky");
                $(".full-div").addClass("full_divNonSticky");
                $(".drop-icon-div_UpArrow").addClass("drop-icon-div_UpArrowWithMargin");
                $(".drop-icon-div_downArrow").addClass("stickyArrow");
            }
            else {
                $(".main-div").removeClass("sticky");
                $(".full-div").removeClass("full_divNonSticky");
                $(".drop-icon-div_UpArrow").removeClass("drop-icon-div_UpArrowWithMargin");
                $(".drop-icon-div_downArrow").removeClass("stickyArrow");
            }

        });
        $(document).delegate('.full-div', 'mouseleave', function () {
            self.setState({
                //  filterOpen: false
            });
            var dist = window.scrollY;
            if (dist > 80) {
                $(".main-div").addClass("sticky");
                // $(".full-div").addClass("full_divNonSticky");
                $(".drop-icon-div_downArrow").addClass("stickyArrow");
            }
            else {
                $(".main-div").removeClass("sticky");
                //  $(".full-div").removeClass("full_divNonSticky");
                $(".drop-icon-div_downArrow").removeClass("stickyArrow");
            }

        });
        window.addEventListener('scroll', self.handleScroll);
    }

    handleScroll(event) {
        var dist = window.scrollY;
        var self = this;

        var st = $(window).scrollTop();

        if (self.props.scrollIdFlagDown) {
            if (st > lastScrollTop) {
                // downscroll code
               // console.log('down scroll');
                if (self.props.scrollProduct_Id !== 1) {
                    $('#content').animate({
                        scrollLeft: "+=45px"
                    }, "slow");
                }
            } else {
               // console.log('remainder upscroll');
                if (self.props.scrollProduct_Id)
                    $('#content').animate({
                        scrollLeft: "-=45px"
                    }, "slow");
            }
            // lastScrollTop = st;
        }

        if (st > lastScrollTop) {
            if (dist > 80) {
                $(".main-div").addClass("sticky");
                $(".full-div").addClass("full_divNonSticky");
                $(".drop-icon-div_UpArrow").addClass("drop-icon-div_UpArrowWithMargin");
                $(".drop-icon-div_downArrow").addClass("stickyArrow");
                $(".drop-icon-div_UpArrow").removeClass("drop-icon-div_UpArrowWithMargin");
            }
            //console.log('down scroll');
        } else {
           // console.log('remainder upscroll');
            $(".main-div").removeClass("sticky");
            $(".drop-icon-div_UpArrow").removeClass("drop-icon-div_UpArrowWithMargin");
            $(".full-div").removeClass("full_divNonSticky");
            $(".drop-icon-div_downArrow").removeClass("stickyArrow");
        }
        if (self.state.filterOpen) {
            self.setState({
                filterOpen: false
            });
        }
        lastScrollTop = st;

    }

    async jumpToMenu(id, row) {
        for (let index = 1; index <= this.props.menu.length; index++) {
            $(".chageColor" + index).removeClass('addColor');
        }
        $(".chageColor" + id).addClass('addColor');
        // $(".img-div-list").css('color','#fff');
        // console.log(" id ", id);
        if (row === 'multiRow') {
            await this.setState({
                filterOpen: false
            });
            $(".chageColor" + id).addClass('addColor');
        }
    }

    componentDidUpdate() {
        var self = this;
        for (let index = 1; index <= self.props.menu.length; index++) {
            $(".chageColor" + index).removeClass('addColor');
        }
       // console.log('id:', self.props.scrollProduct_Id);
        $(".chageColor" + self.props.scrollProduct_Id).addClass('addColor');
    }

    render() {
        // console.log(" menu =================  ", this.props.menu);
        if (this.state.filterOpen) {
            return (
                <ReactScoped encapsulation={ViewEncapsulation.Emulated} styles={[styles]}>
                    <div className="full-div">
                        {this.props.menu.map((el, indexOfEl) => (
                            <a href={`#menuTitile_${el.id}`} >
                                <div className="sub-div m-sub-div" onClick={this.jumpToMenu.bind(this, el.id, 'multiRow')}>
                                    <div className="img-div-list">
                                        <img className="item-icon" src={`/ProductIcon/${el.productType}.svg`}></img>
                                        <div className="name-div">
                                            <p className="productTypeName">{el.productType}</p>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        ))}
                    </div>
                    <div className="drop-icon-div_UpArrow"><i className="material-icons drop-icon" >arrow_drop_up</i></div>
                </ReactScoped>
            )
        } else {
            return (
                <ReactScoped encapsulation={ViewEncapsulation.Emulated} styles={[styles]}>
                    <div className="main-div" id="content">
                        {this.props.menu.map((el, indexOfEl) => (
                            <a href={`#menuTitile_${el.id}`} className={`menuTitile_${el.id}`}>
                                <div className="sub-div" onClick={this.jumpToMenu.bind(this, el.id, 'oneRow')}>
                                    <div className={`img-div chageColor${el.id}`} >
                                        <img className="item-icon" src={`/ProductIcon/${el.productType}.svg`}></img>
                                    </div>
                                </div>
                            </a>
                        ))}
                    </div>
                    <div className="drop-icon-div_downArrow"><i className="material-icons drop-icon" >arrow_drop_down</i></div>
                </ReactScoped>
            )
        }

    }
}