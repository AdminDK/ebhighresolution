import React, { Component } from 'react';
import { ReactScoped, ViewEncapsulation } from 'react-scoped';
import styles from './Extras.css';

export default class Extras extends Component {

    constructor() {
        super();
        this.state = {
            open: false,
            readMoreLessBtn: 'Weitere anzeigen'
        };
    }

    componentDidMount() {
        // console.log(" props ========= ", this.props.extraProductType);

    }


    toggle() {
        if (this.state.open) {
            this.setState({
                readMoreLessBtn: 'Weitere anzeigen'
            });
        }
        else {
            this.setState({
                readMoreLessBtn: 'weniger anzeigen'
            });
        }
        this.setState({
            open: !this.state.open
        });
    }


    render() {
        // console.log(" selectedDropdown ", this.props.selectedDropdown);
        let dropdownValue = this.props.selectedDropdown;
        // console.log(" dropdownValue ============= ", dropdownValue);
        if (dropdownValue.length > 0) {
            dropdownValue = this.props.selectedDropdown.split("==");
            // console.log(" dropdownValue ", dropdownValue);
            let first = dropdownValue[0].split(",");
            dropdownValue = first[0] + "--" + first[1] + "--" + first[2] + "==indexOfEl--" + dropdownValue[1] + "--indexOf";

        } else {
            dropdownValue = 'select';
        }
        // console.log(" dropdownValue ", dropdownValue);

        if (this.props.extraProductType === 'dropd' || this.props.extraProductType === 'dropselect') {
            return (
                <ReactScoped encapsulation={ViewEncapsulation.Emulated} styles={[styles]}>

                    <div className="lightGrayText selectDiv">
                        <select type="text" className="validate extrasSelect" value={dropdownValue} required="" aria-required="true" name="flavor">
                            {/* <option value="select" style={{display:this.props.extraProductType === 'dropselect' ? 'none' : ''}} >Auswahloptionen</option> */}
                            {this.props.productExtras.map(el => (
                                <option value={`${el.rate}--${el.name}--${el.code}==${'indexOfEl'}--${this.props.productId}--${'indexOf'}`}>{el.name} {el.rate > 0 ? el.rate.toFixed(2).toString().replace(".", ",") : '0'} €</option>
                            ))}
                        </select>
                    </div>


                </ReactScoped>
            );
        }
        else if (this.props.extraProductType === 'checkbox') {
            // console.log(" checkbox ================== "); 
            return (
                <ReactScoped encapsulation={ViewEncapsulation.Emulated} styles={[styles]}>
                    <div className="extraItemCheckboxContain" id="demo" className={"checkBox" + (this.state.open ? ' in' : '')}>
                        {this.props.productExtras.map((el, index) => (
                            <div className="extraItemCheckbox">
                                <label className="lblCheckBox" htmlFor={`${this.props.productId}${index}`}>
                                    <input type="checkbox" className="extrasCheckbox filled-in" id={`${this.props.productId}${index}`} value={`${el.rate}--${el.name}--${el.code}--${this.props.productId}${index}==${this.props.indexOfEl}--${this.props.productId}--${this.props.indexOf}`} />
                                    <span className={`badge ${this.props.productId}${index}`} >{el.name} {el.rate > 0 ? el.rate.toFixed(2).toString().replace(".", ",") : '0'} €</span>
                                    {/* <a >
                                        <span className={`badge ${this.props.productId}${index}`} >{el.name} {el.rate > 0 ? el.rate.toFixed(2).toString().replace(".", ",") : '0'} €</span>
                                    </a> */}
                                </label>
                            </div>
                        ))}
                    </div>
                    <div className="readMoreLessBtnDiv">
                        <button className="readMoreLessBtn" onClick={this.toggle.bind(this)}>
                            {this.state.readMoreLessBtn}
                        </button>
                    </div>
                </ReactScoped>
            );
        } else {
            return (
                <div></div>
            );
        }

    }
}