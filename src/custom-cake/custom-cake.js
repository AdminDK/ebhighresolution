import React, { Component } from 'react';
import { ReactScoped, ViewEncapsulation } from 'react-scoped';
import $ from 'jquery';
import M from 'materialize-css/dist/js/materialize.min.js';
import PriceCalculator from '../utils/price-calculator';
import { withRouter } from 'react-router-dom';
import NavigationBar from "../nav-bar/nav-bar";
import Footer from '../footer/footer';

import PriceBox from './price-box';
//import PaypalBtn from './paypal-checkout';
import PaypalButton from './paypal-checkout-button';
import LazyLoad from 'react-lazyload';

import styles from './custom-cake.css';
import cake_icon from '../assets/cake_icon.png';

import chocolate from '../assets/dropdown/chocolate.jpg';
import raspberry from '../assets/dropdown/raspberry.JPG';
import cookies from '../assets/dropdown/cookies.png';
import ganache from '../assets/dropdown/ganache.jpg';
import cherry from '../assets/dropdown/cherry.jpg';
import cwalnut from '../assets/dropdown/cwalnut.jpg';
import banana from '../assets/dropdown/banana.JPG';
import fruitgar from '../assets/dropdown/fruitgreen.jpeg';
import cholate from '../assets/dropdown/cholate.jpg';
import krokant from '../assets/dropdown/krokant.JPG';
import chocoRasp from '../assets/dropdown/chocoraspberry.JPG';
import chocoBana from '../assets/dropdown/chocobanana.JPG';
import Strawbery from '../assets/dropdown/strawberry.JPG';
import Raspberry_kuchen from '../assets/dropdown/Raspberry_kuchen.jpeg';
import Waldfrüchte from '../assets/dropdown/Waldfrüchte.jpeg';
import caramel_wallnuss from '../assets/dropdown/caramel_wallnuss.jpeg';
import banane_kuchen from '../assets/dropdown/banane_kuchen.jpeg';

import smooth from '../assets/colors/smooth.JPG';
import jelly from '../assets/colors/jelly.JPG';
import combed from '../assets/colors/combed.JPG';
import smoothsauce from '../assets/colors/glattsosse.jpg';
import combedsauce from '../assets/colors/gekammtsosse.jpg';
import raspelbraun from '../assets/colors/raspelbraun.jpg';
import raspelcreme from '../assets/colors/raspelcreme.jpg';
import raspelrosa from '../assets/colors/raspelrosa.jpg';
import Drip from '../assets/dropdown/Drip.jpeg';
import Naked from '../assets/dropdown/Naked.jpeg';
import Swal from 'sweetalert2';
import { set } from 'react-ga';
import { i18n_DE } from '../utils/calendarConstant';

//import { GoogleComponent } from 'react-google-location';     Naked   Drip
const API_KEY = "AIzaSyBCSOF6U1hlvADkOSzu8dZB-ftI1ntSR4E"
let anyalyticKey = 'UA-120592631-1';
let dataForAnalytics = true;
const search = window.location.search
let orderId = new URLSearchParams(search).get("orderId");
console.log(" search ", window.location.host);
let hostUrl = window.location.host;
let mollieMessageFunc = null;

var url = window.location.href;
var urlSplit = url.split('/');
if (urlSplit[2] === 'localhost:5443') {
    dataForAnalytics = false;
}
if (urlSplit[2] === 'qa.efendibey.digiklug.com') {
    dataForAnalytics = false;
}

let modalOptions, instanceReview, modalReviewbtn;


class CustomCake extends Component {
    current_fs = null;

    constructor(props) {
        super(props);
        // this.callback = this.callback.bind(this);

        this.state = {
            place: null,
            btn_clicked: false,
            checked: false,
            orderIdReference: 0,
            mollieMessage: true,
            formData: {
                deliveryAddress: '',
                deliveryAddressPincod: '',
                deliveryCharge: 0,
                deliveryDistance: 0,
                hiddenOrderId: '',
                partialAmount: '0,00',
                extraNonFoodPrice: '0,00',
                extraFoodPrice: '0,00',
                balanceAmount: '0,00',
                fname: '',
                lname: '',
                mobile: '',
                email: '',
                pickupDeliverySelection: false,
                deliveryDate: '',
                deliveryTime: '',
                deliveryTimeHour: '',
                deliveryTimeMin: '',
                street: '',
                houseNo: '',
                city: '',
                shape: '',
                size: '',
                color: '',
                decoration: '',
                sauceColor: '',
                dripColor: '',
                flavor: '',
                decoColor: '',
                additional_flavor: [],
                extra_decoration: [],
                text: '',
                remarks: '',
                photo: '',
                cuponname: '',
                cuponPrice: 0,
                paymentType: 'cod',
            },
            price: 0,
            totalTax: 0,
            taxAtDefaultRate: 0,
            taxAtCustomRate: 0,
            shapeAndStylePrice: 0,
            shapeAndStyleTaxAtDefaultRate: 0,
            shapeAndStyleTaxAtCustomRate: 0,
            designAndFlavorPrice: 0,
            designAndFlavorTaxAtDefaultRate: 0,
            designAndFlavorTaxAtCustomRate: 0,
            couponShapeAndStylePrice: 0,
            priceBoxActive: false,
            productPrices: {
                shapes: {},
                sizes: {},
                colors: {},
                decorations: {},
                extraDecorations: {},
                flavors: {},
                extraFlavors: {},
                priceRules: {}
            },
            isCakeShapeFieldActive: {
                SHP01: false,
                SHP02: false,
                SHP03: false,
                SHP04: false,
                SHP99: false,
            },
            colorVisibility: null,
            sauceColorVisibility: { display: 'none' },
            decoColorVisibility: { display: 'none' },
            dripColorVisibility: { display: 'none' },
            isFlavorDisabled: {
                FLV01: false,
                FLV02: false,
                FLV03: false,
                FLV04: false,
                FLV05: false,
                FLV06: false,
                FLV07: false,
                FLV08: false,
                FLV09: false,
                FLV10: false,
                FLV11: false,
                FLV12: false,
                FLV13: false
            },
            isExtraFlavorDisabled: {
                FLV01: false,
                FLV02: false,
                FLV03: false,
                FLV04: false,
                FLV05: false,
                FLV06: false,
                FLV07: false,
                FLV08: false,
                FLV09: false,
                FLV10: false,
                FLV11: false,
                FLV12: false,
                FLV13: false
            },
            isDeliveryTomorrow: false,
            isextraDecorationsDisabled: {
                EDECO01: false,
                EDECO02: false,
                EDECO03: false,
                EDECO04: false,
                EDECO05: false,
                EDECO06: false,
                EDECO07: false,
                EDECO08: false,
            },
            isNakedcolorDisabled: {
                CLR01: true,
                CLR02: true,
                CLR03: true,
                CLR04: true,
                CLR05: true,
                CLR06: true,
                CLR07: true,
                CLR08: true,
                CLR99: true,
            },
            hideBankTransfer: false,
            shapeSizeRules: new Set(),
            isSubmitInProgress: false,
            isLoading: false,
            isCuponUsed: false,
            CampaignList: []
        };
        this.button_clicked = this.button_clicked.bind(this);
        this.closeCakeForm = this.closeCakeForm.bind(this);
    }

    componentDidMount() {
        this.props.setNav(true);
        this.openCakeForm();

        modalOptions = {
            onOpenStart: () => {
                console.log("Open Start");
            },
            onOpenEnd: () => {
                console.log("Open End");
            },
            onCloseStart: () => {
                console.log("Close Start");
            },
            onCloseEnd: () => {
                console.log("Close End");
            },
            inDuration: 250,
            outDuration: 250,
            opacity: 0.5,
            dismissible: true,
            startingTop: "4%",
            endingTop: "10%"
        };

        // console.log(" this.state", this.state);
        var self = this;
        //  console.log('called::::::::::::::::');
        //DK comment
        window.onload = function () {
            document.getElementById('cake_size').focus();
        };

        // $('#cake_size').on('click', function () {
        //     document.getElementById('cake_size').focus();
        // })

        fetch('/api/productList')
            .then((res, err) => {
                if (!res.ok) { throw res }
                return res.json();
            })
            .then((data) => {
                //  console.log('data is #########################', data);
                this.setState({ productPrices: data }, () => {
                    this.setFlavorAvailability();
                });
                //console.log('this.state.productPrices.colors', this.state.productPrices.colors);
            })
            .catch(err => {
                console.log(err);
                err.text().then(errmsg => {
                    console.log("cant fetch product prices " + errmsg);
                })
            });



        $('select').on('change', this.handleDataChange);
        // $('.cash-notice').addClass('hidden');

        //initialize date and time picker
        var t = this;
        /* old pickadate materialize css
        $('.customcakedatepicker').pickadate({
            // onMouseDown:function(event){event.preventDefault();},
            selectMonths: true, // Creates a dropdown to control month
            selectYears: 1, // Creates a dropdown of 15 years to control year,
            closeOnSelect: true, // Close upon selecting a date,
            min: new Date(new Date().valueOf() + 86400000 * 1),//setting min date to tomorrow
            required: 'true',
            firstDay: 1,
            onSet: function (context) {
                if (!context.select) {
                    return;
                }

                var delDate = new Date(context.select);
                console.log(`1: ${delDate}`);
                t.state.formData.deliveryDate = `${delDate.getDate()}.${delDate.getMonth() + 1}.${delDate.getFullYear()}`; //$('input#deldate').val();
                console.log(`2: ${t.state.formData.deliveryDate}`);
                $("input#deldate").removeClass("invalid");
                $("input#deldate").addClass("valid");

                //If the delivery date is within 2 days, Bank Transfer option will grey out.
                var diff = Math.floor((delDate - new Date()) / 86400000);
                if (diff <= 0) {

                    t.setState((prevState) => {
                        let newState = Object.assign({}, prevState);
                        newState.isDeliveryTomorrow = true;
                        newState.hideBankTransfer = true;
                        newState.formData.deliveryTimeHour = '';
                        return newState;
                    }, () => {
                        $('select').material_select();
                    });
                    $(".five-day-notice").removeClass('hidden');

                }
                else if (diff <= 1) {
                    $(".five-day-notice").removeClass('hidden');
                    t.setState({
                        hideBankTransfer: true,
                        isDeliveryTomorrow: false
                    }, () => {
                        $('select').material_select();
                    });

                }
                else {
                    $(".five-day-notice").addClass('hidden');
                    t.setState({
                        hideBankTransfer: false,
                        isDeliveryTomorrow: false
                    }, () => {
                        $('select').material_select();
                    });
                }
            }
        });
*/

        //DK materialize 0.100.2 to 1.0.0 datepicker
        // document.addEventListener('DOMContentLoaded', function () {


        // });
        //DK materialize 0.100.2 to 1.0.0
        // const M = window.M;
        // $('#custom_cake .parallax').parallax();

        const elems = document.querySelectorAll("#custom_cake .parallax");
        M.Parallax.init(elems, { responsiveThreshold: 0 })
        //M.ScrollSpy.init(elems, { scrollOffset: 90 });

        //DK materialize 0.100.2 to 1.0.0
        // $('select').material_select();
        var selectElm = document.querySelectorAll("select");
        M.FormSelect.init(selectElm);


        var elemsDate = document.querySelectorAll('.customcakedatepicker');
        var instances = M.Datepicker.init(elemsDate, {
            minDate: new Date(new Date().valueOf() + 86400000 * 1),//setting min date to tomorrow
            firstDay: 1,
            autoClose: true,
            showClearBtn: true,
            i18n: i18n_DE,

            //Jira Tkt EFB-475
            //Below lines of code for Disabled Wednesday, when required enable disabled 
            //comment or uncommente below lines of code      // 04/09/2021  gaus commented
            /* disableDayFn: function(date) {
                 if(date.getDay() == 3) { // getDay() returns a value from 0 to 6, 3 represents Wednesday
                     return true;
                 }
                 else {
                     return false;
                 }
             },  
             */

            //Created by gaus // 29/12/2021
            //Start : First day in first month in starting new year disabled logic
            disableDayFn: function (date) {
                if (date.getDate() == 1 && date.getMonth() == 0) {
                    return true;
                }
                else {
                    return false;
                }
            },
            //End : First day in first month in starting new year disabled logic

            // format: 'dmyyyy',
            onSelect: function () {
                let disableMorningTime = false;
                var delDate = instances[0].date;
                console.log(`1: ${delDate}`);
                t.state.formData.deliveryDate = `${delDate.getDate()}.${delDate.getMonth() + 1}.${delDate.getFullYear()}`; //$('input#deldate').val();
                console.log(`2: ${t.state.formData.deliveryDate}`);
                $("input#deldate").removeClass("invalid");
                $("input#deldate").addClass("valid");

                //If the delivery date is within 2 days, Bank Transfer option will grey out.
                var diff = Math.floor((delDate - new Date()) / 86400000);
                if (diff <= 0) {

                    t.setState((prevState) => {
                        let newState = Object.assign({}, prevState);
                        newState.isDeliveryTomorrow = true;
                        newState.hideBankTransfer = true;
                        newState.formData.deliveryTimeHour = '';
                        return newState;
                    }, () => {
                        // $('select').material_select();
                        var selectElm = document.querySelectorAll("select");
                        M.FormSelect.init(selectElm, {});
                    });
                    $(".five-day-notice").removeClass('hidden');

                }
                else if (diff <= 1) {
                    // disable 9-13 hours on Thursday
                    //disableMorningTime = (delDate.getDay() == 4) ? true : false;
                    $(".five-day-notice").removeClass('hidden');
                    t.setState((prevState) => {
                        let newState = Object.assign({}, prevState);
                        newState.hideBankTransfer = true;
                        newState.isDeliveryTomorrow = disableMorningTime;
                        newState.formData.deliveryTimeHour = '';
                        return newState;
                    }, () => {
                        // $('select').material_select();
                        var selectElm = document.querySelectorAll("select");
                        M.FormSelect.init(selectElm, {});
                    });

                    // t.setState({
                    //     hideBankTransfer: true,
                    //     isDeliveryTomorrow: false
                    // }, () => {
                    //     // $('select').material_select();
                    //     var selectElm = document.querySelectorAll("select");
                    //     M.FormSelect.init(selectElm, {});
                    // });
                }
                else {
                    // disable 9-13 hours on Thursday
                    //disableMorningTime = (delDate.getDay() == 4) ? true : false; 
                    $(".five-day-notice").addClass('hidden');
                    t.setState((prevState) => {
                        let newState = Object.assign({}, prevState);
                        newState.hideBankTransfer = true;
                        newState.isDeliveryTomorrow = disableMorningTime;
                        newState.formData.deliveryTimeHour = '';
                        return newState;
                    }, () => {
                        // $('select').material_select();
                        var selectElm = document.querySelectorAll("select");
                        M.FormSelect.init(selectElm, {});
                    });

                    // t.setState({
                    //     hideBankTransfer: false,
                    //     isDeliveryTomorrow: false
                    // }, () => {
                    //     // $('select').material_select();
                    //     var selectElm = document.querySelectorAll("select");
                    //     M.FormSelect.init(selectElm, {});
                    // });
                }
            }

        });

        var elemModal = document.querySelectorAll('.modal');
        let instance = M.Modal.init(elemModal);

        var reviewModal = document.querySelectorAll('#modal_review');
        var reviewModalInstance = M.Modal.init(reviewModal, {
            onOpenStart: ()=>{$('.page-footer').addClass('hide')},
            onCloseStart: ()=>{$('.page-footer').removeClass('hide')}
        })

        var elemCollapsible = document.querySelectorAll('.collapsible');
        var instancesCollapsible = M.Collapsible.init(elemCollapsible);
        //domloaded event end here

        $('#hiddenInput').trigger('focus');

        //var searchInput = $('#search_input')[0];
        //console.log('searchInput:', searchInput);
        // $(document).ready(function () {
        let searchInput = 'search_input';


        var service = new window.google.maps.DistanceMatrixService();

        var autocomplete;
        // eslint-disable-next-line no-undef
        autocomplete = new google.maps.places.Autocomplete((document.getElementById(searchInput)), {
            types: ['geocode'],
        });
        console.log(" service ", service);

        // eslint-disable-next-line no-undef
        google.maps.event.addListener(autocomplete, 'place_changed', function () {
            var near_place = autocomplete.getPlace();

            let addressForPin = near_place.address_components;
            //console.log(" address_components ", near_place.address_components);
            let formatted_address = near_place.formatted_address;
            // console.log(" formatted_address ", near_place.formatted_address.split(','));
            let firstPlaceAddress = near_place.formatted_address.split(',');
            // console.log(" firstPlaceAddress ", firstPlaceAddress[0].split(' '));
            let checkIfHouseNumber = firstPlaceAddress[0].split(' ');
            if (typeof checkIfHouseNumber[1] === "undefined") {
                // alert(" Please enter valid address with house number ");
                $(".house-number-not-valid").removeClass('hidden');
                $(".more-then-fifteen").addClass('hidden');
                // $(".more-then-fifteen").removeClass('hidden');
                return false;
            } else {
                $(".house-number-not-valid").addClass('hidden');
            }
            // console.log(" checkIfHouseNumber ", checkIfHouseNumber);
            console.log(" checkIfHouseNumber[1] ", checkIfHouseNumber[1]);
            // console.log(" lat ", near_place.geometry.location.lat());
            // console.log(" lng ", near_place.geometry.location.lng());
            let lat = near_place.geometry.location.lat();
            let long = near_place.geometry.location.lng();
            document.getElementById('loc_lat').value = near_place.geometry.location.lat();
            document.getElementById('loc_long').value = near_place.geometry.location.lng();


            addressForPin.forEach(element => {
                if (element['types'][0] === 'postal_code') {
                    self.state.formData.deliveryAddressPincod = element.long_name;
                }
            });


            // var service = new google.maps.DistanceMatrixService();

            service.getDistanceMatrix({
                origins: [{ lat: 52.3752655, lng: 9.7313067 }, 'Münzstraße 7, 30159 Hannover'],
                destinations: [formatted_address, { lat: lat, lng: long }],
                travelMode: 'DRIVING',
                drivingOptions: {
                    departureTime: new Date(Date.now()),  // for the time N milliseconds from now.
                    trafficModel: 'optimistic'
                }
            }, self.callback.bind(self));

            let distance = self.getDistanceFromLatLonInKm(lat, long, 52.3752655, 9.7313067);
            console.log(" distance ================== ", distance);

        });

        // });

        var opened = false;
        $(document).on('touchstart', function (e) {
            let target = $(e.target);

            if (target.is('input') && target.attr('class') === 'select-dropdown') {
                // a select-dropdown has been tapped
                opened = true;
            } else if (opened && !target.is("span")) {
                // a select-dropdown is open and something outside the drop down
                // has been tapped

                // Close the drop-down and move focus away so that it can be re-tapped
                $(document).trigger('click');
                // $(':focus').blur();
                $(':focus').trigger('blur');
                opened = false;
            } else if ($('.material-tooltip').css('visibility') === 'visible') {
                $('.material-tooltip').css('visibility', 'hidden');
                $('.material-tooltip').css('opacity', '0');
            } else {
                // do nothing
            };
        });
        this.custcakebtn();
        this.setupNavigation();
        this.setextradecorationAvailability();
        if (window.performance) {
            if (performance.navigation.type == 1) {
                // alert( "This page is reloaded" );
            } else {
                // alert(' else ')
                this.showMollieMessage();
                // setInterval(this.showMollieMessage(), 5000)
                mollieMessageFunc = setInterval(() => {
                    this.showMollieMessage()
                }, 5000);
            }
        }

        if(!this.state.checked){
            $(".house-number-not-valid").addClass('hidden');
            $(".more-then-fifteen").addClass('hidden');
        }

        var reviewModalElem = document.querySelector('#modal_review');
        instanceReview = M.Modal.getInstance(reviewModalElem);
        
    }

    showMollieMessage = () => {
        var self = this;
        if (orderId && this.state.mollieMessage) {
            // alert(' fes ')
            let postData = {
                "orderId": orderId
            }

            $.ajax({
                type: 'POST',
                url: '/api/getMolliePayment',
                data: JSON.stringify(postData),
                // async: false,
                datatype: 'json',
                contentType: 'application/json; charset=UTF-8',
            }).done((res) => {
                if (res.status === 'paid') {
                    self.state.mollieMessage = false;
                    clearInterval(mollieMessageFunc);

                    Swal.fire(
                        `Vielen Dank! Ihre Bestellungs Nr:<b style="color:blue;">&nbsp;&nbsp;${res.orderId}</b>`,
                        `Bitte prüfen Sie ihre E-Mail und die Bestellung nochmal sorgfältig und folgen Sie den dortigen Anweisungen.`,
                        'success'
                    )

                } else if (res.status === 'canceled') {
                    self.state.mollieMessage = false;
                    clearInterval(mollieMessageFunc);
                }

            }).fail((jqXhr) => {
                console.log('Order was not saved. ', jqXhr);
            });
        }
    }

    setextradecorationAvailability = () => {
        console.log(" this=========== ", this.state);
        var now = new Date();
        var isextraDecorationsDisabled = this.state.isextraDecorationsDisabled;

        let from = new Date(new Date().getFullYear(), 6, 14);
        // let from = new Date(new Date().getFullYear(), 0, 1);
        let to = new Date(new Date().getFullYear(), 8, 15);

        // console.log('from :',from);
        // console.log('to :',to);

        //uncomment when disabled EDECO04
        /* if (now >= from && now <= to) {
             for (var index in isextraDecorationsDisabled) {
                 if (index === 'EDECO04') {
                     isextraDecorationsDisabled[index] = false;
                     continue;
                 }
 
             }
         }
         else {
             for (var index in isextraDecorationsDisabled) {
                 if (index === 'EDECO04') {
                     isextraDecorationsDisabled[index] = true;
                     continue;
                 }
 
             }
         }
         */

        this.setState({ isextraDecorationsDisabled: isextraDecorationsDisabled });
    }

    callback(response, status) {
        if (status === 'OK') {
            // const selt = this;
            let distance = 0;
            console.log(" response ", response);
            let tempdeliveryCharge = 0;
            let destinationAddresses = '';
            if (response.rows[0].elements[0].status === 'OK') {
                distance = response.rows[0].elements[0].distance.text;
                destinationAddresses = response.destinationAddresses[0];
                console.log(" before round distance ", distance);
                distance = parseFloat(distance.replace(/,/g, '.'));
                distance = distance > 1 ? Math.round(distance) : distance
                // distance = Math.round(distance);

                console.log(" distanceByGoogle =========== ", distance);
                if (distance <= 3 && distance > 0) {
                    tempdeliveryCharge = 3;
                    $(".more-then-fifteen").addClass('hidden');
                } else if (distance > 3 && distance <= 15) {
                    tempdeliveryCharge = distance;
                    $(".more-then-fifteen").addClass('hidden');
                } else {
                    tempdeliveryCharge = 0;
                    $(".more-then-fifteen").removeClass('hidden');
                }
            }
            if (distance > 0 && distance <= 15) {
                this.setState((prevState) => {
                    let newState = Object.assign({}, prevState);
                    newState.formData.deliveryDistance = distance;
                    newState.formData.deliveryAddress = destinationAddresses;
                    newState.formData.deliveryCharge = tempdeliveryCharge;
                    newState.formData.pickupDeliverySelection = true;
                    return newState;
                });
                // return newState;
            } else {
                console.log("distance more than 15");
                this.setState((prevState) => {
                    let newState = Object.assign({}, prevState);
                    newState.formData.deliveryDistance = 0;
                    newState.formData.deliveryCharge = 0;
                    newState.formData.deliveryAddress = '';
                    newState.formData.pickupDeliverySelection = false;
                    return newState;
                });
                $(".more-then-fifteen").removeClass('hidden');
            }
            this.calculatePrice();
        }
    }

    getDistanceFromLatLonInKm(lat1, lon1, lat2, lon2) {
        var R = 6371; // Radius of the earth in km
        var dLat = this.deg2rad(lat2 - lat1);  // deg2rad below
        var dLon = this.deg2rad(lon2 - lon1);
        var a =
            Math.sin(dLat / 2) * Math.sin(dLat / 2) +
            Math.cos(this.deg2rad(lat1)) * Math.cos(this.deg2rad(lat2)) *
            Math.sin(dLon / 2) * Math.sin(dLon / 2)
            ;
        var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        var d = R * c; // Distance in km
        return d;
    }

    deg2rad(deg) {
        return deg * (Math.PI / 180)
    }

    setFlavorAvailability = () => {
        var now = new Date();
        var isFlavorDisabled = this.state.isFlavorDisabled;
        let flavors = this.state.productPrices.flavors;

        for (var flvCode in flavors) {
            var flavor = flavors[flvCode];
            if (flavor.availableFrom == null || flavor.availableTo == null) {
                isFlavorDisabled[flavor.code] = false;
                continue;
            }

            let from = new Date(now.getFullYear(), flavor.availableFrom.month, flavor.availableFrom.date);
            let to = new Date(now.getFullYear(), flavor.availableTo.month, flavor.availableTo.date);
            isFlavorDisabled[flvCode] =
                (from < to && now < from)
                || (from < to && now > to)
                || (to < from && now < from && now > to);
        }

        this.setState({ isFlavorDisabled: isFlavorDisabled, isExtraFlavorDisabled: isFlavorDisabled });
    }

    openCakeForm = (e) => {
        this.setState({
            priceBoxActive: true,
            isSubmitInProgress: false
        });
        $(".custom-cake-button-container").animate({ width: '0%', });
        $(".custom-cake-button-container").css("display", "none");
        $(".custom-cake-form-container").css("display", "block");
        setTimeout(function () {
            $(".custom-cake-form-container").animate({ opacity: 1 });
        }, 400);
        $(".custom-cake-form-container").animate({ width: '100%' });
    };

    setTooltip = (tooltip) => {
        $('.extra_decoration_tooltip').attr('data-tooltip', tooltip);
        // $('.tooltipped').tooltip({ delay: 50, html: true });
        //DK upgrade
        var elems = document.querySelectorAll('.tooltipped');
        M.Tooltip.init(elems, {
            enterDelay: 50,
            html: true
        });
    };

    resetTooltip = () => {
        $('.extra_decoration_tooltip').attr('data-tooltip', 'Foto Hochladen');

        //DK old code comment
        // $('.tooltipped').tooltip({ delay: 50, html: true });

        //DK materialize css 
        var elemTooltip = document.querySelectorAll('.tooltipped');
        M.Tooltip.init(elemTooltip, {
            exitDelay: 50,
            html: true
        });
    };

    closeCakeForm = (e) => {
        console.log('closeCakeForm');
        this.setState({
            hiddenOrderId: '',
        });
        //DK upgrade
        // $("#cake_size").val('').change();
        $("#cake_size").val('').trigger('change');
        this.resetTooltip();

        var checkFnameLbl = $('#fnameLbl').hasClass('active');
        var checkLnameLbl = $('#lnameLbl').hasClass('active');
        var checkEmailLbl = $('#emailLbl').hasClass('active');
        var checkMobileLbl = $('#mobileLbl').hasClass('active');
        var checkCake_Txt = $('#cake_text_lbl').hasClass('active');
        var checkAdditional_remark = $('#additional_remark_lbl').hasClass('active');

        if (checkFnameLbl) {
            $('#fnameLbl').removeClass('active');
        }

        if (checkLnameLbl) {
            $('#lnameLbl').removeClass('active');
        }
        if (checkEmailLbl) {
            $('#emailLbl').removeClass('active');
        }
        if (checkMobileLbl) {
            $('#mobileLbl').removeClass('active');
        }
        if (checkCake_Txt) {
            $('#cake_text_lbl').removeClass('active');
        }
        if (checkAdditional_remark) {
            $('#additional_remark_lbl').removeClass('active');
        }

        this.setState({
            formData: {
                deliveryAddress: '',
                deliveryAddressPincod: '',
                deliveryCharge: 0,
                deliveryDistance: 0,
                partialAmount: '0,00',
                extraNonFoodPrice: '0,00',
                extraFoodPrice: '0,00',
                balanceAmount: '0,00',
                hiddenOrderId: '',
                fname: '',
                lname: '',
                mobile: '',
                email: '',
                pickupDeliverySelection: false,
                deliveryDate: '',
                deliveryTime: '',
                deliveryTimeHour: '',
                deliveryTimeMin: '',
                street: '',
                houseNo: '',
                city: '',
                shape: '',
                size: '',
                color: '',
                decoration: '',
                sauceColor: '',
                dripColor: '',
                flavor: '',
                decoColor: '',
                additional_flavor: [],
                extra_decoration: [],
                text: '',
                remarks: '',
                photo: '',
                cuponname: '',
                paymentType: 'cod',
                cuponPrice: 0,
            },
            checked: false,
            price: 0,
            totalTax: 0,
            taxAtDefaultRate: 0,
            taxAtCustomRate: 0,
            shapeAndStylePrice: 0,
            shapeAndStyleTaxAtDefaultRate: 0,
            shapeAndStyleTaxAtCustomRate: 0,
            designAndFlavorPrice: 0,
            designAndFlavorTaxAtDefaultRate: 0,
            designAndFlavorTaxAtCustomRate: 0,
            couponShapeAndStylePrice: 0,
            priceBoxActive: false,
            isCakeShapeFieldActive: {
                SHP01: false,
                SHP02: false,
                SHP03: false,
                SHP04: false,
                SHP99: false,
            },
            colorVisibility: null,
            sauceColorVisibility: { display: 'none' },
            decoColorVisibility: { display: 'none' },
            dripColorVisibility: { display: 'none' },
            isExtraFlavorDisabled: this.state.isFlavorDisabled,
            hideBankTransfer: false,
            isSubmitInProgress: false,
            isLoading: false,
            isCuponUsed: false,
            orderIdReference: 0
        });

        this.setupNavigation();

        //DK upgrade
        // $("#cake_text").blur();
        // $("#additional_remark").blur();
        // $("#deldate").blur();
        // // $("#deltime").blur();
        // $("#check_meCakeForm").blur();

        $("#cake_text").trigger('blur');
        $("#additional_remark").trigger('blur');
        $("#deldate").trigger('blur');
        // $("#deltime").blur();
        $("#check_meCakeForm").trigger('blur');

        //this.refs.regform.reset();
        this.regform.reset();
        this.setState({
            priceBoxActive: false,
        });

        //setTimeout(function () {
        $(".custom-cake-form-container").animate({ width: '0%', });
        $(".custom-cake-form-container").css("display", "none");
        $(".custom-cake-button-container").css("display", "block");
        //},0);

        setTimeout(function () {
            $(".custom-cake-button-container").animate({ opacity: 1 });
            $(".custom-cake-button-container").animate({ width: '100%' });
        }, 400);


        // $('.cash-notice').addClass('hidden');
        $('.cash-notice').removeClass('hidden');
        $(".five-day-notice").addClass('hidden');

        var reviewModalElem = document.querySelector('#modal_review');
        instanceReview = M.Modal.getInstance(reviewModalElem);
        instanceReview.close();

        this.props.history.goBack();
    };

    custcakebtn = () => {
        // $("#submitbtn").click(function () {
        $("#submitbtn").trigger('click', function () {
            $("#custcakeform_container").animate({ opacity: 0 });
            setTimeout(function () {
                $("#custcakeform_container").css("display", "none");
                $(".cakedesign").animate({ width: "100%", maxHeight: "100%" });
                $(".cakeform").animate({ minwidth: '0%' });
            }, 400);
        });
    };

    nextButtonClicked = () => {
        if (!this.canGoNext()) {
            M.toast({ html: `<span style="font-size:2.6rem; padding: 20px; line-height: 3rem;">Bitte zunächst Details ausfüllen</span>`, displayLength: 5000, inDuration: 2000, classes: 'red-Materialize' });
            return;
        }

        var current_fs = this.current_fs, next_fs; //fieldsets
        var animating; //flag to prevent quick multi-click glitches
        if ($("fieldset").index(current_fs) === 3) {
            $(".next_btn").hide();
            $(".submit_btn").show();
        }
        $(".previous_btn").show();

        if (animating) {
            return false;
        }

        if (this.state.formData.size === 'OTH') {
            this.handleSubmit();
            return;
        }

        next_fs = (current_fs.next().length) ? current_fs.next() : $('fieldset').first();
        animating = true;
        $('.cake-form-container').css({ overflowY: 'auto', overflowX: 'hidden' });

        next_fs = current_fs.next();
        //activate next step on progressbar using the index of next_fs
        next_fs.addClass("activeFS");
        current_fs.removeClass("activeFS");
        $("#progressbar li").eq($("fieldset").index(next_fs)).addClass("active");
        $("#progressbar li").eq($("fieldset").index(current_fs)).addClass("done");
        $(".desc-container div").eq($("fieldset").index(next_fs)).addClass("active");
        $(".desc-container div").eq($("fieldset").index(current_fs)).removeClass("active");

        //show the next fieldset
        next_fs.css({ opacity: 0 });
        next_fs.show();
        //hide the current fieldset with style
        current_fs.animate({ marginLeft: '-100%', opacity: 0 }, {
            duration: 400,
            complete: function () {
                current_fs.hide();
                animating = false;
            },
        });
        next_fs.animate({ marginLeft: '0', opacity: 1 }, {
            duration: 400,
            complete: function () {
                animating = false;
                current_fs = next_fs;
                $('.cake-form-container').css({ overflowY: 'visible', overflowX: 'visible' });
            },
        });

        this.current_fs = next_fs;
    };

    previousButtonClicked = () => {
        var current_fs = this.current_fs, previous_fs; //fieldsets
        var animating; //flag to prevent quick multi-click glitches

        if ($("fieldset").index(current_fs.prev()) === 0) {
            $(".previous_btn").hide();
        }
        if ($("fieldset").index(current_fs.prev()) !== 4) {
            $(".submit_btn").hide();
            $(".next_btn").show();
        }
        if (animating) return false;
        animating = true;
        $('.cake-form-container').css({ overflowY: 'auto', overflowX: 'hidden' });

        previous_fs = current_fs.prev();

        //de-activate current step on progressbar
        previous_fs.addClass("activeFS");
        current_fs.removeClass("activeFS");
        $("#progressbar li").eq($("fieldset").index(current_fs)).removeClass("active");
        $("#progressbar li").eq($("fieldset").index(previous_fs)).removeClass("done");
        $(".desc-container div").eq($("fieldset").index(previous_fs)).addClass("active");
        $(".desc-container div").eq($("fieldset").index(current_fs)).removeClass("active");

        //show the previous fieldset
        previous_fs.css({ opacity: 0 });
        previous_fs.show();
        //hide the current fieldset with style
        current_fs.animate({ marginLeft: '100%', opacity: 0 }, {
            complete: function () {
                current_fs.hide();
                animating = false;
            }
        });
        previous_fs.animate(
            { marginLeft: '0', opacity: 1 },
            {
                duration: 400,
                complete: function () {
                    animating = false;
                    current_fs = previous_fs;
                    $('.cake-form-container').css({ overflowY: 'visible', overflowX: 'visible' });
                }
            }
        );

        this.current_fs = previous_fs;
    }

    setupNavigation() {
        this.current_fs = $('#msform').children().first();

        this.current_fs.addClass('activeFS');
        this.current_fs.show();
        this.current_fs.css('margin-left', '0');
        this.current_fs.css('opacity', 1);
        $("#progressbar li").eq($("fieldset").index(this.current_fs)).removeClass("done");
        $("#progressbar li").eq($("fieldset").index(this.current_fs)).addClass("active");
        $(".desc-container div").eq($("fieldset").index(this.current_fs)).addClass("active");

        var next_fs = this.current_fs.next();
        while (next_fs.length > 0) {
            next_fs.removeClass('activeFS');
            next_fs.hide();
            next_fs.css('margin-left', '100%');
            next_fs.css('opacity', 0);
            $("#progressbar li").eq($("fieldset").index(next_fs)).removeClass("done");
            $("#progressbar li").eq($("fieldset").index(next_fs)).removeClass("active");
            $(".desc-container div").eq($("fieldset").index(next_fs)).removeClass("active");

            next_fs = next_fs.next();
        }

        $(".previous_btn").hide();
        $(".submit_btn").hide();
        $(".next_btn").show();
    }

    button_clicked(e) {
        this.setState({
            btn_clicked: true
        });
    }

    getPrice = () => {
        return this.state.price;
    }

    getOrderIdForReference = () => {
        return this.state.orderIdReference;
    }

    sendDataForAnalytics(data) {

        let datas = JSON.parse(data);
        let name = datas.size + " " + datas.shape + " " + datas.decoration + " " + datas.color;
        if (datas.photo) {
            name += "WP";
        }

        let allItem = [];
        allItem['category'] = "Custome Cake";
        allItem['name'] = name;
        allItem['price'] = datas.price;
        allItem['quantity'] = 1;
        let totalTax = datas.totalTax.replace(",", ".");
        totalTax = parseFloat(totalTax);


        if (dataForAnalytics === true) {
            window.gtagEcommerce('event', 'purchase', {
                "transaction_id": datas.orderId,
                "value": datas.price + totalTax,
                "currency": "EUR",
                "tax": totalTax,
                "shipping": datas.deliveryCharge,
                "items": [
                    {
                        // "id": "P12345",
                        "name": name,
                        // "list_name": name,
                        // "brand": "Google",
                        "category": "Custome Cake",
                        // "variant": "Black",
                        // "list_position": 1,
                        "quantity": 1,
                        "price": datas.price
                    }
                ]
            });
        }

    }

    handleSubmit = (e) => {
        if (e) e.preventDefault();
        this.setState({
            isSubmitInProgress: true
        });

        let t = this;
        let data = this.state.formData;
        data.deliveryTime = `${data.deliveryTimeHour}:${data.deliveryTimeMin}`;
        data.price = this.state.price;
        data.totalTax = this.state.totalTax.toLocaleString('de-DE', { minimumFractionDigits: 2, maximumFractionDigits: 2 });
        data.taxAtDefaultRate = this.state.taxAtDefaultRate.toLocaleString('de-DE', { minimumFractionDigits: 2, maximumFractionDigits: 2 });
        data.taxAtCustomRate = this.state.taxAtCustomRate.toLocaleString('de-DE', { minimumFractionDigits: 2, maximumFractionDigits: 2 });


        if (e && e.payment) {
            data.paymentDetails = e.payment;
        }
        if (data.extra_decoration.length === 0 || data.extra_decoration[0] !== "EDECO01") {
            data.photo = '';
        }
        if (!this.shouldColorBeVisible()) {
            data.color = '';
        }
        if (!this.shouldSauceColorBeVisible()) {
            data.sauceColor = '';
        }
        if (!this.shouldDecoColorBeVisible()) {
            data.decoColor = '';
        }
        if (!this.shouldDripColorBeVisible()) {
            data.dripColor = '';
        }
        //Paypal order being added for the first time
        if (e.storeType == 'paypal') {
            console.log(' this.state.orderIdReference from add ', e.orderId);
            this.setState({
                isSubmitInProgress: false,
                isLoading: true
            });

            data.orderId = e.orderId;
            data.paymentType = e.storeType;
            data = JSON.stringify(data);
            // console.log(" data  ========== ", data);
            console.log(`5:${this.state.formData.deliveryDate}`);
            $.ajax({
                type: 'POST',
                url: '/api/cakeOrder',
                data: data,
                // async: false,
                datatype: 'json',
                contentType: 'application/json; charset=UTF-8',
            }).done((res) => {
                if (res.error) {
                    // console.log(res);
                    M.toast({ html: '<span style="font-size:2.6rem; padding: 20px; line-height: 3rem;">Technical error occurred. Please try again later</span>', displayLength: 5000, inDuration: 2000, classes: 'red-Materialize' });
                    return;
                }

                this.setState({
                    isLoading: false
                });
            }).fail((jqXhr) => {
                //console.log('Order was not saved. ');
                t.closeCakeForm();
                M.toast({ html: '<span style="font-size:2.6rem; padding: 20px; line-height: 3rem;">Ein Server Fehler ist aufgetreten. Bitte Kontaktieren Sie uns Telefonisch +4951117507.</span>', displayLength: 5000, inDuration: 2000, classes: 'red-Materialize' });
                this.setState({
                    isLoading: false
                })
            });

        } else if (e.storeType === 'paypalTrnSuccess') {

            let temp = {};
            temp.orderIdForUpdate = e.orderIdForUpdate;
            temp.partialAmount = data.price.toLocaleString('de-DE', { minimumFractionDigits: 2, maximumFractionDigits: 2 });
            temp.status = 'ORDR';
            temp.price = data.price;
            temp.paymentDetails = data.paymentDetails;

            temp = JSON.stringify(temp);
            console.log(`6:${this.state.formData.deliveryDate}`);
            $.ajax({
                type: 'PUT',
                url: '/api/cakeOrderUpdate',
                data: temp,
                datatype: 'json',
                contentType: 'application/json; charset=UTF-8',
            }).done((res) => {

                if (res.error) {
                    console.log(res);
                    M.toast({ html: '<span style="font-size:2.6rem; padding: 20px; line-height: 3rem;">A technical error has occurred. Please try again later</span>', displayLength: 5000, inDuration: 2000, classes: 'red-Materialize' });
                    return;
                }

                if (this.state.formData.cuponname !== null) {
                    let tempCode = {};
                    tempCode.cuponname = this.state.formData.cuponname;
                    fetch('/api/getCountForCampaign', {
                        method: 'PUT',
                        headers: { 'Content-Type': 'application/json' },
                        body: JSON.stringify({ example: tempCode }),
                    }).then(res => {
                        console.log('res update', res);
                        return res;
                    }).catch(err => {
                        console.log('errr', err);
                    });
                }

                t.closeCakeForm();

                Swal.fire(
                    `Vielen Dank! Ihre Bestellungs Nr:<b style="color:blue;">&nbsp;&nbsp;${e.orderIdForUpdate}</b>`,
                    `Bitte prüfen Sie ihre E-Mail und die Bestellung nochmal sorgfältig und folgen Sie den dortigen Anweisungen.`,
                    'success'
                )

                this.setState({
                    deliveryTimeHour: '',
                    deliveryTimeMin: ''
                });

                this.sendDataForAnalytics(data);

            }).fail(function (jqXhr) {

                t.closeCakeForm();
            });
        } else if (e.storeType == 'paypalTrnCancelled') {
            /** That means either user closed the Paypal window or something went wrong. */
            data.orderIdForUpdate = e.orderIdForUpdate;
            data.status = 'CLSE';
            data = JSON.stringify(data);
            $.ajax({
                type: 'PUT',
                url: `/api/updateCakeOrderStatus?orderId=${e.orderIdForUpdate}`,
                data: data,
                datatype: 'json',
                contentType: 'application/json; charset=UTF-8',
            }).done((res) => {
                console.log(" Paypal Order updated to CLSE! ", res);

            }).fail((err) => {
                console.log('Error occured', err);
            });

        } else if (data.paymentType == 'mollie') {
            // alert($('#hiddenOrderId').val())
            if (this.state.orderIdReference === 0) {
                $.ajax({
                    url: '/api/getNextSequenceValue',
                    async: false,
                    type: 'GET',
                    dataType: 'json', // added data type
                    success: function (res) {
                        data.orderId = new Date().getFullYear() * 100000 + res.value.sequence_value;
                        this.closeCakeForm();
                    }
                }).fail((jqXhr) => {
                    console.log('jqXhr', jqXhr);

                    this.closeCakeForm();
                });
            } else {
                data.orderId = this.state.orderIdReference;
            }

            this.setState({
                isLoading: true
            });

            data = JSON.stringify(data);
            $.ajax({
                type: 'POST',
                url: '/api/cakeOrder',
                data: data,
                datatype: 'json',
                contentType: 'application/json; charset=UTF-8',
            }).done((res) => {
                console.log(" res ", res);
                window.open(res._links.checkout.href, '_self');
            });
        }
        else {
            // alert($('#hiddenOrderId').val())
            console.log(' cash ');
            if ($('#hiddenOrderId').val() == '') {
                $.ajax({
                    url: '/api/getNextSequenceValue',
                    async: false,
                    type: 'GET',
                    dataType: 'json', // added data type
                    success: function (res) {
                        data.orderId = new Date().getFullYear() * 100000 + res.value.sequence_value;
                    }
                }).fail((jqXhr) => {
                    console.log('jqXhr', jqXhr);

                    this.closeCakeForm();
                });
            } else {
                data.orderId = this.state.orderIdReference;
            }

            this.setState({
                isLoading: true
            });

            data = JSON.stringify(data);
            $.ajax({
                type: 'POST',
                url: '/api/cakeOrder',
                data: data,
                datatype: 'json',
                contentType: 'application/json; charset=UTF-8',
            }).done((res) => {
                if (this.state.formData.cuponname !== null) {
                    // let tempCode = this.state.formData;
                    let cuponname = this.state.formData.cuponname;
                    fetch('/api/getCountForCampaign', {
                        method: 'PUT',
                        headers: { 'Content-Type': 'application/json' },
                        body: JSON.stringify({ cuponname: cuponname }),
                    }).then(res => {
                        console.log('res:', res);
                        return res;
                    }).catch(err => {
                        console.log('errr', err);
                    });
                }

                Swal.fire(
                    `Vielen Dank! Ihre Bestellungs Nr:<b style="color:blue;">&nbsp;&nbsp;${res.orderId}</b>`,
                    `Bitte prüfen Sie ihre E-Mail und die Bestellung nochmal sorgfältig und folgen Sie den dortigen Anweisungen.`,
                    'success'
                )

                this.setState({
                    deliveryTimeHour: '',
                    deliveryTimeMin: '',
                    decoration: '',
                    shape: '',
                    color: '',
                    sauceColor: ''
                });

                this.setState({
                    isLoading: false
                });
                this.sendDataForAnalytics(data);
                t.closeCakeForm();
            }).fail(function (jqXhr) {
                M.toast({ html: '<span style="font-size:2.6rem; padding: 20px; line-height: 3rem;">Ein Server Fehler ist aufgetreten. Bitte Kontaktieren Sie uns Telefonisch +4951117507.</span>', displayLength: 5000, inDuration: 2000, classes: 'red-Materialize' });

                t.closeCakeForm();
            });
        }

    };



    refreshFormData = () => {
        let deliveryDate = $('input#deldate').val();
        // let deliveryTime = $('input#deltime').val();
        this.setState((prevState) => {
            let newState = Object.assign({}, prevState);
            newState.formData['deliveryDate'] = deliveryDate;
            // newState.formData['deliveryTime'] = deliveryTime;
            return newState;
        });
        console.log(`3: ${deliveryDate}`);
    };

    calculatePrice = () => {
        console.log(" Cake Form deliveryCharg ", this.state.formData.deliveryCharge);
        console.log(`8: ${this.state.formData.deliveryDate}`);
        this.setState(
            PriceCalculator.calculatePrice(
                this.state.productPrices,
                this.state.formData,
                this.state.formData.cuponPrice,
                this.state.formData.deliveryCharge,
            )
        );
    };

    shouldColorBeVisible = () => {
        return !(this.state.formData.decoration === 'DRN02'
            || this.state.formData.decoration === 'DRN07'
            || this.state.formData.decoration === 'DRN08'
            || this.state.formData.decoration === 'DRN09');
    };

    shouldSauceColorBeVisible = () => {
        return this.state.formData.decoration === 'DRN02'
            || this.state.formData.decoration === 'DRN05'
            || this.state.formData.decoration === 'DRN06';
    };

    shouldDecoColorBeVisible = () => {
        return this.state.formData.decoration === 'DRN10'
            || this.state.formData.decoration === 'DRN11';
    }

    shouldDripColorBeVisible = () => {
        return this.state.formData.decoration === 'DRN13';
    }

    getName = (fromObject, code) => {
        return fromObject[code] ? fromObject[code].name : '';
    };

    trimExtraComma = (str) => {

        if (str.length > 1 && str.slice(str.length - 2) === ', ') {
            return str.substring(0, str.length - 2);
        } else {
            return str;
        }
    };

    checkmailvali = (text) => {
        const regexp = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return regexp.test(text);
    }

    whitespacesHandle(value) {
        return value.replace(/\s/g, '')
        //return value.trim();
    }

    canGoNext = () => {
        var currentFieldsetIndex = $('#msform').find('.activeFS').index();
        console.log(" currentFieldsetIndex ", currentFieldsetIndex);
        var formData = this.state.formData;
        console.log(" formData ", this.state);

        if (currentFieldsetIndex === 4) {
            console.log(" currentFieldsetIndex === 4 ");
            var temp = this.checkmailvali(formData.email);
            var temp1 = this.whitespacesHandle(formData.fname);
            formData.fname = temp1;
            var temp2 = this.whitespacesHandle(formData.lname);
            formData.lname = temp2;
            return formData.fname !== ''
                && formData.lname !== ''
                && formData.email !== ''
                && temp === true
                && formData.mobile !== '';
        }

        if (currentFieldsetIndex === 3) {

            if (this.state.checked === true) {
                //console.log('null check', formData.deliveryAddress);
                return formData.deliveryAddress !== ''
                    && formData.deliveryDate !== ''
                    && formData.deliveryTimeHour !== ''
                    && formData.deliveryTimeMin !== '';
            }
            else {
                return formData.deliveryDate !== ''
                    && formData.deliveryTimeHour !== ''
                    && formData.deliveryTimeMin !== '';
            }
        }

        if (currentFieldsetIndex === 0) {
            var cakeDecoFieldsValid = false;

            if (formData.size === 'OTH') {
                cakeDecoFieldsValid = formData.shape === 'SHP99';
            } else {
                cakeDecoFieldsValid = formData.size !== ''
                    && formData.shape !== ''
                    && formData.decoration !== '';

                if (this.shouldColorBeVisible()) {
                    cakeDecoFieldsValid = cakeDecoFieldsValid && formData.color !== '';
                }

                if (this.shouldSauceColorBeVisible()) {
                    cakeDecoFieldsValid = cakeDecoFieldsValid && formData.sauceColor !== '';
                }

                if (this.shouldDecoColorBeVisible()) {
                    cakeDecoFieldsValid = cakeDecoFieldsValid && formData.decoColor !== '';
                }

                if (this.shouldDripColorBeVisible()) {
                    cakeDecoFieldsValid = cakeDecoFieldsValid && formData.dripColor !== '';
                }
            }

            return cakeDecoFieldsValid;
        }

        if (currentFieldsetIndex === 1) {
            return formData.flavor !== '';
        }

        if (currentFieldsetIndex === 2) {
            //return this.refs.check_meCakeForm.checked === true;
            return this.check_meCakeForm.checked == true;
        }

        return false;
    };


    handleCheck = () => {
        if (this.state.checked) {
            this.setState((prevState) => {
                let newState = Object.assign({}, prevState);
                newState.formData.deliveryDistance = 0;
                newState.formData.deliveryAddress = '';
                newState.formData.deliveryCharge = 0;
                newState.formData.pickupDeliverySelection = false;
                newState.checked = false;
                return newState;
            }, () => { this.calculatePrice() });
        } else {
            this.setState((prevState) => {
                let newState = Object.assign({}, prevState);
                newState.formData.deliveryDistance = 0;
                newState.formData.deliveryAddress = '';
                newState.formData.deliveryCharge = 0;
                newState.formData.pickupDeliverySelection = true;
                newState.checked = true;
                return newState;
            }, () => { this.calculatePrice() });

        }
    }

    showReviewModal = () => {
        // if (this.refs.check_meCakeForm.checked === false) {
        if (this.check_meCakeForm.checked == false) {
            alert('Bitte pflicht Felder ausfüllen');
            return;
        }

        if (!this.canGoNext()) {
            M.toast({ html: `<span style="font-size:2.6rem; padding: 20px; line-height: 3rem;">Bitte zunächst Details ausfüllen</span>`, displayLength: 5000, inDuration: 2000, classes: 'red-Materialize' });
            return;
        }

        //DK team upgrade
        // $('#modal_review').modal('open');
        //Start Modal review
        var reviewModalElem = document.querySelector('#modal_review');
        instanceReview = M.Modal.getInstance(reviewModalElem);
        instanceReview.open();
    }

    updateFieldView = (size) => {
        /**
         * Updates the isCakeShapeFieldActive state oject which
         * toggles the cake-size form fields
         */

        let newIsCakeShapeFieldActive = {};
        for (let shape in this.state.isCakeShapeFieldActive) {
            newIsCakeShapeFieldActive[shape] =
                this.state.productPrices.priceRules[`${shape}_${size}`] !== undefined;
        }

        this.setState({
            isCakeShapeFieldActive: newIsCakeShapeFieldActive
        }, () => {
            //Old materialize css select
            //$('select').material_select();
            // const M = window.M;
            var selectElm = document.querySelectorAll("select");
            M.FormSelect.init(selectElm);
        });
    };

    handleCouponCode = (event) => {
        //console.log('handleCouponCode');
        if (this.state.formData.cuponname.length >= 9 && this.state.formData.cuponname.length <= 12) {
            //console.log('handleCouponCode in if ...............',);
            let tempdata = {};
            tempdata.cuponname = this.state.formData.cuponname;
            tempdata.price = this.state.price;
            this.setState({
                isLoading: true
            });
            fetch('/api/getCampaignList', {
                method: 'POST',
                headers: { 'Content-Type': 'application/json' },
                body: JSON.stringify({ example: tempdata }),
            })
                .then((result, err) => {
                    if (!result.ok) { throw result }
                    return result.json();
                })
                .then((tempdata) => {
                    this.setState({
                        CampaignList: tempdata
                    });
                    if (this.state.CampaignList.message == 'No Such Coupon Found!' || this.state.CampaignList.message == 'Coupon expired!' || this.state.CampaignList.message == 'Coupon code finished!') {
                        M.toast({ html: `<span style="font-size:2.6rem; padding: 20px; line-height: 3rem;">Rabatt-Gutschein ist ungültig!</span>`, displayLength: 5000, inDuration: 5000, classes: 'red-Materialize' });
                        this.state.formData.cuponname = '';
                    }
                    if (this.state.CampaignList.message == 'Price Amount for this should be greater') {
                        M.toast({ html: `<span style="font-size:2.6rem; padding: 20px; line-height: 3rem;">Mindestbestellwert soll 15€ sein!</span>`, displayLength: 5000, inDuration: 5000, classes: 'red-Materialize' });
                        this.state.formData.cuponname = '';
                    }
                    if (this.state.CampaignList[0][0].Code === this.state.formData.cuponname && this.state.isCuponUsed === false) {
                        this.state.couponShapeAndStylePrice = this.state.shapeAndStylePrice;
                        this.state.price = this.state.price - parseInt(this.state.CampaignList[0][0].DiscValue);
                        this.state.formData.cuponPrice = this.state.CampaignList[0][0].DiscValue;
                        this.calculatePrice();
                        this.state.couponShapeAndStylePrice = this.state.shapeAndStylePrice + parseInt(this.state.CampaignList[0][0].DiscValue);
                        this.setState({
                            price: this.state.price,
                            cuponPrice: this.state.CampaignList[0][0].DiscValue,
                            isCuponUsed: true,
                        });
                        this.state.formData.cuponPrice = this.state.CampaignList[0][0].DiscValue;
                        this.handleDataChange();
                    }
                })
                .catch(err => {
                    console.log(err);
                    err.then(errmsg => {
                        console.log("cant fetch product prices " + errmsg);
                    })
                });
            this.setState({
                isLoading: false
            });
        } else {
            this.state.formData.cuponname = '';
        }
    };

    setNakedcolorAvailability = () => {
        var isNakedcolorDisabled = this.state.isNakedcolorDisabled;
        for (var index in isNakedcolorDisabled) {
            if (index === 'CLR01') {
                isNakedcolorDisabled[index] = true;
                continue;
            }
            else {
                isNakedcolorDisabled[index] = false;
                continue;
            }
        }
        this.setState({
            isNakedcolorDisabled: isNakedcolorDisabled
        }, () => {
            //Old materialize css select
            //$('select').material_select();
            // const M = window.M;
            var selectElm = document.querySelectorAll("select");
            M.FormSelect.init(selectElm);
        });
    }

    setNakedcolorAvailabilityreset = () => {
        var isNakedcolorDisabled = this.state.isNakedcolorDisabled;
        for (var index in isNakedcolorDisabled) {
            isNakedcolorDisabled[index] = true;
            continue;
        }
        this.setState({
            isNakedcolorDisabled: isNakedcolorDisabled
        }, () => {
            //Old materialize css select
            //$('select').material_select();
            // const M = window.M;
            var selectElm = document.querySelectorAll("select");
            M.FormSelect.init(selectElm);
        });
    }

    initPaypalBtn = () => {
        console.log(1);
        // console.log($(".xcomponent-outlet"));
        // console.log($(".xcomponent-outlet").style);
        // console.log($(".xcomponent-outlet").style.height);

        document.getElementsByClassName("xcomponent-outlet")[0].style.width = "350px";
        // $(".xcomponent-outlet").style.width = "350px";
        console.log(2);
        document.getElementsByClassName("xcomponent-outlet")[0].style.height = "80px";
        // $(".xcomponent-outlet").style.height = "80px";
        console.log(3);

        // $(".paypal-button-container").style.fontSize = "20px";
        console.log(4);
        // $(".paypal-button").style.maxHeight = "90px";
        document.getElementsByClassName("paypal-button")[0].style.maxHeight = "90px";
        console.log(5);
        // $(".paypal-button").style.height = "90px";
        document.getElementsByClassName("paypal-button")[0].style.height = "90px";
        console.log(6);
        // $(".paypal-button-label-container").style.maxHeight = "80px";
        // console.log(7);
        // $(".paypal-button-label-container").style.height = "60px";
        // console.log(8);
        console.log("Paypal Button styled.....");
    }

    handleDataChange = (event) => {
        /**
         * Handles data change of all form input fields
         */

        if (typeof event === 'undefined')
            return;

        let key = event.target.name;
        let value = '';

        if (key === 'cuponname') {
            this.state.formData.cuponname = event.target.name;
            this.setState({
                isCuponUsed: false,
            });
            this.state.formData.cuponPrice = 0;

        }
        this.state.couponShapeAndStylePrice = this.state.shapeAndStylePrice;

        if (key === 'paymentType') {
            if (this.state.formData.cuponname !== null) {
                this.setState({
                    isCuponUsed: false,
                });
                this.handleCouponCode();
            }
            var newOrderId = 0;
            if (event.target.value === 'paypal') {
                $('.cash-notice').addClass('hidden');

                if ($('#hiddenOrderId').val() === '') {
                    console.log(`4:${this.state.formData.deliveryDate}`);
                    $.ajax({
                        url: '/api/getNextSequenceValue',
                        async: false,
                        type: 'GET',
                        dataType: 'json', // added data type
                        success: function (res) {
                            newOrderId = res.value.sequence_value;
                            // $('#hiddenOrderId').val(res.value.sequence_value);
                        }
                    }).fail((jqXhr) => {
                        console.log('jqXhr', jqXhr);
                        this.closeCakeForm();
                        this.setState({
                            isLoading: false
                        })
                    });

                    // console.log('Outside orderId ',newOrderId);

                    this.setState({
                        orderIdReference: new Date().getFullYear() * 100000 + newOrderId,
                        hiddenOrderId: new Date().getFullYear() * 100000 + newOrderId
                    });

                    this.handleSubmit({
                        preventDefault: function () { },
                        storeType: 'paypal',
                        orderId: new Date().getFullYear() * 100000 + newOrderId
                    });
                }
            } else {
                $('.cash-notice').removeClass('hidden');
            }

            // setTimeout(() => {
            //     this.initPaypalBtn();
            // }, 1000);

            // this.initPaypalBtn();

            // $('#hiddenOrderId').val(10);
            // console.log('newOrderId', newOrderId);
        }

        if (event.target.hasAttribute('multiple') && (key === 'additional_flavor' || key === 'extra_decoration')) {
            // Handling all form inputs that are multiselect
            let multiValues = Array.from(event.target.selectedOptions);
            value = [];
            multiValues.map((ele, idx) => {
                if (ele.value !== '') {
                    value.push(ele.value);
                }
            });
        } else if (key === 'pickupDeliverySelection') {
            alert(" pickupDeliverySelection " + event.target.checked)
            value = event.target.checked;
        } else {
            value = event.target.value;
        }

        this.setState((prevState) => {
            let newState = Object.assign({}, prevState);
            newState.formData[key] = value;
            return newState;
        }, () => {
            this.calculatePrice();
        });

        if (key === 'size') {
            if (value === 'OTH' && this.state.formData.shape !== 'SHP99') {
                this.setState((prevState) => {
                    let newState = Object.assign({}, prevState);
                    newState.formData.shape = '';
                    return newState;
                });
            } else if (value.startsWith('NO') && this.state.formData.shape === 'SHP03') {
                this.setState((prevState) => {
                    let newState = Object.assign({}, prevState);
                    newState.formData.shape = '';
                    return newState;
                });
            } else if (value.startsWith('EB') && this.state.formData.shape !== 'SHP03') {
                this.setState((prevState) => {
                    let newState = Object.assign({}, prevState);
                    newState.formData.shape = '';
                    return newState;
                });
            }
            this.updateFieldView(value);
        }

        if (key === 'flavor') {
            let newIsExtraFlavorDisabled = {};
            for (let extraFlavor in this.state.isExtraFlavorDisabled) {
                newIsExtraFlavorDisabled[extraFlavor] =
                    this.state.isFlavorDisabled[extraFlavor]
                    || extraFlavor === value;
            }

            this.setState((state) => {
                for (var i = 0; i < state.formData.additional_flavor.length; ++i) {
                    if (state.formData.additional_flavor[i] === value) {
                        state.formData.additional_flavor.splice(i, 1);
                        break;
                    }
                }

                state.isExtraFlavorDisabled = newIsExtraFlavorDisabled;
                return state;
            }, () => {
                //Old materialize css select
                //$('select').material_select();
                // const M = window.M;
                var selectElm = document.querySelectorAll("select");
                M.FormSelect.init(selectElm);
                this.calculatePrice();
            });
        }
        if (key === 'decoration') {
            this.setState((state) => {
                state.colorVisibility = this.shouldColorBeVisible() ? null : { display: 'none' };
                state.sauceColorVisibility = this.shouldSauceColorBeVisible() ? null : { display: 'none' };
                state.decoColorVisibility = this.shouldDecoColorBeVisible() ? null : { display: 'none' };
                state.dripColorVisibility = this.shouldDripColorBeVisible() ? null : { display: 'none' };

                return state;
            });
            if (event.target.value === 'DRN12') {
                // console.log('in decoration DRN12');
                this.setNakedcolorAvailability();
            } else {
                //console.log('in else if decoration not ! DRN12');
                this.setNakedcolorAvailabilityreset();
            }
        }
        if (key === 'color' && value === 'CLR99') {
            //   if()
            alert('Bitte Farbe im Anschluss der Bestellung unter "Bemerkung" schreiben.');
            //   else
            //      alert ('Farbe in etwa. Genaue Farbvorgabe bitte Muster abgeben vor Produktion.');
        }
    };

    readURL = (e) => {
        if (e.target.files && e.target.files[0]) {
            var reader = new FileReader();

            reader.onload = (e) => {
                var y = e.target.result;
                var x = `<img src="${e.target.result}" id="upimg" width="200px"/>`;
                this.setTooltip(x);
                this.setState((prevState) => {
                    let newState = Object.assign({}, prevState);
                    newState.formData.photo = y;
                    return newState;
                })
            };

            reader.readAsDataURL(e.target.files[0]);
        }
        else {
            this.resetTooltip();
        }
    };

    render() {
        let extra_decoration = '';
        console.log(" this.state.formData.extra_decoration ", this.state.formData.extra_decoration);
        this.state.formData.extra_decoration.map((decor_code, idx) => {
            extra_decoration += this.getName(this.state.productPrices.extraDecorations, decor_code) + ", ";
        });
        extra_decoration = this.trimExtraComma(extra_decoration);

        let extra_flavor = '';
        this.state.formData.additional_flavor.map((flavor_code, idx) => {
            extra_flavor += this.getName(this.state.productPrices.extraFlavors, flavor_code) + ", ";
        });
        extra_flavor = this.trimExtraComma(extra_flavor);

        let displayImage = false;
        this.state.formData.extra_decoration.map((decor, idx) => {
            if (decor === "EDECO01") {
                displayImage = true;
            }
        });

        // let addressValue;
        // // let disableAddress;
        // let addressIconColor;
        // if (this.state.checked) {
        //     addressIconColor = '';
        //     // disableAddress = false;
        // } else {
        //     addressIconColor = 'gray';
        //     addressValue = '';
        //     // disableAddress = true;
        // }

        // console.log(" addressIconColor ", addressIconColor);



        return (
            <ReactScoped encapsulation={ViewEncapsulation.Emulated} styles={[styles]}>
                {this.state.isLoading && <div className="overlayForSpinner">
                    <div className="overlay__inner">
                        <div className="overlay__content"><span className="spinner"></span></div>
                    </div>
                </div>
                }
                <section className="Bestelltorte section scrollspy" id="Bestelltorte">
                    <div className="custom-cake-container row">
                        {/* <div className="custom-cake-button-container left">
                            <div className="custom-cake-left-wrapper parallax-container">
                                <div className="overlay">
                                    <div className="container">
                                        <LazyLoad once height={"100%"}>
                                            <img className="cake_icon" alt="cake_icon" src={cake_icon} />
                                        </LazyLoad>
                                        <a id="custcakebtn" className={"custom_btn flow-text " + (this.state.btn_clicked ? 'clicked' : '')}
                                            onMouseDown={this.button_clicked}
                                            onMouseUp={() => { this.setState({ btn_clicked: false }) }}
                                            onClick={this.openCakeForm}>
                                            Ihre Torte gestalten!
                                        </a>
                                        <div className="section_borders">
                                            <div className="top-border leftBorder"></div>
                                            <div className="top-border rightBorder"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div> */}

                        <div className="custom-cake-form-container right">
                            <div className="row cake-container">

                                <div className="col l4 m4 offset-l8 offset-m8 hide-on-small-only cake-container-right">

                                </div>

                                <div className="row cake-form-wrapper">
                                    <div className='col l10 m10 offset-l1 offset-m1 s10 offset-s1 cake-form-container'>
                                        <form id="msform"
                                            // ref="regform" 
                                            ref={(el) => {
                                                this.regform = el;
                                            }}
                                            onSubmit={this.handleSubmit}>

                                            <fieldset className="decoration">
                                                <h4 className="fs-title">Tortendekor und -form</h4>
                                                <div className="input-field col s10 offset-s1">
                                                    <i className="material-icons prefix" >bubble_chart</i>
                                                    <select id="cake_size" type="text" value={this.state.formData.size} className="validate" style={{ height: '500px' }} required="" aria-required="true" name="size">
                                                        <option value="" disabled >Tortengrösse</option>
                                                        <option value="NO1">6-8 Stück(1No)</option>
                                                        <option value="NO2">8-10 Stück(2No)</option>
                                                        <option value="NO3">10-12 Stück(3No)</option>
                                                        <option value="EB0">16-20 Stück(EB0) </option>
                                                        <option value="EB1">25-30 Stück(EB1) </option>
                                                        <option value="EB2">30-40 Stück(EB2)</option>
                                                        <option value="EB3">50-60 Stück(EB3)</option>
                                                    </select>
                                                    {/*<option value="OTH">andere Größe</option>*/}
                                                </div>
                                                <div className="input-field col s10 offset-s1 cake_shape_field">
                                                    <i className="material-icons prefix">brightness_high</i>
                                                    <select id="cake_shape" type="text" className="validate" required="" aria-required="true" name="shape" value={this.state.formData.shape}>
                                                        <option value="" disabled selected >Tortenform</option>
                                                        <option value="SHP01" disabled={!this.state.isCakeShapeFieldActive.SHP01}>Rund</option>
                                                        <option value="SHP02" disabled={!this.state.isCakeShapeFieldActive.SHP02}>Quadrat</option>
                                                        <option value="SHP03" disabled={!this.state.isCakeShapeFieldActive.SHP03}>Rechteck</option>
                                                        <option value="SHP04" disabled={!this.state.isCakeShapeFieldActive.SHP04}>Herz</option>
                                                        {/*<option value="SHP99" disabled={!this.state.isCakeShapeFieldActive.SHP99}>Nach Wunsch/Fondantdekor/Hochzeit</option>*/}
                                                    </select>
                                                </div>

                                                <div className="input-field col s10 offset-s1">
                                                    <i className="material-icons prefix" >cake</i>
                                                    <select id="cake_decor" type="text" value={this.state.formData.decoration} className="validate" required="" aria-required="true" name="decoration">
                                                        <option value="" disabled>Tortendeko</option>
                                                        <option value="DRN01" data-icon={smooth} className="left circle">Glatt</option>
                                                        <option value="DRN02" data-icon={jelly} className="left circle">Sosse</option>
                                                        <option value="DRN03" data-icon={combed} className="left circle">Gekämmt</option>
                                                        {/*<option value="DRN04" data-icon={icing} className="left circle">Icing</option>*/}
                                                        <option value="DRN05" data-icon={smoothsauce} className="left circle">Glatt mit Sosse</option>
                                                        <option value="DRN06" data-icon={combedsauce} className="left circle">Gekämmt mit Sosse</option>
                                                        <option value="DRN07" data-icon={raspelcreme} className="left circle">Raspel Creme</option>
                                                        <option value="DRN08" data-icon={raspelbraun} className="left circle">Raspel Braun</option>
                                                        <option value="DRN09" data-icon={raspelrosa} className="left circle">Raspel Rosa</option>
                                                        <option value="DRN10" data-icon={smooth} className="left circle">Glatt mit Deko</option>
                                                        <option value="DRN11" data-icon={combed} className="left circle">Gekämmt mit Deko</option>
                                                        <option value="DRN12" data-icon={Naked} className="left circle">Naked cake</option>
                                                        <option value="DRN13" data-icon={Drip} className="left circle">Drip cake</option>
                                                    </select>
                                                </div>

                                                <div className="input-field col s10 offset-s1" style={this.state.colorVisibility}>
                                                    <i className="material-icons prefix" >color_lens</i>
                                                    <select id="cake_color" type="text" value={this.state.formData.color} className="validate" required="" aria-required="true" name="color">
                                                        <option value="" disabled>Tortenfarbe</option>
                                                        <option value="CLR01" disabled={!this.state.isNakedcolorDisabled.CLR01} data-icon="data:image/svg+xml;utf8,<svg xmlns='http://www.w3.org/2000/svg' version='1.1' xmlns:xlink='http://www.w3.org/1999/xlink' width='40' height='40' style='background:white;'></svg>" className="left circle">Weiss</option>
                                                        <option value="CLR02" disabled={!this.state.isNakedcolorDisabled.CLR02} data-icon="data:image/svg+xml;utf8,<svg xmlns='http://www.w3.org/2000/svg' version='1.1' xmlns:xlink='http://www.w3.org/1999/xlink' width='40' height='40' style='background:pink;'></svg>" className="left circle">Rosa</option>
                                                        <option value="CLR03" disabled={!this.state.isNakedcolorDisabled.CLR03} data-icon="data:image/svg+xml;utf8,<svg xmlns='http://www.w3.org/2000/svg' version='1.1' xmlns:xlink='http://www.w3.org/1999/xlink' width='40' height='40' style='background:Green;'></svg>" className="left circle">Grün</option>
                                                        <option value="CLR04" disabled={!this.state.isNakedcolorDisabled.CLR04} data-icon="data:image/svg+xml;utf8,<svg xmlns='http://www.w3.org/2000/svg' version='1.1' xmlns:xlink='http://www.w3.org/1999/xlink' width='40' height='40' style='background:Blue;'></svg>" className="left circle">Blau</option>
                                                        <option value="CLR05" disabled={!this.state.isNakedcolorDisabled.CLR05} data-icon="data:image/svg+xml;utf8,<svg xmlns='http://www.w3.org/2000/svg' version='1.1' xmlns:xlink='http://www.w3.org/1999/xlink' width='40' height='40' style='background:#e6dec7;'></svg>" className="left circle">Creme</option>
                                                        <option value="CLR06" disabled={!this.state.isNakedcolorDisabled.CLR06} data-icon="data:image/svg+xml;utf8,<svg xmlns='http://www.w3.org/2000/svg' version='1.1' xmlns:xlink='http://www.w3.org/1999/xlink' width='40' height='40' style='background:LightBlue;'></svg>" className="left circle">Hellblau</option>
                                                        <option value="CLR07" disabled={!this.state.isNakedcolorDisabled.CLR07} data-icon="data:image/svg+xml;utf8,<svg xmlns='http://www.w3.org/2000/svg' version='1.1' xmlns:xlink='http://www.w3.org/1999/xlink' width='40' height='40' style='background:Brown;'></svg>" className="left circle">Braun</option>
                                                        <option value="CLR08" disabled={!this.state.isNakedcolorDisabled.CLR08} data-icon="data:image/svg+xml;utf8,<svg xmlns='http://www.w3.org/2000/svg' version='1.1' xmlns:xlink='http://www.w3.org/1999/xlink' width='40' height='40' style='background:Purple;'></svg>" className="left circle">Lila</option>
                                                        <option value="CLR99" disabled={!this.state.isNakedcolorDisabled.CLR99} data-icon="data:image/svg+xml;utf8,<svg xmlns='http://www.w3.org/2000/svg' version='1.1' xmlns:xlink='http://www.w3.org/1999/xlink' width='40' height='40' style='background:Gray;'></svg>" className="left circle">Andere</option>
                                                    </select>
                                                </div>

                                                <div className="input-field col s10 offset-s1" style={this.state.sauceColorVisibility}>
                                                    <i className="material-icons prefix" >color_lens</i>
                                                    <select id="sauce_color" type="text" value={this.state.formData.sauceColor} className="validate" required="" aria-required="true" name="sauceColor">
                                                        <option value="" disabled>Sossenfarbe</option>
                                                        <option value="CLFV01" data-icon={Raspberry_kuchen} className="left circle">Sosse Rot</option>
                                                        <option value="CLFV02" data-icon={caramel_wallnuss} className="left circle">Karamell</option>
                                                        <option value="CLFV03" data-icon={banane_kuchen} className="left circle">Banane</option>
                                                        <option value="CLFV04" data-icon={fruitgar} className="left circle">Sosse Grün</option>
                                                        <option value="CLFV16" data-icon={cholate} className="left circle">Schokolade</option>
                                                        <option value="CLFV05" data-icon="data:image/svg+xml;utf8,<svg xmlns='http://www.w3.org/2000/svg' version='1.1' xmlns:xlink='http://www.w3.org/1999/xlink' width='40' height='40' style='background:white;'></svg>" className="left circle">Weiss</option>
                                                        <option value="CLFV06" data-icon="data:image/svg+xml;utf8,<svg xmlns='http://www.w3.org/2000/svg' version='1.1' xmlns:xlink='http://www.w3.org/1999/xlink' width='40' height='40' style='background:pink;'></svg>" className="left circle">Rosa</option>
                                                        <option value="CLFV07" data-icon="data:image/svg+xml;utf8,<svg xmlns='http://www.w3.org/2000/svg' version='1.1' xmlns:xlink='http://www.w3.org/1999/xlink' width='40' height='40' style='background:Green;'></svg>" className="left circle">Grün</option>
                                                        <option value="CLFV08" data-icon="data:image/svg+xml;utf8,<svg xmlns='http://www.w3.org/2000/svg' version='1.1' xmlns:xlink='http://www.w3.org/1999/xlink' width='40' height='40' style='background:Blue;'></svg>" className="left circle">Blau</option>
                                                        <option value="CLFV09" data-icon="data:image/svg+xml;utf8,<svg xmlns='http://www.w3.org/2000/svg' version='1.1' xmlns:xlink='http://www.w3.org/1999/xlink' width='40' height='40' style='background:#e6dec7;'></svg>" className="left circle">Creme</option>
                                                        <option value="CLFV10" data-icon="data:image/svg+xml;utf8,<svg xmlns='http://www.w3.org/2000/svg' version='1.1' xmlns:xlink='http://www.w3.org/1999/xlink' width='40' height='40' style='background:LightBlue;'></svg>" className="left circle">Hellblau</option>
                                                        <option value="CLFV11" data-icon="data:image/svg+xml;utf8,<svg xmlns='http://www.w3.org/2000/svg' version='1.1' xmlns:xlink='http://www.w3.org/1999/xlink' width='40' height='40' style='background:Brown;'></svg>" className="left circle">Braun</option>
                                                        <option value="CLFV12" data-icon="data:image/svg+xml;utf8,<svg xmlns='http://www.w3.org/2000/svg' version='1.1' xmlns:xlink='http://www.w3.org/1999/xlink' width='40' height='40' style='background:Purple;'></svg>" className="left circle">Lila</option>
                                                        <option value="CLFV13" data-icon="data:image/svg+xml;utf8,<svg xmlns='http://www.w3.org/2000/svg' version='1.1' xmlns:xlink='http://www.w3.org/1999/xlink' width='40' height='40' style='background:Black;'></svg>" className="left circle">Schwarz</option>
                                                        <option value="CLFV14" data-icon="data:image/svg+xml;utf8,<svg xmlns='http://www.w3.org/2000/svg' version='1.1' xmlns:xlink='http://www.w3.org/1999/xlink' width='40' height='40' style='background:Red;'></svg>" className="left circle">Rot</option>
                                                        <option value="CLFV15" data-icon="data:image/svg+xml;utf8,<svg xmlns='http://www.w3.org/2000/svg' version='1.1' xmlns:xlink='http://www.w3.org/1999/xlink' width='40' height='40' style='background:Gray;'></svg>" className="left circle">Andere</option>
                                                    </select>
                                                </div>

                                                <div className="input-field col s10 offset-s1" style={this.state.decoColorVisibility}>
                                                    <i className="material-icons prefix" >filter_vintage</i>
                                                    <select id="cake_flavors" type="text" value={this.state.formData.decoColor} className="validate" required="" aria-required="true" name="decoColor">
                                                        <option value="" disabled>Dekofarbe</option>
                                                        <option value="CLR01" data-icon="data:image/svg+xml;utf8,<svg xmlns='http://www.w3.org/2000/svg' version='1.1' xmlns:xlink='http://www.w3.org/1999/xlink' width='40' height='40' style='background:white;'></svg>" className="left circle">Weiss</option>
                                                        <option value="CLR02" data-icon="data:image/svg+xml;utf8,<svg xmlns='http://www.w3.org/2000/svg' version='1.1' xmlns:xlink='http://www.w3.org/1999/xlink' width='40' height='40' style='background:pink;'></svg>" className="left circle">Rosa</option>
                                                        <option value="CLR03" data-icon="data:image/svg+xml;utf8,<svg xmlns='http://www.w3.org/2000/svg' version='1.1' xmlns:xlink='http://www.w3.org/1999/xlink' width='40' height='40' style='background:Green;'></svg>" className="left circle">Grün</option>
                                                        <option value="CLR04" data-icon="data:image/svg+xml;utf8,<svg xmlns='http://www.w3.org/2000/svg' version='1.1' xmlns:xlink='http://www.w3.org/1999/xlink' width='40' height='40' style='background:Blue;'></svg>" className="left circle">Blau</option>
                                                        <option value="CLR05" data-icon="data:image/svg+xml;utf8,<svg xmlns='http://www.w3.org/2000/svg' version='1.1' xmlns:xlink='http://www.w3.org/1999/xlink' width='40' height='40' style='background:#e6dec7;'></svg>" className="left circle">Creme</option>
                                                        <option value="CLR06" data-icon="data:image/svg+xml;utf8,<svg xmlns='http://www.w3.org/2000/svg' version='1.1' xmlns:xlink='http://www.w3.org/1999/xlink' width='40' height='40' style='background:LightBlue;'></svg>" className="left circle">Hellblau</option>
                                                        <option value="CLR07" data-icon="data:image/svg+xml;utf8,<svg xmlns='http://www.w3.org/2000/svg' version='1.1' xmlns:xlink='http://www.w3.org/1999/xlink' width='40' height='40' style='background:Brown;'></svg>" className="left circle">Braun</option>
                                                        <option value="CLR08" data-icon="data:image/svg+xml;utf8,<svg xmlns='http://www.w3.org/2000/svg' version='1.1' xmlns:xlink='http://www.w3.org/1999/xlink' width='40' height='40' style='background:Purple;'></svg>" className="left circle">Lila</option>
                                                        {/* <option value="CLR09" data-icon="data:image/svg+xml;utf8,<svg xmlns='http://www.w3.org/2000/svg' version='1.1' xmlns:xlink='http://www.w3.org/1999/xlink' width='40' height='40' style='background:Black;'></svg>" className="left circle">Schwarz</option>
                                          <option value="CLR10" data-icon="data:image/svg+xml;utf8,<svg xmlns='http://www.w3.org/2000/svg' version='1.1' xmlns:xlink='http://www.w3.org/1999/xlink' width='40' height='40' style='background:Red;'></svg>" className="left circle">Rot</option>
                                          <option value="CLR99" data-icon="data:image/svg+xml;utf8,<svg xmlns='http://www.w3.org/2000/svg' version='1.1' xmlns:xlink='http://www.w3.org/1999/xlink' width='40' height='40' style='background:Gray;'></svg>" className="left circle">Andere</option> */}
                                                        {/* <option value="CLFV01" data-icon={Raspberry_kuchen} className="left circle">Kirsch</option>
                                          <option value="CLFV02" data-icon={caramel_wallnuss} className="left circle">Karamell</option>
                                          <option value="CLFV03" data-icon={banane_kuchen} className="left circle">Banane</option>
                                          <option value="CLFV04" data-icon={fruitgar} className="left circle">Fruchtgarten</option>
                                          <option value="CLFV05" data-icon="data:image/svg+xml;utf8,<svg xmlns='http://www.w3.org/2000/svg' version='1.1' xmlns:xlink='http://www.w3.org/1999/xlink' width='40' height='40' style='background:white;'></svg>" className="left circle">Weiss</option>
                                          <option value="CLFV06" data-icon="data:image/svg+xml;utf8,<svg xmlns='http://www.w3.org/2000/svg' version='1.1' xmlns:xlink='http://www.w3.org/1999/xlink' width='40' height='40' style='background:pink;'></svg>" className="left circle">Rosa</option>
                                          <option value="CLFV07" data-icon="data:image/svg+xml;utf8,<svg xmlns='http://www.w3.org/2000/svg' version='1.1' xmlns:xlink='http://www.w3.org/1999/xlink' width='40' height='40' style='background:Green;'></svg>" className="left circle">Grün</option>
                                          <option value="CLFV08" data-icon="data:image/svg+xml;utf8,<svg xmlns='http://www.w3.org/2000/svg' version='1.1' xmlns:xlink='http://www.w3.org/1999/xlink' width='40' height='40' style='background:Blue;'></svg>" className="left circle">Blau</option>
                                          <option value="CLFV09" data-icon="data:image/svg+xml;utf8,<svg xmlns='http://www.w3.org/2000/svg' version='1.1' xmlns:xlink='http://www.w3.org/1999/xlink' width='40' height='40' style='background:#e6dec7;'></svg>" className="left circle">Creme</option>
                                          <option value="CLFV10" data-icon="data:image/svg+xml;utf8,<svg xmlns='http://www.w3.org/2000/svg' version='1.1' xmlns:xlink='http://www.w3.org/1999/xlink' width='40' height='40' style='background:LightBlue;'></svg>" className="left circle">Hellblau</option>
                                          <option value="CLFV11" data-icon="data:image/svg+xml;utf8,<svg xmlns='http://www.w3.org/2000/svg' version='1.1' xmlns:xlink='http://www.w3.org/1999/xlink' width='40' height='40' style='background:Brown;'></svg>" className="left circle">Braun</option>
                                          <option value="CLFV12" data-icon="data:image/svg+xml;utf8,<svg xmlns='http://www.w3.org/2000/svg' version='1.1' xmlns:xlink='http://www.w3.org/1999/xlink' width='40' height='40' style='background:Purple;'></svg>" className="left circle">Lila</option> */}
                                                    </select>
                                                </div>

                                                <div className="input-field col s10 offset-s1" style={this.state.dripColorVisibility}>
                                                    <i className="material-icons prefix" >color_lens</i>
                                                    <select id="drip_color" type="text" value={this.state.formData.dripColor} className="validate" required="" aria-required="true" name="dripColor">
                                                        <option value="" disabled>Dripfarbe</option>
                                                        <option value="CLFV01" data-icon={Raspberry_kuchen} className="left circle">Sosse Rot</option>
                                                        <option value="CLFV02" data-icon={caramel_wallnuss} className="left circle">Karamell</option>
                                                        <option value="CLFV03" data-icon={banane_kuchen} className="left circle">Banane</option>
                                                        <option value="CLFV04" data-icon={fruitgar} className="left circle">Sosse Grün</option>
                                                        <option value="CLFV16" data-icon={cholate} className="left circle">Schokolade</option>
                                                        <option value="CLFV05" data-icon="data:image/svg+xml;utf8,<svg xmlns='http://www.w3.org/2000/svg' version='1.1' xmlns:xlink='http://www.w3.org/1999/xlink' width='40' height='40' style='background:white;'></svg>" className="left circle">Weiss</option>
                                                        <option value="CLFV06" data-icon="data:image/svg+xml;utf8,<svg xmlns='http://www.w3.org/2000/svg' version='1.1' xmlns:xlink='http://www.w3.org/1999/xlink' width='40' height='40' style='background:pink;'></svg>" className="left circle">Rosa</option>
                                                        <option value="CLFV07" data-icon="data:image/svg+xml;utf8,<svg xmlns='http://www.w3.org/2000/svg' version='1.1' xmlns:xlink='http://www.w3.org/1999/xlink' width='40' height='40' style='background:Green;'></svg>" className="left circle">Grün</option>
                                                        <option value="CLFV08" data-icon="data:image/svg+xml;utf8,<svg xmlns='http://www.w3.org/2000/svg' version='1.1' xmlns:xlink='http://www.w3.org/1999/xlink' width='40' height='40' style='background:Blue;'></svg>" className="left circle">Blau</option>
                                                        <option value="CLFV09" data-icon="data:image/svg+xml;utf8,<svg xmlns='http://www.w3.org/2000/svg' version='1.1' xmlns:xlink='http://www.w3.org/1999/xlink' width='40' height='40' style='background:#e6dec7;'></svg>" className="left circle">Creme</option>
                                                        <option value="CLFV10" data-icon="data:image/svg+xml;utf8,<svg xmlns='http://www.w3.org/2000/svg' version='1.1' xmlns:xlink='http://www.w3.org/1999/xlink' width='40' height='40' style='background:LightBlue;'></svg>" className="left circle">Hellblau</option>
                                                        <option value="CLFV11" data-icon="data:image/svg+xml;utf8,<svg xmlns='http://www.w3.org/2000/svg' version='1.1' xmlns:xlink='http://www.w3.org/1999/xlink' width='40' height='40' style='background:Brown;'></svg>" className="left circle">Braun</option>
                                                        <option value="CLFV12" data-icon="data:image/svg+xml;utf8,<svg xmlns='http://www.w3.org/2000/svg' version='1.1' xmlns:xlink='http://www.w3.org/1999/xlink' width='40' height='40' style='background:Purple;'></svg>" className="left circle">Lila</option>
                                                        <option value="CLFV13" data-icon="data:image/svg+xml;utf8,<svg xmlns='http://www.w3.org/2000/svg' version='1.1' xmlns:xlink='http://www.w3.org/1999/xlink' width='40' height='40' style='background:Black;'></svg>" className="left circle">Schwarz</option>
                                                        <option value="CLFV14" data-icon="data:image/svg+xml;utf8,<svg xmlns='http://www.w3.org/2000/svg' version='1.1' xmlns:xlink='http://www.w3.org/1999/xlink' width='40' height='40' style='background:Red;'></svg>" className="left circle">Rot</option>
                                                        <option value="CLFV15" data-icon="data:image/svg+xml;utf8,<svg xmlns='http://www.w3.org/2000/svg' version='1.1' xmlns:xlink='http://www.w3.org/1999/xlink' width='40' height='40' style='background:Gray;'></svg>" className="left circle">Andere</option>
                                                    </select>
                                                </div>

                                            </fieldset>

                                            <fieldset className="decoration">
                                                <h4 className="fs-title">Geschmack und Dekor</h4>

                                                <div className="input-field col s10 offset-s1">
                                                    <i className="material-icons prefix">filter_vintage</i>
                                                    <select id="cake_flavor" type="text" value={this.state.formData.flavor} className="validate" required="" aria-required="true" name="flavor">
                                                        <option value="" disabled>Tortengeschmack</option>
                                                        <option value="FLV01" disabled={this.state.isFlavorDisabled.FLV01} data-icon={chocolate} className="left circle">Schokolade</option>
                                                        <option value="FLV02" disabled={this.state.isFlavorDisabled.FLV02} data-icon={raspberry} className="left circle">Himbeere</option>
                                                        <option value="FLV03" disabled={this.state.isFlavorDisabled.FLV03} data-icon={cookies} className="left circle">Oreo</option>
                                                        <option value="FLV04" disabled={this.state.isFlavorDisabled.FLV04} data-icon={ganache} className="left circle">Ganache</option>
                                                        <option value="FLV05" disabled={this.state.isFlavorDisabled.FLV05} data-icon={cherry} className="left circle">Kirsch</option>
                                                        <option value="FLV06" disabled={this.state.isFlavorDisabled.FLV06} data-icon={cwalnut} className="left circle">Karamell-Wallnuss</option>
                                                        <option value="FLV07" disabled={this.state.isFlavorDisabled.FLV07} data-icon={banana} className="left circle">Banane</option>
                                                        <option value="FLV08" disabled={this.state.isFlavorDisabled.FLV08} data-icon={fruitgar} className="left circle">Fruchtgarten</option>
                                                        <option value="FLV09" disabled={this.state.isFlavorDisabled.FLV09} data-icon={krokant} className="left circle">Krokant</option>
                                                        <option value="FLV10" disabled={this.state.isFlavorDisabled.FLV10} data-icon={chocoRasp} className="left circle">Schoko-Himbeere</option>
                                                        <option value="FLV11" disabled={this.state.isFlavorDisabled.FLV11} data-icon={chocoBana} className="left circle">Schoko-Banane</option>
                                                        <option value="FLV12" disabled={this.state.isFlavorDisabled.FLV12} data-icon={Strawbery} className="left circle">Erdbeere (Saisonal)</option>
                                                        <option value="FLV13" disabled={this.state.isFlavorDisabled.FLV13} data-icon={Waldfrüchte} className="left circle">Waldfrüchte</option>
                                                    </select>
                                                </div>
                                                <div className="input-field col s10 offset-s1">
                                                    <i className="material-icons prefix">filter_vintage</i>
                                                    <select multiple id="cake_additional_flavor" className="validate" required="" aria-required="true" name="additional_flavor" value={this.state.formData.additional_flavor}>
                                                        <option value="" disabled>extra Geschmack €</option>
                                                        <option value="FLV01" disabled={this.state.isExtraFlavorDisabled.FLV01} className="left circle">Schokolade</option>
                                                        <option value="FLV02" disabled={this.state.isExtraFlavorDisabled.FLV02} className="left circle">Himbeere</option>
                                                        <option value="FLV03" disabled={this.state.isExtraFlavorDisabled.FLV03} className="left circle">Oreo</option>
                                                        <option value="FLV04" disabled={this.state.isExtraFlavorDisabled.FLV04} className="left circle">Ganache</option>
                                                        <option value="FLV05" disabled={this.state.isExtraFlavorDisabled.FLV05} className="left circle">Kirsch</option>
                                                        <option value="FLV06" disabled={this.state.isExtraFlavorDisabled.FLV06} className="left circle">Karamell-Wallnuss</option>
                                                        <option value="FLV07" disabled={this.state.isExtraFlavorDisabled.FLV07} className="left circle">Banane</option>
                                                        <option value="FLV08" disabled={this.state.isExtraFlavorDisabled.FLV08} className="left circle">Fruchtgarten</option>
                                                        <option value="FLV09" disabled={this.state.isExtraFlavorDisabled.FLV09} className="left circle">Krokant</option>
                                                        <option value="FLV10" disabled={this.state.isExtraFlavorDisabled.FLV10} className="left circle">Schoko-Himbeere</option>
                                                        <option value="FLV11" disabled={this.state.isExtraFlavorDisabled.FLV11} className="left circle">Schoko-Banane</option>
                                                        <option value="FLV12" disabled={this.state.isExtraFlavorDisabled.FLV12} className="left circle">Erdbeere</option>
                                                        <option value="FLV13" disabled={this.state.isExtraFlavorDisabled.FLV13} className="left circle">Waldfrüchte</option>
                                                    </select>
                                                </div>
                                                <div className="input-field col s10 offset-s1">
                                                    <i className="material-icons prefix">camera_enhance</i>
                                                    <select multiple id="cake_deco_extra" className="validate" required="" aria-required="true" name="extra_decoration" value={this.state.formData.extra_decoration}>
                                                        <option value="" disabled >extra Deko €</option>
                                                        <option value="EDECO01" disabled={this.state.isextraDecorationsDisabled.EDECO01}>Mit Foto</option>
                                                        <option value="EDECO02" disabled={this.state.isextraDecorationsDisabled.EDECO02}>Kerzen</option>
                                                        <option value="EDECO03" disabled={this.state.isextraDecorationsDisabled.EDECO03}>Glitzer oder Perlen</option>
                                                        <option value="EDECO04" disabled={this.state.isextraDecorationsDisabled.EDECO04}>Einschulung</option>
                                                        <option value="EDECO05" disabled={this.state.isextraDecorationsDisabled.EDECO05}>Macarons 3stück</option>
                                                        <option value="EDECO06" disabled={this.state.isextraDecorationsDisabled.EDECO06}>Zuckerrosen  2stück</option>
                                                        <option value="EDECO07" disabled={this.state.isextraDecorationsDisabled.EDECO07}>1 Boden höher</option>
                                                        <option value="EDECO08" disabled={this.state.isextraDecorationsDisabled.EDECO08}>Blümchen am Rand</option>
                                                    </select>

                                                </div>

                                                <div className={"file-field input-field col s10 offset-s1 " + (displayImage ? 'active' : 'inactive')} style={{marginTop:"30px"}}>
                                                    <div className="file-path-wrapper">
                                                        <i className="material-icons prefix">file_upload</i>
                                                        <input className="tooltipped extra_decoration_tooltip" data-position="bottom" data-delay="50" data-tooltip="Foto Hochladen" type="file" id="imageup" accept="image/*" onChange={this.readURL} />
                                                        <input className="file-path validate" type="text" placeholder="Foto Hochladen" />
                                                    </div>
                                                </div>

                                            </fieldset>

                                            <fieldset>
                                                <h4 className="fs-title">Text auf der Torte</h4>
                                                {/* <div className="col s12 m12"> */}

                                                <div className="input-field col s10 offset-s1" style={{ marginBottom: "60px" }}>
                                                    <i className="material-icons prefix" style={{ marginTop: "-15px" }}>mode_edit</i>
                                                    <input id="cake_text" type="text" maxLength="75" data-length="75" draggable="false" name="text" value={this.state.formData.text} onChange={this.handleDataChange} />
                                                    <label id="cake_text_lbl" htmlFor="cake_text">Text auf der Torte?</label>
                                                </div>
                                                <div className="input-field col s10 offset-s1" style={{ marginBottom: "23rem" }}>
                                                    <i className="material-icons prefix" style={{ marginTop: "-15px" }}>comment</i>
                                                    <textarea id="additional_remark" className="materialize-textarea" maxLength="100" data-length="100" draggable="false" name="remarks" value={this.state.formData.remarks} onChange={this.handleDataChange}></textarea>
                                                    <input type="hidden" id="hiddenOrderId" value={this.state.hiddenOrderId} name="hiddenOrderId" />
                                                    <label id="additional_remark_lbl" htmlFor="additional_remark" style={{width:"120%"}}>Bemerkungen (Nachricht für den Konditor)</label>
                                                </div>

                                                <div className="ip input-field col s10 offset-s1" style={{ marginLeft: '4rem' }}>
                                                    {/* <input type="checkbox" className="filled-in" id="checkAGBCakeForm" name="checkAGB" ref="check_meCakeForm" />
                                                    <label htmlFor="checkAGBCakeForm">
                                                        <a>
                                                            <span className="badge">Ich habe <u onClick={() => { $('#agb').modal('open'); }}>AGB, Widerrufsrecht</u> & <u onClick={() => { $('#unsere').modal('open'); }}>Datenschutzbestimmungen</u> gelesen und stimme zu.</span>
                                                        </a>
                                                    </label> */}
                                                    <label>
                                                        <input type="checkbox" className="filled-in" id="checkAGBCakeForm" name="checkAGB"
                                                            // ref="check_meCakeForm" 

                                                            ref={(el) => {
                                                                this.check_meCakeForm = el;
                                                            }}
                                                        />
                                                        <span className="badge" style={{ textAlign: 'left', lineHeight: "60px" }}>Ich habe <a className="modal-trigger" data-target="agb"><u style={{ color: '#757575' }}>AGB, Widerrufsrecht</u></a> & <a className="modal-trigger" data-target="unsere"><u style={{ color: '#757575' }}
                                                        >Datenschutzbestimmungen</u></a> gelesen und stimme zu.</span>
                                                    </label>
                                                </div>

                                            </fieldset>

                                            <fieldset>
                                                <h4 className="fs-title">Abholung/Lieferung Ihrer Torte</h4>

                                                <div className="input-field col s10 offset-s1" style={{ marginBottom: "40px" }}>
                                                    <i className="material-icons prefix">date_range</i>
                                                    <input id="deldate" className="customcakedatepicker validate" type="text" required="true" aria-required="true" name="deliveryDate" />
                                                    <label htmlFor="deldate">Datum</label>
                                                    <div className="col m12 l12 s12 alertMsg">
                                                        {/* <span className="five-day-notice hidden">mit weniger als 2 Tage Vorlaufzeit nur 'Barzahlung' oder 'Paypal' möglich.</span> */}
                                                    </div>
                                                </div>

                                                <div className="col s10 offset-s1" style={{ marginBottom: "40px" }}>
                                                    <i className="material-icons prefix input-field col access_time_icon">access_time</i>
                                                    {/*<input id="deltime" className="customcaketimepicker validate" type="hidden" required="true" aria-required="true" name="deliveryTime" />*/}
                                                    <div className="input-field col s5">
                                                        <select id="deltimehour" className="validate" required="true" value={this.state.formData.deliveryTimeHour} name="deliveryTimeHour">
                                                            <option value="" disabled selected>Uhr</option>
                                                            {/* <option value="08">08</option> */}
                                                            <option value="09" disabled={this.state.isDeliveryTomorrow}>09</option>
                                                            <option value="10" disabled={this.state.isDeliveryTomorrow}>10</option>
                                                            <option value="11" disabled={this.state.isDeliveryTomorrow}>11</option>
                                                            <option value="12" disabled={this.state.isDeliveryTomorrow}>12</option>
                                                            <option value="13">13</option>
                                                            <option value="14">14</option>
                                                            <option value="15">15</option>
                                                            <option value="16">16</option>
                                                            <option value="17">17</option>
                                                            <option value="18">18</option>
                                                            <option value="19">19</option>
                                                            <option value="20">20</option>
                                                            <option value="21">21</option>
                                                            <option value="22">22</option>
                                                        </select>
                                                    </div>
                                                    <div className="input-field col s5">
                                                        <select id="deltimemin" className="validate" required="true" value={this.state.formData.deliveryTimeMin} name="deliveryTimeMin">
                                                            <option value="" disabled selected>Min</option>
                                                            <option value="00">00</option>
                                                            <option value="15">15</option>
                                                            <option value="30">30</option>
                                                            <option value="45">45</option>
                                                        </select>
                                                    </div>
                                                    {/*<label htmlFor="deltimehour">Uhr</label>
                                       <label htmlFor="deltimemin">Min</label>*/}
                                                </div>

                                                <div className="HomeDvryCheckbox col s10 offset-s1" style={{ marginBottom: "40px" }}>
                                                    {/* <input type="checkbox" onChange={this.handleCheck} defaultChecked={this.state.checked}/> */}
                                                    {/* <p>Checkbox: {msg}</p> */}
                                                    <label className="lblHomeDvry" htmlFor="checkHomeDeliveryCakeForm">
                                                        <input type="checkbox" className="filled-in" id="checkHomeDeliveryCakeForm" onClick={this.handleCheck} defaultChecked={this.state.checked} />
                                                        <span className="badge" style={{ color: this.state.checked ? 'var(--dark-brown)' : 'gray', paddingTop: "20px", marginLeft: "-1.5rem" }}>Lieferung (Gegen Gebühr)</span>
                                                        {/* <a>
                                                        </a> */}
                                                    </label>
                                                </div>

                                                <div className="input-field col s10 offset-s1 addressDiv" style={{ marginBottom: "40px" }}>
                                                    <i className="material-icons prefix" style={{ color: this.state.checked ? '' : 'gray' }} >room</i>
                                                    <input id="search_input" disabled={this.state.checked ? false : true} type="text" placeholder="Adresse, z.B. Munzstraße 7" name="address_text" value={this.state.checked ? this.state.deliveryAddress : ''} />
                                                    <input type="hidden" id="loc_lat" />
                                                    <input type="hidden" id="loc_long" />
                                                    {/*<label htmlFor="address_text">Adresse</label>*/}
                                                    <div className="col m12 l12 s12 alertMsg">
                                                        <span className="more-then-fifteen hidden">Leider liefern wir nur bis 15km zu uns.</span>
                                                    </div>
                                                    <br />
                                                    <div className="col m12 l12 s12 alertMsg">
                                                        <span className="house-number-not-valid hidden">Bitte die Hausnummer mit angeben.</span>
                                                    </div>
                                                </div>
                                            </fieldset>

                                            <fieldset className="activeFS">
                                                <h4 className="fs-title">Kundendaten und bezahlen</h4>
                                                <div className="input-field col s10 offset-s1" style={{ marginBottom: "50px" }}>
                                                    <input type="hidden" name="hiddenInput" id="hiddenInput" />
                                                    <i className="material-icons prefix form_icon">person</i>
                                                    <input id="cust_fname" type="text" className="validate" required="true" aria-required="true" pattern="[a-zA-Za-zA-ZäöüßğüşöçİĞÜŞÖÇ ]+(?:(?:\. |[' -])[a-zA-Za-zA-ZäöüßğüşöçİĞÜŞÖÇ ]+)*" name="fname" value={this.state.formData.fname} onChange={this.handleDataChange} style={{ marginTop: "10px" }} />
                                                    <label id="fnameLbl" htmlFor="cust_fname" data-error="Falsche Eingabe" style={{ paddingTop: "10px" }}>Vorname</label>
                                                </div>
                                                <div className="input-field col s10 offset-s1" style={{ marginBottom: "50px" }}>
                                                    <i className="material-icons prefix form_icon">person_outline</i>
                                                    <input id="cust_lname" type="text" className="validate" required="true" aria-required="true" pattern="[a-zA-Za-zA-ZäöüßğüşöçİĞÜŞÖÇ ]+(?:(?:\. |[' -])[a-zA-Za-zA-ZäöüßğüşöçİĞÜŞÖÇ ]+)*" name="lname" value={this.state.formData.lname} onChange={this.handleDataChange} style={{ marginTop: "10px" }} />
                                                    <label id="lnameLbl" htmlFor="cust_lname" data-error="Falsche Eingabe" style={{ paddingTop: "10px" }}>Nachname</label>
                                                </div>

                                                <div className="input-field col s10 offset-s1" style={{ marginBottom: "50px" }}>
                                                    <i className="material-icons prefix">email</i>
                                                    <input id="cust_email" type="email" className="validate" required="true" aria-required="true" name="email" value={this.state.formData.email} onChange={this.handleDataChange} style={{ marginTop: "10px" }} />
                                                    <label id="emailLbl" htmlFor="cust_email" data-error="Falsche Eingabe" style={{ paddingTop: "10px" }}>E-Mail</label>
                                                </div>
                                                {/* pattern="[A-Za-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[A-Za-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[A-Za-z0-9](?:[A-Za-z0-9-]*[A-Za-z0-9])?\.)+[A-Za-z0-9](?:[A-Za-z0-9-]*[A-Za-z0-9])?"  */}
                                                <div className="input-field col s10 offset-s1" style={{ marginBottom: "50px" }}>
                                                    <i className="material-icons prefix">phone</i>
                                                    <input id="cust_mobile" type="tel" className="form-control validate" required="true" aria-required="true" name="mobile" pattern="^\d{5,14}$" value={this.state.formData.mobile} onChange={this.handleDataChange} style={{ marginTop: "10px" }} />
                                                    <label id="mobileLbl" htmlFor="cust_mobile" data-error="Falsche Eingabe" style={{ paddingTop: "10px" }}>Handy (für Rückmeldung) </label>
                                                </div>
                                            </fieldset>

                                        </form>

                                        <div className="step-button-container col">
                                            <div className="price-box-container">
                                                <PriceBox price={this.state.price} active={this.state.priceBoxActive} />
                                            </div>
                                            <div className="step-button">
                                                <button name="cancel" className="btn-flat btn-large hoverable cancel_btn waves-effect waves-dark hoverable"
                                                    style={{ color: 'var(--dark-brown)', margin: '25px' }}
                                                    onClick={this.closeCakeForm}>Löschen</button>
                                                <button name="next" className="btn hoverable btn-large next_btn waves-effect waves-light action-button"
                                                    style={{ backgroundColor: 'var(--dark-brown)', margin: '25px', float: 'right' }} onClick={this.nextButtonClicked}>Weiter<i style={{ color: 'white' }} className="material-icons">keyboard_arrow_right</i></button>
                                                <button name="submit" className="btn hoverable btn-large submit_btn waves-effect waves-light hoverable"
                                                    style={{ backgroundColor: 'var(--dark-brown)', margin: '25px', float: 'right' }} id="showReviewModal" onClick={this.showReviewModal}>Bezahlen</button>
                                                <button name="previous" className="btn-flat btn-large hoverable previous_btn waves-effect waves-dark hoverable"
                                                    style={{ color: 'var(--dark-brown)', margin: '25px', float: 'right' }} onClick={this.previousButtonClicked} ><i className="material-icons">keyboard_arrow_left</i>Zurück</button>
                                            </div>
                                        </div>
                                    </div>

                                    <ul id="progressbar" className="progressbar col l1 m1 hide-on-small-only step-container">
                                        <li className="active "></li>
                                        <li className=""></li>
                                        <li className=""></li>
                                        <li className=""></li>
                                        <li className=""></li>
                                    </ul>

                                    {/* <div className="col l2 m2 hide-on-med-and-down desc-container">
                                        <div className="desc-item active">
                                            <h5>Tortendekor und -form</h5>
                                            <span>Wie groß soll die Torte werden?</span>
                                        </div>
                                        <div className="desc-item">
                                            <h5>Tortendesign</h5>
                                            <span>Bestimmen Sie das Aussehen </span>
                                        </div>
                                        <div className="desc-item">
                                            <h5>Text auf der Torte</h5>
                                            <span>Ihre Torte wird großartig</span>
                                        </div>
                                        <div className="desc-item">
                                            <h5>Lieferung Ihrer Torte</h5>
                                            <span>täglich bis 23 Uhr geöffnet</span>
                                        </div>
                                        <div className="desc-item">
                                            <h5>Ihre Daten</h5>
                                            <span>zur Abwicklung und Lieferung</span>
                                        </div>
                                    </div> */}
                                </div>

                                {/* Summary modal */}
                                <div className="col s12 m12">
                                    <div id="modal_review" className="modal bottom-sheet">
                                        <div className="modal-content">

                                            <div className="order-summary">
                                                <h5 style={{ fontSize: "4rem", marginBottom: "1.5rem" }}>Zusammenfassung</h5>
                                                <div className="delivery-tag">
                                                    <h6>SUMME<br /></h6>
                                                </div>
                                                <div className="price-tag">
                                                    <h4>{this.state.price} €</h4>
                                                    <span className="tax-notice">
                                                        Inkl. MwSt:<br />
                                                        <span class="tax-amount">
                                                            {this.state.totalTax.toLocaleString('de-DE', { minimumFractionDigits: 2, maximumFractionDigits: 2 })}&nbsp;&euro;
                                                        </span>
                                                    </span>
                                                </div>
                                                <ul className="collapsible z-depth-0" data-collapsible="accordion">
                                                    <li>
                                                        <div className="collapsible-header"><span><i className="material-icons">account_circle</i>Ihre Daten</span></div>
                                                        <div className="collapsible-body">
                                                            <div>Vor- und Nachname: {this.state.formData.fname + " " + this.state.formData.lname}</div>
                                                            <div>E-Mail: {this.state.formData.email}</div>
                                                            <div>Handy: {this.state.formData.mobile}</div>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div className="collapsible-header"><span><i className="material-icons">place</i> Abholung/Lieferung</span> <span>{this.state.formData.deliveryCharge + " €"}</span></div>
                                                        <div className="collapsible-body">
                                                            <div>{this.state.formData.deliveryAddress ? 'Lieferdatum' : 'Datum'}: {this.state.formData.deliveryDate} &nbsp;&nbsp;&nbsp;&nbsp; Zeit: {this.state.formData.deliveryTimeHour}:{this.state.formData.deliveryTimeMin}</div>
                                                            {this.state.formData.deliveryAddress ? <div>Lieferadresse: {this.state.formData.deliveryAddress}</div> : ''}
                                                            {/* <div>Adresse: {this.state.formData.street + ' ' + this.state.formData.city}</div> */}
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div className="collapsible-header"><span><i className="material-icons">cake</i>Tortendekor und -form</span> <span>{this.state.couponShapeAndStylePrice + " €"}</span></div>
                                                        <div className="collapsible-body">
                                                            <div>Tortengröße:&nbsp;{this.getName(this.state.productPrices.sizes, this.state.formData.size)}</div>
                                                            <div>Tortenform:&nbsp;{this.getName(this.state.productPrices.shapes, this.state.formData.shape)}</div>
                                                            <div>Tortendeko:&nbsp;{this.getName(this.state.productPrices.decorations, this.state.formData.decoration)}</div>
                                                            {
                                                                this.shouldSauceColorBeVisible()
                                                                    ? <div>Sossenfarbe: {this.getName(this.state.productPrices.decoColors, this.state.formData.sauceColor)}</div>
                                                                    : null
                                                            }

                                                            {

                                                                this.shouldDecoColorBeVisible()
                                                                    ? <div>Dekofarbe: {this.getName(this.state.productPrices.colors, this.state.formData.decoColor)}</div>
                                                                    : null
                                                            }
                                                            {

                                                                this.shouldDripColorBeVisible()
                                                                    ? <div>Dripfarbe: {this.getName(this.state.productPrices.decoColors, this.state.formData.dripColor)}</div>
                                                                    : null
                                                            }
                                                            <div>Tortenfarbe:&nbsp;
                                                                {
                                                                    this.shouldColorBeVisible()
                                                                        ? this.getName(this.state.productPrices.colors, this.state.formData.color)
                                                                        : 'Keine angabe'
                                                                }
                                                            </div>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div className="collapsible-header"><span><i className="material-icons">whatshot</i>Geschmack und Dekor</span> <span>{this.state.designAndFlavorPrice + " €"}</span></div>
                                                        <div className="collapsible-body">
                                                            <div>Tortengeschmack:{this.getName(this.state.productPrices.flavors, this.state.formData.flavor)}</div>
                                                            <div>weiterer Geschmack: {extra_flavor}</div>
                                                            <div>weitere Dekoration: {extra_decoration}</div>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div className="collapsible-header"><span><i className="material-icons">local_offer</i>Rabatt Code</span> <span>{"-" + this.state.formData.cuponPrice + " €"}</span></div>
                                                        <div className="collapsible-body">
                                                            <div>
                                                                <div className="input-field coupon-field">{/*style={{ position: 'absolute', 'margin-top': '-2rem' }} style={{ display: 'inline-block', 'margin-top': '-2rem','width': '48%', 'margin-right': '3%' }}*/}
                                                                    <input type="text" id="cupon_name" pattern="^[a-zA-Z0-9]{9,12}$" name="cuponname" value={this.state.formData.cuponname}
                                                                        onChange={this.handleDataChange} className="validate" />
                                                                    <label htmlFor="cupon_name" data-error="Rabatt-Gutschein ist ungültig!"></label>
                                                                </div>
                                                                <div style={{ display: 'inline-block' }}>
                                                                    <a name="apply" class="applyCode" style={{ display: 'inline-block', verticalAlign: 'top' }}
                                                                        onClick={this.handleCouponCode}>Anwenden</a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </li>
                                                </ul>
                                            </div>
                                            <div className="payment_option" style={{marginTop:"20px", marginBottom:"20px"}}>

                                                {/* <PaypalBtn price={this.state.price} /> */}
                                                {/* <h5>Choose Payment Method</h5> */}
                                                {/* <div id="wait" style={this.state.paypalButtonVisiblity}>
                                                                    <img className="cake_icon" alt="cake_icon" src={'https://www.w3schools.com/jquery/demo_wait.gif'} />
                                                                </div> */}
                                                <div className="payment-choice" style={{ display: "flex", justifyContent: "center" }}>
                                                    {/* <p class="cash-notice">Bitte mind. 24 Stunden vor der Abholung/Lieferung eine Anzahlung im Café machen. Weiteres siehe E-Mail.</p> */}

                                                    <label for="test1">
                                                        <input className="with-gap" name="paymentType" type="radio" id="test1" value="cod" onChange={this.handleDataChange} checked={this.state.formData.paymentType === "cod"} />
                                                        <span>Bar</span>
                                                    </label>

                                                    {/* <label for="test2">
                                                        <input className="with-gap" name="paymentType" type="radio" id="test2" value="creditcard" onChange={this.handleDataChange} checked={this.state.formData.paymentType === "creditcard"} disabled={this.state.hideBankTransfer} />
                                                        <span>Überweisung</span>
                                                    </label> */}

                                                    <label for="test3">
                                                        <input className="with-gap" name="paymentType" type="radio" id="test3" value="paypal" onChange={this.handleDataChange} checked={this.state.formData.paymentType === "paypal"} />
                                                        <span>PayPal</span>
                                                    </label>
                                                    <label for="test4">
                                                        <input className="with-gap" name="paymentType" type="radio" id="test4" value="mollie" onChange={this.handleDataChange} checked={this.state.formData.paymentType === "mollie"} />
                                                        <span> Sofort/ Giropay/ Kreditkarte</span>
                                                    </label>
                                                </div>

                                                
                                            </div>
                                        </div>
                                        <div className="modal-footer" style={{display:"flex", justifyContent:"end"}}>
                                            {/*this.state.isLoading && <div class="overlayForSpinner">
                                                                <div class="overlay__inner">
                                                                    <div class="overlay__content"><span class="spinner"></span></div>
                                                                </div>
                                                            </div>
                                                            */}
                                            <button style={{ marginRight: '15px' }} className="modal-action modal-close waves-effect waves-dark btn-flat btn-large">Zurück</button>
                                            <button style={{ marginRight: '15px' }} disabled={this.state.isSubmitInProgress} onClick={this.handleSubmit} className={("modal-action modal-close waves-effect waves-dark btn btn-large " + (this.state.formData.paymentType === 'paypal' ? 'hide' : ''))}>Bestätigen</button>
                                            {/*<PaypalBtn price={this.state.price} active={this.state.formData.paymentType === 'paypal' ? 'true' : 'false'} handleSubmit={this.handleSubmit} />*/}
                                            <PaypalButton getPrice={this.getPrice} getOrderIdForReference={this.getOrderIdForReference} active={this.state.formData.paymentType === 'paypal' ? 'true' : 'false'} handleSubmit={this.handleSubmit} />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                        </div>
                    </div >

                    <div id="unsere" className="modal modal-fixed-footer">
                        <div className="modal-content">
                            <h3><center>Datenschutz­erklärung</center></h3>
                            <h5>1. Datenschutz auf einen Blick</h5>
                            <h6>- Allgemeine Hinweise</h6> <p>Die folgenden Hinweise geben einen einfachen Überblick darüber, was mit Ihren personenbezogenen Daten passiert, wenn Sie diese Website besuchen. Personenbezogene Daten sind alle Daten, mit denen Sie persönlich identifiziert werden können. Ausführliche Informationen zum Thema Datenschutz entnehmen Sie unserer unter diesem Text aufgeführten Datenschutzerklärung.</p>
                            <h6>- Datenerfassung auf dieser Website</h6> <h6>- Wer ist verantwortlich für die Datenerfassung auf dieser Website?</h6> <p>Die Datenverarbeitung auf dieser Website erfolgt durch den Websitebetreiber. Dessen Kontaktdaten können Sie dem Abschnitt „Hinweis zur Verantwortlichen Stelle“ in dieser Datenschutzerklärung entnehmen.</p> <h6>- Wie erfassen wir Ihre Daten?</h6> <p>Ihre Daten werden zum einen dadurch erhoben, dass Sie uns diese mitteilen. Hierbei kann es sich z. B. um Daten handeln, die Sie in ein Kontaktformular eingeben.</p> <p>Andere Daten werden automatisch oder nach Ihrer Einwilligung beim Besuch der Website durch unsere IT-Systeme erfasst. Das sind vor allem technische Daten (z. B. Internetbrowser, Betriebssystem oder Uhrzeit des Seitenaufrufs). Die Erfassung dieser Daten erfolgt automatisch, sobald Sie diese Website betreten.</p> <h6>- Wofür nutzen wir Ihre Daten?</h6> <p>Ein Teil der Daten wird erhoben, um eine fehlerfreie Bereitstellung der Website zu gewährleisten. Andere Daten können zur Analyse Ihres Nutzerverhaltens verwendet werden.</p> <h6>- Welche Rechte haben Sie bezüglich Ihrer Daten?</h6> <p>Sie haben jederzeit das Recht, unentgeltlich Auskunft über Herkunft, Empfänger und Zweck Ihrer gespeicherten personenbezogenen Daten zu erhalten. Sie haben außerdem ein Recht, die Berichtigung oder Löschung dieser Daten zu verlangen. Wenn Sie eine Einwilligung zur Datenverarbeitung erteilt haben, können Sie diese Einwilligung jederzeit für die Zukunft widerrufen. Außerdem haben Sie das Recht, unter bestimmten Umständen die Einschränkung der Verarbeitung Ihrer personenbezogenen Daten zu verlangen. Des Weiteren steht Ihnen ein Beschwerderecht bei der zuständigen Aufsichtsbehörde zu.</p> <p>Hierzu sowie zu weiteren Fragen zum Thema Datenschutz können Sie sich jederzeit an uns wenden.</p>
                            <h6>- Analyse-Tools und Tools von Dritt­anbietern</h6> <p>Beim Besuch dieser Website kann Ihr Surf-Verhalten statistisch ausgewertet werden. Das geschieht vor allem mit sogenannten Analyseprogrammen.</p> <p>Detaillierte Informationen zu diesen Analyseprogrammen finden Sie in der folgenden Datenschutzerklärung.</p>
                            <h5>2. Hosting und Content Delivery Networks (CDN)</h5>
                            <h6>- Externes Hosting</h6> <p>Diese Website wird bei einem externen Dienstleister gehostet (Hoster). Die personenbezogenen Daten, die auf dieser Website erfasst werden, werden auf den Servern des Hosters gespeichert. Hierbei kann es sich v. a. um IP-Adressen, Kontaktanfragen, Meta- und Kommunikationsdaten, Vertragsdaten, Kontaktdaten, Namen, Websitezugriffe und sonstige Daten, die über eine Website generiert werden, handeln.</p> <p>Der Einsatz des Hosters erfolgt zum Zwecke der Vertragserfüllung gegenüber unseren potenziellen und bestehenden Kunden (Art. 6 Abs. 1 lit. b DSGVO) und im Interesse einer sicheren, schnellen und effizienten Bereitstellung unseres Online-Angebots durch einen professionellen Anbieter (Art. 6 Abs. 1 lit. f DSGVO).</p> <p>Unser Hoster wird Ihre Daten nur insoweit verarbeiten, wie dies zur Erfüllung seiner Leistungspflichten erforderlich ist und unsere Weisungen in Bezug auf diese Daten befolgen.</p> <p>Wir setzen folgenden Hoster ein:</p>
                            <p>Host Europe GmbH <br />
                                Hansestrasse 111 <br />
                                51149 Köln<br />
                                <br />
                                Vultr<br />
                                319 Clematis Street Suite 900 <br />
                                West Palm Beach, FL 33401<br />
                                <br />
                                Amazon Web Services, Inc.<br />
                                410 Terry Avenue North<br />
                                Seattle WA 98109<br />
                                United States</p>
                            <h6>- Abschluss eines Vertrages über Auftragsverarbeitung</h6> <p>Um die datenschutzkonforme Verarbeitung zu gewährleisten, haben wir einen Vertrag über Auftragsverarbeitung mit unserem Hoster geschlossen.</p>
                            <h5>3. Allgemeine Hinweise und Pflicht­informationen</h5>
                            <h6>- Datenschutz</h6> <p>Die Betreiber dieser Seiten nehmen den Schutz Ihrer persönlichen Daten sehr ernst. Wir behandeln Ihre personenbezogenen Daten vertraulich und entsprechend der gesetzlichen Datenschutzvorschriften sowie dieser Datenschutzerklärung.</p> <p>Wenn Sie diese Website benutzen, werden verschiedene personenbezogene Daten erhoben. Personenbezogene Daten sind Daten, mit denen Sie persönlich identifiziert werden können. Die vorliegende Datenschutzerklärung erläutert, welche Daten wir erheben und wofür wir sie nutzen. Sie erläutert auch, wie und zu welchem Zweck das geschieht.</p> <p>Wir weisen darauf hin, dass die Datenübertragung im Internet (z. B. bei der Kommunikation per E-Mail) Sicherheitslücken aufweisen kann. Ein lückenloser Schutz der Daten vor dem Zugriff durch Dritte ist nicht möglich.</p>
                            <h6>- Hinweis zur verantwortlichen Stelle</h6> <p>Die verantwortliche Stelle für die Datenverarbeitung auf dieser Website ist:</p> <p>Ince GmbH<br />
                                Hikmet Ince<br />
                                Münzstr. 7<br />
                                30159 Hannover</p>

                            <p>Telefon: 0511 17507<br />
                                E-Mail: info@efendibey.de</p>
                            <p>Verantwortliche Stelle ist die natürliche oder juristische Person, die allein oder gemeinsam mit anderen über die Zwecke und Mittel der Verarbeitung von personenbezogenen Daten (z. B. Namen, E-Mail-Adressen o. Ä.) entscheidet.</p>

                            <h6>- Speicherdauer</h6> <p>Soweit innerhalb dieser Datenschutzerklärung keine speziellere Speicherdauer genannt wurde, verbleiben Ihre personenbezogenen Daten bei uns, bis der Zweck für die Datenverarbeitung entfällt. Wenn Sie ein berechtigtes Löschersuchen geltend machen oder eine Einwilligung zur Datenverarbeitung widerrufen, werden Ihre Daten gelöscht, sofern wir keinen anderen rechtlich zulässigen  Gründe für die Speicherung Ihrer personenbezogenen Daten haben (z.B. steuer- oder handelsrechtliche Aufbewahrungsfristen); im letztgenannten Fall erfolgt die Löschung nach Fortfall dieser Gründe.</p>
                            <h6>- Hinweis zur Datenweitergabe in die USA</h6> <p>Auf unserer Website sind unter anderem Tools von Unternehmen mit Sitz in den USA eingebunden. Wenn diese Tools aktiv sind, können Ihre personenbezogenen Daten an die US-Server der jeweiligen Unternehmen weitergegeben werden. Wir weisen darauf hin, dass die USA kein sicherer Drittstaat im Sinne des EU-Datenschutzrechts sind. US-Unternehmen sind dazu verpflichtet, personenbezogene Daten an Sicherheitsbehörden herauszugeben, ohne dass Sie als Betroffener hiergegen gerichtlich vorgehen könnten. Es kann daher nicht ausgeschlossen werden, dass US-Behörden (z.B. Geheimdienste) Ihre auf US-Servern befindlichen Daten zu Überwachungszwecken verarbeiten, auswerten und dauerhaft speichern. Wir haben auf diese Verarbeitungstätigkeiten keinen Einfluss.</p><h6>- Widerruf Ihrer Einwilligung zur Datenverarbeitung</h6> <p>Viele Datenverarbeitungsvorgänge sind nur mit Ihrer ausdrücklichen Einwilligung möglich. Sie können eine bereits erteilte Einwilligung jederzeit widerrufen. Die Rechtmäßigkeit der bis zum Widerruf erfolgten Datenverarbeitung bleibt vom Widerruf unberührt.</p>
                            <h6>- Widerspruchsrecht gegen die Datenerhebung in besonderen Fällen sowie gegen Direktwerbung (Art. 21 DSGVO)</h6> <p>WENN DIE DATENVERARBEITUNG AUF GRUNDLAGE VON ART. 6 ABS. 1 LIT. E ODER F DSGVO ERFOLGT, HABEN SIE JEDERZEIT DAS RECHT, AUS GRÜNDEN, DIE SICH AUS IHRER BESONDEREN SITUATION ERGEBEN, GEGEN DIE VERARBEITUNG IHRER PERSONENBEZOGENEN DATEN WIDERSPRUCH EINZULEGEN; DIES GILT AUCH FÜR EIN AUF DIESE BESTIMMUNGEN GESTÜTZTES PROFILING. DIE JEWEILIGE RECHTSGRUNDLAGE, AUF DENEN EINE VERARBEITUNG BERUHT, ENTNEHMEN SIE DIESER DATENSCHUTZERKLÄRUNG. WENN SIE WIDERSPRUCH EINLEGEN, WERDEN WIR IHRE BETROFFENEN PERSONENBEZOGENEN DATEN NICHT MEHR VERARBEITEN, ES SEI DENN, WIR KÖNNEN ZWINGENDE SCHUTZWÜRDIGE GRÜNDE FÜR DIE VERARBEITUNG NACHWEISEN, DIE IHRE INTERESSEN, RECHTE UND FREIHEITEN ÜBERWIEGEN ODER DIE VERARBEITUNG DIENT DER GELTENDMACHUNG, AUSÜBUNG ODER VERTEIDIGUNG VON RECHTSANSPRÜCHEN (WIDERSPRUCH NACH ART. 21 ABS. 1 DSGVO).</p> <p>WERDEN IHRE PERSONENBEZOGENEN DATEN VERARBEITET, UM DIREKTWERBUNG ZU BETREIBEN, SO HABEN SIE DAS RECHT, JEDERZEIT WIDERSPRUCH GEGEN DIE VERARBEITUNG SIE BETREFFENDER PERSONENBEZOGENER DATEN ZUM ZWECKE DERARTIGER WERBUNG EINZULEGEN; DIES GILT AUCH FÜR DAS PROFILING, SOWEIT ES MIT SOLCHER DIREKTWERBUNG IN VERBINDUNG STEHT. WENN SIE WIDERSPRECHEN, WERDEN IHRE PERSONENBEZOGENEN DATEN ANSCHLIESSEND NICHT MEHR ZUM ZWECKE DER DIREKTWERBUNG VERWENDET (WIDERSPRUCH NACH ART. 21 ABS. 2 DSGVO).</p>
                            <h6>- Beschwerde­recht bei der zuständigen Aufsichts­behörde</h6> <p>Im Falle von Verstößen gegen die DSGVO steht den Betroffenen ein Beschwerderecht bei einer Aufsichtsbehörde, insbesondere in dem Mitgliedstaat ihres gewöhnlichen Aufenthalts, ihres Arbeitsplatzes oder des Orts des mutmaßlichen Verstoßes zu. Das Beschwerderecht besteht unbeschadet anderweitiger verwaltungsrechtlicher oder gerichtlicher Rechtsbehelfe.</p>
                            <h6>- Recht auf Daten­übertrag­barkeit</h6> <p>Sie haben das Recht, Daten, die wir auf Grundlage Ihrer Einwilligung oder in Erfüllung eines Vertrags automatisiert verarbeiten, an sich oder an einen Dritten in einem gängigen, maschinenlesbaren Format aushändigen zu lassen. Sofern Sie die direkte Übertragung der Daten an einen anderen Verantwortlichen verlangen, erfolgt dies nur, soweit es technisch machbar ist.</p>
                            <h6>- SSL- bzw. TLS-Verschlüsselung</h6> <p>Diese Seite nutzt aus Sicherheitsgründen und zum Schutz der Übertragung vertraulicher Inhalte, wie zum Beispiel Bestellungen oder Anfragen, die Sie an uns als Seitenbetreiber senden, eine SSL- bzw. TLS-Verschlüsselung. Eine verschlüsselte Verbindung erkennen Sie daran, dass die Adresszeile des Browsers von „http://“ auf „https://“ wechselt und an dem Schloss-Symbol in Ihrer Browserzeile.</p> <p>Wenn die SSL- bzw. TLS-Verschlüsselung aktiviert ist, können die Daten, die Sie an uns übermitteln, nicht von Dritten mitgelesen werden.</p>
                            <h6>- Verschlüsselter Zahlungsverkehr auf dieser Website</h6> <p>Besteht nach dem Abschluss eines kostenpflichtigen Vertrags eine Verpflichtung, uns Ihre Zahlungsdaten (z. B. Kontonummer bei Einzugsermächtigung) zu übermitteln, werden diese Daten zur Zahlungsabwicklung benötigt.</p> <p>Der Zahlungsverkehr über die gängigen Zahlungsmittel (Visa/MasterCard, Lastschriftverfahren) erfolgt ausschließlich über eine verschlüsselte SSL- bzw. TLS-Verbindung. Eine verschlüsselte Verbindung erkennen Sie daran, dass die Adresszeile des Browsers von „http://“ auf „https://“ wechselt und an dem Schloss-Symbol in Ihrer Browserzeile.</p> <p>Bei verschlüsselter Kommunikation können Ihre Zahlungsdaten, die Sie an uns übermitteln, nicht von Dritten mitgelesen werden.</p>
                            <h6>- Auskunft, Löschung und Berichtigung</h6> <p>Sie haben im Rahmen der geltenden gesetzlichen Bestimmungen jederzeit das Recht auf unentgeltliche Auskunft über Ihre gespeicherten personenbezogenen Daten, deren Herkunft und Empfänger und den Zweck der Datenverarbeitung und ggf. ein Recht auf Berichtigung oder Löschung dieser Daten. Hierzu sowie zu weiteren Fragen zum Thema personenbezogene Daten können Sie sich jederzeit an uns wenden.</p>
                            <h6>- Recht auf Einschränkung der Verarbeitung</h6> <p>Sie haben das Recht, die Einschränkung der Verarbeitung Ihrer personenbezogenen Daten zu verlangen. Hierzu können Sie sich jederzeit an uns wenden. Das Recht auf Einschränkung der Verarbeitung besteht in folgenden Fällen:</p> <ul> <li>Wenn Sie die Richtigkeit Ihrer bei uns gespeicherten personenbezogenen Daten bestreiten, benötigen wir in der Regel Zeit, um dies zu überprüfen. Für die Dauer der Prüfung haben Sie das Recht, die Einschränkung der Verarbeitung Ihrer personenbezogenen Daten zu verlangen.</li> <li>Wenn die Verarbeitung Ihrer personenbezogenen Daten unrechtmäßig geschah/geschieht, können Sie statt der Löschung die Einschränkung der Datenverarbeitung verlangen.</li> <li>Wenn wir Ihre personenbezogenen Daten nicht mehr benötigen, Sie sie jedoch zur Ausübung, Verteidigung oder Geltendmachung von Rechtsansprüchen benötigen, haben Sie das Recht, statt der Löschung die Einschränkung der Verarbeitung Ihrer personenbezogenen Daten zu verlangen.</li> <li>Wenn Sie einen Widerspruch nach Art. 21 Abs. 1 DSGVO eingelegt haben, muss eine Abwägung zwischen Ihren und unseren Interessen vorgenommen werden. Solange noch nicht feststeht, wessen Interessen überwiegen, haben Sie das Recht, die Einschränkung der Verarbeitung Ihrer personenbezogenen Daten zu verlangen.</li> </ul> <p>Wenn Sie die Verarbeitung Ihrer personenbezogenen Daten eingeschränkt haben, dürfen diese Daten – von ihrer Speicherung abgesehen – nur mit Ihrer Einwilligung oder zur Geltendmachung, Ausübung oder Verteidigung von Rechtsansprüchen oder zum Schutz der Rechte einer anderen natürlichen oder juristischen Person oder aus Gründen eines wichtigen öffentlichen Interesses der Europäischen Union oder eines Mitgliedstaats verarbeitet werden.</p>
                            <h6>- Widerspruch gegen Werbe-E-Mails</h6> <p>Der Nutzung von im Rahmen der Impressumspflicht veröffentlichten Kontaktdaten zur Übersendung von nicht ausdrücklich angeforderter Werbung und Informationsmaterialien wird hiermit widersprochen. Die Betreiber der Seiten behalten sich ausdrücklich rechtliche Schritte im Falle der unverlangten Zusendung von Werbeinformationen, etwa durch Spam-E-Mails, vor.</p>
                            <h5>4. Datenerfassung auf dieser Website</h5>
                            <h6>- Cookies</h6> <p>Unsere Internetseiten verwenden so genannte „Cookies“. Cookies sind kleine Textdateien und richten auf Ihrem Endgerät keinen Schaden an. Sie werden entweder vorübergehend für die Dauer einer Sitzung (Session-Cookies) oder dauerhaft (permanente Cookies) auf Ihrem Endgerät gespeichert. Session-Cookies werden nach Ende Ihres Besuchs automatisch gelöscht. Permanente Cookies bleiben auf Ihrem Endgerät gespeichert, bis Sie diese selbst löschen oder eine automatische Löschung durch Ihren Webbrowser erfolgt.</p> <p>Teilweise können auch Cookies von Drittunternehmen auf Ihrem Endgerät gespeichert werden, wenn Sie unsere Seite betreten (Third-Party-Cookies). Diese ermöglichen uns oder Ihnen die Nutzung bestimmter Dienstleistungen des Drittunternehmens (z.B. Cookies zur Abwicklung von Zahlungsdienstleistungen).</p> <p>Cookies haben verschiedene Funktionen. Zahlreiche Cookies sind technisch notwendig, da bestimmte Websitefunktionen ohne diese nicht funktionieren würden (z.B. die Warenkorbfunktion oder die Anzeige von Videos). Andere Cookies dienen dazu, das Nutzerverhalten auszuwerten oder Werbung anzuzeigen.</p> <p>Cookies, die zur Durchführung des elektronischen Kommunikationsvorgangs (notwendige Cookies) oder zur Bereitstellung bestimmter, von Ihnen erwünschter Funktionen (funktionale Cookies, z. B. für die Warenkorbfunktion) oder zur Optimierung der Website (z.B. Cookies zur Messung des Webpublikums) erforderlich sind, werden auf Grundlage von Art. 6 Abs. 1 lit. f DSGVO gespeichert, sofern keine andere Rechtsgrundlage angegeben wird. Der Websitebetreiber hat ein berechtigtes Interesse an der Speicherung von Cookies zur technisch fehlerfreien und optimierten Bereitstellung seiner Dienste. Sofern eine Einwilligung zur Speicherung von Cookies abgefragt wurde, erfolgt die Speicherung der betreffenden Cookies ausschließlich auf Grundlage dieser Einwilligung (Art. 6 Abs. 1 lit. a DSGVO); die Einwilligung ist jederzeit widerrufbar.</p> <p>Sie können Ihren Browser so einstellen, dass Sie über das Setzen von Cookies informiert werden und Cookies nur im Einzelfall erlauben, die Annahme von Cookies für bestimmte Fälle oder generell ausschließen sowie das automatische Löschen der Cookies beim Schließen des Browsers aktivieren. Bei der Deaktivierung von Cookies kann die Funktionalität dieser Website eingeschränkt sein.</p> <p>Soweit Cookies von Drittunternehmen oder zu Analysezwecken eingesetzt werden, werden wir Sie hierüber im Rahmen dieser Datenschutzerklärung gesondert informieren und ggf. eine Einwilligung abfragen.</p>
                            <h6>- Server-Log-Dateien</h6> <p>Der Provider der Seiten erhebt und speichert automatisch Informationen in so genannten Server-Log-Dateien, die Ihr Browser automatisch an uns übermittelt. Dies sind:</p> <ul> <li>Browsertyp und Browserversion</li> <li>verwendetes Betriebssystem</li> <li>Referrer URL</li> <li>Hostname des zugreifenden Rechners</li> <li>Uhrzeit der Serveranfrage</li> <li>IP-Adresse</li> </ul> <p>Eine Zusammenführung dieser Daten mit anderen Datenquellen wird nicht vorgenommen.</p> <p>Die Erfassung dieser Daten erfolgt auf Grundlage von Art. 6 Abs. 1 lit. f DSGVO. Der Websitebetreiber hat ein berechtigtes Interesse an der technisch fehlerfreien Darstellung und der Optimierung seiner Website – hierzu müssen die Server-Log-Files erfasst werden.</p>
                            <h6>- Kontaktformular</h6> <p>Wenn Sie uns per Kontaktformular Anfragen zukommen lassen, werden Ihre Angaben aus dem Anfrageformular inklusive der von Ihnen dort angegebenen Kontaktdaten zwecks Bearbeitung der Anfrage und für den Fall von Anschlussfragen bei uns gespeichert. Diese Daten geben wir nicht ohne Ihre Einwilligung weiter.</p> <p>Die Verarbeitung dieser Daten erfolgt auf Grundlage von Art. 6 Abs. 1 lit. b DSGVO, sofern Ihre Anfrage mit der Erfüllung eines Vertrags zusammenhängt oder zur Durchführung vorvertraglicher Maßnahmen erforderlich ist. In allen übrigen Fällen beruht die Verarbeitung auf unserem berechtigten Interesse an der effektiven Bearbeitung der an uns gerichteten Anfragen (Art. 6 Abs. 1 lit. f DSGVO) oder auf Ihrer Einwilligung (Art. 6 Abs. 1 lit. a DSGVO) sofern diese abgefragt wurde.</p> <p>Die von Ihnen im Kontaktformular eingegebenen Daten verbleiben bei uns, bis Sie uns zur Löschung auffordern, Ihre Einwilligung zur Speicherung widerrufen oder der Zweck für die Datenspeicherung entfällt (z. B. nach abgeschlossener Bearbeitung Ihrer Anfrage). Zwingende gesetzliche Bestimmungen – insbesondere Aufbewahrungsfristen – bleiben unberührt.</p>
                            <h6>- Anfrage per E-Mail, Telefon oder Telefax</h6> <p>Wenn Sie uns per E-Mail, Telefon oder Telefax kontaktieren, wird Ihre Anfrage inklusive aller daraus hervorgehenden personenbezogenen Daten (Name, Anfrage) zum Zwecke der Bearbeitung Ihres Anliegens bei uns gespeichert und verarbeitet. Diese Daten geben wir nicht ohne Ihre Einwilligung weiter.</p> <p>Die Verarbeitung dieser Daten erfolgt auf Grundlage von Art. 6 Abs. 1 lit. b DSGVO, sofern Ihre Anfrage mit der Erfüllung eines Vertrags zusammenhängt oder zur Durchführung vorvertraglicher Maßnahmen erforderlich ist. In allen übrigen Fällen beruht die Verarbeitung auf unserem berechtigten Interesse an der effektiven Bearbeitung der an uns gerichteten Anfragen (Art. 6 Abs. 1 lit. f DSGVO) oder auf Ihrer Einwilligung (Art. 6 Abs. 1 lit. a DSGVO) sofern diese abgefragt wurde.</p> <p>Die von Ihnen an uns per Kontaktanfragen übersandten Daten verbleiben bei uns, bis Sie uns zur Löschung auffordern, Ihre Einwilligung zur Speicherung widerrufen oder der Zweck für die Datenspeicherung entfällt (z. B. nach abgeschlossener Bearbeitung Ihres Anliegens). Zwingende gesetzliche Bestimmungen – insbesondere gesetzliche Aufbewahrungsfristen – bleiben unberührt.</p>
                            <h5>5. Analyse-Tools und Werbung</h5>
                            <h6>- Google Tag Manager</h6> <p>Wir setzen den Google Tag Manager ein. Anbieter ist die Google Ireland Limited, Gordon House, Barrow Street, Dublin 4, Irland.</p> <p>Der Google Tag Manager ist ein Tool, mit dessen Hilfe wir Tracking- oder Statistik-Tools und andere Technologien auf unserer Website einbinden können. Der Google Tag Manager selbst erstellt keine Nutzerprofile, speichert keine Cookies und nimmt keine eigenständigen Analysen vor. Er dient lediglich der Verwaltung und Ausspielung der über ihn eingebundenen Tools. Der Google Tag Manager erfasst jedoch Ihre IP-Adresse, die auch an das Mutterunternehmen von Google in die Vereinigten Staaten übertragen werden kann.</p> <p>Der Einsatz des Google Tag Managers erfolgt auf Grundlage von Art. 6 Abs. 1 lit. f DSGVO. Der Websitebetreiber hat ein berechtigtes Interesse an einer schnellen und unkomplizierten Einbindung und Verwaltung verschiedener Tools auf seiner Website. Sofern eine entsprechende Einwilligung abgefragt wurde, erfolgt die Verarbeitung ausschließlich auf Grundlage von Art. 6 Abs. 1 lit. a DSGVO; die Einwilligung ist jederzeit widerrufbar.</p>
                            <h6>- Google Analytics</h6> <p>Diese Website nutzt Funktionen des Webanalysedienstes Google Analytics. Anbieter ist die Google Ireland Limited („Google“), Gordon House, Barrow Street, Dublin 4, Irland.</p> <p>Google Analytics ermöglicht es dem Websitebetreiber, das Verhalten der Websitebesucher zu analysieren. Hierbei erhält der Websitebetreiber verschiedene Nutzungsdaten, wie z.B. Seitenaufrufe, Verweildauer, verwendete Betriebssysteme und Herkunft des Nutzers. Diese Daten werden von Google ggf. in einem Profil zusammengefasst, das dem jeweiligen Nutzer bzw. dessen Endgerät zugeordnet ist.</p> <p>Google Analytics verwendet Technologien, die die Wiedererkennung des Nutzers zum Zwecke der Analyse des Nutzerverhaltens ermöglichen (z.B. Cookies oder Device-Fingerprinting). Die von Google erfassten Informationen über die Benutzung dieser Website werden in der Regel an einen Server von Google in den USA übertragen und dort gespeichert.</p> <p>Die Nutzung dieses Analyse-Tools erfolgt auf Grundlage von Art. 6 Abs. 1 lit. f DSGVO. Der Websitebetreiber hat ein berechtigtes Interesse an der Analyse des Nutzerverhaltens, um sowohl sein Webangebot als auch seine Werbung zu optimieren. Sofern eine entsprechende Einwilligung abgefragt wurde (z. B. eine Einwilligung zur Speicherung von Cookies), erfolgt die Verarbeitung ausschließlich auf Grundlage von Art. 6 Abs. 1 lit. a DSGVO; die Einwilligung ist jederzeit widerrufbar.</p> <p>Die Datenübertragung in die USA wird auf die Standardvertragsklauseln der EU-Kommission gestützt. Details finden Sie hier: <a href="https://privacy.google.com/businesses/controllerterms/mccs/" target="_blank" rel="noopener noreferrer">https://privacy.google.com/businesses/controllerterms/mccs/</a>.</p> <h6>- IP Anonymisierung</h6> <p>Wir haben auf dieser Website die Funktion IP-Anonymisierung aktiviert. Dadurch wird Ihre IP-Adresse von Google innerhalb von Mitgliedstaaten der Europäischen Union oder in anderen Vertragsstaaten des Abkommens über den Europäischen Wirtschaftsraum vor der Übermittlung in die USA gekürzt. Nur in Ausnahmefällen wird die volle IP-Adresse an einen Server von Google in den USA übertragen und dort gekürzt. Im Auftrag des Betreibers dieser Website wird Google diese Informationen benutzen, um Ihre Nutzung der Website auszuwerten, um Reports über die Websiteaktivitäten zusammenzustellen und um weitere mit der Websitenutzung und der Internetnutzung verbundene Dienstleistungen gegenüber dem Websitebetreiber zu erbringen. Die im Rahmen von Google Analytics von Ihrem Browser übermittelte IP-Adresse wird nicht mit anderen Daten von Google zusammengeführt.</p>
                            <h6>- Browser Plugin</h6> <p>Sie können die Erfassung und Verarbeitung Ihrer Daten durch Google verhindern, indem Sie das unter dem folgenden Link verfügbare Browser-Plugin herunterladen und installieren: <a href="https://tools.google.com/dlpage/gaoptout?hl=de" target="_blank" rel="noopener noreferrer">https://tools.google.com/dlpage/gaoptout?hl=de</a>.</p> <p>Mehr Informationen zum Umgang mit Nutzerdaten bei Google Analytics finden Sie in der Datenschutzerklärung von Google: <a href="https://support.google.com/analytics/answer/6004245?hl=de" target="_blank" rel="noopener noreferrer">https://support.google.com/analytics/answer/6004245?hl=de</a>.</p><h6>- Auftragsverarbeitung</h6> <p>Wir haben mit Google einen Vertrag zur Auftragsverarbeitung abgeschlossen und setzen die strengen Vorgaben der deutschen Datenschutzbehörden bei der Nutzung von Google Analytics vollständig um.</p>
                            <h6>- Demografische Merkmale bei Google Analytics</h6> <p>Diese Website nutzt die Funktion „demografische Merkmale“ von Google Analytics, um den Websitebesuchern passende Werbeanzeigen innerhalb des Google-Werbenetzwerks anzeigen zu können. Dadurch können Berichte erstellt werden, die Aussagen zu Alter, Geschlecht und Interessen der Seitenbesucher enthalten. Diese Daten stammen aus interessenbezogener Werbung von Google sowie aus Besucherdaten von Drittanbietern. Diese Daten können keiner bestimmten Person zugeordnet werden. Sie können diese Funktion jederzeit über die Anzeigeneinstellungen in Ihrem Google-Konto deaktivieren oder die Erfassung Ihrer Daten durch Google Analytics wie im Punkt „Widerspruch gegen Datenerfassung“ dargestellt generell untersagen.</p>
                            <h6>- Google Analytics E-Commerce-Tracking</h6> <p>Diese Website nutzt die Funktion „E-Commerce-Tracking“ von Google Analytics. Mit Hilfe von E-Commerce-Tracking kann der Websitebetreiber das Kaufverhalten der Websitebesucher zur Verbesserung seiner Online-Marketing-Kampagnen analysieren. Hierbei werden Informationen, wie zum Beispiel die getätigten Bestellungen, durchschnittliche Bestellwerte, Versandkosten und die Zeit von der Ansicht bis zum Kauf eines Produktes erfasst. Diese Daten können von Google unter einer Transaktions-ID zusammengefasst werden, die dem jeweiligen Nutzer bzw. dessen Gerät zugeordnet ist.</p>
                            <h6>- Speicherdauer</h6> <p>Bei Google gespeicherte Daten auf Nutzer- und Ereignisebene, die mit Cookies, Nutzerkennungen (z. B. User ID) oder Werbe-IDs (z. B. DoubleClick-Cookies, Android-Werbe-ID) verknüpft sind, werden nach 26 Monaten anonymisiert bzw. gelöscht. Details hierzu ersehen Sie unter folgendem Link: <a href="https://support.google.com/analytics/answer/7667196?hl=de" target="_blank" rel="noopener noreferrer">https://support.google.com/analytics/answer/7667196?hl=de</a></p>

                            <h6>- Google Ads</h6> <p>Der Websitebetreiber verwendet Google Ads. Google Ads ist ein Online-Werbeprogramm der Google Ireland Limited („Google“), Gordon House, Barrow Street, Dublin 4, Irland.</p> <p>Google Ads ermöglicht es uns Werbeanzeigen in der Google-Suchmaschine oder auf Drittwebseiten auszuspielen, wenn der Nutzer bestimmte Suchbegriffe bei Google eingibt (Keyword-Targeting). Ferner können zielgerichtete Werbeanzeigen anhand der bei Google vorhandenen Nutzerdaten (z.B. Standortdaten und Interessen) ausgespielt werden (Zielgruppen-Targeting). Wir als Websitebetreiber können diese Daten quantitativ auswerten, indem wir beispielsweise analysieren, welche Suchbegriffe zur Ausspielung unserer Werbeanzeigen geführt haben und wie viele Anzeigen zu entsprechenden Klicks geführt haben.</p> <p>Die Nutzung von Google Ads erfolgt auf Grundlage von Art. 6 Abs. 1 lit. f DSGVO. Der Websitebetreiber hat ein berechtigtes Interesse an einer möglichst effektiven Vermarktung seiner Dienstleistung Produkte.</p> <p>Die Datenübertragung in die USA wird auf die Standardvertragsklauseln der EU-Kommission gestützt. Details finden Sie hier: <a href="https://privacy.google.com/businesses/controllerterms/mccs/" target="_blank" rel="noopener noreferrer">https://privacy.google.com/businesses/controllerterms/mccs/</a>.</p>
                            <h6>- Google Remarketing</h6> <p>Diese Website nutzt die Funktionen von Google Analytics Remarketing. Anbieter ist die Google Ireland Limited („Google“), Gordon House, Barrow Street, Dublin 4, Irland.</p> <p>Google Remarketing analysiert Ihr Nutzerverhalten auf unserer Website (z.B. Klick auf bestimmte Produkte), um Sie in bestimmte Werbe-Zielgruppen einzuordnen und Ihnen anschließend beim Besuch von anderen Onlineangeboten passende Webebotschaften auszuspielen (Remarketing bzw. Retargeting).</p> <p>Des Weiteren können die mit Google Remarketing erstellten Werbe-Zielgruppen mit den geräteübergreifenden Funktionen von Google verknüpft werden. Auf diese Weise können interessenbezogene, personalisierte Werbebotschaften, die in Abhängigkeit Ihres früheren Nutzungs- und Surfverhaltens auf einem Endgerät (z. B. Handy) an Sie angepasst wurden auch auf einem anderen Ihrer Endgeräte (z. B. Tablet oder PC) angezeigt werden.</p> <p>Wenn Sie über einen Google-Account verfügen, können Sie der personalisierten Werbung unter folgendem Link widersprechen: <a href="https://www.google.com/settings/ads/onweb/" target="_blank" rel="noopener noreferrer">https://www.google.com/settings/ads/onweb/</a>.</p> <p>Die Nutzung von Google Remarketing erfolgt auf Grundlage von Art. 6 Abs. 1 lit. f DSGVO. Der Websitebetreiber hat ein berechtigtes Interesse an einer möglichst effektiven Vermarktung seiner Produkte. Sofern eine entsprechende Einwilligung abgefragt wurde, erfolgt die Verarbeitung ausschließlich auf Grundlage von Art. 6 Abs. 1 lit. a DSGVO; die Einwilligung ist jederzeit widerrufbar.</p> <p>Weitergehende Informationen und die Datenschutzbestimmungen finden Sie in der Datenschutzerklärung von Google unter: <a href="https://policies.google.com/technologies/ads?hl=de" target="_blank" rel="noopener noreferrer">https://policies.google.com/technologies/ads?hl=de</a>.</p>
                            <h6>- Zielgruppenbildung mit Kundenabgleich</h6> <p>Zur Zielgruppenbildung verwenden wir unter anderem den Kundenabgleich von Google Remarketing. Hierbei übergeben wir bestimmte Kundendaten (z.B. E-Mail-Adressen) aus unseren Kundenlisten an Google. Sind die betreffenden Kunden Google-Nutzer und in ihrem Google-Konto eingeloggt, werden ihnen passende Werbebotschaften innerhalb des Google-Netzwerks (z.B. bei YouTube, Gmail oder in der Suchmaschine) angezeigt.</p>
                            <h6>- Google Conversion-Tracking</h6> <p>Diese Website nutzt Google Conversion Tracking. Anbieter ist die Google Ireland Limited („Google“), Gordon House, Barrow Street, Dublin 4, Irland.</p> <p>Mit Hilfe von Google-Conversion-Tracking können Google und wir erkennen, ob der Nutzer bestimmte Aktionen durchgeführt hat. So können wir beispielsweise auswerten, welche Buttons auf unserer Website wie häufig geklickt und welche Produkte besonders häufig angesehen oder gekauft wurden. Diese Informationen dienen dazu, Conversion-Statistiken zu erstellen. Wir erfahren die Gesamtanzahl der Nutzer, die auf unsere Anzeigen geklickt haben und welche Aktionen sie durchgeführt haben. Wir erhalten keine Informationen, mit denen wir den Nutzer persönlich identifizieren können. Google selbst nutzt zur Identifikation Cookies oder vergleichbare Wiedererkennungstechnologien.</p> <p>Die Nutzung von Google Conversion-Tracking erfolgt auf Grundlage von Art. 6 Abs. 1 lit. f DSGVO. Der Websitebetreiber hat ein berechtigtes Interesse an der Analyse des Nutzerverhaltens, um sowohl sein Webangebot als auch seine Werbung zu optimieren. Sofern eine entsprechende Einwilligung abgefragt wurde (z. B. eine Einwilligung zur Speicherung von Cookies), erfolgt die Verarbeitung ausschließlich auf Grundlage von Art. 6 Abs. 1 lit. a DSGVO; die Einwilligung ist jederzeit widerrufbar.</p> <p>Mehr Informationen zu Google Conversion-Tracking finden Sie in den Datenschutzbestimmungen von Google: <a href="https://policies.google.com/privacy?hl=de" target="_blank" rel="noopener noreferrer">https://policies.google.com/privacy?hl=de</a>.</p>
                            <h6>- Facebook Pixel</h6> <p>Diese Website nutzt zur Konversionsmessung der Besucheraktions-Pixel von Facebook. Anbieter dieses Dienstes ist die Facebook Ireland Limited, 4 Grand Canal Square, Dublin 2, Irland. Die erfassten Daten werden nach Aussage von Facebook jedoch auch in die USA und in andere Drittländer übertragen.</p> <p>So kann das Verhalten der Seitenbesucher nachverfolgt werden, nachdem diese durch Klick auf eine Facebook-Werbeanzeige auf die Website des Anbieters weitergeleitet wurden. Dadurch können die Wirksamkeit der Facebook-Werbeanzeigen für statistische und Marktforschungszwecke ausgewertet werden und zukünftige Werbemaßnahmen optimiert werden.</p> <p>Die erhobenen Daten sind für uns als Betreiber dieser Website anonym, wir können keine Rückschlüsse auf die Identität der Nutzer ziehen. Die Daten werden aber von Facebook gespeichert und verarbeitet, sodass eine Verbindung zum jeweiligen Nutzerprofil möglich ist und Facebook die Daten für eigene Werbezwecke, entsprechend der <a href="https://de-de.facebook.com/about/privacy/" target="_blank" rel="noopener noreferrer">Facebook-Datenverwendungsrichtlinie</a> verwenden kann. Dadurch kann Facebook das Schalten von Werbeanzeigen auf Seiten von Facebook sowie außerhalb von Facebook ermöglichen. Diese Verwendung der Daten kann von uns als Seitenbetreiber nicht beeinflusst werden.</p> <p>Die Nutzung von Facebook-Pixel erfolgt auf Grundlage von Art. 6 Abs. 1 lit. f DSGVO. Der Websitebetreiber hat ein berechtigtes Interesse an effektiven Werbemaßnahmen unter Einschluss der sozialen Medien. Sofern eine entsprechende Einwilligung abgefragt wurde (z. B. eine Einwilligung zur Speicherung von Cookies), erfolgt die Verarbeitung ausschließlich auf Grundlage von Art. 6 Abs. 1 lit. a DSGVO; die Einwilligung ist jederzeit widerrufbar.</p> <p>Die Datenübertragung in die USA wird auf die Standardvertragsklauseln der EU-Kommission gestützt. Details finden Sie hier: <a href="https://www.facebook.com/legal/EU_data_transfer_addendum" target="_blank" rel="noopener noreferrer">https://www.facebook.com/legal/EU_data_transfer_addendum</a> und <a href="https://de-de.facebook.com/help/566994660333381" target="_blank" rel="noopener noreferrer">https://de-de.facebook.com/help/566994660333381</a>.</p> <p>In den Datenschutzhinweisen von Facebook finden Sie weitere Hinweise zum Schutz Ihrer Privatsphäre: <a href="https://de-de.facebook.com/about/privacy/" target="_blank" rel="noopener noreferrer">https://de-de.facebook.com/about/privacy/</a>.</p> <p>Sie können außerdem die Remarketing-Funktion „Custom Audiences“ im Bereich Einstellungen für Werbeanzeigen unter <a href="https://www.facebook.com/ads/preferences/?entry_product=ad_settings_screen" target="_blank" rel="noopener noreferrer">https://www.facebook.com/ads/preferences/?entry_product=ad_settings_screen</a> deaktivieren. Dazu müssen Sie bei Facebook angemeldet sein.</p> <p>Wenn Sie kein Facebook Konto besitzen, können Sie nutzungsbasierte Werbung von Facebook auf der Website der European Interactive Digital Advertising Alliance deaktivieren: <a href="http://www.youronlinechoices.com/de/praferenzmanagement/" target="_blank" rel="noopener noreferrer">http://www.youronlinechoices.com/de/praferenzmanagement/</a>.</p>
                            <h5>6. Newsletter</h5>
                            <h6>- Newsletter­daten</h6> <p>Wenn Sie den auf der Website angebotenen Newsletter beziehen möchten, benötigen wir von Ihnen eine E-Mail-Adresse sowie Informationen, welche uns die Überprüfung gestatten, dass Sie der Inhaber der angegebenen E-Mail-Adresse sind und mit dem Empfang des Newsletters einverstanden sind. Weitere Daten werden nicht bzw. nur auf freiwilliger Basis erhoben. Diese Daten verwenden wir ausschließlich für den Versand der angeforderten Informationen und geben diese nicht an Dritte weiter.</p> <p>Die Verarbeitung der in das Newsletteranmeldeformular eingegebenen Daten erfolgt ausschließlich auf Grundlage Ihrer Einwilligung (Art. 6 Abs. 1 lit. a DSGVO). Die erteilte Einwilligung zur Speicherung der Daten, der E-Mail-Adresse sowie deren Nutzung zum Versand des Newsletters können Sie jederzeit widerrufen, etwa über den „Austragen“-Link im Newsletter. Die Rechtmäßigkeit der bereits erfolgten Datenverarbeitungsvorgänge bleibt vom Widerruf unberührt.</p> <p>Die von Ihnen zum Zwecke des Newsletter-Bezugs bei uns hinterlegten Daten werden von uns bis zu Ihrer Austragung aus dem Newsletter bei uns bzw. dem Newsletterdiensteanbieter gespeichert und nach der Abbestellung des Newsletters oder nach Zweckfortfall aus der Newsletterverteilerliste gelöscht. Wir behalten uns vor, E-Mail-Adressen aus unserem Newsletterverteiler nach eigenem Ermessen im Rahmen unseres berechtigten Interesses nach Art. 6 Abs. 1 lit. f DSGVO zu löschen oder zu sperren.</p> <p>Nach Ihrer Austragung aus der Newsletterverteilerliste wird Ihre E-Mail-Adresse bei uns bzw. dem Newsletterdiensteanbieter ggf. in einer Blacklist gespeichert, um künftige Mailings zu verhindern. Die Daten aus der Blacklist werden nur für diesen Zweck verwendet und nicht mit anderen Daten zusammengeführt. Dies dient sowohl Ihrem Interesse als auch unserem Interesse an der Einhaltung der gesetzlichen Vorgaben beim Versand von Newslettern (berechtigtes Interesse im Sinne des Art. 6 Abs. 1 lit. f DSGVO). Die Speicherung in der Blacklist ist zeitlich nicht befristet. <strong>Sie können der Speicherung widersprechen, sofern Ihre Interessen unser berechtigtes Interesse überwiegen.</strong></p>
                            <h5>7. Plugins und Tools</h5>
                            <h6>- Google Web Fonts</h6> <p>Diese Seite nutzt zur einheitlichen Darstellung von Schriftarten so genannte Web Fonts, die von Google bereitgestellt werden. Beim Aufruf einer Seite lädt Ihr Browser die benötigten Web Fonts in ihren Browsercache, um Texte und Schriftarten korrekt anzuzeigen.</p> <p>Zu diesem Zweck muss der von Ihnen verwendete Browser Verbindung zu den Servern von Google aufnehmen. Hierdurch erlangt Google Kenntnis darüber, dass über Ihre IP-Adresse diese Website aufgerufen wurde. Die Nutzung von Google WebFonts erfolgt auf Grundlage von Art. 6 Abs. 1 lit. f DSGVO. Der Websitebetreiber hat ein berechtigtes Interesse an der einheitlichen Darstellung des Schriftbildes auf seiner Website. Sofern eine entsprechende Einwilligung abgefragt wurde (z. B. eine Einwilligung zur Speicherung von Cookies), erfolgt die Verarbeitung ausschließlich auf Grundlage von Art. 6 Abs. 1 lit. a DSGVO; die Einwilligung ist jederzeit widerrufbar.</p> <p>Wenn Ihr Browser Web Fonts nicht unterstützt, wird eine Standardschrift von Ihrem Computer genutzt.</p> <p>Weitere Informationen zu Google Web Fonts finden Sie unter <a href="https://developers.google.com/fonts/faq" target="_blank" rel="noopener noreferrer">https://developers.google.com/fonts/faq</a> und in der Datenschutzerklärung von Google: <a href="https://policies.google.com/privacy?hl=de" target="_blank" rel="noopener noreferrer">https://policies.google.com/privacy?hl=de</a>.</p>
                            <h5>8. eCommerce und Zahlungs­anbieter</h5>
                            <h6>- Verarbeiten von Daten (Kunden- und Vertragsdaten)</h6> <p>Wir erheben, verarbeiten und nutzen personenbezogene Daten nur, soweit sie für die Begründung, inhaltliche Ausgestaltung oder Änderung des Rechtsverhältnisses erforderlich sind (Bestandsdaten). Dies erfolgt auf Grundlage von Art. 6 Abs. 1 lit. b DSGVO, der die Verarbeitung von Daten zur Erfüllung eines Vertrags oder vorvertraglicher Maßnahmen gestattet. Personenbezogene Daten über die Inanspruchnahme dieser Website (Nutzungsdaten) erheben, verarbeiten und nutzen wir nur, soweit dies erforderlich ist, um dem Nutzer die Inanspruchnahme des Dienstes zu ermöglichen oder abzurechnen.</p> <p>Die erhobenen Kundendaten werden nach Abschluss des Auftrags oder Beendigung der Geschäftsbeziehung gelöscht. Gesetzliche Aufbewahrungsfristen bleiben unberührt.</p>
                            <h6>- Daten­übermittlung bei Vertragsschluss für Online-Shops, Händler und Warenversand</h6> <p>Wir übermitteln personenbezogene Daten an Dritte nur dann, wenn dies im Rahmen der Vertragsabwicklung notwendig ist, etwa an die mit der Lieferung der Ware betrauten Unternehmen oder das mit der Zahlungsabwicklung beauftragte Kreditinstitut. Eine weitergehende Übermittlung der Daten erfolgt nicht bzw. nur dann, wenn Sie der Übermittlung ausdrücklich zugestimmt haben. Eine Weitergabe Ihrer Daten an Dritte ohne ausdrückliche Einwilligung, etwa zu Zwecken der Werbung, erfolgt nicht.</p> <p>Grundlage für die Datenverarbeitung ist Art. 6 Abs. 1 lit. b DSGVO, der die Verarbeitung von Daten zur Erfüllung eines Vertrags oder vorvertraglicher Maßnahmen gestattet.</p>
                            <h6>- Zahlungsdienste</h6> <p>Wir binden Zahlungsdienste von Drittunternehmen auf unserer Website ein. Wenn Sie einen Kauf bei uns tätigen, werden Ihre Zahlungsdaten (z.B. Name, Zahlungssumme, Kontoverbindung, Kreditkartennummer) vom Zahlungsdienstleister zum Zwecke der Zahlungsabwicklung verarbeitet. Für diese Transaktionen gelten die jeweiligen Vertrags- und Datenschutzbestimmungen der jeweiligen Anbieter. Der Einsatz der Zahlungsdienstleister erfolgt auf Grundlage von Art. 6 Abs. 1 lit. b DSGVO (Vertragsabwicklung) sowie im Interesse eines möglichst reibungslosen, komfortablen und sicheren Zahlungsvorgangs (Art. 6 Abs. 1 lit. f DSGVO). Soweit für bestimmte Handlungen Ihre Einwilligung abgefragt wird, ist Art. 6 Abs. 1 lit. a DSGVO Rechtsgrundlage der Datenverarbeitung; Einwilligungen sind jederzeit für die Zukunft widerrufbar.</p> <p>Folgende Zahlungsdienste / Zahlungsdienstleister setzen wir im Rahmen dieser Website ein:</p><h6>- PayPal</h6> <p>Anbieter dieses Zahlungsdienstes ist PayPal (Europe) S.à.r.l. et Cie, S.C.A., 22-24 Boulevard Royal, L-2449 Luxembourg (im Folgenden „PayPal“).</p> <p>Die Datenübertragung in die USA wird auf die Standardvertragsklauseln der EU-Kommission gestützt. Details finden Sie hier: <a href="https://www.paypal.com/de/webapps/mpp/ua/pocpsa-full" target="_blank" rel="noopener noreferrer">https://www.paypal.com/de/webapps/mpp/ua/pocpsa-full</a>.</p> <p>Details entnehmen Sie der Datenschutzerklärung von PayPal: <a href="https://www.paypal.com/de/webapps/mpp/ua/privacy-full" target="_blank" rel="noopener noreferrer">https://www.paypal.com/de/webapps/mpp/ua/privacy-full</a>.</p>



                        </div>
                        <div class="modal-footer">
                            <button class="modal-action btn-large modal-close waves-effect btn-flat close-button">OK</button>
                        </div>
                    </div>

                    <div id="agb" className="modal modal-fixed-footer ">
                        <div className="modal-content">
                            <p><h4><center>Allgemeine Geschäftsbedingungen mit Kundeninformationen</center></h4></p>
                            <br />
                            <h5>Inhaltsverzeichnis</h5>
                            <p><h6>
                                1. Geltungsbereich <br />
                                2. Vertragsschluss <br />
                                3. Widerrufsrecht <br />
                                4. Preise und Zahlungsbedingungen <br />
                                5. Liefer- und Versandbedingungen <br />
                                6. Eigentumsvorbehalt <br />
                                7. Mängelhaftung (Gewährleistung) <br />
                                8. Besondere Bedingungen für die Verarbeitung von Waren nach bestimmten Vorgaben des Kunden <br />
                                9. Einlösung von Aktionsgutscheinen <br />
                                10. Einlösung von Geschenkgutscheinen <br />
                                11. Anwendbares Recht <br />
                                12. Gerichtsstand <br />
                                13. Alternative Streitbeilegung <br />
                                14. Widerrufsbelehrung & Widerrufsformular<br />
                            </h6></p><br />

                            <h5>§ 1. Geltungsbereich </h5>
                            <p>1.1 Diese Allgemeinen Geschäftsbedingungen (nachfolgend "AGB") der Ince GmbH (nachfolgend "Verkäufer"), gelten für alle Verträge über die Lieferung von Waren, die ein Verbraucher oder Unternehmer (nachfolgend „Kunde“) mit dem Verkäufer hinsichtlich der vom Verkäufer in seinem Online-Shop dargestellten Waren abschließt. Hiermit wird der Einbeziehung von eigenen Bedingungen des Kunden widersprochen, es sei denn, es ist etwas anderes vereinbart.
                            </p><p>1.2 Für Verträge über die Lieferung von Gutscheinen gelten diese AGB entsprechend, sofern insoweit nicht ausdrücklich etwas Abweichendes geregelt ist.
                            </p><p>1.3 Verbraucher im Sinne dieser AGB ist jede natürliche Person, die ein Rechtsgeschäft zu Zwecken abschließt, die überwiegend weder ihrer gewerblichen noch ihrer selbständigen beruflichen Tätigkeit zugerechnet werden können. Unternehmer im Sinne dieser AGB ist eine natürliche oder juristische Person oder eine rechtsfähige Personengesellschaft, die bei Abschluss eines Rechtsgeschäfts in Ausübung ihrer gewerblichen oder selbständigen beruflichen Tätigkeit handelt.
                            </p><br />

                            <h5>§ 2. Vertragsschluss </h5>
                            <p>2.1 Die im Online-Shop des Verkäufers enthaltenen Produktbeschreibungen stellen keine verbindlichen Angebote seitens des Verkäufers dar, sondern dienen zur Abgabe eines verbindlichen Angebots durch den Kunden.
                            </p><p>2.2 Der Kunde kann das Angebot über das in den Online-Shop des Verkäufers integrierte Online-Bestellformular abgeben. Dabei gibt der Kunde, nachdem er die ausgewählten Waren in den virtuellen Warenkorb gelegt und den elektronischen Bestellprozess durchlaufen hat, durch Klicken des den Bestellvorgang abschließenden Buttons ein rechtlich verbindliches Vertragsangebot in Bezug auf die im Warenkorb enthaltenen Waren ab.
                            </p><p>2.3 Der Verkäufer kann das Angebot des Kunden innerhalb von fünf Tagen annehmen,
                            </p><p>- indem er dem Kunden eine schriftliche Auftragsbestätigung oder eine Auftragsbestätigung in Textform (Fax oder E-Mail) übermittelt, wobei insoweit der Zugang der Auftragsbestätigung beim Kunden maßgeblich ist, oder
                            </p><p>- indem er dem Kunden die bestellte Ware liefert, wobei insoweit der Zugang der Ware beim Kunden maßgeblich ist, oder
                            </p><p>- indem er den Kunden nach Abgabe von dessen Bestellung zur Zahlung auffordert.
                                Liegen mehrere der vorgenannten Alternativen vor, kommt der Vertrag in dem Zeitpunkt zustande, in dem eine der vorgenannten Alternativen zuerst eintritt. Die Frist zur Annahme des Angebots beginnt am Tag nach der Absendung des Angebots durch den Kunden zu laufen und endet mit dem Ablauf des fünften Tages, welcher auf die Absendung des Angebots folgt. Nimmt der Verkäufer das Angebot des Kunden innerhalb vorgenannter Frist nicht an, so gilt dies als Ablehnung des Angebots mit der Folge, dass der Kunde nicht mehr an seine Willenserklärung gebunden ist.
                            </p><p>2.4 Bei der Abgabe eines Angebots über das Online-Bestellformular des Verkäufers wird der Vertragstext nach dem Vertragsschluss vom Verkäufer gespeichert und dem Kunden nach Absendung von dessen Bestellung in Textform (z. B. E-Mail, Fax oder Brief) übermittelt. Eine darüber hinausgehende Zugänglichmachung des Vertragstextes durch den Verkäufer erfolgt nicht.
                            </p><p>2.5 Vor verbindlicher Abgabe der Bestellung über das Online-Bestellformular des Verkäufers kann der Kunde mögliche Eingabefehler durch aufmerksames Lesen der auf dem Bildschirm dargestellten Informationen erkennen. Ein wirksames technisches Mittel zur besseren Erkennung von Eingabefehlern kann dabei die Vergrößerungsfunktion des Browsers sein, mit deren Hilfe die Darstellung auf dem Bildschirm vergrößert wird. Seine Eingaben kann der Kunde im Rahmen des elektronischen Bestellprozesses so lange über die üblichen Tastatur- und Mausfunktionen korrigieren, bis er den den Bestellvorgang abschließenden Button anklickt.
                            </p><p>2.6 Für den Vertragsschluss stehen die deutsche und die englische Sprache zur Verfügung.
                            </p><p>2.7 Die Bestellabwicklung und Kontaktaufnahme finden in der Regel per E-Mail und automatisierter Bestellabwicklung statt. Der Kunde hat sicherzustellen, dass die von ihm zur Bestellabwicklung angegebene E-Mail-Adresse zutreffend ist, so dass unter dieser Adresse die vom Verkäufer versandten E-Mails empfangen werden können. Insbesondere hat der Kunde bei dem Einsatz von SPAM-Filtern sicherzustellen, dass alle vom Verkäufer oder von diesem mit der Bestellabwicklung beauftragten Dritten versandten E-Mails zugestellt werden können.
                            </p><br />

                            <h5>§ 3. Widerrufsrecht </h5>
                            <p>3.1 Verbrauchern steht grundsätzlich ein Widerrufsrecht zu.
                            </p><p>3.2 Nähere Informationen zum Widerrufsrecht ergeben sich aus der Widerrufsbelehrung §14  des Verkäufers.</p><br />

                            <h5>§ 4. Preise und Zahlungsbedingungen</h5>
                            <p>4.1 Sofern sich aus der Produktbeschreibung des Verkäufers nichts anderes ergibt, handelt es sich bei den angegebenen Preisen um Gesamtpreise, die die gesetzliche Umsatzsteuer enthalten. Gegebenenfalls zusätzlich anfallende Liefer- und Versandkosten werden in der jeweiligen Produktbeschreibung gesondert angegeben.
                            </p><p>4.2 Bei Lieferungen in Länder außerhalb der Europäischen Union können im Einzelfall weitere Kosten anfallen, die der Verkäufer nicht zu vertreten hat und die vom Kunden zu tragen sind. Hierzu zählen beispielsweise Kosten für die Geldübermittlung durch Kreditinstitute (z.B. Überweisungsgebühren, Wechselkursgebühren) oder einfuhrrechtliche Abgaben bzw. Steuern (z.B. Zölle). Solche Kosten können in Bezug auf die Geldübermittlung auch dann anfallen, wenn die Lieferung nicht in ein Land außerhalb der Europäischen Union erfolgt, der Kunde die Zahlung aber von einem Land außerhalb der Europäischen Union aus vornimmt.
                            </p><p>4.3 Die Zahlungsmöglichkeit/en wird/werden dem Kunden im Online-Shop des Verkäufers mitgeteilt.
                            </p><p>4.4 Ist Vorauskasse per Banküberweisung vereinbart, ist die Zahlung sofort nach Vertragsabschluss fällig, sofern die Parteien keinen späteren Fälligkeitstermin vereinbart haben.
                            </p><p>4.5 Bei Zahlung mittels einer von PayPal angebotenen Zahlungsart erfolgt die Zahlungsabwicklung über den Zahlungsdienstleister PayPal (Europe) S.à r.l. et Cie, S.C.A., 22-24 Boulevard Royal, L-2449 Luxembourg (im Folgenden: "PayPal"), unter Geltung der PayPal-Nutzungsbedingungen, einsehbar unter <a href="https://www.paypal.com/de/webapps/mpp/ua/useragreement-full" target="_blank" rel="noopener">https://www.paypal.com/de/webapps/mpp/ua/useragreement-full</a> oder - falls der Kunde nicht über ein PayPal-Konto verfügt – unter Geltung der Bedingungen für Zahlungen ohne PayPal-Konto, einsehbar unter <a href="https://www.paypal.com/de/webapps/mpp/ua/privacywax-full" target="_blank" rel="noopener">https://www.paypal.com/de/webapps/mpp/ua/privacywax-full</a>.
                            </p><br />

                            <h5>§ 5. Liefer- und Versandbedingungen</h5>
                            <p>5.1 Die Lieferung von Waren erfolgt auf dem Versandweg an die vom Kunden angegebene Lieferanschrift, sofern nichts anderes vereinbart ist. Bei der Abwicklung der Transaktion ist die in der Bestellabwicklung des Verkäufers angegebene Lieferanschrift maßgeblich. Abweichend hiervon ist bei Auswahl der Zahlungsart PayPal die vom Kunden zum Zeitpunkt der Bezahlung bei PayPal hinterlegte Lieferanschrift maßgeblich.
                            </p><p>5.2 Sendet das Transportunternehmen die versandte Ware an den Verkäufer zurück, da eine Zustellung beim Kunden nicht möglich war, trägt der Kunde die Kosten für den erfolglosen Versand. Dies gilt nicht, wenn der Kunde den Umstand, der zur Unmöglichkeit der Zustellung geführt hat, nicht zu vertreten hat oder wenn er vorübergehend an der Annahme der angebotenen Leistung verhindert war, es sei denn, dass der Verkäufer ihm die Leistung eine angemessene Zeit vorher angekündigt hatte. Ferner gilt dies im Hinblick auf die Kosten für die Hinsendung nicht, wenn der Kunde sein Widerrufsrecht wirksam ausübt. Für die Rücksendekosten gilt bei wirksamer Ausübung des Widerrufsrechts durch den Kunden die in der Widerrufsbelehrung des Verkäufers hierzu getroffene Regelung.
                            </p><p>5.3 Bei Selbstabholung informiert der Verkäufer den Kunden zunächst per E-Mail darüber, dass die von ihm bestellte Ware zur Abholung bereit steht. Nach Erhalt dieser E-Mail kann der Kunde die Ware nach Absprache mit dem Verkäufer am Sitz des Verkäufers abholen. In diesem Fall werden keine Versandkosten berechnet.
                            </p><p>5.4 Gutscheine werden dem Kunden wie folgt überlassen:
                            </p><p>- per Download
                            </p><p>- per E-Mail
                            </p><p>- postalisch
                            </p><br />

                            <h5>§ 6. Eigentumsvorbehalt</h5>
                            <p>Tritt der Verkäufer in Vorleistung, behält er sich bis zur vollständigen Bezahlung des geschuldeten Kaufpreises das Eigentum an der gelieferten Ware vor.</p>
                            <br />

                            <h5>§ 7. Mängelhaftung (Gewährleistung)</h5>
                            <p>7.1 Ist die Kaufsache mangelhaft, gelten die Vorschriften der gesetzlichen Mängelhaftung.
                            </p><p>7.2 Der Kunde wird gebeten, angelieferte Waren mit offensichtlichen Transportschäden bei dem Zusteller zu reklamieren und den Verkäufer hiervon in Kenntnis zu setzen. Kommt der Kunde dem nicht nach, hat dies keinerlei Auswirkungen auf seine gesetzlichen oder vertraglichen Mängelansprüche.
                            </p><br />

                            <h5>§ 8. Besondere Bedingungen für die Verarbeitung von Waren nach bestimmten Vorgaben des Kunden</h5>
                            <p>8.1 Schuldet der Verkäufer nach dem Inhalt des Vertrages neben der Warenlieferung auch die Verarbeitung der Ware nach bestimmten Vorgaben des Kunden, hat der Kunde dem Betreiber alle für die Verarbeitung erforderlichen Inhalte wie Texte, Bilder oder Grafiken in den vom Betreiber vorgegebenen Dateiformaten, Formatierungen, Bild- und Dateigrößen zur Verfügung zu stellen und ihm die hierfür erforderlichen Nutzungsrechte einzuräumen. Für die Beschaffung und den Rechteerwerb an diesen Inhalten ist allein der Kunde verantwortlich. Der Kunde erklärt und übernimmt die Verantwortung dafür, dass er das Recht besitzt, die dem Verkäufer überlassenen Inhalte zu nutzen. Er trägt insbesondere dafür Sorge, dass hierdurch keine Rechte Dritter verletzt werden, insbesondere Urheber-, Marken- und Persönlichkeitsrechte.
                            </p><p>8.2 Der Kunde stellt den Verkäufer von Ansprüchen Dritter frei, die diese im Zusammenhang mit einer Verletzung ihrer Rechte durch die vertragsgemäße Nutzung der Inhalte des Kunden durch den Verkäufer diesem gegenüber geltend machen können. Der Kunde übernimmt hierbei auch die angemessenen Kosten der notwendigen Rechtsverteidigung einschließlich aller Gerichts- und Anwaltskosten in gesetzlicher Höhe. Dies gilt nicht, wenn die Rechtsverletzung vom Kunden nicht zu vertreten ist. Der Kunde ist verpflichtet, dem Verkäufer im Falle einer Inanspruchnahme durch Dritte unverzüglich, wahrheitsgemäß und vollständig alle Informationen zur Verfügung zu stellen, die für die Prüfung der Ansprüche und eine Verteidigung erforderlich sind.
                            </p><p>8.3 Der Verkäufer behält sich vor, Verarbeitungsaufträge abzulehnen, wenn die vom Kunden hierfür überlassenen Inhalte gegen gesetzliche oder behördliche Verbote oder gegen die guten Sitten verstoßen. Dies gilt insbesondere bei Überlassung verfassungsfeindlicher, rassistischer, fremdenfeindlicher, diskriminierender, beleidigender, Jugend gefährdender und/oder Gewalt verherrlichender Inhalte.
                            </p><br />

                            <h5>§ 9. Einlösung von Aktionsgutscheinen</h5>
                            <p>9.1 Gutscheine, die vom Verkäufer im Rahmen von Werbeaktionen mit einer bestimmten Gültigkeitsdauer unentgeltlich ausgegeben werden und die vom Kunden nicht käuflich erworben werden können (nachfolgend "Aktionsgutscheine"), können nur im Online-Shop des Verkäufers und nur im angegebenen Zeitraum eingelöst werden.
                            </p><p>9.2 Einzelne Produkte können von der Gutscheinaktion ausgeschlossen sein, sofern sich eine entsprechende Einschränkung aus dem Inhalt des Aktionsgutscheins ergibt.
                            </p><p>9.3 Aktionsgutscheine können nur vor Abschluss des Bestellvorgangs eingelöst werden. Eine nachträgliche Verrechnung ist nicht möglich.
                            </p><p>9.4 Bei einer Bestellung können auch mehrere Aktionsgutscheine eingelöst werden.
                            </p><p>9.5 Der Warenwert muss mindestens dem Betrag des Aktionsgutscheins entsprechen. Etwaiges Restguthaben wird vom Verkäufer nicht erstattet.
                            </p><p>9.6 Reicht der Wert des Aktionsgutscheins zur Deckung der Bestellung nicht aus, kann zur Begleichung des Differenzbetrages eine der übrigen vom Verkäufer angebotenen Zahlungsarten gewählt werden.
                            </p><p>9.7 Das Guthaben eines Aktionsgutscheins wird weder in Bargeld ausgezahlt noch verzinst.
                            </p><p>9.8 Der Aktionsgutschein wird nicht erstattet, wenn der Kunde die mit dem Aktionsgutschein ganz oder teilweise bezahlte Ware im Rahmen seines gesetzlichen Widerrufsrechts zurückgibt.
                            </p><p>9.9 Der Aktionsgutschein ist nur für die Verwendung durch die auf ihm benannte Person bestimmt. Eine Übertragung des Aktionsgutscheins auf Dritte ist ausgeschlossen. Der Verkäufer ist berechtigt, jedoch nicht verpflichtet, die materielle Anspruchsberechtigung des jeweiligen Gutscheininhabers zu prüfen.
                            </p><br />

                            <h5>§ 10. Einlösung von Geschenkgutscheinen</h5>
                            <p>10.1 Gutscheine, die über den Online-Shop des Verkäufers käuflich erworben werden können (nachfolgend "Geschenkgutscheine"), können nur im Online-Shop des Verkäufers eingelöst werden, sofern sich aus dem Gutschein nichts anderes ergibt.
                            </p><p>10.2 Geschenkgutscheine und Restguthaben von Geschenkgutscheinen sind bis zum Ende des dritten Jahres nach dem Jahr des Gutscheinkaufs einlösbar. Restguthaben werden dem Kunden bis zum Ablaufdatum gutgeschrieben.
                            </p><p>10.3 Geschenkgutscheine können nur vor Abschluss des Bestellvorgangs eingelöst werden. Eine nachträgliche Verrechnung ist nicht möglich.
                            </p><p>10.4 Pro Bestellung kann immer nur ein Geschenkgutschein eingelöst werden.
                            </p><p>10.5 Geschenkgutscheine können nur für den Kauf von Waren und nicht für den Kauf von weiteren Geschenkgutscheinen verwendet werden.
                            </p><p>10.6 Reicht der Wert des Geschenkgutscheins zur Deckung der Bestellung nicht aus, kann zur Begleichung des Differenzbetrages eine der übrigen vom Verkäufer angebotenen Zahlungsarten gewählt werden.
                            </p><p>10.7 Das Guthaben eines Geschenkgutscheins wird weder in Bargeld ausgezahlt noch verzinst.
                            </p><p>10.8 Der Geschenkgutschein ist übertragbar. Der Verkäufer kann mit befreiender Wirkung an den jeweiligen Inhaber, der den Geschenkgutschein im Online-Shop des Verkäufers einlöst, leisten. Dies gilt nicht, wenn der Verkäufer Kenntnis oder grob fahrlässige Unkenntnis von der Nichtberechtigung, der Geschäftsunfähigkeit oder der fehlenden Vertretungsberechtigung des jeweiligen Inhabers hat.
                            </p><br />

                            <h5>§ 11. Anwendbares Recht</h5>
                            <p>Für sämtliche Rechtsbeziehungen der Parteien gilt das Recht der Bundesrepublik Deutschland unter Ausschluss der Gesetze über den internationalen Kauf beweglicher Waren. Bei Verbrauchern gilt diese Rechtswahl nur insoweit, als nicht der gewährte Schutz durch zwingende Bestimmungen des Rechts des Staates, in dem der Verbraucher seinen gewöhnlichen Aufenthalt hat, entzogen wird.
                            </p><br />

                            <h5>§ 12. Gerichtsstand</h5>
                            <p>Handelt der Kunde als Kaufmann, juristische Person des öffentlichen Rechts oder öffentlich-rechtliches Sondervermögen mit Sitz im Hoheitsgebiet der Bundesrepublik Deutschland, ist ausschließlicher Gerichtsstand für alle Streitigkeiten aus diesem Vertrag der Geschäftssitz des Verkäufers. Hat der Kunde seinen Sitz außerhalb des Hoheitsgebiets der Bundesrepublik Deutschland, so ist der Geschäftssitz des Verkäufers ausschließlicher Gerichtsstand für alle Streitigkeiten aus diesem Vertrag, wenn der Vertrag oder Ansprüche aus dem Vertrag der beruflichen oder gewerblichen Tätigkeit des Kunden zugerechnet werden können. Der Verkäufer ist in den vorstehenden Fällen jedoch in jedem Fall berechtigt, das Gericht am Sitz des Kunden anzurufen.
                            </p><br />

                            <h5>§ 13. Alternative Streitbeilegung</h5>
                            <p>13.1 Die EU-Kommission stellt im Internet unter folgendem Link eine Plattform zur Online-Streitbeilegung bereit: https://ec.europa.eu/consumers/odr
                                Diese Plattform dient als Anlaufstelle zur außergerichtlichen Beilegung von Streitigkeiten aus Online-Kauf- oder Dienstleistungsverträgen, an denen ein Verbraucher beteiligt ist.
                            </p><p>13.2 Der Verkäufer ist zur Teilnahme an einem Streitbeilegungsverfahren vor einer Verbraucherschlichtungsstelle weder verpflichtet noch bereit.
                            </p><br />

                            <h5>§ 14. Widerrufsbelehrung & Widerrufsformular </h5>
                            <p>Verbrauchern steht ein Widerrufsrecht nach folgender Maßgabe zu, wobei Verbraucher jede natürliche Person ist, die ein Rechtsgeschäft zu Zwecken abschließt, die überwiegend weder ihrer gewerblichen noch ihrer selbständigen beruflichen Tätigkeit zugerechnet werden können:</p><br />
                            <h6>14.1 Widerrufsbelehrung</h6>
                            <p>Widerrufsrecht</p>
                            <p>Sie haben das Recht, binnen vierzehn Tagen ohne Angabe von Gründen diesen Vertrag zu widerrufen.</p>
                            <p>Die Widerrufsfrist beträgt vierzehn Tage ab dem Tag, an dem Sie oder ein von Ihnen benannter Dritter, der nicht der Beförderer ist, die letzte Ware in Besitz genommen haben bzw. hat. </p>
                            <p>Um Ihr Widerrufsrecht auszuüben, müssen Sie uns (Ince GmbH, Münzstraße 7, 30159 Hannover, Deutschland, Tel.: 0511 2155 001, E-Mail: info@efendibey.de) mittels einer eindeutigen Erklärung (z. B. ein mit der Post versandter Brief oder E-Mail) über Ihren Entschluss, diesen Vertrag zu widerrufen, informieren. Sie können dafür das beigefügte Muster-Widerrufsformular verwenden, das jedoch nicht vorgeschrieben ist.</p>
                            <p>Zur Wahrung der Widerrufsfrist reicht es aus, dass Sie die Mitteilung über die Ausübung des Widerrufsrechts vor Ablauf der Widerrufsfrist absenden.</p><br />

                            <p>Folgen des Widerrufs</p>
                            <p>Wenn Sie diesen Vertrag widerrufen, haben wir Ihnen alle Zahlungen, die wir von Ihnen erhalten haben, einschließlich der Lieferkosten (mit Ausnahme der zusätzlichen Kosten, die sich daraus ergeben, dass Sie eine andere Art der Lieferung als die von uns angebotene, günstigste Standardlieferung gewählt haben), unverzüglich und spätestens binnen vierzehn Tagen ab dem Tag zurückzuzahlen, an dem die Mitteilung über Ihren Widerruf dieses Vertrags bei uns eingegangen ist. Für diese Rückzahlung verwenden wir dasselbe Zahlungsmittel, das Sie bei der ursprünglichen Transaktion eingesetzt haben, es sei denn, mit Ihnen wurde ausdrücklich etwas anderes vereinbart; in keinem Fall werden Ihnen wegen dieser Rückzahlung Entgelte berechnet. Wir können die Rückzahlung verweigern, bis wir die Waren wieder zurückerhalten haben oder bis Sie den Nachweis erbracht haben, dass Sie die Waren zurückgesandt haben, je nachdem, welches der frühere Zeitpunkt ist.</p>
                            <p>Sie haben die Waren unverzüglich und in jedem Fall spätestens binnen vierzehn Tagen ab dem Tag, an dem Sie uns über den Widerruf dieses Vertrags unterrichten, an uns zurückzusenden oder zu übergeben. Die Frist ist gewahrt, wenn Sie die Waren vor Ablauf der Frist von vierzehn Tagen absenden.
                            </p><p>Sie tragen die unmittelbaren Kosten der Rücksendung der Waren.
                            </p><p>Sie müssen für einen etwaigen Wertverlust der Waren nur aufkommen, wenn dieser Wertverlust auf einen zur Prüfung der Beschaffenheit, Eigenschaften und Funktionsweise der Waren nicht notwendigen Umgang mit ihnen zurückzuführen ist.
                                <br />
                            </p><p>Ausschluss bzw. vorzeitiges Erlöschen des Widerrufsrechts
                            </p><p>Das Widerrufsrecht besteht nicht bei Verträgen zur Lieferung von Waren, die nicht vorgefertigt sind und für deren Herstellung eine individuelle Auswahl oder Bestimmung durch den Verbraucher maßgeblich ist oder die eindeutig auf die persönlichen Bedürfnisse des Verbrauchers zugeschnitten sind.
                            </p><p>Das Widerrufsrecht besteht nicht bei Verträgen zur Lieferung von Waren, die schnell verderben können oder deren Verfallsdatum schnell überschritten würde.
                            </p>
                            <h6>14.2 Widerrufsformular</h6>
                            <p>Wenn Sie den Vertrag widerrufen wollen, dann füllen Sie bitte dieses Formular aus und senden es zurück.
                            </p><p>An
                            </p><p>Ince GmbH
                            </p><p>Münzstraße 7
                            </p><p>30159 Hannover
                            </p><p>Deutschland
                            </p><p>E-Mail: info@efendibey.de
                            </p><p>Hiermit widerrufe(n) ich/wir (*) den von mir/uns (*) abgeschlossenen Vertrag über den Kauf der folgenden Waren (*)/die Erbringung der folgenden Dienstleistung (*)
                            </p><p>___________________________
                            </p><p>Bestellt am (*) _________ /
                            </p><p>erhalten am (*) _________
                            </p><p>___________________________
                            </p><p>Name des/der Verbraucher(s)
                            </p><p>____________________________
                            </p><p>Anschrift des/der Verbraucher(s)
                            </p><p>____________________________
                            </p><p>Unterschrift des/der Verbraucher(s) (nur bei Mitteilung auf Papier)
                            </p><p>_______________
                            </p><p>Datum
                            </p><p>(*) Unzutreffendes streichen</p>
                        </div>
                        <div class="modal-footer">
                            <button class="modal-action modal-close waves-effect btn-flat close-button btn-large">OK</button>
                        </div>
                    </div>

                </section >
            </ReactScoped >
        )
    }
}

export default withRouter(CustomCake);