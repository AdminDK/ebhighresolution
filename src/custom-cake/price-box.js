import React, { Component } from 'react';
import { ReactScoped, ViewEncapsulation } from 'react-scoped';
import styles from './price-box.css';

export default class PriceBox extends Component{

    constructor(props){
        super(props);
    }

    render(){
        return (
            <ReactScoped encapsulation={ViewEncapsulation.Emulated} styles={[styles]}>
                <div className={'price-box '+(this.props.active ? 'active': '')} >
                    <span className='label'>
                        Gesamt Preis&nbsp;:&nbsp;&nbsp;
                    </span>
                    <span className='amount'>
                        {this.props.price} €
                    </span>
                </div>
            </ReactScoped>
        );
    }

}
