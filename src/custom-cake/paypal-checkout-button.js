/* jshint esversion: 6 */

import React, { Component } from 'react';
import paypal from 'paypal-checkout';
import { ReactScoped, ViewEncapsulation } from 'react-scoped';
import styles from "./paypal-checkout-button.css";

export default class PaypalButton extends Component {
   constructor(props) {
      super(props);
   }

   componentDidMount() {
      // alert(window.location.hostname);
      // console.log("this.props ================ ",this.props);
      var url = window.location.href;
      var urlSplit = url.split('/');
      //console.log("urlSplit ============== ",urlSplit[2]);
      let paypalEnviroment = 'production';
      if (urlSplit[2] === 'localhost:5443' || urlSplit[2] === 'qa.efendibey.digiklug.com') {
         paypalEnviroment = 'sandbox';
      }

      var props = this.props;

      paypal.Button.render({
         env: paypalEnviroment, // sandbox | production
         style: {
            label: 'pay',
            size: 'large',   // medium | large | responsive
            shape: 'rect',    // pill | rect
            color: 'blue',     // gold | blue | silver | black
            tagline: false
         },
         locale: 'de_DE',
         client: {
            production: 'AcO5EqY244fl2hZVJ5F-AhyONnBLNUS2ci8oTbV4bFYDk4AixMiiDBTacKfT4YHMp4YKogPcJXBcSxB2',
            sandbox: 'AXeQU6o74B_ypeAw87D5B_e4HnpCxIbSZglvwbbau2R9uDEBDZpCQgF1aJYaYoOQQng-xKeF5Y6VTGAR'
         },
         payment: function (data, actions) {
            // console.log('getOrderIdForReference', props.getOrderIdForReference());
            props.handleSubmit({
               preventDefault: function () { },
               storeType: 'paypal',
               orderId: props.getOrderIdForReference(),
            });
            return actions.payment.create({
               payment: {
                  transactions: [
                     {
                        amount: { total: props.getPrice(), currency: 'EUR' },
                        custom: props.getOrderIdForReference()
                     }
                  ]
               }
            });
         },
         onAuthorize: function (data, actions) {
            return actions.payment.execute().then(function (payment_data) {
               //console.log('payment_data ', payment_data);
               let payment = {
                  payerID: data.payerID,
                  paymentID: data.paymentID,
                  paymentToken: data.paymentToken,
                  returnUrl: data.returnUrl,
                  address: payment_data.payer.payer_info.shipping_address,
                  email: payment_data.payer.payer_info.email,
                  transactionID: payment_data.transactions[0].related_resources[0].sale.id,
               };

               props.handleSubmit({
                  preventDefault: function () { },
                  payment: payment,
                  storeType: 'paypalTrnSuccess',
                  orderIdForUpdate: payment_data.transactions[0].custom
               });
            });
         },
         onCancel: function (data, actions) {
            props.handleSubmit({
               preventDefault: function () { },
               storeType: 'paypalTrnCancelled',
               orderIdForUpdate: props.getOrderIdForReference()
            });
         }
      }, '#paypal-button-container');

      console.log("At bottom of paypal button render.....")
   }

   render() {
      let btnState = 'inline-block';
      if (this.props.active === 'false') {
         btnState = 'none';
      }
      this.btnStyle = {
         'display': btnState,
      };

      console.log("In paypal button....");

      return (
         <ReactScoped encapsulation={ViewEncapsulation.Emulated} styles={[styles]}>
            <div style={this.btnStyle}>
               {/* <div id="paypal-button-container" style={{height:"80px",marginRight:"15px",backgroundColor:"#009cde",paddingTop:"22px"}}></div> */}
               <div class="outer-container">
                  <div id="paypal-button-container"></div>
               </div>
            </div>
         </ReactScoped>
      );
   }
}
