
import React, { Component } from 'react';
import PaypalExpressBtn from 'react-paypal-express-checkout';

export default class PaypalBtn extends Component {

	constructor(props) {
		super(props);
	}

	render() {
		const client = {
			production: 'AcO5EqY244fl2hZVJ5F-AhyONnBLNUS2ci8oTbV4bFYDk4AixMiiDBTacKfT4YHMp4YKogPcJXBcSxB2',
			sandbox: 'AXeQU6o74B_ypeAw87D5B_e4HnpCxIbSZglvwbbau2R9uDEBDZpCQgF1aJYaYoOQQng-xKeF5Y6VTGAR'
		};

		const env = 'production'; // you can set here to 'sandbox' for sandbox and 'production' for production

		// Document on Paypal's currency code: https://developer.paypal.com/docs/classic/api/currency_codes/
		const currency = 'EUR';

		let btnState = 'inline-block';
		if (this.props.active === 'false') {
			btnState = 'none';
		}
		this.btnStyle = { 'display': btnState };

		const onSuccess = (payment) => {
			// Payment Successfull!
			console.log("The payment was succeeded!", payment);
			let dummyEvent = { preventDefault: () => { } }
			this.props.handleSubmit(dummyEvent);
		}

		const onCancel = (data) => {
			// User pressed "cancel" or close Paypal's popup!
			console.log('The payment was cancelled!', data);
		}

		const onError = (err) => {
			// The main Paypal's script cannot be loaded or somethings block the loading of that script!
			console.log("Error!", err);
			// Because the Paypal's main script is loaded asynchronously from "https://www.paypalobjects.com/api/checkout.js"
			// => sometimes it may take about 0.5 second for everything to get set, or for the button to appear
		};

		return (
			<div style={this.btnStyle}>
				<PaypalExpressBtn env={env} client={client} currency={currency} total={this.props.price} onError={onError} onSuccess={onSuccess} onCancel={onCancel} />
			</div>
		);
	}
}
