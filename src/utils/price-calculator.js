/* jshint esversion: 6 */

export default class PriceCalculator {
   static calculatePrice(productPrices, selectedValues, cuponPrice, deliveryCharge) {
      // console.log(" deliveryCharge ", deliveryCharge);
      
   
      let totalPrice = 0;
      totalPrice += deliveryCharge;
      let taxAtDefaultRate = 0;
      let taxAtCustomRate = 0;
      let shapeAndStylePrice = 0;
      let shapeAndStyleTaxAtDefaultRate = 0;
      let shapeAndStyleTaxAtCustomRate = 0;
      let designAndFlavorPrice = 0;
      let designAndFlavorTaxAtDefaultRate = 0;
      let designAndFlavorTaxAtCustomRate = 0;

      let basePrice = PriceCalculator.calculateBasePrice(productPrices, selectedValues,cuponPrice);
      if (basePrice.price !== undefined && basePrice.price != null) {
         totalPrice += basePrice.price;
         taxAtDefaultRate += basePrice.taxAtDefaultRate;
         taxAtCustomRate += basePrice.taxAtCustomRate;
         shapeAndStylePrice += basePrice.price;
         shapeAndStyleTaxAtDefaultRate += basePrice.taxAtDefaultRate;
         shapeAndStyleTaxAtCustomRate += basePrice.taxAtCustomRate;
      } else {
         return {
            price: 'N/A',
            totalTax: 0,
            taxAtDefaultRate: 0,
            taxAtCustomRate: 0,
            shapeAndStylePrice: 0,
            shapeAndStyleTaxAtDefaultRate: 0,
            shapeAndStyleTaxAtCustomRate: 0,
            designAndFlavorPrice: 0,
            designAndFlavorTaxAtDefaultRate: 0,
            designAndFlavorTaxAtCustomRate: 0 
         };
      }
      //console.log('Base Price: ', basePrice);

      let decorationPrice = PriceCalculator.calculateDecorationPrice(productPrices, selectedValues);
      totalPrice += decorationPrice.price;
      taxAtDefaultRate += decorationPrice.taxAtDefaultRate;
      taxAtCustomRate += decorationPrice.taxAtCustomRate;
      designAndFlavorPrice += decorationPrice.price;
      designAndFlavorTaxAtDefaultRate += decorationPrice.taxAtDefaultRate;
      designAndFlavorTaxAtCustomRate += decorationPrice.taxAtCustomRate;
      //console.log('Decoration Price: ', decorationPrice);

      let extraDecorationPrice = PriceCalculator.calculateExtraDecorationPrice(productPrices, selectedValues.extra_decoration);
      totalPrice += extraDecorationPrice.price;
      taxAtDefaultRate += extraDecorationPrice.taxAtDefaultRate;
      taxAtCustomRate += extraDecorationPrice.taxAtCustomRate;
      designAndFlavorPrice += extraDecorationPrice.price;
      designAndFlavorTaxAtDefaultRate += extraDecorationPrice.taxAtDefaultRate;
      designAndFlavorTaxAtCustomRate += extraDecorationPrice.taxAtCustomRate;
      //console.log('Extra Decoration Price: ', extraDecorationPrice);

      let flavorPrice = PriceCalculator.calculateFlavorPrice(productPrices, selectedValues.flavor);
      totalPrice += flavorPrice.price;
      taxAtDefaultRate += flavorPrice.taxAtDefaultRate;
      taxAtCustomRate += flavorPrice.taxAtCustomRate;
      designAndFlavorPrice += flavorPrice.price;
      designAndFlavorTaxAtDefaultRate += flavorPrice.taxAtDefaultRate;
      designAndFlavorTaxAtCustomRate += flavorPrice.taxAtCustomRate;
      //console.log('Flavor Price: ', flavorPrice.price);

      let extraFlavorPrice = PriceCalculator.calculateExtraFlavorPrice(productPrices, selectedValues.additional_flavor);
      totalPrice += extraFlavorPrice.price;
      taxAtDefaultRate += extraFlavorPrice.taxAtDefaultRate;
      taxAtCustomRate += extraFlavorPrice.taxAtCustomRate;
      designAndFlavorPrice += extraFlavorPrice.price;
      designAndFlavorTaxAtDefaultRate += extraFlavorPrice.taxAtDefaultRate;
      designAndFlavorTaxAtCustomRate += extraFlavorPrice.taxAtCustomRate;
      //console.log('Extra Flavor Price: ', extraFlavorPrice);

      return {
         price: parseFloat(totalPrice.toFixed(2)),
         totalTax: parseFloat((taxAtDefaultRate + taxAtCustomRate).toFixed(2)),
         taxAtDefaultRate: parseFloat(taxAtDefaultRate.toFixed(2)),
         taxAtCustomRate: parseFloat(taxAtCustomRate.toFixed(2)),
         shapeAndStylePrice: parseFloat(shapeAndStylePrice.toFixed(2)),
         shapeAndStyleTaxAtDefaultRate: parseFloat(shapeAndStyleTaxAtDefaultRate.toFixed(2)),
         shapeAndStyleTaxAtCustomRate: parseFloat(shapeAndStyleTaxAtCustomRate.toFixed(2)),
         designAndFlavorPrice: parseFloat(designAndFlavorPrice.toFixed(2)),
         designAndFlavorTaxAtDefaultRate: parseFloat(designAndFlavorTaxAtDefaultRate.toFixed(2)),
         designAndFlavorTaxAtCustomRate: parseFloat(designAndFlavorTaxAtCustomRate.toFixed(2)),
      };
   }

   static calculateBasePrice(productPrices, selectedValues,cuponPrice) {
      var basePrice = (productPrices.priceRules[`${selectedValues.shape}_${selectedValues.size}`])-cuponPrice;
      var taxAtDefaultRate = 0.0;
      var taxAtCustomRate = 0.0;

      if (!basePrice) {
         return {
            price: basePrice,
            taxAtDefaultRate: 0,
            taxAtCustomRate: 0
         };
      }

      var taxRate = productPrices.taxRules[`${selectedValues.shape}_${selectedValues.size}`];

      if (!taxRate) {
         taxRate = productPrices.taxRules.Default;
         taxAtDefaultRate = basePrice - basePrice / (1 + taxRate);
      } else {
         taxAtCustomRate = basePrice - basePrice / (1 + taxRate);
      }

      return {
         price: basePrice,
         taxAtDefaultRate: taxAtDefaultRate,
         taxAtCustomRate: taxAtCustomRate
      };
   }

   static calculateDecorationPrice(productPrices, selectedValues) {
      var decorationPrice = productPrices.priceRules[`${selectedValues.shape}_${selectedValues.size}_${selectedValues.decoration}`];
      var taxAtDefaultRate = 0.0;
      var taxAtCustomRate = 0.0;

      if (!decorationPrice) {
         decorationPrice = 0.0;
      }

      var taxRate = productPrices.taxRules[`${selectedValues.shape}_${selectedValues.size}_${selectedValues.decoration}`];
      if (!taxRate) {
         taxRate = productPrices.taxRules.Default;
         taxAtDefaultRate = decorationPrice - decorationPrice / (1 + taxRate);
      } else {
         taxAtCustomRate = decorationPrice - decorationPrice / (1 + taxRate);
      }

      return {
         price: decorationPrice,
         taxAtDefaultRate: taxAtDefaultRate,
         taxAtCustomRate: taxAtCustomRate
      };
   }

   static calculateExtraDecorationPrice(productPrices, extraDecorations) {
      var extraDecorationsPrice = 0;
      var taxAtDefaultRate = 0;
      var taxAtCustomRate = 0;
      extraDecorations.forEach(decoration => {
         if (productPrices.extraDecorations[decoration]) {
            extraDecorationsPrice += productPrices.extraDecorations[decoration].price;

            var taxRate = productPrices.taxRules[decoration];
            if (!taxRate) {
               taxRate = productPrices.taxRules.Default;
               taxAtDefaultRate += productPrices.extraDecorations[decoration].price - productPrices.extraDecorations[decoration].price / (1 + taxRate);
            } else {
               taxAtCustomRate += productPrices.extraDecorations[decoration].price - productPrices.extraDecorations[decoration].price / (1 + taxRate);
            }
         }
      });

      return {
         price: extraDecorationsPrice,
         taxAtDefaultRate: taxAtDefaultRate,
         taxAtCustomRate: taxAtCustomRate
      };
   }

   static calculateFlavorPrice(productPrices, flavor) {
      var flavorPrice = productPrices.flavors[flavor];
      var taxAtDefaultRate = 0.0;
      var taxAtCustomRate = 0.0;

      flavorPrice = flavorPrice ? flavorPrice.price : 0.0;

      var taxRate = productPrices.taxRules[`${flavor}`];
      if (!taxRate) {
         taxRate = productPrices.taxRules.Default;
         taxAtDefaultRate = flavorPrice - flavorPrice / (1 + taxRate);
      } else {
         taxAtCustomRate = flavorPrice - flavorPrice / (1 + taxRate);
      }

      return {
         price: flavorPrice,
         taxAtDefaultRate: taxAtDefaultRate,
         taxAtCustomRate: taxAtCustomRate
      };
   }

   static calculateExtraFlavorPrice(productPrices, extraFlavors) {
      var extraFlavorsPrice = 0;
      var taxAtDefaultRate = 0.0;
      var taxAtCustomRate = 0.0;

      extraFlavors.forEach(flavor => {
         if (productPrices.extraFlavors[flavor]) {
            extraFlavorsPrice += productPrices.extraFlavors[flavor].price;

            var taxRate = productPrices.taxRules[flavor];
            if (!taxRate) {
               taxRate = productPrices.taxRules.Default;
               taxAtDefaultRate += productPrices.extraFlavors[flavor].price - productPrices.extraFlavors[flavor].price / (1 + taxRate);
            } else {
               taxAtCustomRate += productPrices.extraFlavors[flavor].price - productPrices.extraFlavors[flavor].price / (1 + taxRate);
            }
         }
      });

      return {
         price: extraFlavorsPrice,
         taxAtDefaultRate: taxAtDefaultRate,
         taxAtCustomRate: taxAtCustomRate
      };
   }

   static testingIt() {
      return {
         price: 10.20,
         totalTax: 0.45,
         taxAtDefaultRate: 0.40,
         taxAtCustomRate: 0.05,
         shapeAndStylePrice: 6.00,
         shapeAndStyleTaxAtDefaultRate: 0.50,
         shapeAndStyleTaxAtCustomRate: 0.05,
         designAndFlavorPrice: 2.00,
         designAndFlavorTaxAtDefaultRate: 0.18,
         designAndFlavorTaxAtCustomRate: 0.09
      };
   }
}
