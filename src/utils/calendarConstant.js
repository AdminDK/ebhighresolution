export const i18n_DE = {
    months: [
        'Januar',
        'Februar',
        'März',
        'April',
        'Mai',
        'Juni',
        'Juli',
        'August',
        'September',
        'Oktober',
        'November',
        'Dezember'
    ],
    monthsShort: [
        'Jan',
        'Feb',
        'Mär',
        'Apr',
        'Mai',
        'Jun',
        'Jul',
        'Aug',
        'Sep',
        'Okt',
        'Nov',
        'Dez'
    ],
    weekdaysShort: [
        'So',//Sunday
        'Mo',//Monday
        'Di',//Tuesday
        'Mi',//Wednesday
        'Do',//Thursday
        'Fr',//Friday
        'Sa'//Saturday
    ],
    clear: 'Löschen',
    done: 'Schließen'
}