import PriceCalculator from './price-calculator';
import { equal } from 'assert';
import { isNullOrUndefined } from 'util';


describe('Error Cases', () => {
   var productPrices = require('../../models/product-list').getProductList();

   var formData = {
      fname: '',
      lname: '',
      mobile: '',
      email: '',
      pickupDeliverySelection: false,
      deliveryDate: '',
      deliveryTime: '',
      street: '',
      houseNo: '',
      city: '',
      shape: '',
      size: '',
      color: '',
      decoration: '',
      flavor: '',
      additional_flavor: [],
      extra_decoration: [],
      text: '',
      remarks: '',
      photo: '',
      paymentType: 'cod',
   };

   it('Shows N/A for unset values', () => {
      var calculatedPrices = PriceCalculator.calculatePrice(productPrices, formData);

      equal(calculatedPrices.price, 'N/A');
      equal(calculatedPrices.totalTax, 0);
      equal(calculatedPrices.taxAtDefaultRate, 0);
      equal(calculatedPrices.taxAtCustomRate, 0);
      equal(calculatedPrices.shapeAndStylePrice, 0);
      equal(calculatedPrices.shapeAndStyleTaxAtDefaultRate, 0);
      equal(calculatedPrices.shapeAndStyleTaxAtCustomRate, 0);
      equal(calculatedPrices.designAndFlavorPrice, 0);
      equal(calculatedPrices.designAndFlavorTaxAtDefaultRate, 0);
      equal(calculatedPrices.designAndFlavorTaxAtCustomRate, 0);
   });

   it('shows N/A for unsupported combination', () => {
      formData.size = "NO1";
      formData.shape = "SHP03"; // Rectangle
      var calculatedPrices = PriceCalculator.calculatePrice(productPrices, formData);

      equal(calculatedPrices.price, 'N/A');
      equal(calculatedPrices.totalTax, 0);
      equal(calculatedPrices.taxAtDefaultRate, 0);
      equal(calculatedPrices.taxAtCustomRate, 0);
      equal(calculatedPrices.shapeAndStylePrice, 0);
      equal(calculatedPrices.shapeAndStyleTaxAtDefaultRate, 0);
      equal(calculatedPrices.shapeAndStyleTaxAtCustomRate, 0);
      equal(calculatedPrices.designAndFlavorPrice, 0);
      equal(calculatedPrices.designAndFlavorTaxAtDefaultRate, 0);
      equal(calculatedPrices.designAndFlavorTaxAtCustomRate, 0);
   });
});


describe('Flavor Availability', () => {
   var productPrices = require('../../models/product-list').getProductList();

   it('Flavor FLV01 - Always available', () => {
      equal(isNullOrUndefined(productPrices.flavors.FLV01.availableFrom), true);
      equal(isNullOrUndefined(productPrices.flavors.FLV01.availableTo), true);
   });

   it('Flavor FLV02 - Always available', () => {
      equal(isNullOrUndefined(productPrices.flavors.FLV02.availableFrom), true);
      equal(isNullOrUndefined(productPrices.flavors.FLV02.availableTo), true);
   });

   it('Flavor FLV03 - Always available', () => {
      equal(isNullOrUndefined(productPrices.flavors.FLV03.availableFrom), true);
      equal(isNullOrUndefined(productPrices.flavors.FLV03.availableTo), true);
   });

   it('Flavor FLV04 - Always available', () => {
      equal(isNullOrUndefined(productPrices.flavors.FLV04.availableFrom), true);
      equal(isNullOrUndefined(productPrices.flavors.FLV04.availableTo), true);
   });

   it('Flavor FLV05 - Always available', () => {
      equal(isNullOrUndefined(productPrices.flavors.FLV05.availableFrom), true);
      equal(isNullOrUndefined(productPrices.flavors.FLV05.availableTo), true);
   });

   it('Flavor FLV06 - Always available', () => {
      equal(isNullOrUndefined(productPrices.flavors.FLV06.availableFrom), true);
      equal(isNullOrUndefined(productPrices.flavors.FLV06.availableTo), true);
   });

   it('Flavor FLV07 - Always available', () => {
      equal(isNullOrUndefined(productPrices.flavors.FLV07.availableFrom), true);
      equal(isNullOrUndefined(productPrices.flavors.FLV07.availableTo), true);
   });

   it('Flavor FLV08 - Always available', () => {
      equal(isNullOrUndefined(productPrices.flavors.FLV08.availableFrom), true);
      equal(isNullOrUndefined(productPrices.flavors.FLV08.availableTo), true);
   });

   it('Flavor FLV09 - Always available', () => {
      equal(isNullOrUndefined(productPrices.flavors.FLV09.availableFrom), true);
      equal(isNullOrUndefined(productPrices.flavors.FLV09.availableTo), true);
   });

   it('Flavor FLV10 - Always available', () => {
      equal(isNullOrUndefined(productPrices.flavors.FLV10.availableFrom), true);
      equal(isNullOrUndefined(productPrices.flavors.FLV10.availableTo), true);
   });

   it('Flavor FLV11 - Always available', () => {
      equal(isNullOrUndefined(productPrices.flavors.FLV11.availableFrom), true);
      equal(isNullOrUndefined(productPrices.flavors.FLV11.availableTo), true);
   });

   it('Flavor FLV12 - Available from 20-May to 31-Aug', () => {
      equal(isNullOrUndefined(productPrices.flavors.FLV12.availableFrom), false);
      equal(isNullOrUndefined(productPrices.flavors.FLV12.availableTo), false);

      equal(productPrices.flavors.FLV12.availableFrom.month, 3);
      equal(productPrices.flavors.FLV12.availableFrom.date, 1);
      equal(productPrices.flavors.FLV12.availableTo.month, 7);
      equal(productPrices.flavors.FLV12.availableTo.date, 31);
   });

   it('Extra Flavor FLV01 - Always available', () => {
      equal(isNullOrUndefined(productPrices.extraFlavors.FLV01.availableFrom), true);
      equal(isNullOrUndefined(productPrices.extraFlavors.FLV01.availableTo), true);
   });

   it('Extra Flavor FLV02 - Always available', () => {
      equal(isNullOrUndefined(productPrices.extraFlavors.FLV02.availableFrom), true);
      equal(isNullOrUndefined(productPrices.extraFlavors.FLV02.availableTo), true);
   });

   it('Extra Flavor FLV03 - Always available', () => {
      equal(isNullOrUndefined(productPrices.extraFlavors.FLV03.availableFrom), true);
      equal(isNullOrUndefined(productPrices.extraFlavors.FLV03.availableTo), true);
   });

   it('Extra Flavor FLV04 - Always available', () => {
      equal(isNullOrUndefined(productPrices.extraFlavors.FLV04.availableFrom), true);
      equal(isNullOrUndefined(productPrices.extraFlavors.FLV04.availableTo), true);
   });

   it('Extra Flavor FLV05 - Always available', () => {
      equal(isNullOrUndefined(productPrices.extraFlavors.FLV05.availableFrom), true);
      equal(isNullOrUndefined(productPrices.extraFlavors.FLV05.availableTo), true);
   });

   it('Extra Flavor FLV06 - Always available', () => {
      equal(isNullOrUndefined(productPrices.extraFlavors.FLV06.availableFrom), true);
      equal(isNullOrUndefined(productPrices.extraFlavors.FLV06.availableTo), true);
   });

   it('Extra Flavor FLV07 - Always available', () => {
      equal(isNullOrUndefined(productPrices.extraFlavors.FLV07.availableFrom), true);
      equal(isNullOrUndefined(productPrices.extraFlavors.FLV07.availableTo), true);
   });

   it('Extra Flavor FLV08 - Always available', () => {
      equal(isNullOrUndefined(productPrices.extraFlavors.FLV08.availableFrom), true);
      equal(isNullOrUndefined(productPrices.extraFlavors.FLV08.availableTo), true);
   });

   it('Extra Flavor FLV09 - Always available', () => {
      equal(isNullOrUndefined(productPrices.extraFlavors.FLV09.availableFrom), true);
      equal(isNullOrUndefined(productPrices.extraFlavors.FLV09.availableTo), true);
   });

   it('Extra Flavor FLV10 - Always available', () => {
      equal(isNullOrUndefined(productPrices.extraFlavors.FLV10.availableFrom), true);
      equal(isNullOrUndefined(productPrices.extraFlavors.FLV10.availableTo), true);
   });

   it('Extra Flavor FLV11 - Always available', () => {
      equal(isNullOrUndefined(productPrices.extraFlavors.FLV11.availableFrom), true);
      equal(isNullOrUndefined(productPrices.extraFlavors.FLV11.availableTo), true);
   });

   it('Extra Flavor FLV12 - Available from 20-May to 31-Aug', () => {
      equal(isNullOrUndefined(productPrices.extraFlavors.FLV12.availableFrom), false);
      equal(isNullOrUndefined(productPrices.extraFlavors.FLV12.availableTo), false);

      equal(productPrices.extraFlavors.FLV12.availableFrom.month, 3);
      equal(productPrices.extraFlavors.FLV12.availableFrom.date, 1);
      equal(productPrices.extraFlavors.FLV12.availableTo.month, 7);
      equal(productPrices.extraFlavors.FLV12.availableTo.date, 31);
   });
});


describe('Base Pricing Rules', () => {
   var productPrices = require('../../models/product-list').getProductList();

   var formData = {
      fname: '',
      lname: '',
      mobile: '',
      email: '',
      pickupDeliverySelection: false,
      deliveryDate: '',
      deliveryTime: '',
      street: '',
      houseNo: '',
      city: '',
      shape: '',
      size: '',
      color: '',
      decoration: '',
      flavor: '',
      additional_flavor: [],
      extra_decoration: [],
      text: '',
      remarks: '',
      photo: '',
      paymentType: 'cod',
   };

   it('SHP01_NO1 = 18.0', () => {
      formData.shape = "SHP01";
      formData.size = "NO1";

      var calculatedPrices = PriceCalculator.calculatePrice(productPrices, formData);

      equal(calculatedPrices.price, 18.0);
      equal(calculatedPrices.totalTax, 1.18);
      equal(calculatedPrices.taxAtDefaultRate, 1.18);
      equal(calculatedPrices.taxAtCustomRate, 0);
      equal(calculatedPrices.shapeAndStylePrice, 18.0);
      equal(calculatedPrices.shapeAndStyleTaxAtDefaultRate, 1.18);
      equal(calculatedPrices.shapeAndStyleTaxAtCustomRate, 0);
      equal(calculatedPrices.designAndFlavorPrice, 0);
      equal(calculatedPrices.designAndFlavorTaxAtDefaultRate, 0);
      equal(calculatedPrices.designAndFlavorTaxAtCustomRate, 0);
   });

   it('SHP01_NO2 = 24.0', () => {
      formData.shape = "SHP01";
      formData.size = "NO2";

      var calculatedPrices = PriceCalculator.calculatePrice(productPrices, formData);

      equal(calculatedPrices.price, 24.0);
      equal(calculatedPrices.totalTax, 1.57);
      equal(calculatedPrices.taxAtDefaultRate, 1.57);
      equal(calculatedPrices.taxAtCustomRate, 0);
      equal(calculatedPrices.shapeAndStylePrice, 24.0);
      equal(calculatedPrices.shapeAndStyleTaxAtDefaultRate, 1.57);
      equal(calculatedPrices.shapeAndStyleTaxAtCustomRate, 0);
      equal(calculatedPrices.designAndFlavorPrice, 0);
      equal(calculatedPrices.designAndFlavorTaxAtDefaultRate, 0);
      equal(calculatedPrices.designAndFlavorTaxAtCustomRate, 0);
   });

   it('SHP01_NO3 = 30.0', () => {
      formData.shape = "SHP01";
      formData.size = "NO3";

      var calculatedPrices = PriceCalculator.calculatePrice(productPrices, formData);

      equal(calculatedPrices.price, 30.0);
      equal(calculatedPrices.totalTax, 1.96);
      equal(calculatedPrices.taxAtDefaultRate, 1.96);
      equal(calculatedPrices.taxAtCustomRate, 0);
      equal(calculatedPrices.shapeAndStylePrice, 30.0);
      equal(calculatedPrices.shapeAndStyleTaxAtDefaultRate, 1.96);
      equal(calculatedPrices.shapeAndStyleTaxAtCustomRate, 0);
      equal(calculatedPrices.designAndFlavorPrice, 0);
      equal(calculatedPrices.designAndFlavorTaxAtDefaultRate, 0);
      equal(calculatedPrices.designAndFlavorTaxAtCustomRate, 0);
   });

   it('SHP02_NO1 = 24.0', () => {
      formData.shape = "SHP02";
      formData.size = "NO1";

      var calculatedPrices = PriceCalculator.calculatePrice(productPrices, formData);

      equal(calculatedPrices.price, 24.0);
      equal(calculatedPrices.totalTax, 1.57);
      equal(calculatedPrices.taxAtDefaultRate, 1.57);
      equal(calculatedPrices.taxAtCustomRate, 0);
      equal(calculatedPrices.shapeAndStylePrice, 24.0);
      equal(calculatedPrices.shapeAndStyleTaxAtDefaultRate, 1.57);
      equal(calculatedPrices.shapeAndStyleTaxAtCustomRate, 0);
      equal(calculatedPrices.designAndFlavorPrice, 0);
      equal(calculatedPrices.designAndFlavorTaxAtDefaultRate, 0);
      equal(calculatedPrices.designAndFlavorTaxAtCustomRate, 0);
   });

   it('SHP02_NO2 = 30.0', () => {
      formData.shape = "SHP02";
      formData.size = "NO2";

      var calculatedPrices = PriceCalculator.calculatePrice(productPrices, formData);

      equal(calculatedPrices.price, 30.0);
      equal(calculatedPrices.totalTax, 1.96);
      equal(calculatedPrices.taxAtDefaultRate, 1.96);
      equal(calculatedPrices.taxAtCustomRate, 0);
      equal(calculatedPrices.shapeAndStylePrice, 30.0);
      equal(calculatedPrices.shapeAndStyleTaxAtDefaultRate, 1.96);
      equal(calculatedPrices.shapeAndStyleTaxAtCustomRate, 0);
      equal(calculatedPrices.designAndFlavorPrice, 0);
      equal(calculatedPrices.designAndFlavorTaxAtDefaultRate, 0);
      equal(calculatedPrices.designAndFlavorTaxAtCustomRate, 0);
   });

   it('SHP02_NO3 = 36.0', () => {
      formData.shape = "SHP02";
      formData.size = "NO3";

      var calculatedPrices = PriceCalculator.calculatePrice(productPrices, formData);

      equal(calculatedPrices.price, 36.0);
      equal(calculatedPrices.totalTax, 2.36);
      equal(calculatedPrices.taxAtDefaultRate, 2.36);
      equal(calculatedPrices.taxAtCustomRate, 0);
      equal(calculatedPrices.shapeAndStylePrice, 36.0);
      equal(calculatedPrices.shapeAndStyleTaxAtDefaultRate, 2.36);
      equal(calculatedPrices.shapeAndStyleTaxAtCustomRate, 0);
      equal(calculatedPrices.designAndFlavorPrice, 0);
      equal(calculatedPrices.designAndFlavorTaxAtDefaultRate, 0);
      equal(calculatedPrices.designAndFlavorTaxAtCustomRate, 0);
   });

   it('SHP03_EB0 = 45.0', () => {
      formData.shape = "SHP03";
      formData.size = "EB0";

      var calculatedPrices = PriceCalculator.calculatePrice(productPrices, formData);

      equal(calculatedPrices.price, 45.0);
      equal(calculatedPrices.totalTax, 2.94);
      equal(calculatedPrices.taxAtDefaultRate, 2.94);
      equal(calculatedPrices.taxAtCustomRate, 0);
      equal(calculatedPrices.shapeAndStylePrice, 45.0);
      equal(calculatedPrices.shapeAndStyleTaxAtDefaultRate, 2.94);
      equal(calculatedPrices.shapeAndStyleTaxAtCustomRate, 0);
      equal(calculatedPrices.designAndFlavorPrice, 0);
      equal(calculatedPrices.designAndFlavorTaxAtDefaultRate, 0);
      equal(calculatedPrices.designAndFlavorTaxAtCustomRate, 0);
   });

   it('SHP03_EB1 = 75.0', () => {
      formData.shape = "SHP03";
      formData.size = "EB1";

      var calculatedPrices = PriceCalculator.calculatePrice(productPrices, formData);

      equal(calculatedPrices.price, 75.0);
      equal(calculatedPrices.totalTax, 4.91);
      equal(calculatedPrices.taxAtDefaultRate, 4.91);
      equal(calculatedPrices.taxAtCustomRate, 0);
      equal(calculatedPrices.shapeAndStylePrice, 75.0);
      equal(calculatedPrices.shapeAndStyleTaxAtDefaultRate, 4.91);
      equal(calculatedPrices.shapeAndStyleTaxAtCustomRate, 0);
      equal(calculatedPrices.designAndFlavorPrice, 0);
      equal(calculatedPrices.designAndFlavorTaxAtDefaultRate, 0);
      equal(calculatedPrices.designAndFlavorTaxAtCustomRate, 0);
   });

   it('SHP03_EB2 = 90.0', () => {
      formData.shape = "SHP03";
      formData.size = "EB2";

      var calculatedPrices = PriceCalculator.calculatePrice(productPrices, formData);

      equal(calculatedPrices.price, 90.0);
      equal(calculatedPrices.totalTax, 5.89);
      equal(calculatedPrices.taxAtDefaultRate, 5.89);
      equal(calculatedPrices.taxAtCustomRate, 0);
      equal(calculatedPrices.shapeAndStylePrice, 90.0);
      equal(calculatedPrices.shapeAndStyleTaxAtDefaultRate, 5.89);
      equal(calculatedPrices.shapeAndStyleTaxAtCustomRate, 0);
      equal(calculatedPrices.designAndFlavorPrice, 0);
      equal(calculatedPrices.designAndFlavorTaxAtDefaultRate, 0);
      equal(calculatedPrices.designAndFlavorTaxAtCustomRate, 0);
   });

   it('SHP03_EB3 = 150.0', () => {
      formData.shape = "SHP03";
      formData.size = "EB3";

      var calculatedPrices = PriceCalculator.calculatePrice(productPrices, formData);

      equal(calculatedPrices.price, 150.0);
      equal(calculatedPrices.totalTax, 9.81);
      equal(calculatedPrices.taxAtDefaultRate, 9.81);
      equal(calculatedPrices.taxAtCustomRate, 0);
      equal(calculatedPrices.shapeAndStylePrice, 150.0);
      equal(calculatedPrices.shapeAndStyleTaxAtDefaultRate, 9.81);
      equal(calculatedPrices.shapeAndStyleTaxAtCustomRate, 0);
      equal(calculatedPrices.designAndFlavorPrice, 0);
      equal(calculatedPrices.designAndFlavorTaxAtDefaultRate, 0);
      equal(calculatedPrices.designAndFlavorTaxAtCustomRate, 0);
   });
});


describe('Decoration Pricing Rules', () => {
   var productPrices = require('../../models/product-list').getProductList();

   var formData = {
      fname: '',
      lname: '',
      mobile: '',
      email: '',
      pickupDeliverySelection: false,
      deliveryDate: '',
      deliveryTime: '',
      street: '',
      houseNo: '',
      city: '',
      shape: '',
      size: '',
      color: '',
      decoration: '',
      flavor: '',
      additional_flavor: [],
      extra_decoration: [],
      text: '',
      remarks: '',
      photo: '',
      paymentType: 'cod',
   };

   it('SHP01_NO1_DRN01 = 18.0 + 0.0 = 18.0', () => {
      formData.shape = "SHP01";
      formData.size = "NO1";
      formData.decoration = "DRN01";

      var calculatedPrices = PriceCalculator.calculatePrice(productPrices, formData);

      equal(calculatedPrices.price, 18.0);
      equal(calculatedPrices.totalTax, 1.18);
      equal(calculatedPrices.taxAtDefaultRate, 1.18);
      equal(calculatedPrices.taxAtCustomRate, 0);
      equal(calculatedPrices.shapeAndStylePrice, 18.0);
      equal(calculatedPrices.shapeAndStyleTaxAtDefaultRate, 1.18);
      equal(calculatedPrices.shapeAndStyleTaxAtCustomRate, 0);
      equal(calculatedPrices.designAndFlavorPrice, 0);
      equal(calculatedPrices.designAndFlavorTaxAtDefaultRate, 0);
      equal(calculatedPrices.designAndFlavorTaxAtCustomRate, 0);
   });

   it('SHP01_NO1_DRN02 = 18.0 + 0.0 = 18.0', () => {
      formData.shape = "SHP01";
      formData.size = "NO1";
      formData.decoration = "DRN02";

      var calculatedPrices = PriceCalculator.calculatePrice(productPrices, formData);

      equal(calculatedPrices.price, 18.0);
      equal(calculatedPrices.totalTax, 1.18);
      equal(calculatedPrices.taxAtDefaultRate, 1.18);
      equal(calculatedPrices.taxAtCustomRate, 0);
      equal(calculatedPrices.shapeAndStylePrice, 18.0);
      equal(calculatedPrices.shapeAndStyleTaxAtDefaultRate, 1.18);
      equal(calculatedPrices.shapeAndStyleTaxAtCustomRate, 0);
      equal(calculatedPrices.designAndFlavorPrice, 0);
      equal(calculatedPrices.designAndFlavorTaxAtDefaultRate, 0);
      equal(calculatedPrices.designAndFlavorTaxAtCustomRate, 0);
   });

   it('SHP01_NO1_DRN03 = 18.0 + 0.0 = 18.0', () => {
      formData.shape = "SHP01";
      formData.size = "NO1";
      formData.decoration = "DRN03";

      var calculatedPrices = PriceCalculator.calculatePrice(productPrices, formData);

      equal(calculatedPrices.price, 18.0);
      equal(calculatedPrices.totalTax, 1.18);
      equal(calculatedPrices.taxAtDefaultRate, 1.18);
      equal(calculatedPrices.taxAtCustomRate, 0);
      equal(calculatedPrices.shapeAndStylePrice, 18.0);
      equal(calculatedPrices.shapeAndStyleTaxAtDefaultRate, 1.18);
      equal(calculatedPrices.shapeAndStyleTaxAtCustomRate, 0);
      equal(calculatedPrices.designAndFlavorPrice, 0);
      equal(calculatedPrices.designAndFlavorTaxAtDefaultRate, 0);
      equal(calculatedPrices.designAndFlavorTaxAtCustomRate, 0);
   });

   it('SHP01_NO1_DRN04 = 18.0 + 0.0 = 18.0', () => {
      formData.shape = "SHP01";
      formData.size = "NO1";
      formData.decoration = "DRN04";

      var calculatedPrices = PriceCalculator.calculatePrice(productPrices, formData);

      equal(calculatedPrices.price, 18.0);
      equal(calculatedPrices.totalTax, 1.18);
      equal(calculatedPrices.taxAtDefaultRate, 1.18);
      equal(calculatedPrices.taxAtCustomRate, 0);
      equal(calculatedPrices.shapeAndStylePrice, 18.0);
      equal(calculatedPrices.shapeAndStyleTaxAtDefaultRate, 1.18);
      equal(calculatedPrices.shapeAndStyleTaxAtCustomRate, 0);
      equal(calculatedPrices.designAndFlavorPrice, 0);
      equal(calculatedPrices.designAndFlavorTaxAtDefaultRate, 0);
      equal(calculatedPrices.designAndFlavorTaxAtCustomRate, 0);
   });

   it('SHP01_NO1_DRN05 = 18.0 + 2.0 = 20.0', () => {
      formData.shape = "SHP01";
      formData.size = "NO1";
      formData.decoration = "DRN05";

      var calculatedPrices = PriceCalculator.calculatePrice(productPrices, formData);

      equal(calculatedPrices.price, 20.0);
      equal(calculatedPrices.totalTax, 1.31);
      equal(calculatedPrices.taxAtDefaultRate, 1.31);
      equal(calculatedPrices.taxAtCustomRate, 0);
      equal(calculatedPrices.shapeAndStylePrice, 18.0);
      equal(calculatedPrices.shapeAndStyleTaxAtDefaultRate, 1.18);
      equal(calculatedPrices.shapeAndStyleTaxAtCustomRate, 0);
      equal(calculatedPrices.designAndFlavorPrice, 2.0);
      equal(calculatedPrices.designAndFlavorTaxAtDefaultRate, 0.13);
      equal(calculatedPrices.designAndFlavorTaxAtCustomRate, 0);
   });

   it('SHP01_NO2_DRN05 = 24.0 + 2.0 = 26.0', () => {
      formData.shape = "SHP01";
      formData.size = "NO2";
      formData.decoration = "DRN05";

      var calculatedPrices = PriceCalculator.calculatePrice(productPrices, formData);

      equal(calculatedPrices.price, 26.0);
      equal(calculatedPrices.totalTax, 1.70);
      equal(calculatedPrices.taxAtDefaultRate, 1.70);
      equal(calculatedPrices.taxAtCustomRate, 0);
      equal(calculatedPrices.shapeAndStylePrice, 24.0);
      equal(calculatedPrices.shapeAndStyleTaxAtDefaultRate, 1.57);
      equal(calculatedPrices.shapeAndStyleTaxAtCustomRate, 0);
      equal(calculatedPrices.designAndFlavorPrice, 2.0);
      equal(calculatedPrices.designAndFlavorTaxAtDefaultRate, 0.13);
      equal(calculatedPrices.designAndFlavorTaxAtCustomRate, 0);
   });

   it('SHP01_NO3_DRN05 = 30.0 + 2.0 = 32.0', () => {
      formData.shape = "SHP01";
      formData.size = "NO3";
      formData.decoration = "DRN05";

      var calculatedPrices = PriceCalculator.calculatePrice(productPrices, formData);

      equal(calculatedPrices.price, 32.0);
      equal(calculatedPrices.totalTax, 2.09);
      equal(calculatedPrices.taxAtDefaultRate, 2.09);
      equal(calculatedPrices.taxAtCustomRate, 0);
      equal(calculatedPrices.shapeAndStylePrice, 30.0);
      equal(calculatedPrices.shapeAndStyleTaxAtDefaultRate, 1.96);
      equal(calculatedPrices.shapeAndStyleTaxAtCustomRate, 0);
      equal(calculatedPrices.designAndFlavorPrice, 2.0);
      equal(calculatedPrices.designAndFlavorTaxAtDefaultRate, 0.13);
      equal(calculatedPrices.designAndFlavorTaxAtCustomRate, 0);
   });

   it('SHP02_NO1_DRN05 = 24.0 + 2.0 = 26.0', () => {
      formData.shape = "SHP02";
      formData.size = "NO1";
      formData.decoration = "DRN05";

      var calculatedPrices = PriceCalculator.calculatePrice(productPrices, formData);

      equal(calculatedPrices.price, 26.0);
      equal(calculatedPrices.totalTax, 1.70);
      equal(calculatedPrices.taxAtDefaultRate, 1.70);
      equal(calculatedPrices.taxAtCustomRate, 0);
      equal(calculatedPrices.shapeAndStylePrice, 24.0);
      equal(calculatedPrices.shapeAndStyleTaxAtDefaultRate, 1.57);
      equal(calculatedPrices.shapeAndStyleTaxAtCustomRate, 0);
      equal(calculatedPrices.designAndFlavorPrice, 2.0);
      equal(calculatedPrices.designAndFlavorTaxAtDefaultRate, 0.13);
      equal(calculatedPrices.designAndFlavorTaxAtCustomRate, 0);
   });

   it('SHP02_NO2_DRN05 = 30.0 + 2.0 = 32.0', () => {
      formData.shape = "SHP02";
      formData.size = "NO2";
      formData.decoration = "DRN05";

      var calculatedPrices = PriceCalculator.calculatePrice(productPrices, formData);

      equal(calculatedPrices.price, 32.0);
      equal(calculatedPrices.totalTax, 2.09);
      equal(calculatedPrices.taxAtDefaultRate, 2.09);
      equal(calculatedPrices.taxAtCustomRate, 0);
      equal(calculatedPrices.shapeAndStylePrice, 30.0);
      equal(calculatedPrices.shapeAndStyleTaxAtDefaultRate, 1.96);
      equal(calculatedPrices.shapeAndStyleTaxAtCustomRate, 0);
      equal(calculatedPrices.designAndFlavorPrice, 2.0);
      equal(calculatedPrices.designAndFlavorTaxAtDefaultRate, 0.13);
      equal(calculatedPrices.designAndFlavorTaxAtCustomRate, 0);
   });

   it('SHP02_NO3_DRN05 = 36.0 + 2.0 = 38.0', () => {
      formData.shape = "SHP02";
      formData.size = "NO3";
      formData.decoration = "DRN05";

      var calculatedPrices = PriceCalculator.calculatePrice(productPrices, formData);

      equal(calculatedPrices.price, 38.0);
      equal(calculatedPrices.totalTax, 2.49);
      equal(calculatedPrices.taxAtDefaultRate, 2.49);
      equal(calculatedPrices.taxAtCustomRate, 0);
      equal(calculatedPrices.shapeAndStylePrice, 36.0);
      equal(calculatedPrices.shapeAndStyleTaxAtDefaultRate, 2.36);
      equal(calculatedPrices.shapeAndStyleTaxAtCustomRate, 0);
      equal(calculatedPrices.designAndFlavorPrice, 2.0);
      equal(calculatedPrices.designAndFlavorTaxAtDefaultRate, 0.13);
      equal(calculatedPrices.designAndFlavorTaxAtCustomRate, 0);
   });

   it('SHP03_EB0_DRN05 = 45.0 + 3.0 = 48.0', () => {
      formData.shape = "SHP03";
      formData.size = "EB0";
      formData.decoration = "DRN05";

      var calculatedPrices = PriceCalculator.calculatePrice(productPrices, formData);

      equal(calculatedPrices.price, 48.0);
      equal(calculatedPrices.totalTax, 3.14);
      equal(calculatedPrices.taxAtDefaultRate, 3.14);
      equal(calculatedPrices.taxAtCustomRate, 0);
      equal(calculatedPrices.shapeAndStylePrice, 45.0);
      equal(calculatedPrices.shapeAndStyleTaxAtDefaultRate, 2.94);
      equal(calculatedPrices.shapeAndStyleTaxAtCustomRate, 0);
      equal(calculatedPrices.designAndFlavorPrice, 3.0);
      equal(calculatedPrices.designAndFlavorTaxAtDefaultRate, 0.20);
      equal(calculatedPrices.designAndFlavorTaxAtCustomRate, 0);
   });

   it('SHP03_EB1_DRN05 = 75.0 + 5.0 = 80.0', () => {
      formData.shape = "SHP03";
      formData.size = "EB1";
      formData.decoration = "DRN05";

      var calculatedPrices = PriceCalculator.calculatePrice(productPrices, formData);

      equal(calculatedPrices.price, 80.0);
      equal(calculatedPrices.totalTax, 5.23);
      equal(calculatedPrices.taxAtDefaultRate, 5.23);
      equal(calculatedPrices.taxAtCustomRate, 0);
      equal(calculatedPrices.shapeAndStylePrice, 75.0);
      equal(calculatedPrices.shapeAndStyleTaxAtDefaultRate, 4.91);
      equal(calculatedPrices.shapeAndStyleTaxAtCustomRate, 0);
      equal(calculatedPrices.designAndFlavorPrice, 5.0);
      equal(calculatedPrices.designAndFlavorTaxAtDefaultRate, 0.33);
      equal(calculatedPrices.designAndFlavorTaxAtCustomRate, 0);
   });

   it('SHP03_EB2_DRN05 = 90.0 + 5.0 = 95.0', () => {
      formData.shape = "SHP03";
      formData.size = "EB2";
      formData.decoration = "DRN05";

      var calculatedPrices = PriceCalculator.calculatePrice(productPrices, formData);

      equal(calculatedPrices.price, 95.0);
      equal(calculatedPrices.totalTax, 6.21);
      equal(calculatedPrices.taxAtDefaultRate, 6.21);
      equal(calculatedPrices.taxAtCustomRate, 0);
      equal(calculatedPrices.shapeAndStylePrice, 90.0);
      equal(calculatedPrices.shapeAndStyleTaxAtDefaultRate, 5.89);
      equal(calculatedPrices.shapeAndStyleTaxAtCustomRate, 0);
      equal(calculatedPrices.designAndFlavorPrice, 5.0);
      equal(calculatedPrices.designAndFlavorTaxAtDefaultRate, 0.33);
      equal(calculatedPrices.designAndFlavorTaxAtCustomRate, 0);
   });

   it('SHP03_EB3_DRN05 = 150.0 + 7.0 = 157.0', () => {
      formData.shape = "SHP03";
      formData.size = "EB3";
      formData.decoration = "DRN05";

      var calculatedPrices = PriceCalculator.calculatePrice(productPrices, formData);

      equal(calculatedPrices.price, 157.0);
      equal(calculatedPrices.totalTax, 10.27);
      equal(calculatedPrices.taxAtDefaultRate, 10.27);
      equal(calculatedPrices.taxAtCustomRate, 0);
      equal(calculatedPrices.shapeAndStylePrice, 150.0);
      equal(calculatedPrices.shapeAndStyleTaxAtDefaultRate, 9.81);
      equal(calculatedPrices.shapeAndStyleTaxAtCustomRate, 0);
      equal(calculatedPrices.designAndFlavorPrice, 7.0);
      equal(calculatedPrices.designAndFlavorTaxAtDefaultRate, 0.46);
      equal(calculatedPrices.designAndFlavorTaxAtCustomRate, 0);
   });

   it('SHP01_NO1_DRN06 = 18.0 + 2.0 = 20.0', () => {
      formData.shape = "SHP01";
      formData.size = "NO1";
      formData.decoration = "DRN06";

      var calculatedPrices = PriceCalculator.calculatePrice(productPrices, formData);

      equal(calculatedPrices.price, 20.0);
      equal(calculatedPrices.totalTax, 1.31);
      equal(calculatedPrices.taxAtDefaultRate, 1.31);
      equal(calculatedPrices.taxAtCustomRate, 0);
      equal(calculatedPrices.shapeAndStylePrice, 18.0);
      equal(calculatedPrices.shapeAndStyleTaxAtDefaultRate, 1.18);
      equal(calculatedPrices.shapeAndStyleTaxAtCustomRate, 0);
      equal(calculatedPrices.designAndFlavorPrice, 2.0);
      equal(calculatedPrices.designAndFlavorTaxAtDefaultRate, 0.13);
      equal(calculatedPrices.designAndFlavorTaxAtCustomRate, 0);
   });

   it('SHP01_NO2_DRN06 = 24.0 + 2.0 = 26.0', () => {
      formData.shape = "SHP01";
      formData.size = "NO2";
      formData.decoration = "DRN06";

      var calculatedPrices = PriceCalculator.calculatePrice(productPrices, formData);

      equal(calculatedPrices.price, 26.0);
      equal(calculatedPrices.totalTax, 1.70);
      equal(calculatedPrices.taxAtDefaultRate, 1.70);
      equal(calculatedPrices.taxAtCustomRate, 0);
      equal(calculatedPrices.shapeAndStylePrice, 24.0);
      equal(calculatedPrices.shapeAndStyleTaxAtDefaultRate, 1.57);
      equal(calculatedPrices.shapeAndStyleTaxAtCustomRate, 0);
      equal(calculatedPrices.designAndFlavorPrice, 2.0);
      equal(calculatedPrices.designAndFlavorTaxAtDefaultRate, 0.13);
      equal(calculatedPrices.designAndFlavorTaxAtCustomRate, 0);
   });

   it('SHP01_NO3_DRN06 = 30.0 + 2.0 = 32.0', () => {
      formData.shape = "SHP01";
      formData.size = "NO3";
      formData.decoration = "DRN06";

      var calculatedPrices = PriceCalculator.calculatePrice(productPrices, formData);

      equal(calculatedPrices.price, 32.0);
      equal(calculatedPrices.totalTax, 2.09);
      equal(calculatedPrices.taxAtDefaultRate, 2.09);
      equal(calculatedPrices.taxAtCustomRate, 0);
      equal(calculatedPrices.shapeAndStylePrice, 30.0);
      equal(calculatedPrices.shapeAndStyleTaxAtDefaultRate, 1.96);
      equal(calculatedPrices.shapeAndStyleTaxAtCustomRate, 0);
      equal(calculatedPrices.designAndFlavorPrice, 2.0);
      equal(calculatedPrices.designAndFlavorTaxAtDefaultRate, 0.13);
      equal(calculatedPrices.designAndFlavorTaxAtCustomRate, 0);
   });

   it('SHP02_NO1_DRN06 = 24.0 + 2.0 = 26.0', () => {
      formData.shape = "SHP02";
      formData.size = "NO1";
      formData.decoration = "DRN06";

      var calculatedPrices = PriceCalculator.calculatePrice(productPrices, formData);

      equal(calculatedPrices.price, 26.0);
      equal(calculatedPrices.totalTax, 1.70);
      equal(calculatedPrices.taxAtDefaultRate, 1.70);
      equal(calculatedPrices.taxAtCustomRate, 0);
      equal(calculatedPrices.shapeAndStylePrice, 24.0);
      equal(calculatedPrices.shapeAndStyleTaxAtDefaultRate, 1.57);
      equal(calculatedPrices.shapeAndStyleTaxAtCustomRate, 0);
      equal(calculatedPrices.designAndFlavorPrice, 2.0);
      equal(calculatedPrices.designAndFlavorTaxAtDefaultRate, 0.13);
      equal(calculatedPrices.designAndFlavorTaxAtCustomRate, 0);
   });

   it('SHP02_NO2_DRN06 = 30.0 + 2.0 = 32.0', () => {
      formData.shape = "SHP02";
      formData.size = "NO2";
      formData.decoration = "DRN06";

      var calculatedPrices = PriceCalculator.calculatePrice(productPrices, formData);

      equal(calculatedPrices.price, 32.0);
      equal(calculatedPrices.totalTax, 2.09);
      equal(calculatedPrices.taxAtDefaultRate, 2.09);
      equal(calculatedPrices.taxAtCustomRate, 0);
      equal(calculatedPrices.shapeAndStylePrice, 30.0);
      equal(calculatedPrices.shapeAndStyleTaxAtDefaultRate, 1.96);
      equal(calculatedPrices.shapeAndStyleTaxAtCustomRate, 0);
      equal(calculatedPrices.designAndFlavorPrice, 2.0);
      equal(calculatedPrices.designAndFlavorTaxAtDefaultRate, 0.13);
      equal(calculatedPrices.designAndFlavorTaxAtCustomRate, 0);
   });

   it('SHP02_NO3_DRN06 = 36.0 + 2.0 = 38.0', () => {
      formData.shape = "SHP02";
      formData.size = "NO3";
      formData.decoration = "DRN06";

      var calculatedPrices = PriceCalculator.calculatePrice(productPrices, formData);

      equal(calculatedPrices.price, 38.0);
      equal(calculatedPrices.totalTax, 2.49);
      equal(calculatedPrices.taxAtDefaultRate, 2.49);
      equal(calculatedPrices.taxAtCustomRate, 0);
      equal(calculatedPrices.shapeAndStylePrice, 36.0);
      equal(calculatedPrices.shapeAndStyleTaxAtDefaultRate, 2.36);
      equal(calculatedPrices.shapeAndStyleTaxAtCustomRate, 0);
      equal(calculatedPrices.designAndFlavorPrice, 2.0);
      equal(calculatedPrices.designAndFlavorTaxAtDefaultRate, 0.13);
      equal(calculatedPrices.designAndFlavorTaxAtCustomRate, 0);
   });

   it('SHP03_EB0_DRN06 = 45.0 + 3.0 = 48.0', () => {
      formData.shape = "SHP03";
      formData.size = "EB0";
      formData.decoration = "DRN06";

      var calculatedPrices = PriceCalculator.calculatePrice(productPrices, formData);

      equal(calculatedPrices.price, 48.0);
      equal(calculatedPrices.totalTax, 3.14);
      equal(calculatedPrices.taxAtDefaultRate, 3.14);
      equal(calculatedPrices.taxAtCustomRate, 0);
      equal(calculatedPrices.shapeAndStylePrice, 45.0);
      equal(calculatedPrices.shapeAndStyleTaxAtDefaultRate, 2.94);
      equal(calculatedPrices.shapeAndStyleTaxAtCustomRate, 0);
      equal(calculatedPrices.designAndFlavorPrice, 3.0);
      equal(calculatedPrices.designAndFlavorTaxAtDefaultRate, 0.20);
      equal(calculatedPrices.designAndFlavorTaxAtCustomRate, 0);
   });

   it('SHP03_EB1_DRN06 = 75.0 + 5.0 = 80.0', () => {
      formData.shape = "SHP03";
      formData.size = "EB1";
      formData.decoration = "DRN06";

      var calculatedPrices = PriceCalculator.calculatePrice(productPrices, formData);

      equal(calculatedPrices.price, 80.0);
      equal(calculatedPrices.totalTax, 5.23);
      equal(calculatedPrices.taxAtDefaultRate, 5.23);
      equal(calculatedPrices.taxAtCustomRate, 0);
      equal(calculatedPrices.shapeAndStylePrice, 75.0);
      equal(calculatedPrices.shapeAndStyleTaxAtDefaultRate, 4.91);
      equal(calculatedPrices.shapeAndStyleTaxAtCustomRate, 0);
      equal(calculatedPrices.designAndFlavorPrice, 5.0);
      equal(calculatedPrices.designAndFlavorTaxAtDefaultRate, 0.33);
      equal(calculatedPrices.designAndFlavorTaxAtCustomRate, 0);
   });

   it('SHP03_EB2_DRN06 = 90.0 + 5.0 = 95.0', () => {
      formData.shape = "SHP03";
      formData.size = "EB2";
      formData.decoration = "DRN06";

      var calculatedPrices = PriceCalculator.calculatePrice(productPrices, formData);

      equal(calculatedPrices.price, 95.0);
      equal(calculatedPrices.totalTax, 6.21);
      equal(calculatedPrices.taxAtDefaultRate, 6.21);
      equal(calculatedPrices.taxAtCustomRate, 0);
      equal(calculatedPrices.shapeAndStylePrice, 90.0);
      equal(calculatedPrices.shapeAndStyleTaxAtDefaultRate, 5.89);
      equal(calculatedPrices.shapeAndStyleTaxAtCustomRate, 0);
      equal(calculatedPrices.designAndFlavorPrice, 5.0);
      equal(calculatedPrices.designAndFlavorTaxAtDefaultRate, 0.33);
      equal(calculatedPrices.designAndFlavorTaxAtCustomRate, 0);
   });

   it('SHP03_EB3_DRN06 = 150.0 + 7.0 = 157.0', () => {
      formData.shape = "SHP03";
      formData.size = "EB3";
      formData.decoration = "DRN06";

      var calculatedPrices = PriceCalculator.calculatePrice(productPrices, formData);

      equal(calculatedPrices.price, 157.0);
      equal(calculatedPrices.totalTax, 10.27);
      equal(calculatedPrices.taxAtDefaultRate, 10.27);
      equal(calculatedPrices.taxAtCustomRate, 0);
      equal(calculatedPrices.shapeAndStylePrice, 150.0);
      equal(calculatedPrices.shapeAndStyleTaxAtDefaultRate, 9.81);
      equal(calculatedPrices.shapeAndStyleTaxAtCustomRate, 0);
      equal(calculatedPrices.designAndFlavorPrice, 7.0);
      equal(calculatedPrices.designAndFlavorTaxAtDefaultRate, 0.46);
      equal(calculatedPrices.designAndFlavorTaxAtCustomRate, 0);
   });

   it('SHP01_NO1_DRN07 = 18.0 + 3.0 = 21.0', () => {
      formData.shape = "SHP01";
      formData.size = "NO1";
      formData.decoration = "DRN07";

      var calculatedPrices = PriceCalculator.calculatePrice(productPrices, formData);

      equal(calculatedPrices.price, 21.0);
      equal(calculatedPrices.totalTax, 1.37);
      equal(calculatedPrices.taxAtDefaultRate, 1.37);
      equal(calculatedPrices.taxAtCustomRate, 0);
      equal(calculatedPrices.shapeAndStylePrice, 18.0);
      equal(calculatedPrices.shapeAndStyleTaxAtDefaultRate, 1.18);
      equal(calculatedPrices.shapeAndStyleTaxAtCustomRate, 0);
      equal(calculatedPrices.designAndFlavorPrice, 3.0);
      equal(calculatedPrices.designAndFlavorTaxAtDefaultRate, 0.20);
      equal(calculatedPrices.designAndFlavorTaxAtCustomRate, 0);
   });

   it('SHP01_NO2_DRN07 = 24.0 + 3.0 = 27.0', () => {
      formData.shape = "SHP01";
      formData.size = "NO2";
      formData.decoration = "DRN07";

      var calculatedPrices = PriceCalculator.calculatePrice(productPrices, formData);

      equal(calculatedPrices.price, 27.0);
      equal(calculatedPrices.totalTax, 1.77);
      equal(calculatedPrices.taxAtDefaultRate, 1.77);
      equal(calculatedPrices.taxAtCustomRate, 0);
      equal(calculatedPrices.shapeAndStylePrice, 24.0);
      equal(calculatedPrices.shapeAndStyleTaxAtDefaultRate, 1.57);
      equal(calculatedPrices.shapeAndStyleTaxAtCustomRate, 0);
      equal(calculatedPrices.designAndFlavorPrice, 3.0);
      equal(calculatedPrices.designAndFlavorTaxAtDefaultRate, 0.20);
      equal(calculatedPrices.designAndFlavorTaxAtCustomRate, 0);
   });

   it('SHP01_NO3_DRN07 = 30.0 + 3.0 = 33.0', () => {
      formData.shape = "SHP01";
      formData.size = "NO3";
      formData.decoration = "DRN07";

      var calculatedPrices = PriceCalculator.calculatePrice(productPrices, formData);

      equal(calculatedPrices.price, 33.0);
      equal(calculatedPrices.totalTax, 2.16);
      equal(calculatedPrices.taxAtDefaultRate, 2.16);
      equal(calculatedPrices.taxAtCustomRate, 0);
      equal(calculatedPrices.shapeAndStylePrice, 30.0);
      equal(calculatedPrices.shapeAndStyleTaxAtDefaultRate, 1.96);
      equal(calculatedPrices.shapeAndStyleTaxAtCustomRate, 0);
      equal(calculatedPrices.designAndFlavorPrice, 3.0);
      equal(calculatedPrices.designAndFlavorTaxAtDefaultRate, 0.20);
      equal(calculatedPrices.designAndFlavorTaxAtCustomRate, 0);
   });

   it('SHP02_NO1_DRN07 = 24.0 + 3.0 = 27.0', () => {
      formData.shape = "SHP02";
      formData.size = "NO1";
      formData.decoration = "DRN07";

      var calculatedPrices = PriceCalculator.calculatePrice(productPrices, formData);

      equal(calculatedPrices.price, 27.0);
      equal(calculatedPrices.totalTax, 1.77);
      equal(calculatedPrices.taxAtDefaultRate, 1.77);
      equal(calculatedPrices.taxAtCustomRate, 0);
      equal(calculatedPrices.shapeAndStylePrice, 24.0);
      equal(calculatedPrices.shapeAndStyleTaxAtDefaultRate, 1.57);
      equal(calculatedPrices.shapeAndStyleTaxAtCustomRate, 0);
      equal(calculatedPrices.designAndFlavorPrice, 3.0);
      equal(calculatedPrices.designAndFlavorTaxAtDefaultRate, 0.20);
      equal(calculatedPrices.designAndFlavorTaxAtCustomRate, 0);
   });

   it('SHP02_NO2_DRN07 = 30.0 + 3.0 = 33.0', () => {
      formData.shape = "SHP02";
      formData.size = "NO2";
      formData.decoration = "DRN07";

      var calculatedPrices = PriceCalculator.calculatePrice(productPrices, formData);

      equal(calculatedPrices.price, 33.0);
      equal(calculatedPrices.totalTax, 2.16);
      equal(calculatedPrices.taxAtDefaultRate, 2.16);
      equal(calculatedPrices.taxAtCustomRate, 0);
      equal(calculatedPrices.shapeAndStylePrice, 30.0);
      equal(calculatedPrices.shapeAndStyleTaxAtDefaultRate, 1.96);
      equal(calculatedPrices.shapeAndStyleTaxAtCustomRate, 0);
      equal(calculatedPrices.designAndFlavorPrice, 3.0);
      equal(calculatedPrices.designAndFlavorTaxAtDefaultRate, 0.20);
      equal(calculatedPrices.designAndFlavorTaxAtCustomRate, 0);
   });

   it('SHP02_NO3_DRN07 = 36.0 + 3.0 = 39.0', () => {
      formData.shape = "SHP02";
      formData.size = "NO3";
      formData.decoration = "DRN07";

      var calculatedPrices = PriceCalculator.calculatePrice(productPrices, formData);

      equal(calculatedPrices.price, 39.0);
      equal(calculatedPrices.totalTax, 2.55);
      equal(calculatedPrices.taxAtDefaultRate, 2.55);
      equal(calculatedPrices.taxAtCustomRate, 0);
      equal(calculatedPrices.shapeAndStylePrice, 36.0);
      equal(calculatedPrices.shapeAndStyleTaxAtDefaultRate, 2.36);
      equal(calculatedPrices.shapeAndStyleTaxAtCustomRate, 0);
      equal(calculatedPrices.designAndFlavorPrice, 3.0);
      equal(calculatedPrices.designAndFlavorTaxAtDefaultRate, 0.20);
      equal(calculatedPrices.designAndFlavorTaxAtCustomRate, 0);
   });

   it('SHP03_EB0_DRN07 = 45.0 + 4.0 = 49.0', () => {
      formData.shape = "SHP03";
      formData.size = "EB0";
      formData.decoration = "DRN07";

      var calculatedPrices = PriceCalculator.calculatePrice(productPrices, formData);

      equal(calculatedPrices.price, 49.0);
      equal(calculatedPrices.totalTax, 3.21);
      equal(calculatedPrices.taxAtDefaultRate, 3.21);
      equal(calculatedPrices.taxAtCustomRate, 0);
      equal(calculatedPrices.shapeAndStylePrice, 45.0);
      equal(calculatedPrices.shapeAndStyleTaxAtDefaultRate, 2.94);
      equal(calculatedPrices.shapeAndStyleTaxAtCustomRate, 0);
      equal(calculatedPrices.designAndFlavorPrice, 4.0);
      equal(calculatedPrices.designAndFlavorTaxAtDefaultRate, 0.26);
      equal(calculatedPrices.designAndFlavorTaxAtCustomRate, 0);
   });

   it('SHP03_EB1_DRN07 = 75.0 + 6.0 = 81.0', () => {
      formData.shape = "SHP03";
      formData.size = "EB1";
      formData.decoration = "DRN07";

      var calculatedPrices = PriceCalculator.calculatePrice(productPrices, formData);

      equal(calculatedPrices.price, 81.0);
      equal(calculatedPrices.totalTax, 5.30);
      equal(calculatedPrices.taxAtDefaultRate, 5.30);
      equal(calculatedPrices.taxAtCustomRate, 0);
      equal(calculatedPrices.shapeAndStylePrice, 75.0);
      equal(calculatedPrices.shapeAndStyleTaxAtDefaultRate, 4.91);
      equal(calculatedPrices.shapeAndStyleTaxAtCustomRate, 0);
      equal(calculatedPrices.designAndFlavorPrice, 6.0);
      equal(calculatedPrices.designAndFlavorTaxAtDefaultRate, 0.39);
      equal(calculatedPrices.designAndFlavorTaxAtCustomRate, 0);
   });

   it('SHP03_EB2_DRN07 = 90.0 + 6.0 = 96.0', () => {
      formData.shape = "SHP03";
      formData.size = "EB2";
      formData.decoration = "DRN07";

      var calculatedPrices = PriceCalculator.calculatePrice(productPrices, formData);

      equal(calculatedPrices.price, 96.0);
      equal(calculatedPrices.totalTax, 6.28);
      equal(calculatedPrices.taxAtDefaultRate, 6.28);
      equal(calculatedPrices.taxAtCustomRate, 0);
      equal(calculatedPrices.shapeAndStylePrice, 90.0);
      equal(calculatedPrices.shapeAndStyleTaxAtDefaultRate, 5.89);
      equal(calculatedPrices.shapeAndStyleTaxAtCustomRate, 0);
      equal(calculatedPrices.designAndFlavorPrice, 6.0);
      equal(calculatedPrices.designAndFlavorTaxAtDefaultRate, 0.39);
      equal(calculatedPrices.designAndFlavorTaxAtCustomRate, 0);
   });

   it('SHP03_EB3_DRN07 = 150.0 + 8.0 = 158.0', () => {
      formData.shape = "SHP03";
      formData.size = "EB3";
      formData.decoration = "DRN07";

      var calculatedPrices = PriceCalculator.calculatePrice(productPrices, formData);

      equal(calculatedPrices.price, 158.0);
      equal(calculatedPrices.totalTax, 10.34);
      equal(calculatedPrices.taxAtDefaultRate, 10.34);
      equal(calculatedPrices.taxAtCustomRate, 0);
      equal(calculatedPrices.shapeAndStylePrice, 150.0);
      equal(calculatedPrices.shapeAndStyleTaxAtDefaultRate, 9.81);
      equal(calculatedPrices.shapeAndStyleTaxAtCustomRate, 0);
      equal(calculatedPrices.designAndFlavorPrice, 8.0);
      equal(calculatedPrices.designAndFlavorTaxAtDefaultRate, 0.52);
      equal(calculatedPrices.designAndFlavorTaxAtCustomRate, 0);
   });

   it('SHP01_NO1_DRN08 = 18.0 + 3.0 = 21.0', () => {
      formData.shape = "SHP01";
      formData.size = "NO1";
      formData.decoration = "DRN08";

      var calculatedPrices = PriceCalculator.calculatePrice(productPrices, formData);

      equal(calculatedPrices.price, 21.0);
      equal(calculatedPrices.totalTax, 1.37);
      equal(calculatedPrices.taxAtDefaultRate, 1.37);
      equal(calculatedPrices.taxAtCustomRate, 0);
      equal(calculatedPrices.shapeAndStylePrice, 18.0);
      equal(calculatedPrices.shapeAndStyleTaxAtDefaultRate, 1.18);
      equal(calculatedPrices.shapeAndStyleTaxAtCustomRate, 0);
      equal(calculatedPrices.designAndFlavorPrice, 3.0);
      equal(calculatedPrices.designAndFlavorTaxAtDefaultRate, 0.20);
      equal(calculatedPrices.designAndFlavorTaxAtCustomRate, 0);
   });

   it('SHP01_NO2_DRN08 = 24.0 + 3.0 = 27.0', () => {
      formData.shape = "SHP01";
      formData.size = "NO2";
      formData.decoration = "DRN08";

      var calculatedPrices = PriceCalculator.calculatePrice(productPrices, formData);

      equal(calculatedPrices.price, 27.0);
      equal(calculatedPrices.totalTax, 1.77);
      equal(calculatedPrices.taxAtDefaultRate, 1.77);
      equal(calculatedPrices.taxAtCustomRate, 0);
      equal(calculatedPrices.shapeAndStylePrice, 24.0);
      equal(calculatedPrices.shapeAndStyleTaxAtDefaultRate, 1.57);
      equal(calculatedPrices.shapeAndStyleTaxAtCustomRate, 0);
      equal(calculatedPrices.designAndFlavorPrice, 3.0);
      equal(calculatedPrices.designAndFlavorTaxAtDefaultRate, 0.20);
      equal(calculatedPrices.designAndFlavorTaxAtCustomRate, 0);
   });

   it('SHP01_NO3_DRN08 = 30.0 + 3.0 = 33.0', () => {
      formData.shape = "SHP01";
      formData.size = "NO3";
      formData.decoration = "DRN08";

      var calculatedPrices = PriceCalculator.calculatePrice(productPrices, formData);

      equal(calculatedPrices.price, 33.0);
      equal(calculatedPrices.totalTax, 2.16);
      equal(calculatedPrices.taxAtDefaultRate, 2.16);
      equal(calculatedPrices.taxAtCustomRate, 0);
      equal(calculatedPrices.shapeAndStylePrice, 30.0);
      equal(calculatedPrices.shapeAndStyleTaxAtDefaultRate, 1.96);
      equal(calculatedPrices.shapeAndStyleTaxAtCustomRate, 0);
      equal(calculatedPrices.designAndFlavorPrice, 3.0);
      equal(calculatedPrices.designAndFlavorTaxAtDefaultRate, 0.20);
      equal(calculatedPrices.designAndFlavorTaxAtCustomRate, 0);
   });

   it('SHP02_NO1_DRN08 = 24.0 + 3.0 = 27.0', () => {
      formData.shape = "SHP02";
      formData.size = "NO1";
      formData.decoration = "DRN08";

      var calculatedPrices = PriceCalculator.calculatePrice(productPrices, formData);

      equal(calculatedPrices.price, 27.0);
      equal(calculatedPrices.totalTax, 1.77);
      equal(calculatedPrices.taxAtDefaultRate, 1.77);
      equal(calculatedPrices.taxAtCustomRate, 0);
      equal(calculatedPrices.shapeAndStylePrice, 24.0);
      equal(calculatedPrices.shapeAndStyleTaxAtDefaultRate, 1.57);
      equal(calculatedPrices.shapeAndStyleTaxAtCustomRate, 0);
      equal(calculatedPrices.designAndFlavorPrice, 3.0);
      equal(calculatedPrices.designAndFlavorTaxAtDefaultRate, 0.20);
      equal(calculatedPrices.designAndFlavorTaxAtCustomRate, 0);
   });

   it('SHP02_NO2_DRN08 = 30.0 + 3.0 = 33.0', () => {
      formData.shape = "SHP02";
      formData.size = "NO2";
      formData.decoration = "DRN08";

      var calculatedPrices = PriceCalculator.calculatePrice(productPrices, formData);

      equal(calculatedPrices.price, 33.0);
      equal(calculatedPrices.totalTax, 2.16);
      equal(calculatedPrices.taxAtDefaultRate, 2.16);
      equal(calculatedPrices.taxAtCustomRate, 0);
      equal(calculatedPrices.shapeAndStylePrice, 30.0);
      equal(calculatedPrices.shapeAndStyleTaxAtDefaultRate, 1.96);
      equal(calculatedPrices.shapeAndStyleTaxAtCustomRate, 0);
      equal(calculatedPrices.designAndFlavorPrice, 3.0);
      equal(calculatedPrices.designAndFlavorTaxAtDefaultRate, 0.20);
      equal(calculatedPrices.designAndFlavorTaxAtCustomRate, 0);
   });

   it('SHP02_NO3_DRN08 = 36.0 + 3.0 = 39.0', () => {
      formData.shape = "SHP02";
      formData.size = "NO3";
      formData.decoration = "DRN08";

      var calculatedPrices = PriceCalculator.calculatePrice(productPrices, formData);

      equal(calculatedPrices.price, 39.0);
      equal(calculatedPrices.totalTax, 2.55);
      equal(calculatedPrices.taxAtDefaultRate, 2.55);
      equal(calculatedPrices.taxAtCustomRate, 0);
      equal(calculatedPrices.shapeAndStylePrice, 36.0);
      equal(calculatedPrices.shapeAndStyleTaxAtDefaultRate, 2.36);
      equal(calculatedPrices.shapeAndStyleTaxAtCustomRate, 0);
      equal(calculatedPrices.designAndFlavorPrice, 3.0);
      equal(calculatedPrices.designAndFlavorTaxAtDefaultRate, 0.20);
      equal(calculatedPrices.designAndFlavorTaxAtCustomRate, 0);
   });

   it('SHP03_EB0_DRN08 = 45.0 + 4.0 = 49.0', () => {
      formData.shape = "SHP03";
      formData.size = "EB0";
      formData.decoration = "DRN08";

      var calculatedPrices = PriceCalculator.calculatePrice(productPrices, formData);

      equal(calculatedPrices.price, 49.0);
      equal(calculatedPrices.totalTax, 3.21);
      equal(calculatedPrices.taxAtDefaultRate, 3.21);
      equal(calculatedPrices.taxAtCustomRate, 0);
      equal(calculatedPrices.shapeAndStylePrice, 45.0);
      equal(calculatedPrices.shapeAndStyleTaxAtDefaultRate, 2.94);
      equal(calculatedPrices.shapeAndStyleTaxAtCustomRate, 0);
      equal(calculatedPrices.designAndFlavorPrice, 4.0);
      equal(calculatedPrices.designAndFlavorTaxAtDefaultRate, 0.26);
      equal(calculatedPrices.designAndFlavorTaxAtCustomRate, 0);
   });

   it('SHP03_EB1_DRN08 = 75.0 + 6.0 = 81.0', () => {
      formData.shape = "SHP03";
      formData.size = "EB1";
      formData.decoration = "DRN08";

      var calculatedPrices = PriceCalculator.calculatePrice(productPrices, formData);

      equal(calculatedPrices.price, 81.0);
      equal(calculatedPrices.totalTax, 5.30);
      equal(calculatedPrices.taxAtDefaultRate, 5.30);
      equal(calculatedPrices.taxAtCustomRate, 0);
      equal(calculatedPrices.shapeAndStylePrice, 75.0);
      equal(calculatedPrices.shapeAndStyleTaxAtDefaultRate, 4.91);
      equal(calculatedPrices.shapeAndStyleTaxAtCustomRate, 0);
      equal(calculatedPrices.designAndFlavorPrice, 6.0);
      equal(calculatedPrices.designAndFlavorTaxAtDefaultRate, 0.39);
      equal(calculatedPrices.designAndFlavorTaxAtCustomRate, 0);
   });

   it('SHP03_EB2_DRN08 = 90.0 + 6.0 = 96.0', () => {
      formData.shape = "SHP03";
      formData.size = "EB2";
      formData.decoration = "DRN08";

      var calculatedPrices = PriceCalculator.calculatePrice(productPrices, formData);

      equal(calculatedPrices.price, 96.0);
      equal(calculatedPrices.totalTax, 6.28);
      equal(calculatedPrices.taxAtDefaultRate, 6.28);
      equal(calculatedPrices.taxAtCustomRate, 0);
      equal(calculatedPrices.shapeAndStylePrice, 90.0);
      equal(calculatedPrices.shapeAndStyleTaxAtDefaultRate, 5.89);
      equal(calculatedPrices.shapeAndStyleTaxAtCustomRate, 0);
      equal(calculatedPrices.designAndFlavorPrice, 6.0);
      equal(calculatedPrices.designAndFlavorTaxAtDefaultRate, 0.39);
      equal(calculatedPrices.designAndFlavorTaxAtCustomRate, 0);
   });

   it('SHP03_EB3_DRN08 = 150.0 + 8.0 = 158.0', () => {
      formData.shape = "SHP03";
      formData.size = "EB3";
      formData.decoration = "DRN08";

      var calculatedPrices = PriceCalculator.calculatePrice(productPrices, formData);

      equal(calculatedPrices.price, 158.0);
      equal(calculatedPrices.totalTax, 10.34);
      equal(calculatedPrices.taxAtDefaultRate, 10.34);
      equal(calculatedPrices.taxAtCustomRate, 0);
      equal(calculatedPrices.shapeAndStylePrice, 150.0);
      equal(calculatedPrices.shapeAndStyleTaxAtDefaultRate, 9.81);
      equal(calculatedPrices.shapeAndStyleTaxAtCustomRate, 0);
      equal(calculatedPrices.designAndFlavorPrice, 8.0);
      equal(calculatedPrices.designAndFlavorTaxAtDefaultRate, 0.52);
      equal(calculatedPrices.designAndFlavorTaxAtCustomRate, 0);
   });

   it('SHP01_NO1_DRN09 = 18.0 + 5.0 = 23.0', () => {
      formData.shape = "SHP01";
      formData.size = "NO1";
      formData.decoration = "DRN09";

      var calculatedPrices = PriceCalculator.calculatePrice(productPrices, formData);

      equal(calculatedPrices.price, 23.0);
      equal(calculatedPrices.totalTax, 1.50);
      equal(calculatedPrices.taxAtDefaultRate, 1.50);
      equal(calculatedPrices.taxAtCustomRate, 0);
      equal(calculatedPrices.shapeAndStylePrice, 18.0);
      equal(calculatedPrices.shapeAndStyleTaxAtDefaultRate, 1.18);
      equal(calculatedPrices.shapeAndStyleTaxAtCustomRate, 0);
      equal(calculatedPrices.designAndFlavorPrice, 5.0);
      equal(calculatedPrices.designAndFlavorTaxAtDefaultRate, 0.33);
      equal(calculatedPrices.designAndFlavorTaxAtCustomRate, 0);
   });

   it('SHP01_NO2_DRN09 = 24.0 + 5.0 = 29.0', () => {
      formData.shape = "SHP01";
      formData.size = "NO2";
      formData.decoration = "DRN09";

      var calculatedPrices = PriceCalculator.calculatePrice(productPrices, formData);

      equal(calculatedPrices.price, 29.0);
      equal(calculatedPrices.totalTax, 1.90);
      equal(calculatedPrices.taxAtDefaultRate, 1.90);
      equal(calculatedPrices.taxAtCustomRate, 0);
      equal(calculatedPrices.shapeAndStylePrice, 24.0);
      equal(calculatedPrices.shapeAndStyleTaxAtDefaultRate, 1.57);
      equal(calculatedPrices.shapeAndStyleTaxAtCustomRate, 0);
      equal(calculatedPrices.designAndFlavorPrice, 5.0);
      equal(calculatedPrices.designAndFlavorTaxAtDefaultRate, 0.33);
      equal(calculatedPrices.designAndFlavorTaxAtCustomRate, 0);
   });

   it('SHP01_NO3_DRN09 = 30.0 + 5.0 = 35.0', () => {
      formData.shape = "SHP01";
      formData.size = "NO3";
      formData.decoration = "DRN09";

      var calculatedPrices = PriceCalculator.calculatePrice(productPrices, formData);

      equal(calculatedPrices.price, 35.0);
      equal(calculatedPrices.totalTax, 2.29);
      equal(calculatedPrices.taxAtDefaultRate, 2.29);
      equal(calculatedPrices.taxAtCustomRate, 0);
      equal(calculatedPrices.shapeAndStylePrice, 30.0);
      equal(calculatedPrices.shapeAndStyleTaxAtDefaultRate, 1.96);
      equal(calculatedPrices.shapeAndStyleTaxAtCustomRate, 0);
      equal(calculatedPrices.designAndFlavorPrice, 5.0);
      equal(calculatedPrices.designAndFlavorTaxAtDefaultRate, 0.33);
      equal(calculatedPrices.designAndFlavorTaxAtCustomRate, 0);
   });

   it('SHP02_NO1_DRN09 = 24.0 + 5.0 = 29.0', () => {
      formData.shape = "SHP02";
      formData.size = "NO1";
      formData.decoration = "DRN09";

      var calculatedPrices = PriceCalculator.calculatePrice(productPrices, formData);

      equal(calculatedPrices.price, 29.0);
      equal(calculatedPrices.totalTax, 1.90);
      equal(calculatedPrices.taxAtDefaultRate, 1.90);
      equal(calculatedPrices.taxAtCustomRate, 0);
      equal(calculatedPrices.shapeAndStylePrice, 24.0);
      equal(calculatedPrices.shapeAndStyleTaxAtDefaultRate, 1.57);
      equal(calculatedPrices.shapeAndStyleTaxAtCustomRate, 0);
      equal(calculatedPrices.designAndFlavorPrice, 5.0);
      equal(calculatedPrices.designAndFlavorTaxAtDefaultRate, 0.33);
      equal(calculatedPrices.designAndFlavorTaxAtCustomRate, 0);
   });

   it('SHP02_NO2_DRN09 = 30.0 + 5.0 = 35.0', () => {
      formData.shape = "SHP02";
      formData.size = "NO2";
      formData.decoration = "DRN09";

      var calculatedPrices = PriceCalculator.calculatePrice(productPrices, formData);

      equal(calculatedPrices.price, 35.0);
      equal(calculatedPrices.totalTax, 2.29);
      equal(calculatedPrices.taxAtDefaultRate, 2.29);
      equal(calculatedPrices.taxAtCustomRate, 0);
      equal(calculatedPrices.shapeAndStylePrice, 30.0);
      equal(calculatedPrices.shapeAndStyleTaxAtDefaultRate, 1.96);
      equal(calculatedPrices.shapeAndStyleTaxAtCustomRate, 0);
      equal(calculatedPrices.designAndFlavorPrice, 5.0);
      equal(calculatedPrices.designAndFlavorTaxAtDefaultRate, 0.33);
      equal(calculatedPrices.designAndFlavorTaxAtCustomRate, 0);
   });

   it('SHP02_NO3_DRN09 = 36.0 + 5.0 = 41.0', () => {
      formData.shape = "SHP02";
      formData.size = "NO3";
      formData.decoration = "DRN09";

      var calculatedPrices = PriceCalculator.calculatePrice(productPrices, formData);

      equal(calculatedPrices.price, 41.0);
      equal(calculatedPrices.totalTax, 2.68);
      equal(calculatedPrices.taxAtDefaultRate, 2.68);
      equal(calculatedPrices.taxAtCustomRate, 0);
      equal(calculatedPrices.shapeAndStylePrice, 36.0);
      equal(calculatedPrices.shapeAndStyleTaxAtDefaultRate, 2.36);
      equal(calculatedPrices.shapeAndStyleTaxAtCustomRate, 0);
      equal(calculatedPrices.designAndFlavorPrice, 5.0);
      equal(calculatedPrices.designAndFlavorTaxAtDefaultRate, 0.33);
      equal(calculatedPrices.designAndFlavorTaxAtCustomRate, 0);
   });

   it('SHP03_EB0_DRN09 = 45.0 + 5.0 = 50.0', () => {
      formData.shape = "SHP03";
      formData.size = "EB0";
      formData.decoration = "DRN09";

      var calculatedPrices = PriceCalculator.calculatePrice(productPrices, formData);

      equal(calculatedPrices.price, 50.0);
      equal(calculatedPrices.totalTax, 3.27);
      equal(calculatedPrices.taxAtDefaultRate, 3.27);
      equal(calculatedPrices.taxAtCustomRate, 0);
      equal(calculatedPrices.shapeAndStylePrice, 45.0);
      equal(calculatedPrices.shapeAndStyleTaxAtDefaultRate, 2.94);
      equal(calculatedPrices.shapeAndStyleTaxAtCustomRate, 0);
      equal(calculatedPrices.designAndFlavorPrice, 5.0);
      equal(calculatedPrices.designAndFlavorTaxAtDefaultRate, 0.33);
      equal(calculatedPrices.designAndFlavorTaxAtCustomRate, 0);
   });

   it('SHP03_EB1_DRN09 = 75.0 + 7.0 = 82.0', () => {
      formData.shape = "SHP03";
      formData.size = "EB1";
      formData.decoration = "DRN09";

      var calculatedPrices = PriceCalculator.calculatePrice(productPrices, formData);

      equal(calculatedPrices.price, 82.0);
      equal(calculatedPrices.totalTax, 5.36);
      equal(calculatedPrices.taxAtDefaultRate, 5.36);
      equal(calculatedPrices.taxAtCustomRate, 0);
      equal(calculatedPrices.shapeAndStylePrice, 75.0);
      equal(calculatedPrices.shapeAndStyleTaxAtDefaultRate, 4.91);
      equal(calculatedPrices.shapeAndStyleTaxAtCustomRate, 0);
      equal(calculatedPrices.designAndFlavorPrice, 7.0);
      equal(calculatedPrices.designAndFlavorTaxAtDefaultRate, 0.46);
      equal(calculatedPrices.designAndFlavorTaxAtCustomRate, 0);
   });

   it('SHP03_EB2_DRN09 = 90.0 + 7.0 = 97.0', () => {
      formData.shape = "SHP03";
      formData.size = "EB2";
      formData.decoration = "DRN09";

      var calculatedPrices = PriceCalculator.calculatePrice(productPrices, formData);

      equal(calculatedPrices.price, 97.0);
      equal(calculatedPrices.totalTax, 6.35);
      equal(calculatedPrices.taxAtDefaultRate, 6.35);
      equal(calculatedPrices.taxAtCustomRate, 0);
      equal(calculatedPrices.shapeAndStylePrice, 90.0);
      equal(calculatedPrices.shapeAndStyleTaxAtDefaultRate, 5.89);
      equal(calculatedPrices.shapeAndStyleTaxAtCustomRate, 0);
      equal(calculatedPrices.designAndFlavorPrice, 7.0);
      equal(calculatedPrices.designAndFlavorTaxAtDefaultRate, 0.46);
      equal(calculatedPrices.designAndFlavorTaxAtCustomRate, 0);
   });

   it('SHP03_EB3_DRN09 = 150.0 + 10.0 = 160.0', () => {
      formData.shape = "SHP03";
      formData.size = "EB3";
      formData.decoration = "DRN09";

      var calculatedPrices = PriceCalculator.calculatePrice(productPrices, formData);

      equal(calculatedPrices.price, 160.0);
      equal(calculatedPrices.totalTax, 10.47);
      equal(calculatedPrices.taxAtDefaultRate, 10.47);
      equal(calculatedPrices.taxAtCustomRate, 0);
      equal(calculatedPrices.shapeAndStylePrice, 150.0);
      equal(calculatedPrices.shapeAndStyleTaxAtDefaultRate, 9.81);
      equal(calculatedPrices.shapeAndStyleTaxAtCustomRate, 0);
      equal(calculatedPrices.designAndFlavorPrice, 10.0);
      equal(calculatedPrices.designAndFlavorTaxAtDefaultRate, 0.65);
      equal(calculatedPrices.designAndFlavorTaxAtCustomRate, 0);
   });
});


describe('Extra Decoration Pricing Rules', () => {
   var productPrices = require('../../models/product-list').getProductList();

   var formData = {
      fname: '',
      lname: '',
      mobile: '',
      email: '',
      pickupDeliverySelection: false,
      deliveryDate: '',
      deliveryTime: '',
      street: '',
      houseNo: '',
      city: '',
      shape: '',
      size: '',
      color: '',
      decoration: '',
      flavor: '',
      additional_flavor: [],
      extra_decoration: [],
      text: '',
      remarks: '',
      photo: '',
      paymentType: 'cod',
   };

   it('SHP01_NO1_DRN01 - EDECO01 = 18.0 + (0.0 + 10.0) = 18.0 + 10.0 = 28.0', () => {
      formData.shape = "SHP01";
      formData.size = "NO1";
      formData.decoration = "DRN01";
      formData.extra_decoration = ["EDECO01"];

      var calculatedPrices = PriceCalculator.calculatePrice(productPrices, formData);

      equal(calculatedPrices.price, 28.0);
      equal(calculatedPrices.totalTax, 1.83);
      equal(calculatedPrices.taxAtDefaultRate, 1.83);
      equal(calculatedPrices.taxAtCustomRate, 0.0);
      equal(calculatedPrices.shapeAndStylePrice, 18.0);
      equal(calculatedPrices.shapeAndStyleTaxAtDefaultRate, 1.18);
      equal(calculatedPrices.shapeAndStyleTaxAtCustomRate, 0.0);
      equal(calculatedPrices.designAndFlavorPrice, 10.0);
      equal(calculatedPrices.designAndFlavorTaxAtDefaultRate, 0.65);
      equal(calculatedPrices.designAndFlavorTaxAtCustomRate, 0.0);
   });

   it('SHP01_NO1_DRN01 - EDECO02 = 18.0 + (0.0 + 2.0) = 18.0 + 2.0 = 20.0', () => {
      formData.shape = "SHP01";
      formData.size = "NO1";
      formData.decoration = "DRN01";
      formData.extra_decoration = ["EDECO02"];

      var calculatedPrices = PriceCalculator.calculatePrice(productPrices, formData);

      equal(calculatedPrices.price, 20.0);
      equal(calculatedPrices.totalTax, 1.50);
      equal(calculatedPrices.taxAtDefaultRate, 1.18);
      equal(calculatedPrices.taxAtCustomRate, 0.32);
      equal(calculatedPrices.shapeAndStylePrice, 18.0);
      equal(calculatedPrices.shapeAndStyleTaxAtDefaultRate, 1.18);
      equal(calculatedPrices.shapeAndStyleTaxAtCustomRate, 0);
      equal(calculatedPrices.designAndFlavorPrice, 2.0);
      equal(calculatedPrices.designAndFlavorTaxAtDefaultRate, 0);
      equal(calculatedPrices.designAndFlavorTaxAtCustomRate, 0.32);
   });

   it('SHP01_NO1_DRN01 - EDECO03 = 18.0 + (0.0 + 3.0) = 18.0 + 3.0 = 21.0', () => {
      formData.shape = "SHP01";
      formData.size = "NO1";
      formData.decoration = "DRN01";
      formData.extra_decoration = ["EDECO03"];

      var calculatedPrices = PriceCalculator.calculatePrice(productPrices, formData);

      equal(calculatedPrices.price, 21.0);
      equal(calculatedPrices.totalTax, 1.37);
      equal(calculatedPrices.taxAtDefaultRate, 1.37);
      equal(calculatedPrices.taxAtCustomRate, 0);
      equal(calculatedPrices.shapeAndStylePrice, 18.0);
      equal(calculatedPrices.shapeAndStyleTaxAtDefaultRate, 1.18);
      equal(calculatedPrices.shapeAndStyleTaxAtCustomRate, 0);
      equal(calculatedPrices.designAndFlavorPrice, 3.0);
      equal(calculatedPrices.designAndFlavorTaxAtDefaultRate, 0.20);
      equal(calculatedPrices.designAndFlavorTaxAtCustomRate, 0);
   });

   it('SHP01_NO1_DRN01 - EDECO01,EDECO02,EDECO03 = 18.0 + (0.0 + (10.0 + 2.0 + 3.0)) = 18.0 + 15.0 = 33.0', () => {
      formData.shape = "SHP01";
      formData.size = "NO1";
      formData.decoration = "DRN01";
      formData.extra_decoration = ["EDECO01", "EDECO02", "EDECO03"];

      var calculatedPrices = PriceCalculator.calculatePrice(productPrices, formData);

      equal(calculatedPrices.price, 33.0);
      equal(calculatedPrices.totalTax, 2.35);
      equal(calculatedPrices.taxAtDefaultRate, 2.03);
      equal(calculatedPrices.taxAtCustomRate, 0.32);
      equal(calculatedPrices.shapeAndStylePrice, 18.0);
      equal(calculatedPrices.shapeAndStyleTaxAtDefaultRate, 1.18);
      equal(calculatedPrices.shapeAndStyleTaxAtCustomRate, 0);
      equal(calculatedPrices.designAndFlavorPrice, 15.0);
      equal(calculatedPrices.designAndFlavorTaxAtDefaultRate, 0.85);
      equal(calculatedPrices.designAndFlavorTaxAtCustomRate, 0.32);
   });

   it('SHP01_NO1_DRN01 - FLV01 - FLV02 - EDECO01 = 18.0 + (0.0 + 0.0 + 5.0 + 10.0) = 18.0 + 15.0 = 33.0', () => {
      formData.shape = "SHP01";
      formData.size = "NO1";
      formData.decoration = "DRN01";
      formData.extra_decoration = ["EDECO01"];
      formData.flavor = "FLV01";
      formData.additional_flavor = ["FLV02"];

      var calculatedPrices = PriceCalculator.calculatePrice(productPrices, formData);

      equal(calculatedPrices.price, 33.0);
      equal(calculatedPrices.totalTax, 2.16);
      equal(calculatedPrices.taxAtDefaultRate, 2.16);
      equal(calculatedPrices.taxAtCustomRate, 0);
      equal(calculatedPrices.shapeAndStylePrice, 18.0);
      equal(calculatedPrices.shapeAndStyleTaxAtDefaultRate, 1.18);
      equal(calculatedPrices.shapeAndStyleTaxAtCustomRate, 0);
      equal(calculatedPrices.designAndFlavorPrice, 15.0);
      equal(calculatedPrices.designAndFlavorTaxAtDefaultRate, 0.98);
      equal(calculatedPrices.designAndFlavorTaxAtCustomRate, 0);
   });

   it('SHP02_NO3_DRN06 - FLV01 - FLV02 - EDECO02 = 36.0 + (2.0 + 0.0 + 5.0 + 2.0) = 36.0 + 9.0 = 45.0', () => {
      formData.shape = "SHP02";
      formData.size = "NO3";
      formData.decoration = "DRN06";
      formData.extra_decoration = ["EDECO02"];
      formData.flavor = "FLV01";
      formData.additional_flavor = ["FLV02"];

      var calculatedPrices = PriceCalculator.calculatePrice(productPrices, formData);

      equal(calculatedPrices.price, 45.0);
      equal(calculatedPrices.totalTax, 3.13);
      equal(calculatedPrices.taxAtDefaultRate, 2.81);
      equal(calculatedPrices.taxAtCustomRate, 0.32);
      equal(calculatedPrices.shapeAndStylePrice, 36.0);
      equal(calculatedPrices.shapeAndStyleTaxAtDefaultRate, 2.36);
      equal(calculatedPrices.shapeAndStyleTaxAtCustomRate, 0);
      equal(calculatedPrices.designAndFlavorPrice, 9.0);
      equal(calculatedPrices.designAndFlavorTaxAtDefaultRate, 0.46);
      equal(calculatedPrices.designAndFlavorTaxAtCustomRate, 0.32);
   });
});


describe('Flavor and Extra Flavor Pricing Rules', () => {
   var productPrices = require('../../models/product-list').getProductList();

   var formData = {
      fname: '',
      lname: '',
      mobile: '',
      email: '',
      pickupDeliverySelection: false,
      deliveryDate: '',
      deliveryTime: '',
      street: '',
      houseNo: '',
      city: '',
      shape: '',
      size: '',
      color: '',
      decoration: '',
      flavor: '',
      additional_flavor: [],
      extra_decoration: [],
      text: '',
      remarks: '',
      photo: '',
      paymentType: 'cod',
   };

   it('SHP02_NO3_DRN06 - FLV01 = 36.0 + (2.0 + 0.0) = 36.0 + 2.0 = 38.0', () => {
      formData.shape = "SHP02";
      formData.size = "NO3";
      formData.decoration = "DRN06";
      formData.flavor = "FLV01";

      var calculatedPrices = PriceCalculator.calculatePrice(productPrices, formData);

      equal(calculatedPrices.price, 38.0);
      equal(calculatedPrices.totalTax, 2.49);
      equal(calculatedPrices.taxAtDefaultRate, 2.49);
      equal(calculatedPrices.taxAtCustomRate, 0);
      equal(calculatedPrices.shapeAndStylePrice, 36.0);
      equal(calculatedPrices.shapeAndStyleTaxAtDefaultRate, 2.36);
      equal(calculatedPrices.shapeAndStyleTaxAtCustomRate, 0);
      equal(calculatedPrices.designAndFlavorPrice, 2.0);
      equal(calculatedPrices.designAndFlavorTaxAtDefaultRate, 0.13);
      equal(calculatedPrices.designAndFlavorTaxAtCustomRate, 0);
   });

   it('SHP02_NO3_DRN06 - FLV02 = 36.0 + (2.0 + 0.0) = 36.0 + 2.0 = 38.0', () => {
      formData.shape = "SHP02";
      formData.size = "NO3";
      formData.decoration = "DRN06";
      formData.flavor = "FLV02";

      var calculatedPrices = PriceCalculator.calculatePrice(productPrices, formData);

      equal(calculatedPrices.price, 38.0);
      equal(calculatedPrices.totalTax, 2.49);
      equal(calculatedPrices.taxAtDefaultRate, 2.49);
      equal(calculatedPrices.taxAtCustomRate, 0);
      equal(calculatedPrices.shapeAndStylePrice, 36.0);
      equal(calculatedPrices.shapeAndStyleTaxAtDefaultRate, 2.36);
      equal(calculatedPrices.shapeAndStyleTaxAtCustomRate, 0);
      equal(calculatedPrices.designAndFlavorPrice, 2.0);
      equal(calculatedPrices.designAndFlavorTaxAtDefaultRate, 0.13);
      equal(calculatedPrices.designAndFlavorTaxAtCustomRate, 0);
   });

   it('SHP02_NO3_DRN06 - FLV03 = 36.0 + (2.0 + 0.0) = 36.0 + 2.0 = 38.0', () => {
      formData.shape = "SHP02";
      formData.size = "NO3";
      formData.decoration = "DRN06";
      formData.flavor = "FLV03";

      var calculatedPrices = PriceCalculator.calculatePrice(productPrices, formData);

      equal(calculatedPrices.price, 38.0);
      equal(calculatedPrices.totalTax, 2.49);
      equal(calculatedPrices.taxAtDefaultRate, 2.49);
      equal(calculatedPrices.taxAtCustomRate, 0);
      equal(calculatedPrices.shapeAndStylePrice, 36.0);
      equal(calculatedPrices.shapeAndStyleTaxAtDefaultRate, 2.36);
      equal(calculatedPrices.shapeAndStyleTaxAtCustomRate, 0);
      equal(calculatedPrices.designAndFlavorPrice, 2.0);
      equal(calculatedPrices.designAndFlavorTaxAtDefaultRate, 0.13);
      equal(calculatedPrices.designAndFlavorTaxAtCustomRate, 0);
   });

   it('SHP02_NO3_DRN06 - FLV04 = 36.0 + (2.0 + 0.0) = 36.0 + 2.0 = 38.0', () => {
      formData.shape = "SHP02";
      formData.size = "NO3";
      formData.decoration = "DRN06";
      formData.flavor = "FLV04";

      var calculatedPrices = PriceCalculator.calculatePrice(productPrices, formData);

      equal(calculatedPrices.price, 38.0);
      equal(calculatedPrices.totalTax, 2.49);
      equal(calculatedPrices.taxAtDefaultRate, 2.49);
      equal(calculatedPrices.taxAtCustomRate, 0);
      equal(calculatedPrices.shapeAndStylePrice, 36.0);
      equal(calculatedPrices.shapeAndStyleTaxAtDefaultRate, 2.36);
      equal(calculatedPrices.shapeAndStyleTaxAtCustomRate, 0);
      equal(calculatedPrices.designAndFlavorPrice, 2.0);
      equal(calculatedPrices.designAndFlavorTaxAtDefaultRate, 0.13);
      equal(calculatedPrices.designAndFlavorTaxAtCustomRate, 0);
   });

   it('SHP02_NO3_DRN06 - FLV05 = 36.0 + (2.0 + 0.0) = 36.0 + 2.0 = 38.0', () => {
      formData.shape = "SHP02";
      formData.size = "NO3";
      formData.decoration = "DRN06";
      formData.flavor = "FLV05";

      var calculatedPrices = PriceCalculator.calculatePrice(productPrices, formData);

      equal(calculatedPrices.price, 38.0);
      equal(calculatedPrices.totalTax, 2.49);
      equal(calculatedPrices.taxAtDefaultRate, 2.49);
      equal(calculatedPrices.taxAtCustomRate, 0);
      equal(calculatedPrices.shapeAndStylePrice, 36.0);
      equal(calculatedPrices.shapeAndStyleTaxAtDefaultRate, 2.36);
      equal(calculatedPrices.shapeAndStyleTaxAtCustomRate, 0);
      equal(calculatedPrices.designAndFlavorPrice, 2.0);
      equal(calculatedPrices.designAndFlavorTaxAtDefaultRate, 0.13);
      equal(calculatedPrices.designAndFlavorTaxAtCustomRate, 0);
   });

   it('SHP02_NO3_DRN06 - FLV06 = 36.0 + (2.0 + 0.0) = 36.0 + 2.0 = 38.0', () => {
      formData.shape = "SHP02";
      formData.size = "NO3";
      formData.decoration = "DRN06";
      formData.flavor = "FLV06";

      var calculatedPrices = PriceCalculator.calculatePrice(productPrices, formData);

      equal(calculatedPrices.price, 38.0);
      equal(calculatedPrices.totalTax, 2.49);
      equal(calculatedPrices.taxAtDefaultRate, 2.49);
      equal(calculatedPrices.taxAtCustomRate, 0);
      equal(calculatedPrices.shapeAndStylePrice, 36.0);
      equal(calculatedPrices.shapeAndStyleTaxAtDefaultRate, 2.36);
      equal(calculatedPrices.shapeAndStyleTaxAtCustomRate, 0);
      equal(calculatedPrices.designAndFlavorPrice, 2.0);
      equal(calculatedPrices.designAndFlavorTaxAtDefaultRate, 0.13);
      equal(calculatedPrices.designAndFlavorTaxAtCustomRate, 0);
   });

   it('SHP02_NO3_DRN06 - FLV07 = 36.0 + (2.0 + 0.0) = 36.0 + 2.0 = 38.0', () => {
      formData.shape = "SHP02";
      formData.size = "NO3";
      formData.decoration = "DRN06";
      formData.flavor = "FLV07";

      var calculatedPrices = PriceCalculator.calculatePrice(productPrices, formData);

      equal(calculatedPrices.price, 38.0);
      equal(calculatedPrices.totalTax, 2.49);
      equal(calculatedPrices.taxAtDefaultRate, 2.49);
      equal(calculatedPrices.taxAtCustomRate, 0);
      equal(calculatedPrices.shapeAndStylePrice, 36.0);
      equal(calculatedPrices.shapeAndStyleTaxAtDefaultRate, 2.36);
      equal(calculatedPrices.shapeAndStyleTaxAtCustomRate, 0);
      equal(calculatedPrices.designAndFlavorPrice, 2.0);
      equal(calculatedPrices.designAndFlavorTaxAtDefaultRate, 0.13);
      equal(calculatedPrices.designAndFlavorTaxAtCustomRate, 0);
   });

   it('SHP02_NO3_DRN06 - FLV08 = 36.0 + (2.0 + 0.0) = 36.0 + 2.0 = 38.0', () => {
      formData.shape = "SHP02";
      formData.size = "NO3";
      formData.decoration = "DRN06";
      formData.flavor = "FLV08";

      var calculatedPrices = PriceCalculator.calculatePrice(productPrices, formData);

      equal(calculatedPrices.price, 38.0);
      equal(calculatedPrices.totalTax, 2.49);
      equal(calculatedPrices.taxAtDefaultRate, 2.49);
      equal(calculatedPrices.taxAtCustomRate, 0);
      equal(calculatedPrices.shapeAndStylePrice, 36.0);
      equal(calculatedPrices.shapeAndStyleTaxAtDefaultRate, 2.36);
      equal(calculatedPrices.shapeAndStyleTaxAtCustomRate, 0);
      equal(calculatedPrices.designAndFlavorPrice, 2.0);
      equal(calculatedPrices.designAndFlavorTaxAtDefaultRate, 0.13);
      equal(calculatedPrices.designAndFlavorTaxAtCustomRate, 0);
   });

   it('SHP02_NO3_DRN06 - FLV09 = 36.0 + (2.0 + 0.0) = 36.0 + 2.0 = 38.0', () => {
      formData.shape = "SHP02";
      formData.size = "NO3";
      formData.decoration = "DRN06";
      formData.flavor = "FLV09";

      var calculatedPrices = PriceCalculator.calculatePrice(productPrices, formData);

      equal(calculatedPrices.price, 38.0);
      equal(calculatedPrices.totalTax, 2.49);
      equal(calculatedPrices.taxAtDefaultRate, 2.49);
      equal(calculatedPrices.taxAtCustomRate, 0);
      equal(calculatedPrices.shapeAndStylePrice, 36.0);
      equal(calculatedPrices.shapeAndStyleTaxAtDefaultRate, 2.36);
      equal(calculatedPrices.shapeAndStyleTaxAtCustomRate, 0);
      equal(calculatedPrices.designAndFlavorPrice, 2.0);
      equal(calculatedPrices.designAndFlavorTaxAtDefaultRate, 0.13);
      equal(calculatedPrices.designAndFlavorTaxAtCustomRate, 0);
   });

   it('SHP02_NO3_DRN06 - FLV10 = 36.0 + (2.0 + 0.0) = 36.0 + 2.0 = 38.0', () => {
      formData.shape = "SHP02";
      formData.size = "NO3";
      formData.decoration = "DRN06";
      formData.flavor = "FLV10";

      var calculatedPrices = PriceCalculator.calculatePrice(productPrices, formData);

      equal(calculatedPrices.price, 38.0);
      equal(calculatedPrices.totalTax, 2.49);
      equal(calculatedPrices.taxAtDefaultRate, 2.49);
      equal(calculatedPrices.taxAtCustomRate, 0);
      equal(calculatedPrices.shapeAndStylePrice, 36.0);
      equal(calculatedPrices.shapeAndStyleTaxAtDefaultRate, 2.36);
      equal(calculatedPrices.shapeAndStyleTaxAtCustomRate, 0);
      equal(calculatedPrices.designAndFlavorPrice, 2.0);
      equal(calculatedPrices.designAndFlavorTaxAtDefaultRate, 0.13);
      equal(calculatedPrices.designAndFlavorTaxAtCustomRate, 0);
   });

   it('SHP02_NO3_DRN06 - FLV11 = 36.0 + (2.0 + 0.0) = 36.0 + 2.0 = 38.0', () => {
      formData.shape = "SHP02";
      formData.size = "NO3";
      formData.decoration = "DRN06";
      formData.flavor = "FLV11";

      var calculatedPrices = PriceCalculator.calculatePrice(productPrices, formData);

      equal(calculatedPrices.price, 38.0);
      equal(calculatedPrices.totalTax, 2.49);
      equal(calculatedPrices.taxAtDefaultRate, 2.49);
      equal(calculatedPrices.taxAtCustomRate, 0);
      equal(calculatedPrices.shapeAndStylePrice, 36.0);
      equal(calculatedPrices.shapeAndStyleTaxAtDefaultRate, 2.36);
      equal(calculatedPrices.shapeAndStyleTaxAtCustomRate, 0);
      equal(calculatedPrices.designAndFlavorPrice, 2.0);
      equal(calculatedPrices.designAndFlavorTaxAtDefaultRate, 0.13);
      equal(calculatedPrices.designAndFlavorTaxAtCustomRate, 0);
   });

   it('SHP02_NO3_DRN06 - FLV12 = 36.0 + (2.0 + 0.0) = 36.0 + 2.0 = 38.0', () => {
      formData.shape = "SHP02";
      formData.size = "NO3";
      formData.decoration = "DRN06";
      formData.flavor = "FLV12";

      var calculatedPrices = PriceCalculator.calculatePrice(productPrices, formData);

      equal(calculatedPrices.price, 38.0);
      equal(calculatedPrices.totalTax, 2.49);
      equal(calculatedPrices.taxAtDefaultRate, 2.49);
      equal(calculatedPrices.taxAtCustomRate, 0);
      equal(calculatedPrices.shapeAndStylePrice, 36.0);
      equal(calculatedPrices.shapeAndStyleTaxAtDefaultRate, 2.36);
      equal(calculatedPrices.shapeAndStyleTaxAtCustomRate, 0);
      equal(calculatedPrices.designAndFlavorPrice, 2.0);
      equal(calculatedPrices.designAndFlavorTaxAtDefaultRate, 0.13);
      equal(calculatedPrices.designAndFlavorTaxAtCustomRate, 0);
   });

   it('SHP01_NO2_DRN07 - FLV02 - FLV01 = 24.0 + (3.0 + 0.0 + 5.0) = 24.0 + 8.0 = 32.0', () => {
      formData.shape = "SHP01";
      formData.size = "NO2";
      formData.decoration = "DRN07";
      formData.flavor = "FLV02";
      formData.additional_flavor = ["FLV01"];

      var calculatedPrices = PriceCalculator.calculatePrice(productPrices, formData);

      equal(calculatedPrices.price, 32.0);
      equal(calculatedPrices.totalTax, 2.09);
      equal(calculatedPrices.taxAtDefaultRate, 2.09);
      equal(calculatedPrices.taxAtCustomRate, 0);
      equal(calculatedPrices.shapeAndStylePrice, 24.0);
      equal(calculatedPrices.shapeAndStyleTaxAtDefaultRate, 1.57);
      equal(calculatedPrices.shapeAndStyleTaxAtCustomRate, 0);
      equal(calculatedPrices.designAndFlavorPrice, 8.0);
      equal(calculatedPrices.designAndFlavorTaxAtDefaultRate, 0.52);
      equal(calculatedPrices.designAndFlavorTaxAtCustomRate, 0);
   });

   it('SHP01_NO2_DRN07 - FLV01 - FLV02 = 24.0 + (3.0 + 0.0 + 5.0) = 24.0 + 8.0 = 32.0', () => {
      formData.shape = "SHP01";
      formData.size = "NO2";
      formData.decoration = "DRN07";
      formData.flavor = "FLV01";
      formData.additional_flavor = ["FLV02"];

      var calculatedPrices = PriceCalculator.calculatePrice(productPrices, formData);

      equal(calculatedPrices.price, 32.0);
      equal(calculatedPrices.totalTax, 2.09);
      equal(calculatedPrices.taxAtDefaultRate, 2.09);
      equal(calculatedPrices.taxAtCustomRate, 0);
      equal(calculatedPrices.shapeAndStylePrice, 24.0);
      equal(calculatedPrices.shapeAndStyleTaxAtDefaultRate, 1.57);
      equal(calculatedPrices.shapeAndStyleTaxAtCustomRate, 0);
      equal(calculatedPrices.designAndFlavorPrice, 8.0);
      equal(calculatedPrices.designAndFlavorTaxAtDefaultRate, 0.52);
      equal(calculatedPrices.designAndFlavorTaxAtCustomRate, 0);
   });

   it('SHP01_NO2_DRN07 - FLV01 - FLV03 = 24.0 + (3.0 + 0.0 + 5.0) = 24.0 + 8.0 = 32.0', () => {
      formData.shape = "SHP01";
      formData.size = "NO2";
      formData.decoration = "DRN07";
      formData.flavor = "FLV01";
      formData.additional_flavor = ["FLV03"];

      var calculatedPrices = PriceCalculator.calculatePrice(productPrices, formData);

      equal(calculatedPrices.price, 32.0);
      equal(calculatedPrices.totalTax, 2.09);
      equal(calculatedPrices.taxAtDefaultRate, 2.09);
      equal(calculatedPrices.taxAtCustomRate, 0);
      equal(calculatedPrices.shapeAndStylePrice, 24.0);
      equal(calculatedPrices.shapeAndStyleTaxAtDefaultRate, 1.57);
      equal(calculatedPrices.shapeAndStyleTaxAtCustomRate, 0);
      equal(calculatedPrices.designAndFlavorPrice, 8.0);
      equal(calculatedPrices.designAndFlavorTaxAtDefaultRate, 0.52);
      equal(calculatedPrices.designAndFlavorTaxAtCustomRate, 0);
   });

   it('SHP01_NO2_DRN07 - FLV01 - FLV04 = 24.0 + (3.0 + 0.0 + 5.0) = 24.0 + 8.0 = 32.0', () => {
      formData.shape = "SHP01";
      formData.size = "NO2";
      formData.decoration = "DRN07";
      formData.flavor = "FLV01";
      formData.additional_flavor = ["FLV04"];

      var calculatedPrices = PriceCalculator.calculatePrice(productPrices, formData);

      equal(calculatedPrices.price, 32.0);
      equal(calculatedPrices.totalTax, 2.09);
      equal(calculatedPrices.taxAtDefaultRate, 2.09);
      equal(calculatedPrices.taxAtCustomRate, 0);
      equal(calculatedPrices.shapeAndStylePrice, 24.0);
      equal(calculatedPrices.shapeAndStyleTaxAtDefaultRate, 1.57);
      equal(calculatedPrices.shapeAndStyleTaxAtCustomRate, 0);
      equal(calculatedPrices.designAndFlavorPrice, 8.0);
      equal(calculatedPrices.designAndFlavorTaxAtDefaultRate, 0.52);
      equal(calculatedPrices.designAndFlavorTaxAtCustomRate, 0);
   });

   it('SHP01_NO2_DRN07 - FLV01 - FLV05 = 24.0 + (3.0 + 0.0 + 5.0) = 24.0 + 8.0 = 32.0', () => {
      formData.shape = "SHP01";
      formData.size = "NO2";
      formData.decoration = "DRN07";
      formData.flavor = "FLV01";
      formData.additional_flavor = ["FLV05"];

      var calculatedPrices = PriceCalculator.calculatePrice(productPrices, formData);

      equal(calculatedPrices.price, 32.0);
      equal(calculatedPrices.totalTax, 2.09);
      equal(calculatedPrices.taxAtDefaultRate, 2.09);
      equal(calculatedPrices.taxAtCustomRate, 0);
      equal(calculatedPrices.shapeAndStylePrice, 24.0);
      equal(calculatedPrices.shapeAndStyleTaxAtDefaultRate, 1.57);
      equal(calculatedPrices.shapeAndStyleTaxAtCustomRate, 0);
      equal(calculatedPrices.designAndFlavorPrice, 8.0);
      equal(calculatedPrices.designAndFlavorTaxAtDefaultRate, 0.52);
      equal(calculatedPrices.designAndFlavorTaxAtCustomRate, 0);
   });

   it('SHP01_NO2_DRN07 - FLV01 - FLV06 = 24.0 + (3.0 + 0.0 + 5.0) = 24.0 + 8.0 = 32.0', () => {
      formData.shape = "SHP01";
      formData.size = "NO2";
      formData.decoration = "DRN07";
      formData.flavor = "FLV01";
      formData.additional_flavor = ["FLV06"];

      var calculatedPrices = PriceCalculator.calculatePrice(productPrices, formData);

      equal(calculatedPrices.price, 32.0);
      equal(calculatedPrices.totalTax, 2.09);
      equal(calculatedPrices.taxAtDefaultRate, 2.09);
      equal(calculatedPrices.taxAtCustomRate, 0);
      equal(calculatedPrices.shapeAndStylePrice, 24.0);
      equal(calculatedPrices.shapeAndStyleTaxAtDefaultRate, 1.57);
      equal(calculatedPrices.shapeAndStyleTaxAtCustomRate, 0);
      equal(calculatedPrices.designAndFlavorPrice, 8.0);
      equal(calculatedPrices.designAndFlavorTaxAtDefaultRate, 0.52);
      equal(calculatedPrices.designAndFlavorTaxAtCustomRate, 0);
   });

   it('SHP01_NO2_DRN07 - FLV01 - FLV07 = 24.0 + (3.0 + 0.0 + 5.0) = 24.0 + 8.0 = 32.0', () => {
      formData.shape = "SHP01";
      formData.size = "NO2";
      formData.decoration = "DRN07";
      formData.flavor = "FLV01";
      formData.additional_flavor = ["FLV07"];

      var calculatedPrices = PriceCalculator.calculatePrice(productPrices, formData);

      equal(calculatedPrices.price, 32.0);
      equal(calculatedPrices.totalTax, 2.09);
      equal(calculatedPrices.taxAtDefaultRate, 2.09);
      equal(calculatedPrices.taxAtCustomRate, 0);
      equal(calculatedPrices.shapeAndStylePrice, 24.0);
      equal(calculatedPrices.shapeAndStyleTaxAtDefaultRate, 1.57);
      equal(calculatedPrices.shapeAndStyleTaxAtCustomRate, 0);
      equal(calculatedPrices.designAndFlavorPrice, 8.0);
      equal(calculatedPrices.designAndFlavorTaxAtDefaultRate, 0.52);
      equal(calculatedPrices.designAndFlavorTaxAtCustomRate, 0);
   });

   it('SHP01_NO2_DRN07 - FLV01 - FLV08 = 24.0 + (3.0 + 0.0 + 5.0) = 24.0 + 8.0 = 32.0', () => {
      formData.shape = "SHP01";
      formData.size = "NO2";
      formData.decoration = "DRN07";
      formData.flavor = "FLV01";
      formData.additional_flavor = ["FLV08"];

      var calculatedPrices = PriceCalculator.calculatePrice(productPrices, formData);

      equal(calculatedPrices.price, 32.0);
      equal(calculatedPrices.totalTax, 2.09);
      equal(calculatedPrices.taxAtDefaultRate, 2.09);
      equal(calculatedPrices.taxAtCustomRate, 0);
      equal(calculatedPrices.shapeAndStylePrice, 24.0);
      equal(calculatedPrices.shapeAndStyleTaxAtDefaultRate, 1.57);
      equal(calculatedPrices.shapeAndStyleTaxAtCustomRate, 0);
      equal(calculatedPrices.designAndFlavorPrice, 8.0);
      equal(calculatedPrices.designAndFlavorTaxAtDefaultRate, 0.52);
      equal(calculatedPrices.designAndFlavorTaxAtCustomRate, 0);
   });

   it('SHP01_NO2_DRN07 - FLV01 - FLV09 = 24.0 + (3.0 + 0.0 + 5.0) = 24.0 + 8.0 = 32.0', () => {
      formData.shape = "SHP01";
      formData.size = "NO2";
      formData.decoration = "DRN07";
      formData.flavor = "FLV01";
      formData.additional_flavor = ["FLV09"];

      var calculatedPrices = PriceCalculator.calculatePrice(productPrices, formData);

      equal(calculatedPrices.price, 32.0);
      equal(calculatedPrices.totalTax, 2.09);
      equal(calculatedPrices.taxAtDefaultRate, 2.09);
      equal(calculatedPrices.taxAtCustomRate, 0);
      equal(calculatedPrices.shapeAndStylePrice, 24.0);
      equal(calculatedPrices.shapeAndStyleTaxAtDefaultRate, 1.57);
      equal(calculatedPrices.shapeAndStyleTaxAtCustomRate, 0);
      equal(calculatedPrices.designAndFlavorPrice, 8.0);
      equal(calculatedPrices.designAndFlavorTaxAtDefaultRate, 0.52);
      equal(calculatedPrices.designAndFlavorTaxAtCustomRate, 0);
   });

   it('SHP01_NO2_DRN07 - FLV01 - FLV10 = 24.0 + (3.0 + 0.0 + 5.0) = 24.0 + 8.0 = 32.0', () => {
      formData.shape = "SHP01";
      formData.size = "NO2";
      formData.decoration = "DRN07";
      formData.flavor = "FLV01";
      formData.additional_flavor = ["FLV10"];

      var calculatedPrices = PriceCalculator.calculatePrice(productPrices, formData);

      equal(calculatedPrices.price, 32.0);
      equal(calculatedPrices.totalTax, 2.09);
      equal(calculatedPrices.taxAtDefaultRate, 2.09);
      equal(calculatedPrices.taxAtCustomRate, 0);
      equal(calculatedPrices.shapeAndStylePrice, 24.0);
      equal(calculatedPrices.shapeAndStyleTaxAtDefaultRate, 1.57);
      equal(calculatedPrices.shapeAndStyleTaxAtCustomRate, 0);
      equal(calculatedPrices.designAndFlavorPrice, 8.0);
      equal(calculatedPrices.designAndFlavorTaxAtDefaultRate, 0.52);
      equal(calculatedPrices.designAndFlavorTaxAtCustomRate, 0);
   });

   it('SHP01_NO2_DRN07 - FLV01 - FLV11 = 24.0 + (3.0 + 0.0 + 5.0) = 24.0 + 8.0 = 32.0', () => {
      formData.shape = "SHP01";
      formData.size = "NO2";
      formData.decoration = "DRN07";
      formData.flavor = "FLV01";
      formData.additional_flavor = ["FLV11"];

      var calculatedPrices = PriceCalculator.calculatePrice(productPrices, formData);

      equal(calculatedPrices.price, 32.0);
      equal(calculatedPrices.totalTax, 2.09);
      equal(calculatedPrices.taxAtDefaultRate, 2.09);
      equal(calculatedPrices.taxAtCustomRate, 0);
      equal(calculatedPrices.shapeAndStylePrice, 24.0);
      equal(calculatedPrices.shapeAndStyleTaxAtDefaultRate, 1.57);
      equal(calculatedPrices.shapeAndStyleTaxAtCustomRate, 0);
      equal(calculatedPrices.designAndFlavorPrice, 8.0);
      equal(calculatedPrices.designAndFlavorTaxAtDefaultRate, 0.52);
      equal(calculatedPrices.designAndFlavorTaxAtCustomRate, 0);
   });

   it('SHP01_NO2_DRN07 - FLV01 - FLV12 = 24.0 + (3.0 + 0.0 + 5.0) = 24.0 + 8.0 = 32.0', () => {
      formData.shape = "SHP01";
      formData.size = "NO2";
      formData.decoration = "DRN07";
      formData.flavor = "FLV01";
      formData.additional_flavor = ["FLV12"];

      var calculatedPrices = PriceCalculator.calculatePrice(productPrices, formData);

      equal(calculatedPrices.price, 32.0);
      equal(calculatedPrices.totalTax, 2.09);
      equal(calculatedPrices.taxAtDefaultRate, 2.09);
      equal(calculatedPrices.taxAtCustomRate, 0);
      equal(calculatedPrices.shapeAndStylePrice, 24.0);
      equal(calculatedPrices.shapeAndStyleTaxAtDefaultRate, 1.57);
      equal(calculatedPrices.shapeAndStyleTaxAtCustomRate, 0);
      equal(calculatedPrices.designAndFlavorPrice, 8.0);
      equal(calculatedPrices.designAndFlavorTaxAtDefaultRate, 0.52);
      equal(calculatedPrices.designAndFlavorTaxAtCustomRate, 0);
   });

   it('SHP01_NO2_DRN07 - FLV01 - FLV02,FLV04,FLV08 = 24.0 + (3.0 + 0.0 + (5.0 + 5.0 + 5.0)) = 24.0 + (3.0 + 15.0) = 24.0 + 18.0 = 42.0', () => {
      formData.shape = "SHP01";
      formData.size = "NO2";
      formData.decoration = "DRN07";
      formData.flavor = "FLV01";
      formData.additional_flavor = ["FLV02", "FLV04", "FLV08"];

      var calculatedPrices = PriceCalculator.calculatePrice(productPrices, formData);

      equal(calculatedPrices.price, 42.0);
      equal(calculatedPrices.totalTax, 2.75);
      equal(calculatedPrices.taxAtDefaultRate, 2.75);
      equal(calculatedPrices.taxAtCustomRate, 0);
      equal(calculatedPrices.shapeAndStylePrice, 24.0);
      equal(calculatedPrices.shapeAndStyleTaxAtDefaultRate, 1.57);
      equal(calculatedPrices.shapeAndStyleTaxAtCustomRate, 0);
      equal(calculatedPrices.designAndFlavorPrice, 18.0);
      equal(calculatedPrices.designAndFlavorTaxAtDefaultRate, 1.18);
      equal(calculatedPrices.designAndFlavorTaxAtCustomRate, 0);
   });


   it('SHP01_NO2_DRN07 - EDECO02 - FLV01 - FLV02,FLV04,FLV08 = 24.0 + (3.0 + 2.0 + 0.0 + (5.0 + 5.0 + 5.0)) = 24.0 + (5.0 + 15.0) = 24.0 + 20.0 = 44.0', () => {
      formData.shape = "SHP01";
      formData.size = "NO2";
      formData.decoration = "DRN07";
      formData.flavor = "FLV01";
      formData.additional_flavor = ["FLV02", "FLV04", "FLV08"];
      formData.extra_decoration = ["EDECO02"];

      var calculatedPrices = PriceCalculator.calculatePrice(productPrices, formData);

      equal(calculatedPrices.price, 44.0);
      equal(calculatedPrices.totalTax, 3.07);
      equal(calculatedPrices.taxAtDefaultRate, 2.75);
      equal(calculatedPrices.taxAtCustomRate, 0.32);
      equal(calculatedPrices.shapeAndStylePrice, 24.0);
      equal(calculatedPrices.shapeAndStyleTaxAtDefaultRate, 1.57);
      equal(calculatedPrices.shapeAndStyleTaxAtCustomRate, 0);
      equal(calculatedPrices.designAndFlavorPrice, 20.0);
      equal(calculatedPrices.designAndFlavorTaxAtDefaultRate, 1.18);
      equal(calculatedPrices.designAndFlavorTaxAtCustomRate, 0.32);
   });
});

