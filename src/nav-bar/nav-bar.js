import React, { Component } from 'react';
import { ReactScoped, ViewEncapsulation } from 'react-scoped';
import logo from '../assets/header-logo.png';
import $ from 'jquery';
import M from 'materialize-css/dist/js/materialize.min.js';

import Login from '../forms/login';
import styles from './nav-bar.css';
var lastScrollTop = 0;
var delta = 50;

class NavigationBar extends Component {
    render() {
        return (
            <ReactScoped encapsulation={ViewEncapsulation.Emulated} styles={[styles]}>
                <div>
                    <div className="navbar-fixed">
                        <nav className={this.props.navActive ? 'active' : null}>
                            <div className="nav-wrapper">
                                <a href="/" className="brand-logo"><img className={this.props.navActive ? 'active' : null} src={logo} alt="brand_logo" /></a>
                            </div>
                            <div className="header_pink_border"></div>
                            <div className="header_gold_border"></div>
                        </nav>
                    </div>
                </div>
            </ReactScoped>
        )
    }
}

export default NavigationBar;
