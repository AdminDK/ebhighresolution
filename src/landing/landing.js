import React, { Component } from 'react';
import $ from 'jquery';
import { ReactScoped, ViewEncapsulation } from 'react-scoped';
import M from 'materialize-css/dist/js/materialize.min.js';
import { withRouter } from "react-router-dom";

import styles from './landing.css';
// import offerSVG from '../assets/EfendiBey_ButtonV4.svg';
// import offerSVG2 from '../assets/EfendiBey_ButtonV6.svg';
// import qr_Code from '../assets/QRcode.jpeg';

var landing1 = 'lp1c';
var landing2 = 'lp2c';
var landing4 = 'lp4c';
var landing5 = 'lp5c';
var landing6 = 'lp6c';
var landing10 = 'pp4c';

function importAll(r) {
    let images = {};
    r.keys().map((item, index) => { images[item.replace('./', '')] = r(item); });
    return images;
}

const images = importAll(require.context('../assets/images', false, /\.(png|jpe?g|webp)$/));

function isHighDensity() {
    return ((window.matchMedia && (window.matchMedia('only screen and (min-resolution: 124dpi), only screen and (min-resolution: 1.3dppx), only screen and (min-resolution: 48.8dpcm)').matches || window.matchMedia('only screen and (-webkit-min-device-pixel-ratio: 1.3), only screen and (-o-min-device-pixel-ratio: 2.6/2), only screen and (min--moz-device-pixel-ratio: 1.3), only screen and (min-device-pixel-ratio: 1.3)').matches)) || (window.devicePixelRatio && window.devicePixelRatio > 1.3));
}

var res, den;
function detectRes() {
    if (window.screen.width >= 900) {
        res = '-lg';
    }
    else if (window.screen.width >= 600) {
        res = '-md';
    }
    else {
        res = '-sm';
    }
    if (isHighDensity()) {
        den = '_2x';
    }
    else {
        den = '_1x';
    }
    return res + den;
}

var imgRes = detectRes();

function isChrome() {
    var isChromium = window.chrome,
        winNav = window.navigator,
        isIEedge = winNav.userAgent.indexOf("Edge") > -1,
        isIOSChrome = winNav.userAgent.match("CriOS");

    if (isIOSChrome) {
        return true;
    }
    else if (isChromium !== null && typeof isChromium !== "undefined" && isIEedge === false) {
        return true;
    }
    else {
        return false;
    }
}

if (isChrome()) {
    imgRes += '.webp';
}
else {
    imgRes += '.jpg';
}

let floatElems, floatInstances;
class Landing extends Component {

    constructor(props) {
        super(props);
        this.handleClick = this.handleClick.bind(this);
    }

    componentDidMount() {
        this.props.setNav(false);
        //DK materialize 0.100.2 to 1.0.0
        //const M = window.M;
        const elems = document.querySelectorAll(".scrollspy");
        M.ScrollSpy.init(elems, { scrollOffset: 90 });
        // $('.scrollspy').scrollSpy({ scrollOffset: 90 });

        // var elemModal = document.querySelectorAll('.modal');
        // M.Modal.init(elemModal, {});

        var elemSlider = document.querySelectorAll('.slider');
        M.Slider.init(elemSlider, {})

        floatElems = document.querySelectorAll('.fixed-action-btn');
        floatInstances = M.FloatingActionButton.init(floatElems, {
            direction: 'top',
            hoverEnabled: false
        });


        if ('true' === localStorage.getItem('AgbAlreadyShown')) {
            $('.cookie-popup').addClass('dismissed');
        }

        // $(document).ready(function () {
        //     $('.slider').slider();
        // });
    }

    // showAGB() {
    //     // $('#unsere').modal('open');
    //     var unsereModalElem = document.querySelector('#unsere');
    //     let instanceunsere = M.Modal.getInstance(unsereModalElem);
    //     instanceunsere.open();
    // }

    // dismissCookiePopup() {
    //     $(".cookie-popup").animate({ bottom: '-500px' }, 3000, function () {
    //         $('.cookie-popup').addClass('dismissed');
    //         localStorage.setItem('AgbAlreadyShown', 'true');
    //     });
    // }

    handleClick() {
        this.props.history.push("/custom-cake");
    }

    render() {
        return (
            <ReactScoped encapsulation={ViewEncapsulation.Emulated} styles={[styles]}>
                <section id="landing" className="section scrollspy">
                    <div className="content">
                        <button onClick={this.handleClick}>
                            <i className="material-icons cakeDeliveryIcon">cake</i>
                            <b>Ihre Torte gestalten</b>
                        </button>
                    </div>
                    <div className="slider fullscreen">
                        <ul className="slides">
                            <li style={{ backgroundImage: `linear-gradient(rgba(0,0,0,0.5),rgba(0,0,0,0.5)), url(${images[landing10 + imgRes]})`, backgroundRepeat: 'no-repeat', backgroundPosition: 'center', backgroundSize: 'cover' }}>
                            </li>
                            <li style={{ backgroundImage: `linear-gradient(rgba(0,0,0,0.5),rgba(0,0,0,0.5)), url(${images[landing2 + imgRes]})`, backgroundRepeat: 'no-repeat', backgroundPosition: '41% center', backgroundSize: 'cover' }}>
                            </li>
                            <li style={{ backgroundImage: `linear-gradient(rgba(0,0,0,0.5),rgba(0,0,0,0.5)), url(${images[landing4 + imgRes]})`, backgroundRepeat: 'no-repeat', backgroundPosition: 'center', backgroundSize: 'cover' }}>
                            </li>
                            <li style={{ backgroundImage: `linear-gradient(rgba(0,0,0,0.5),rgba(0,0,0,0.5)), url(${images[landing5 + imgRes]})`, backgroundRepeat: 'no-repeat', backgroundPosition: 'center', backgroundSize: 'cover' }}>
                            </li>
                            <li style={{ backgroundImage: `linear-gradient(rgba(0,0,0,0.5),rgba(0,0,0,0.5)), url(${images[landing1 + imgRes]})`, backgroundRepeat: 'no-repeat', backgroundPosition: 'center', backgroundSize: 'cover' }}>
                            </li>
                        </ul>
                    </div>
                </section>
                {/* <div className="banner" style={{ backgroundImage: `linear-gradient(rgba(0,0,0,0.5),rgba(0,0,0,0.5)), url(${images[landing2 + imgRes]})` }}>
                    <div className="content">
                        <button onClick={this.handleClick}>
                            <i className="material-icons cakeDeliveryIcon">cake</i>
                            <b>Ihre Torte gestalten</b>
                        </button>
                    </div>
                </div> */}
            </ReactScoped>
        )
    }
}

export default withRouter(Landing);
