import React, { Component } from 'react';
import { ReactScoped, ViewEncapsulation } from 'react-scoped';
import { Switch, Route } from 'react-router-dom';

// import logo from './logo.svg';
import styles from './App.css';
import NavigationBar from './nav-bar/nav-bar';
import Landing from './landing/landing';
import CustomCake from './custom-cake/custom-cake';
import OnlineOrder from './component/online-order/onlineOrder';
import Footer from'./footer/footer'

class App extends Component {
    constructor(props){
        super(props);
        this.state = {
            redirect: false,
            navActive: false,
        }
        // this.setNav = this.setNav.bind(this);
    }

    setNav = (navState) => {
        this.setState({navActive: navState});
    }

    render() {

        var url = window.location.href;
        // console.log(" url ", url);
        var urlSplit = url.split('/');
        // Taking last array value
        let checkOnline = urlSplit.slice(-1).pop();
        console.log('checkOnline:', checkOnline);
        if (checkOnline !== 'online') {
            let online = checkOnline.split("#");
            checkOnline = online[0];
        }

        // console.log('url:', window.location.href);
        //console.log('url split:', urlSplit);
        console.log('url checkOnline:', checkOnline);

        const search = window.location.search
        let orderId = new URLSearchParams(search).get("menuOrderId");
        if (checkOnline === 'online' || checkOnline === `online?menuOrderId=${orderId}`) {
            return (
                <ReactScoped encapsulation={ViewEncapsulation.None} styles={[styles]}>
                    <NavigationBar />
                    <OnlineOrder />
                </ReactScoped>
            );
        }
        else {
            return (
                <ReactScoped encapsulation={ViewEncapsulation.None} styles={[styles]}>
                    <NavigationBar navActive={this.state.navActive}/>
                    <Switch>
                        <Route exact path='/'>
                            <Landing setNav={this.setNav}/>
                        </Route>
                        <Route path='/custom-cake'>
                            <CustomCake setNav={this.setNav}/>
                        </Route>
                    </Switch>
                    <Footer/>
                </ReactScoped>
            );

            // }
        }

    }
}

export default App;
