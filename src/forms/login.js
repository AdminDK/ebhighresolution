import React, {Component} from 'react'
import { ReactScoped, ViewEncapsulation } from 'react-scoped';

import styles from './login.css'

class Login extends Component {

    constructor(props){
        super(props)
        this.isActive = false;

        this.closeAnimation = this.closeAnimation.bind(this);
        this.openAnimation = this.openAnimation.bind(this);
    }

    openAnimation() {
        if (!this.isActive) {
            let upper_half = document.querySelector(".form_bg.upper_half")
            let lower_half = document.querySelector(".form_bg.lower_half")
            let anim_duration = 500

            this.upper_half_anim = upper_half.animate([
                { transform: 'translateY(-100%)' },
                { transform: 'translateY(0)' }
            ], {
                duration: anim_duration,
                fill: 'forwards',
                easing: 'cubic-bezier(0.4, 0.0, 0.2, 1)',
                endDelay: 0.5*anim_duration
            })

            this.lower_half_anim = lower_half.animate([
                { transform: 'translateY(100%)' },
                { transform: 'translateY(0)' }
            ], {
                duration: anim_duration,
                fill: 'forwards',
                easing: 'cubic-bezier(0.4, 0.0, 0.2, 1)',
                endDelay: 0.5*anim_duration
            })

            let form = document.querySelector(".login_form")

            this.form_anim = form.animate([
                {
                    transform: 'translate(-50%, -50%) scale(0.8)',
                    opacity: 0
                },
                {
                    transform: 'translate(-50%, -50%) scale(1)',
                    opacity: 1
                }
            ], {
                duration: anim_duration,
                fill: 'forwards',
                easing: 'cubic-bezier(0.4, 0.0, 0.2, 1)',
                delay: 0.7*anim_duration
            })
        }
    }

    closeAnimation() {
        if (this.isActive){
            this.form_anim.reverse()
            this.upper_half_anim.reverse()
            this.lower_half_anim.reverse()
            this.lower_half_anim.onfinish = () => {
                this.props.toggleLoginScreen()
            }
        }

    }

    render(){


        if (this.props.active === "active") {
            this.openAnimation()
            this.isActive = true
        } else {
            this.isActive = false
        }

        return (
            <ReactScoped encapsulation={ViewEncapsulation.Emulated} styles={[styles]}>
                <div className={this.isActive ? 'active' : ''}>
                    <div className="form_section_container">
                        <div className="upper_half form_bg"></div>
                        <div className="lower_half form_bg"></div>

                        <div className="form_container">
                            <div className="login_form">

                                <div className="close_btn" onClick={this.closeAnimation}>
                                    <div className="left_dash dash"></div>
                                    <div className="right_dash dash"></div>
                                </div>

                                <h4 className="center ">Login</h4>
                                <div className="row">
                                    <div className="input-field col s12">
                                        <input id="email" type="email" />
                                        <label htmlFor="email">Email</label>
                                    </div>
                                    <div className="input-field col s12">
                                        <input id="password" type="password" />
                                        <label htmlFor="password">Password</label>
                                    </div>
                                    <div className="center col s12">
                                        <button className="btn login_btn waves-effect waves-light">Login</button>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </ReactScoped>
        )
    }

}

export default Login;
