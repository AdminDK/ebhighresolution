
const MongoClient = require('mongodb').MongoClient;
const readXlsxFile = require('read-excel-file/node');
var Katalog = require('../models/katalog');

var db = require('../api_server/db_controller');

var dbRef = null;
var dbConn = null;

const dbConnectionString = 'mongodb://localhost:27017/efendibey?authMechanism=DEFAULT&authSource=efendibey';

function connect(done) {
   if (dbRef) return done();

   MongoClient.connect(dbConnectionString, function (err, database) {
      if (err) return done(err);
      dbRef = database.db('efendibey');
      dbConn = database;
      done();
   });
}

connect((err) => {
   if (err) {
      console.log('Unable to connect...');
      console.log(err);
      return;
   }
   console.log('Connected...');
   dbRef.createCollection('Katalog')
      .then((collection) => {
         console.log('Collection Katalog Created.');
      })
      .catch(err => {
         console.log('Error while creating Katalog.');
         done(false);
      });
})

readXlsxFile('./scripts/KatalogPrice.xlsx').then((rows) => {
  var data = rows.slice(1)
  db.connect(function (err) {
      if (err) {
        console.log(err);
      } else {
        var collection = db.get().collection('Katalog');
        data.forEach(item => {
          let price = item[2]
          if (typeof price === 'string') {
            price = Number(price.replace(',', '.'))
          }
          let insertData = {
            katalogId: item[0],
            price: price,
            showGrid: item[3] ? true : false
          }
          if (typeof item[1] === 'number') {
            insertData['minSize'] = item[1]
          }
          console.log(insertData)
          collection.insertOne(insertData, function (err) {
            if (err) {
               console.log('Error while inserting - ' + item[0]);
            } else {
               console.log('Value Inserted - ' + item[0]);
            }
          });
        })
      }
    });
  })
