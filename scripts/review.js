/* jshint esversion: 6 */

var MongoClient = require('mongodb').MongoClient;
var fetch = require('node-fetch');

// PROD
const url = "mongodb://zSXXTGeJnRXOcVhoyiRNDxwdO:qRHyLVdtldfoLn1o4QDICHwwn@199.247.23.157:60076/efendibey?authMechanism=DEFAULT&authSource=efendibey";

// QA
//const url = 'mongodb://yTxsssDcPk4DLfd3S4xg:KmYJMVXybJ8Nbay5zT4Y@45.76.162.227:27017/efendibey?authMechanism=DEFAULT&authSource=efendibey';

//const url = "mongodb://web:12345678@efendibey.digiklug.com:27017/efendibey?authMechanism=DEFAULT&authSource=efendibey";

const maxReviews = 5;

function fetchReviews(apiUrl, reviewCount, reviewCountCallback) {
   console.log(`Before: ${reviewCount}`);

   fetch(apiUrl)
      .then(res => res.json())
      .then(function (body) {
         var newArr = [];
         var i = 0;

         MongoClient.connect(url, function (err, database) {
            if (err) {
               console.log('Error occurred while connecting:');
               console.log(err);

               return;
            }

            var dbRef = database.db('efendibey');
            var collection = dbRef.collection('google_reviews');
            var insertRecords = () => {
               body.result.reviews.forEach(function (entry) {
                  if (entry.rating < 4 || reviewCount === maxReviews) {
                     return;
                  } else {
                     reviewCount = reviewCount + 1;
                  }

                  var review = {
                     "name": entry.author_name,
                     "pic": entry.profile_photo_url,
                     "review": entry.text,
                     "rating": entry.rating,
                     "relative_time_description": entry.relative_time_description
                  };

                  collection.insertOne(review, function (err) {
                     if (err) {
                        console.log('Error finding collection');
                     } else {
                        console.log("Successfully inserted");
                     }
                  });
               });

               if (reviewCountCallback) {
                  reviewCountCallback(reviewCount);
               }

               console.log(`After: ${reviewCount}`);

               database.close();
            };

            if (reviewCount === 0) {
               collection.drop().then(insertRecords);
            } else {
               insertRecords();
            }
         });
      });
}

let germanReviewsApiUrl = 'https://maps.googleapis.com/maps/api/place/details/json?placeid=ChIJ49PcaLt0sEcRgimH0tEUscg&key=AIzaSyBE1nZRgNzIQGxOUkP7IDhSOQ4JDDtONEc&language=de&fields=review';
let englishReviewsApiUrl = 'https://maps.googleapis.com/maps/api/place/details/json?placeid=ChIJ49PcaLt0sEcRgimH0tEUscg&key=AIzaSyBE1nZRgNzIQGxOUkP7IDhSOQ4JDDtONEc&language=en&fields=review';

fetchReviews(germanReviewsApiUrl, 0, (reviewCount) => {
   if (reviewCount < maxReviews) {
      fetchReviews(englishReviewsApiUrl, reviewCount);
   }
});
