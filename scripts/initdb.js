/* jshint esversion: 6 */

const MongoClient = require('mongodb').MongoClient;

const ProductList = require('../models/product-list');


//local connection string
// const dbConnectionString = 'mongodb://localhost:27017/efendibey?authMechanism=DEFAULT&authSource=efendibey';
const dbConnectionString = 'mongodb://localhost:27017/efendibey?readPreference=primary&directConnection=true&ssl=false';

var dbRef = null;
var dbConn = null;

var argv = process.argv.slice(2);
var resetCounters = false;
var resetGoogleReviews = false;
var resetOrders = false;
var resetReservations = false;

argv.forEach(arg => {
   resetCounters = resetCounters || arg === '--resetCounters' || arg === '--all';
   resetGoogleReviews = resetGoogleReviews || arg === '--resetGoogleReviews' || arg === '--all';
   resetOrders = resetOrders || arg === '--resetOrders' || arg === '--all';
   resetReservations = resetReservations || arg === '--resetReservations' || arg === '--all';
});

function connect(done) {
   if (dbRef) return done();

   MongoClient.connect(dbConnectionString, function (err, database) {
      if (err) return done(err);
      dbRef = database.db('efendibey');
      dbConn = database;
      done();
   });
}

function close() {
   if (dbConn) {
      dbConn.close(function (err, result) {
         dbConn = null;
         dbRef = null;

         if (err) {
            console.log('Unable to disconnect...');
            console.log(err);
         } else {
            console.log('Disconnected...');
         }
      });
   }
}

function insertProductList(done) {
   dbRef.dropCollection('ProductList')
      .catch(err => {
         // do nothing
      });

   dbRef.createCollection('ProductList')
      .then((collection) => {
         collection.insertOne(ProductList.getProductList(), function (err) {
            if (err) {
               console.log('Unable to insert ProductList');
               console.log(err);
               done(false);
               return;
            }
            console.log('Inserted ProductList');

            done(true);
         });
      })
      .catch(err => {
         console.log('Error creating ProductList collection');
         done(false);
      });
}

function insertCounters(done) {
   if (!resetCounters) {
      done(true);
      return;
   }

   dbRef.dropCollection('Counters')
      .catch(err => {
         // do nothing
      });

   dbRef.createCollection('Counters')
      .then((collection) => {
         collection.insertMany(counters, function (err) {
            if (err) {
               console.log('Unable to insert Counters');
               console.log(err);
               done(false);
               return;
            }
            console.log('Inserted Counters');

            done(true);
         });
      })
      .catch(err => {
         console.log('Error creating Counters collection');
         done(false);
      });
}

function insertGoogleReviews(done) {
   if (!resetGoogleReviews) {
      done(true);
      return;
   }

   dbRef.dropCollection('google_reviews')
      .catch(err => {
         // do nothing
      });

   dbRef.createCollection('google_reviews')
      .then((collection) => {
         collection.insertMany(google_reviews, function (err) {
            if (err) {
               console.log('Unable to insert google_reviews');
               console.log(err);
               done(false);
               return;
            }
            console.log('Inserted google_reviews');

            done(true);
         });
      })
      .catch(err => {
         console.log('Error creating google_reviews collection');
         done(false);
      });
}

function deleteCakeOrder(done) {
   if (!resetOrders) {
      done(true);
      return;
   }

   dbRef.dropCollection('CakeOrder')
      .then(() => {
         console.log('Deleted CakeOrder');
         done(true);
      })
      .catch(err => {
         // Don't care if the call failed
         done(true);
      });

}

function deleteReservations(done) {
   if (!resetReservations) {
      done(true);
      return;
   }

   dbRef.dropCollection('EventReservation')
      .then((p) => {
         console.log('Deleted EventReservation');

         dbRef.dropCollection('TableReservation')
            .then((p) => {
               console.log('Deleted TableReservation');
               done(true);
            })
            .catch(err => {
               // Don't care if the call failed
               done(true);
            });
      })
      .catch(err => {
         // Don't care if the call failed
         dbRef.dropCollection('TableReservation')
            .then((p) => {
               console.log('Deleted TableReservation');
               done(true);
            })
            .catch(err => {
               // Don't care if the call failed
               done(true);
            });
      });
}

var counters = [{
      _id: "CakeOrder",
      sequence_value: 0
   },
   {
      _id: "EventReservation",
      sequence_value: 0
   },
   {
      _id: "TableReservation",
      sequence_value: 0
   }
];

var google_reviews = [{
      name: "Nizam Uddin",
      pic: "https://lh5.googleusercontent.com/-gdi7MWBQ1A4/AAAAAAAAAAI/AAAAAAAASco/_mBpj71MhCY/s128-c0x00000000-cc-rp-mo-ba4/photo.jpg",
      review: "Nice place to have small snacks and deserts with coffee or tea. The food is ready good and the price point is very reasonable. They also have indoor sitting are where smoking is allowed.",
      rating: 5,
      relative_time_description: "3 months ago"
   },
   {
      name: "H.M.C YILMAZ",
      pic: "https://lh5.googleusercontent.com/-rcZnmB551Oc/AAAAAAAAAAI/AAAAAAAAAPA/adeHfztJqD4/s128-c0x00000000-cc-rp-mo/photo.jpg",
      review: "Good food delicious traditional sweets try out the kunfe with maras ice cream",
      rating: 4,
      relative_time_description: "a month ago"
   },
   {
      name: "alex england",
      pic: "https://lh5.googleusercontent.com/-JsGNsnb2-Bg/AAAAAAAAAAI/AAAAAAAAAAA/ACLGyWAKvXjOlwUonUf18NOv13dPv4APWQ/s128-c0x00000000-cc-rp-mo-ba5/photo.jpg",
      review: "Dangerously good confections.",
      rating: 5,
      relative_time_description: "a month ago"
   },
   {
      name: "Daniel Obenchain",
      pic: "https://lh6.googleusercontent.com/-eYe0HKRnOzk/AAAAAAAAAAI/AAAAAAAAAAo/3lcdtKLxnvM/s128-c0x00000000-cc-rp-mo-ba4/photo.jpg",
      review: "Great selection of all sorts of sweets!",
      rating: 5,
      relative_time_description: "2 months ago"
   },
   {
      name: "Vanessa Oliveira",
      pic: "https://lh5.googleusercontent.com/-YosZbPKFnEk/AAAAAAAAAAI/AAAAAAAAASI/08CnkPSVYog/s128-c0x00000000-cc-rp-mo/photo.jpg",
      review: "Awesome service. Awesome cakes. I am every weekend back here.",
      rating: 5,
      relative_time_description: "8 months ago"
   }
];

connect((err) => {
   if (err) {
      console.log('Unable to connect...');
      console.log(err);
      return;
   }
   console.log('Connected...');

   insertProductList(success => {
      if (!success) {
         close();
         return;
      }

      insertCounters(success => {
         if (!success) {
            close();
            return;
         }

         insertGoogleReviews(success => {
            if (!success) {
               close();
               return;
            }

            deleteCakeOrder(success => {
               if (!success) {
                  close();
                  return;
               }

               deleteReservations(success => {
                  close();
               });
            });
         });
      });
   });
});
